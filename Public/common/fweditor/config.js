/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function(config) {
    // Define changes to default configuration here. For example:
    config.language = 'zh-cn';
    // config.uiColor = '#AADC6E';
    //config.filebrowserBrowseUrl = 'ckfinder/ckfinder.html';
//    config.filebrowserImageBrowseUrl = 'ckfinder/ckfinder.html?Type=Images';
//    config.filebrowserFlashBrowseUrl = 'ckfinder/ckfinder.html?Type=Flash';
//    config.filebrowserUploadUrl = 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
//    config.filebrowserImageUploadUrl = 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
//    config.filebrowserFlashUploadUrl = 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';
    config.filebrowserUploadUrl = editor_upload_file_url;
    config.filebrowserImageUploadUrl = editor_upload_img_url;
    config.filebrowserFlashUploadUrl = editor_upload_flash_url;
    config.multiimgUrl = editor_multiimgUrl;
    config.enterMode = CKEDITOR.ENTER_BR;
    config.shiftEnterMode = CKEDITOR.ENTER_P;
    config.allowedContent = true;
    config.toolbar = 'MyToolbar';
    config.font_names = '宋体;黑体;微软雅黑;Arial';
    config.extraPlugins = "textindent,flvPlayer,lineheight,multiimg";
    config.toolbar_MyToolbar =
            [
                ['Source', '-', 'Preview', '-', 'Templates'],
                ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Print', 'SpellChecker', 'Scayt'],
                ['Undo', 'Redo', '-', 'Find', 'Replace', '-', 'SelectAll', 'RemoveFormat'],
                ['BidiLtr', 'BidiRtl'],
                ['Maximize', 'ShowBlocks', 'Templates'],
                '/',
                ['Image', 'multiimg', 'Flash', 'flvPlayer', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak'],
                ['Link', 'Unlink', 'Anchor'],
                ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', 'textindent', 'Blockquote', 'CreateDiv'],
                ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
                '/',
                ['Styles', 'Format', 'Font', 'FontSize', 'lineheight'],
                ['Bold', 'Italic', 'Underline', 'Strike', '-', 'Subscript', 'Superscript'],
                ['TextColor', 'BGColor']
            ];



    config.toolbar_Full =
            [
                ['Source', '-', 'Save', 'NewPage', 'Preview', '-', 'Templates'],
                ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Print', 'SpellChecker', 'Scayt'],
                ['Undo', 'Redo', '-', 'Find', 'Replace', '-', 'SelectAll', 'RemoveFormat'],
                ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField'],
                ['BidiLtr', 'BidiRtl'],
                '/',
                ['Bold', 'Italic', 'Underline', 'Strike', '-', 'Subscript', 'Superscript'],
                ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', 'Blockquote', 'CreateDiv'],
                ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
                ['Link', 'Unlink', 'Anchor'],
                ['Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak'],
                '/',
                ['Styles', 'Format', 'Font', 'FontSize'],
                ['TextColor', 'BGColor'],
                ['Maximize', 'ShowBlocks', '-', 'About']
            ];

    config.toolbar_Basic =
            [
                ['Source', 'Bold', 'Italic', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'Link', 'Unlink', 'Styles', 'Format', 'Font', 'FontSize', 'TextColor', 'BGColor']
            ];
};