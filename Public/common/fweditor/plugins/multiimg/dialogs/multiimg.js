window.moreimgs = new Array();  
(function() {  
    CKEDITOR.dialog.add("multiimg",  
        function(a) {
            return {  
                title: "批量上传图片",  
                minWidth: "660px",  
                minHeight:"400px",  
                contents: [{  
                    id: "tab1",  
                    label: "",  
                    title: "",  
                    expand: true,  
                    width: "420px",  
                    height: "300px",  
                    padding: 0,  
                    elements: [{  
                        type: "html",  
                        style: "width:660px;height:400px",  
                        html: '<iframe id="uploadFrame" src="'+a.config.multiimgUrl+'?v=' +new Date().getSeconds() + '" frameborder="0"></iframe>'  
                    }]  
                }],  
                onOk: function() {  
                    var ins = a;  
                    var num = window.moreimgs.length;
                  //   alert(window.moreimgs.length);
                    for(var i=0;i<num;i++){  
                        var imgHtml = "<p><img src=\"" + window.moreimgs[i] + "\" /></p>";  
                        ins.insertHtml(imgHtml);  
                    }  
                    window.moreimgs = new Array();  
                    //点击确定按钮后的操作  
                    //a.insertHtml("编辑器追加内容");  
                },  
                onShow: function () {  
                    document.getElementById("uploadFrame").setAttribute("src",a.config.multiimgUrl+"?v=" +new Date().getSeconds());  
                }  
            }  
        })  
})();

