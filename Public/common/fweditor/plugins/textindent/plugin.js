/*
Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/
function textindent(editor)
{
	
}
textindent.prototype = {
		exec : function( editor )
		
	{
	    	var selection = editor.getSelection(),
				enterMode = editor.config.enterMode;

			if ( !selection )
				return;

			var bookmarks = selection.createBookmarks(),
				ranges = selection.getRanges( true );

			var cssClassName = this.cssClassName,
				iterator,
				block;

			
			for ( var i = ranges.length - 1 ; i >= 0 ; i-- )
			{
				iterator = ranges[ i ].createIterator();
				
				while ( ( block = iterator.getNextParagraph( enterMode == CKEDITOR.ENTER_P ? 'p' : 'div' ) ) )
				{
					if(block.getStyle('text-indent'))
					{
					 block.removeStyle('text-indent');
					}
					else
					{
                     block.setStyle('text-indent',"2em");
					}
				
				}

			}

			editor.focus();
			editor.forceNextSelectionCheck();
			selection.selectBookmarks( bookmarks );
	}
}

CKEDITOR.plugins.add( 'textindent',
{
	
	init : function( editor )
	{
		 var pluginName = 'textindent';
		 editor.addCommand( pluginName,new textindent(editor));
		editor.ui.addButton( pluginName,
			{
				label : '首行缩进',
				icon: this.path + 'textindent.gif',
				command : 'textindent'
			});		
	}
});
