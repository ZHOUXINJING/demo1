//表单检查
function checkform() {
	var cat_id1 = $('#cat_id1').val();
	 var goods_name = $.trim($("#goods_name").val());
	var goods_sn_0 = $('#goods_sn_0').val();
	if(cat_id1 == '') {
        alert('请选择类别！');
        $("#cat_id1").focus();
        return false;
    } 
   
    if(goods_name == '') {
        alert('请填写商品标题！');
        $("#goods_name").focus();
        return false;
    } 
	   if(goods_sn_0==''){
	   alert('请商品编号');
	   $('#goods_sn_0').focus()
	   return false;
	  }
	
}
//增加类别
function add_cat() {
    var i = $("#category_id_num").val();
    i++;
    var templet = $("#td_cat_templet").html();
    templet = templet.replace(/_iiii/g, "_" + i);
    $("#td_cat").append(templet);
    $("#category_id_num").val(i);
}
//TAB切换
function show_panel(i) {
    $(".panel_handle li").removeClass("sel");
    $("#handle" + i).addClass("sel");
    $(".panel").hide();
    $("#panel" + i).show();
}
//设置规格
function set_spec() {
    art.dialog.open(url_set_spec, {title: "设置规格参数", width: "800px", height: "500px"});
}
//新增规格 2016-08-06
function add_spec() {
    var sel_spec_value = $("#sel_spec_value").val();
    url_add_spec2 = url_add_spec + "?spec_array="+sel_spec_value;
    art.dialog.open(url_add_spec2, {title: "设置规格参数", width: "800px", height: "500px"});
}

//显示规格组合
function show_spec() {
    var sel_spec_value = $("#sel_spec_value").val();
    if (sel_spec_value == '') {
        return;
    }
    var arr_spec_list = $.parseJSON(sel_spec_value);
    
    //获得已选规格值数组
    var arr_title = new Array(); //已选规格名数组
    var arr_sel = new Array(); //已选规格值数组
    var arr_id = new Array(); //ID列表
    var arr_type = new Array(); //类型列表
    $.each(arr_spec_list,function(index,item){
        arr_title[index-1] = item.name;
        arr_sel[index-1] = item.val.split("|");
        arr_id[index-1] = item.id;
        arr_type[index-1]= item.type;
    });
    
    var result = new Array();//结果数组
    //初始化第一个数组
    for (var z = 0; z < arr_sel[0].length; z++) {
        result[result.length] = arr_title[0] + ':' + arr_sel[0][z];
    }

    //循环获得结果集
    for (var i = 1; i < arr_sel.length; i++) {
        result = CombineArray(result, arr_sel[i], arr_title[i]);
    }

    //递归获得结果集
    function CombineArray(result, arr_s, arr_t) {
        var result_sub = new Array();
        for (var i = 0; i < result.length; i++) {
            for (var k = 0; k < arr_s.length; k++) {
                result_sub[result_sub.length] = result[i] + ',' + arr_t + ":" + arr_s[k];
            }
        }
        return result_sub;
    }

    if (result.length > 0) {
        //获取以前的值
        var goods_sn = $("#goods_sn").val();
        var price_market = $("#price_market").val();
        var price = $("#price").val();
        var price_cost = $("#price_cost").val();
        var weight = $("#weight").val();
        var num = $("#num").val();
        var tbody_html = '';
        for (var i = 0; i < result.length; i++) {
            var goods_sn_i = '';
            if (goods_sn != '' && goods_sn != undefined) {
                goods_sn_i = goods_sn + '-' + (i + 1);
            }
            if (price_market == undefined)
                price_market = '';
            if (price == undefined)
                price = '';
            if (price_cost == undefined)
                price_cost = '';
            if (weight == undefined)
                weight = '';
            if (num == undefined)
                num = '';
            tbody_html += '<tr id="tr_spec_' + i + '"><td class="th_spec">' + result[i] + '</td>';
            tbody_html += '<td><input type="hidden" name="spec_' + i + '" id="spec_' + i + '" value="' + result[i] + '" /><input type="hidden" name="spec_id[]" value="' + i + '" /><input class="input1" type="text" name="goods_sn_' + i + '" id="goods_sn_' + i + '" value="' + goods_sn_i + '" style="width:120px" /></td>';
            tbody_html += '<td><input class="input1" type="text" name="price_market_' + i + '" id="price_market_' + i + '" value="' + price_market + '" style="width:60px" /></td>';
            tbody_html += '<td><input class="input1" type="text" name="price_' + i + '" id="price_' + i + '" value="' + price + '" style="width:60px" /></td>';
            tbody_html += '<td  style="display:none"><input class="input1" type="text" name="price_cost_' + i + '" id="price_cost_' + i + '" value="' + price_cost + '" style="width:60px" /></td>';
            tbody_html += '<td><input class="input1" type="text" name="weight_' + i + '" id="weight_' + i + '" value="' + weight + '" style="width:60px" /></td>';
            tbody_html += '<td><input class="input1" type="text" name="num_' + i + '" id="num_' + i + '" value="' + num + '" style="width:60px" /></td>';
			 tbody_html += '<td><input type="button" onclick="check_store(' + i + ')"  value="查询"></td>';
            tbody_html += '<td style="text-align:center;"><img src="'+pub+'/admin/images/no1.gif" style="cursor:pointer" alt="删除" onclick="del_spec_row(' + i + ')" /></td></tr>';
        }
        $(".th_spec").show();
        $(".table_sub_spec tbody").html(tbody_html);
        $("#sel_spec_count").val(result.length);
        if(result.length > 1) {
            spec_paixu();//启动排序
        }
    } else {
        $(".th_spec").hide();
        var tbody_html = '';
        tbody_html += '<tr><td><input type="hidden" name="spec_0" id="spec_0" value="" /><input type="hidden" name="spec_id[]" value="' + i + '" /><input class="input1" type="text" name="goods_sn_0" id="goods_sn_0" value="" style="width:120px" /></td>';
        tbody_html += '<td><input class="input1" type="text" name="price_market_0" id="price_market_0" value="" style="width:60px" /></td>';
        tbody_html += '<td><input class="input1" type="text" name="price_0" id="price_0" value="" style="width:60px" /></td>';
        tbody_html += '<td style="display:none"><input class="input1" type="text" name="price_cost_0" id="price_cost_0" value="" style="width:60px" /></td>';
        tbody_html += '<td><input class="input1" type="text" name="weight_0" id="weight_0" value="" style="width:60px" /></td>';
        tbody_html += '<td><input class="input1" type="text" name="num_0" id="num_0" value="" style="width:60px" /></td>';
		 tbody_html += '<td><input type="button" onclick="check_store(0)" value="查询"></td>';
        tbody_html += '<td></td></tr>';
        $(".table_sub_spec tbody").html(tbody_html);
        $("#sel_spec_value").val('');
        $("#sel_spec_count").val("0");
    }

}

//更新新增规格组合 2016-08-06
function update_spec() {
    var sel_spec_value = $("#sel_spec_value").val();
    if (sel_spec_value == '') {
        return;
    }
    var arr_spec_list = $.parseJSON(sel_spec_value);
    
    //获得已选规格值数组
    var arr_title = new Array(); //已选规格名数组
    var arr_sel = new Array(); //已选规格值数组
    var arr_id = new Array(); //ID列表
    var arr_type = new Array(); //类型列表
    $.each(arr_spec_list,function(index,item){
        arr_title[index-1] = item.name;
        arr_sel[index-1] = item.val.split("|");
        arr_id[index-1] = item.id;
        arr_type[index-1]= item.type;
    });
    
    var result = new Array();//结果数组
    //初始化第一个数组
    for (var z = 0; z < arr_sel[0].length; z++) {
        result[result.length] = arr_title[0] + ':' + arr_sel[0][z];
    }

    //循环获得结果集
    for (var i = 1; i < arr_sel.length; i++) {
        result = CombineArray(result, arr_sel[i], arr_title[i]);
    }

    //递归获得结果集
    function CombineArray(result, arr_s, arr_t) {
        var result_sub = new Array();
        for (var i = 0; i < result.length; i++) {
            for (var k = 0; k < arr_s.length; k++) {
                result_sub[result_sub.length] = result[i] + ',' + arr_t + ":" + arr_s[k];
            }
        }
        return result_sub;
    }

    if (result.length > 0) {
        //获取以前的值
        var goods_sn = $("#goods_sn").val();
        var price_market = $("#price_market").val();
        var price = $("#price").val();
        var price_cost = $("#price_cost").val();
        var weight = $("#weight").val();
        var num = $("#num").val();
        var tbody_html = '';
        var sel_spec_count =  parseInt($("#sel_spec_count").val());
        for (var i = 0; i < result.length; i++) {
            var goods_sn_i = '';
            if (goods_sn != '' && goods_sn != undefined) {
                goods_sn_i = goods_sn + '-' + (i + 1);
            }
            if (price_market == undefined)
                price_market = '';
            if (price == undefined)
                price = '';
            if (price_cost == undefined)
                price_cost = '';
            if (weight == undefined)
                weight = '';
            if (num == undefined)
                num = '';
            var i2 = i + sel_spec_count;
            var is_exist = false;
            for(var k=0;k<sel_spec_count;k++) {
                if(result[i] == $("#spec_"+k).val()) {
                    is_exist = true;
                }
            }
            if(!is_exist) {
            tbody_html += '<tr id="tr_spec_' + i2 + '"><td class="th_spec">' + result[i] + '</td>';
            tbody_html += '<td><input type="hidden" name="spec_' + i2 + '" id="spec_' + i2 + '" value="' + result[i] + '" /><input type="hidden" name="spec_id[]" value="' + i2 + '" /><input class="input1" type="text" name="goods_sn_' + i2 + '" id="goods_sn_' + i2 + '" value="' + goods_sn_i + '" style="width:120px" /></td>';
            tbody_html += '<td><input class="input1" type="text" name="price_market_' + i2 + '" id="price_market_' + i2 + '" value="' + price_market + '" style="width:60px" /></td>';
            tbody_html += '<td><input class="input1" type="text" name="price_' + i2+ '" id="price_' + i2 + '" value="' + price + '" style="width:60px" /></td>';
            tbody_html += '<td  style="display:none"><input class="input1" type="text" name="price_cost_' + i2 + '" id="price_cost_' + i2 + '" value="' + price_cost + '" style="width:60px" /></td>';
            tbody_html += '<td><input class="input1" type="text" name="weight_' + i2 + '" id="weight_' + i2 + '" value="' + weight + '" style="width:60px" /></td>';
            tbody_html += '<td><input class="input1" type="text" name="num_' + i2 + '" id="num_' + i2 + '" value="' + num + '" style="width:60px" /></td>';
			 tbody_html += '<td><input type="button"  onclick="check_store(' + i2 + ')" value="查询"></td>';
            tbody_html += '<td style="text-align:center;"><img src="'+pub+'/admin/images/no1.gif" style="cursor:pointer" alt="删除" onclick="del_spec_row(' + i2 + ')" /></td></tr>';
            }
        }
        $(".th_spec").show();
        $(".table_sub_spec tbody").append(tbody_html);
        $("#sel_spec_count").val(result.length+sel_spec_count);
        if(result.length > 1) {
            spec_paixu();//启动排序
        }
    } else {
        $(".th_spec").hide();
        var tbody_html = '';
        tbody_html += '<tr><td><input type="hidden" name="spec_0" id="spec_0" value="" /><input type="hidden" name="spec_id[]" value="' + i + '" /><input class="input1" type="text" name="goods_sn_0" id="goods_sn_0" value="" style="width:120px" /></td>';
        tbody_html += '<td><input class="input1" type="text" name="price_market_0" id="price_market_0" value="" style="width:60px" /></td>';
        tbody_html += '<td><input class="input1" type="text" name="price_0" id="price_0" value="" style="width:60px" /></td>';
        tbody_html += '<td style="display:none"><input class="input1" type="text" name="price_cost_0" id="price_cost_0" value="" style="width:60px" /></td>';
        tbody_html += '<td><input class="input1" type="text" name="weight_0" id="weight_0" value="" style="width:60px" /></td>';
        tbody_html += '<td><input class="input1" type="text" name="num_0" id="num_0" value="" style="width:60px" /></td>';
		 tbody_html += '<td><input type="button" onclick="check_store(0) value="查询"></td>';
        tbody_html += '<td></td></tr>';
        $(".table_sub_spec tbody").html(tbody_html);
        $("#sel_spec_value").val('');
        $("#sel_spec_count").val("0");
    }

}
function show_spec2() {
    var sel_spec_value = $("#sel_spec_value").val();
    if (sel_spec_value == '') {
        return;
    }
    var arr_spec_list = sel_spec_value.split(",");
    //获得已选规格值数组
    var arr_title = new Array(); //已选规格名数组
    var arr_sel = new Array(); //已选规格值数组
    for (var i = 0; i < arr_spec_list.length; i++) {
        var arr_spec_sub = arr_spec_list[i].split("-");
        arr_title[i] = arr_spec_sub[0];
        arr_sel[i] = arr_spec_sub[1].split("|");
    }

    var result = new Array();//结果数组
    //初始化第一个数组
    for (var z = 0; z < arr_sel[0].length; z++) {
        result[result.length] = arr_title[0] + ':' + arr_sel[0][z];
    }

    //循环获得结果集
    for (var i = 1; i < arr_sel.length; i++) {
        result = CombineArray(result, arr_sel[i], arr_title[i]);
    }

    //递归获得结果集
    function CombineArray(result, arr2, arr_t) {
        var result_sub = new Array();
        for (var i = 0; i < result.length; i++) {
            for (var k = 0; k < arr2.length; k++) {
                result_sub[result_sub.length] = result[i] + ',' + arr_t + ":" + arr2[k];
            }
        }
        return result_sub;
    }

    if (result.length > 0) {
        //获取以前的值
        var goods_sn = $("#goods_sn").val();
        var price_market = $("#price_market").val();
        var price = $("#price").val();
        var price_cost = $("#price_cost").val();
        var weight = $("#weight").val();
        var num = $("#num").val();
        var tbody_html = '';
        for (var i = 0; i < result.length; i++) {
            var goods_sn_i = '';
            if (goods_sn != '' && goods_sn != undefined) {
                goods_sn_i = goods_sn + '-' + (i + 1);
            }
            if (price_market == undefined)
                price_market = '';
            if (price == undefined)
                price = '';
            if (price_cost == undefined)
                price_cost = '';
            if (weight == undefined)
                weight = '';
            if (num == undefined)
                num = '';
            tbody_html += '<tr id="tr_spec_' + i + '"><td class="th_spec">' + result[i] + '</td>';
            tbody_html += '<td><input type="hidden" name="spec_' + i + '" id="spec_' + i + '" value="' + result[i] + '" /><input type="hidden" name="spec_id[]" value="' + i + '" /><input class="input1" type="text" name="goods_sn_' + i + '" id="goods_sn_' + i + '" value="' + goods_sn_i + '" style="width:120px" /></td>';
            tbody_html += '<td><input class="input1" type="text" name="price_market_' + i + '" id="price_market_' + i + '" value="' + price_market + '" style="width:60px" /></td>';
            tbody_html += '<td><input class="input1" type="text" name="price_' + i + '" id="price_' + i + '" value="' + price + '" style="width:60px" /></td>';
            tbody_html += '<td  style="display:none"><input class="input1" type="text" name="price_cost_' + i + '" id="price_cost_' + i + '" value="' + price_cost + '" style="width:60px" /></td>';
            tbody_html += '<td><input class="input1" type="text" name="weight_' + i + '" id="weight_' + i + '" value="' + weight + '" style="width:60px" /></td>';
            tbody_html += '<td><input class="input1" type="text" name="num_' + i + '" id="num_' + i + '" value="' + num + '" style="width:60px" /></td>';
			 tbody_html += '<td><input type="button" class="check_store" onclick="check_store(' + i + ')" value="查询"></td>';
            tbody_html += '<td style="text-align:center;"><img src="'+pub+'/admin/images/no1.gif" style="cursor:pointer" alt="删除" onclick="del_spec_row(' + i + ')" /></td></tr>';
        }
        $(".th_spec").show();
        $(".table_sub_spec tbody").html(tbody_html);
        $("#sel_spec_count").val(result.length);
        if(result.length > 1) {
            spec_paixu();//启动排序
        }
    } else {
        $(".th_spec").hide();
        var tbody_html = '';
        tbody_html += '<tr><td><input type="hidden" name="spec_0" id="spec_0" value="" /><input type="hidden" name="spec_id[]" value="' + i + '" /><input class="input1" type="text" name="goods_sn_0" id="goods_sn_0" value="" style="width:120px" /></td>';
        tbody_html += '<td><input class="input1" type="text" name="price_market_0" id="price_market_0" value="" style="width:60px" /></td>';
        tbody_html += '<td><input class="input1" type="text" name="price_0" id="price_0" value="" style="width:60px" /></td>';
        tbody_html += '<td  style="display:none"><input class="input1" type="text" name="price_cost_0" id="price_cost_0" value="" style="width:60px" /></td>';
        tbody_html += '<td><input class="input1" type="text" name="weight_0" id="weight_0" value="" style="width:60px" /></td>';
		 tbody_html += '<td><input type="button"   onclick="check_store(0)" value="查询"></td>';
        tbody_html += '<td><input class="input1" type="text" name="num_0" id="num_0" value="" style="width:60px" /></td>';
        tbody_html += '<td></td></tr>';
        $(".table_sub_spec tbody").html(tbody_html);
        $("#sel_spec_value").val('');
        $("#sel_spec_count").val("0");
    }

}
//删除规格行
function del_spec_row(i) {
    var len = $(".table_sub_spec tbody tr").length;
    $("#tr_spec_" + i).remove();
    if (len == 1) {
        var tbody_html = '';
        tbody_html += '<tr><td><input type="hidden" name="spec_0" id="spec_0" value="" /><input type="hidden" name="spec_id[]" value="0" /><input class="input1" type="text" name="goods_sn_0" id="goods_sn_0" value="" style="width:120px" /></td>';
        tbody_html += '<td><input class="input1" type="text" name="price_market_0" id="price_market_0" value="" style="width:60px" /></td>';
        tbody_html += '<td><input class="input1" type="text" name="price_0" id="price_0" value="" style="width:60px" /></td>';
        tbody_html += '<td  style="display:none"><input class="input1" type="text" name="price_cost_0" id="price_cost_0" value="" style="width:60px" /></td>';
        tbody_html += '<td><input class="input1" type="text" name="weight_0" id="weight_0" value="" style="width:60px" /></td>';
        tbody_html += '<td><input class="input1" type="text" name="num_0" id="num_0" value="" style="width:60px" /></td>';
		 tbody_html += '<td><input type="button"   onclick="check_store(0)"  value="查询"></td>';
        tbody_html += '<td></td></tr>';
        $(".table_sub_spec tbody").html(tbody_html);
        $(".th_spec").hide();
    }
}
//模型自定义属性绑定事件
function change_model() {
    var model_id = $("#model_id").val();
    //看是否是编辑页面
    var goods_id = $("#goods_id").val();
    if(goods_id == undefined || goods_id == '') {
        goods_id = 0;
    }
    if (model_id == 0) {
        $("#model_par").html('');
    } else {
        $.ajax({
            url: url_get_model_par+"?model_id=" + model_id+"&goods_id="+goods_id,
            type: 'GET',
            success: function(val) {
                $("#model_par").html(val);
            }
        });
    }
}

//模型变化调取参数
$("#model_id").change(function() {
    change_model();
})

//推荐商品
function select_goods() {
    var tuijian = $("#tuijian").val();
    art.dialog.open(url_select_tuijian + "?tuijian=" + tuijian, {title: "选择推荐商品", width: "800px"});
}

//显示推荐的商品
function show_tuijian() {
    var tuijian = $("#tuijian").val();
    tuijian = tuijian.replace(',,', ',');
    $("#tuijian").val(tuijian);
    $.ajax({
        url: url_show_tuijian + "?tuijian=" + tuijian,
        cache: false,
        success: function(html) {
            $("#tuijian_goods_list").html(html);
        }
    })
}

//删除已选择的推荐商品
function remove_sel(pid) {
    var tuijian = $("#tuijian").val();
    tuijian = tuijian.replace(pid + ',', '');
    $("#tuijian").val(tuijian);
    show_tuijian();
}

//启动商品规格排序
function spec_paixu() {
    $(".table_sub_spec").sortable({
        revert: false,
        items: 'tr',
        update: function(event, ui) {
        }
    });
}

/***查询库存***/
function check_store(i){
var goods_sn = $('#goods_sn_'+i).val()
$('#num_'+i).val('50')
}
