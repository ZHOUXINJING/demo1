﻿/**
 * 登陆页面JS
 * Created by www.szfangwei.cn on 2015-9-18
 */

$(function () {
    //打开页面焦点放在用户名输入框里
    $("#username").focus();
    //登录操作
    $("#btn_login").bind('click', function () {
         login();
    })
    //enter键登录
    $('#password').bind('keypress', function (event) {
        if (event.keyCode == "13")
        {
            login();
        }
    });
})

function login() {
    var username = $.trim($("#username").val());
    var password = $.trim($("#password").val());
	 var code = $.trim($("#code").val());
    var remember = 0;
    var remember_name = 1;
    if ($("#remember").val()==1) {
        remember = 1;
    }else{
	remember = 0;
	}
	

    if (username == '') {
        $("#username").focus();
		 layer.tips('用户名不能为空', '#username', { time: 1000, tips: [1,'#dd2727'] });
         return;
    }else if(password==''){
		  $("#password").focus();
		 layer.tips('密码不能为空', '#password', { time: 1000, tips: [1,'#dd2727'] });
         return;
		
		}else if(code==''){
		 layer.tips('验证码不能为空', '#code', { time: 1000, tips: [1,'#dd2727'] });
         return;	
			
			} else {
        //提交数据
		var index = layer.load(2, {time: 10*1000});

        //d.content('<img src="' + fwshop_root + '/Public/system/images/ajax_loading.gif" />正在登录中...，请稍等！');
        //d.show();
        $.ajax({
            url: url_login_submit,
            type: "POST",
            cache: false,
            data: {username: username, password: password,code: code, remember: remember, remember_name: remember_name},
            success: function (val) {
                val = $.trim(val);
                if(val=='Incorrect'){
				  	$("#code").focus();
					$('.msg_error').html('验证码不正确');
					
					layer.close(index);
				 }else if (val == '1') {
                    $('.login-box>.msg_error').html('');
					//layer.msg('登录成功!', {icon: 1});
					
                    var login_url = $("#login_url").val();
                    if(login_url == '') {
                        login_url == url_go_index;
                    }
                    document.location = login_url;
                }else if(val=='-2'){
				   $("#username").focus();
					$('.msg_error').html('你的账号已冻结，请联系客服');
					layer.close(index);
					//layer.msg('用户名或者密码错误!');
				}else {
                    $("#username").focus();
					$('.msg_error').html('账户名与密码不匹配，请重新输入');
				
					layer.close(index);
					//layer.msg('用户名或者密码错误!');
                }
				document.getElementById("validate").src = url_verify_code+"?time=" + (new Date()).getTime();
				 
            },
            error: function () {
				layer.msg('登录出现错误!');
				layer.close(index); 
            }
        });
    }
}


