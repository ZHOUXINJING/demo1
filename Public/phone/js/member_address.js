﻿//选择收货地址
function sel_address(id) {
    $("#address_item_" + id).addClass("on");
    $("#address_item_" + id).siblings().removeClass("on");
    $("#sel_address_id").val(id);
    count_total_delivery();
}

//删除收货地址
function del_address(id,obj) {
    $(obj).html("<img src='"+fwshop_root+"/Public/system/images/loading.gif' />");
    $.ajax({
        url: url_del_address + '?id=' + id,
        cache: false,
        success: function(val) {
            if (val == 'success') {
				layer.msg('删除成功!', {icon: 1});
				window.location.reload();
            } else {
				
				layer.msg('删除地址出错!');
            }
        }
    })
}

var dialog_add; 
    
//新增收货地址
function add_address() {    

    dialog_add = dialog({
        title: '新增收货地址',
        url:url_add_address,
        width:'700px',
        height:'400px'
    });
    dialog_add.showModal();
}

//修改收货地址
function edit_address(id) {    
    dialog_add = dialog({
        title: '修改收货地址',
        width:'700px',
        height:'400px',
        url:url_edit_address+'?id='+id
    });
    dialog_add.showModal();
}

//新增收货地址完成
function complet_add_address(){
   dialog_add.close().remove();
   get_address();
}
