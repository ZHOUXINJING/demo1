$(function () {
    $("#phone_menu_left").click(function () {
        if ($("#main_menu_phone").is(":hidden")) {
            $("#main_menu_phone").show();
            $("#member_menu_phone").hide();
            $(".gray_bg").show();
        } else {
            $("#main_menu_phone").hide();
            $(".gray_bg").hide();
        }
    });
    $("#phone_menu_right").click(function () {
        if ($("#member_menu_phone").is(":hidden")) {
            $("#member_menu_phone").show();
            $("#main_menu_phone").hide();
            $(".gray_bg").show();
        } else {
            $("#member_menu_phone").hide();
            $(".gray_bg").hide();
        }
    });
    /* 字体缩放 */
   // change_fontsize();

    //更新顶部购物车数量
   // get_cart_num_count();
	//get_message_num_count() ;

    //顶部搜索
    //按enter键搜索
    $('#k').bind('keypress', function (event) {
        if (event.keyCode == "13")
        {
            search();
        }
    });
    $("#btn_top_search").click(function () {
        search();
    });
    
    $('#k2').bind('keypress', function (event) {
        if (event.keyCode == "13")
        {
            search2();
        }
    });
    $("#btn_top_search2").click(function () {
        search2();
    });
});
$(window).resize(function () {
   // change_fontsize();
});
function change_fontsize() {
    var width = $('body').width();
    if (width < 769) {
        var font_size = width / 640 * 20;
        $('html').css('font-size', font_size + 'px');
        //alert(font_size);
    }
}

function show_search() {
    if ($("#top_box_search").is(":hidden")) {
        $("#top_box_search").show();
    } else {
        $("#top_box_search").hide();
    }
}

//顶部搜索
function search() {
    var k = $.trim($("#k").val());
    if (k == "") {
		layer.tips('请输入搜索关键词', "#k" , { time: 1000, tips: [1,'#dd2727'] });
        reutrn;
        return false;
    } else {
        document.location = url_top_search + "keywords=" + encodeURI(k);
    }
}
function search2() {
    var k2 = $.trim($("#k2").val());
    if (k2 == "") {
		layer.tips('请输入搜索关键词', "#k" , { time: 1000, tips: [1,'#dd2727'] });
        reutrn;
        return false;
    } else {
        document.location = url_top_search + "keywords=" + encodeURI(k2);
    }
}

/*
//顶部更新购物车数量
function get_cart_num_count() {
    $.ajax({
        url: url_cart_num_count,
        cache: false,
        success: function (val) {
            $("#cart_num_count").html(val);
			$("#cart_num_count2").html(val);
        }
    })
}

//顶部更新购物车数量
function get_message_num_count() {
    $.ajax({
        url: url_msg_num_count,
        cache: false,
        success: function (val) {
            $("#message_num_count").html(val);
        }
    })
}


//获取顶部登陆状态
function get_top_login_state() {
    $.getJSON(url_top_login, function (data) {
        var top_login_html = '您好！请<a class="login" href="' + url_login_back + '">登录</a>'
        top_login_html += '<a class="reg" href="' + url_register_back + '">免费注册</a>';
        if (data.member_id > 0) {
            top_login_html = '<a href="' + url_member_index + '" class="login target="_blank">' + data.member_name + '</a>';
            top_login_html += '<a href="javascript:void(0)" onclick="logout_ajax()" class="reg">退出登录</a>';
        }
        $("#top_login_state").html(top_login_html);
		

    });
}

//顶部退出登陆
function logout_ajax() {
    $.ajax({
        url: url_logout_ajax,
        success: function (val) {
            if (val == "1") {
                document.location.reload();
            } else {
                document.location = url_logout;
            }
        }
    })
}
*/

//常用函数

//检查邮箱帐号格式
function is_email(e) {
    var pattern = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
    return pattern.test(e);
}
//检查手机号码格式
function is_phone(e) {
    var pattern = /^1[3|5|7|8|][0-9]{9}$/;
    return pattern.test(e);
}
//检查整数
function is_int(num) {
    var pattern = /^\d+$/;
    return pattern.test(num);
}
//检查浮点数
function is_float(num) {
    var pattern = /^\d+(\.\d+)?$/;
    return pattern.test(num);
}