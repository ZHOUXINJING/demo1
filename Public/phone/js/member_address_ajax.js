﻿/**
 * 会员中心-收货地址JS
 * Created by www.szfangwei.cn on 2015-9-18
 */
$(function() {
    //绑定添加按钮事件
    $("#btn_add").bind("click", function() {
        address_add();
    });
    
    //绑定修改按钮事件
    $("#btn_edit").bind("click",function(){
        address_edit();
    });
    
    var province = $("#hid_province").val();
    var city = $("#hid_city").val();
    var area = $("#hid_area").val();
    if (province != "") {
        $("#province").val(province);
        $("#span_city").load(url_city_sel+"province=" + encodeURI(province) + "&city=" + encodeURI(city));
        if (city != "") {
            $("#span_area").load(url_area_sel+"city=" + encodeURI(city) + "&area=" + encodeURI(area));
        }
    }
    $("#province").change(function() {
        change_province();
    });

    //省份变更事件
    $("#province").change(function() {
        change_province();
    });
})

//添加收货地址
function address_add() {
    //获取数据
    var realname = $.trim($("#realname").val());
    var province = $.trim($("#province").val());
    var city = $.trim($("#city").val());
    var area = $.trim($("#area").val());
    var address = $.trim($("#address").val());
    var zip = $.trim($("#zip").val());
    var phone = $.trim($("#phone").val());
    var is_default = 0;
    if ($("#is_default").is(':checked')) {
        is_default = 1;
    }

    if (realname == '') {
		layer.tips('请填写收件人', "#realname", { time: 1000, tips: [1,'#dd2727'] });
		
        return false;
    }
    if (province == '') {
		layer.tips('请选择省份', "#province", { time: 1000, tips: [1,'#dd2727'] });
		
        return false;
    }
    if (city == '') {
		layer.tips('请选择城市', "#city", { time: 1000, tips: [1,'#dd2727'] });
		
        return false;
    }
	 if (address == '') {
		layer.tips('请填写详细地址', "#address", { time: 1000, tips: [1,'#dd2727'] });
       
	   return false;
    }
    if (phone == '') {
		layer.tips('请填写手机号码', "#phone", { time: 1000, tips: [1,'#dd2727'] });
		
        return false;
    }
    if(phone !== '' && !is_phone(phone)) {
		layer.tips('手机号码格式不对', "#phone", { time: 1000, tips: [1,'#dd2727'] });
		
        return false;
    }
	/*
    if(email !== '' && !is_email(email)) {
		layer.tips('邮箱格式不对', "#email", { time: 1000, tips: [1,'#dd2727'] });
        return false;
    }
	*/
	layer.load(2, {time: 10*1000});
    $.ajax({
        url: url_address_add,
        data: {realname: realname, province: province, city: city, area: area, address: address, zip: zip, phone: phone, is_default: is_default},
        cache: false,
        type: "POST",
        success: function(val) {
            if (val == "success") {
			
				//layer.alert('添加收货地址成功', function(index){
             
				 window.location.reload();
                 //});
				
            }
            else {
				layer.msg('添加收货地址出错!', {icon: 5});
				
    
            }
        },
        error: function() {
			layer.msg('系统出错!', {icon: 5});
			
        }

    });
}

//修改收货地址
function address_edit() {
    var id = $("#id").val();
    //获取数据
    var realname = $.trim($("#realname").val());
    var province = $.trim($("#province").val());
    var city = $.trim($("#city").val());
    var area = $.trim($("#area").val());
    var address = $.trim($("#address").val());
    var zip = $.trim($("#zip").val());
    var phone = $.trim($("#phone").val());
    var tel = $.trim($("#tel").val());
    var email = $.trim($("#email").val());
    var is_default = 0;
    if ($("#is_default").is(':checked')) {
        is_default = 1;
    }
      if (realname == '') {
		 layer.tips('请填写收件人', "#realname", { time: 1000, tips: [1,'#dd2727'] });
        return false;
    }
    if (province == '') {
		layer.tips('请选择省份', "#province", { time: 1000, tips: [1,'#dd2727'] });
        return false;
    }
    if (city == '') {
		layer.tips('请选择城市', "#city", { time: 1000, tips: [1,'#dd2727'] });
        return false;
    }
	
	 if (address == '') {
		layer.tips('请填写详细地址', "#address", { time: 1000, tips: [1,'#dd2727'] });
        return false;
    }
    if (phone == '') {
		layer.tips('请填写手机号码', "#phone", { time: 1000, tips: [1,'#dd2727'] });
        return false;
    }
    if(phone !== '' && !is_phone(phone)) {
		layer.tips('手机号码格式不对', "#phone", { time: 1000, tips: [1,'#dd2727'] });
        return false;
    }
	
    layer.load(2, {time: 10*1000});
    $.ajax({
        url: url_address_edit + "id=" + id,
        data: {realname: realname, province: province, city: city, area: area, address: address, zip: zip, phone: phone, is_default: is_default},
        cache: false,
        type: "POST",
        success: function(val) {
            if (val == "success") {
				layer.msg('修改收货地址成功!', {icon: 1});
				parent.window.location.reload();
				
		
				
            }
            else {
				layer.msg('修改收货地址出错!', {icon: 5});
            }
        },
        error: function() {
			layer.msg('系统出错!', {icon: 5});
        }

    });
}


//省份变更
function change_province() {
    var province = $("#province").val();
    $("#span_city").load(url_city_sel + "province=" + encodeURI(province) + "&city=");
    $("#span_area").html("");
}

//城市变更
function change_city() {
    var city = $("#city").val();
    $("#span_area").load(url_area_sel + "city=" + encodeURI(city) + "&area=");
}

//地区变更
function change_area() {
    var area = $("#area").val();
    $.get(url_get_zip + "area=" + encodeURI(area), function(zip) {
        if (zip != '') {
            $("#zip").val(zip);
        }
    });
}

//检查邮箱帐号格式
function is_email(e) {
    var pattern = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
    return pattern.test(e);
}
//检查手机号码格式
function is_phone(e) {
    var pattern = /^1[3|4|5|6|9|7|8|][0-9]{9}$/;
    return pattern.test(e);
}

