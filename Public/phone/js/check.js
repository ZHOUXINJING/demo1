﻿/**
 * 订购页面JS
 * Created by www.szfangwei.cn on 2018-11-18
 */

$(function() {
    //显示收货地址列表
    //get_address();
    
    //绑定提交订单按钮事件
    $("#btn_checkout").bind("click",function() {
        checkout();
    });
	/*
	    $("#btn_add").bind("click", function() {
        address_add();
    });
    */
    //配送方式选择
    $("#sel_delivery_id").change(function(){
        var sel_address_id = $.trim($("#sel_address_id").val());
        if(sel_address_id == 0 || sel_address_id == "") {
			 layer.msg('请先选择或者填写收货地址!');
        } else {
            //$(this).addClass("delivery_type_sel");
            //var delivery_id = $(this).attr('rel');
           // $("#sel_delivery_id").val(delivery_id);
            //$(this).siblings().removeClass("delivery_type_sel");
            count_total_delivery();
        }
    });
})


/*****获得收货地址******/
function get_mmeber_address(id){

   $.ajax({
        url: url_get_address,
        cache: false,
		 type:"POST",
         data:{id:id},
        success: function(obj) {
			$('.vip2_loc .number').html(obj.phone);
			$('.vip2_loc .name').html('收货人：'+obj.realname);
			$('.vip2_loc .loc').html('收货地址：'+obj.province+' '+obj.city+' '+obj.area+' '+obj.address);
			$('#sel_address_id').val(obj.id);
		    $('.closes').click()
			
           // $("#address_list").html(val);
        }
    })
}



/*****获得城市********/
 function get_city(province){
	$('#province').val(province);
	$('#had_selected').html(province);
   $("#province_city_area").load(url_city_sel+"province=" + encodeURI(province));
 }

/***获得区域***/
 function get_area(city){
   $('#city').html(city);
   $('#city').val(city);
   var province = $('#province').val();
   $('#had_selected').html(province+'-'+city);
   $("#province_city_area").load(url_area_sel+"city=" + encodeURI(city));
 }

 function get_zip(area){
   $('#area').html(area);
   $('#area').val(area);
	var province = $('#province').val();
	 var city = $('#city').val();
   $('#had_selected').html(province+'-'+city+'-'+area);
   
    $.get(url_get_zip + "area=" + encodeURI(area), function(zip) {
        if (zip != '') {
            $("#zip").val(zip);
        }
    });
	$('#loc-select').html(province+'-'+city+'-'+area);
	$('.close').click();

 }
 
 
 
//添加收货地址
function address_add() {
    //获取数据
    var realname = $.trim($("#realname").val());
    var province = $.trim($("#province").val());
    var city = $.trim($("#city").val());
    var area = $.trim($("#area").val());
    var address = $.trim($("#address").val());
    var zip = $.trim($("#zip").val());
    var phone = $.trim($("#phone").val());
    var is_default = 0;
    if ($("#is_default").is(':checked')) {
        is_default = 1;
    }

    if (realname == '') {
		 $('#err_msg').html('请填写收件人');
		 $('#realname').focus();
		 //layer.tips('请填写收件人', "#realname", { time: 1000, tips: [1,'#dd2727'] });
        return false;
    }
    if (province == '') {
		$('#err_msg').html('请选择省份');
		//layer.tips('请选择省份', "#province", { time: 1000, tips: [1,'#dd2727'] });
        return false;
    }
    if (city == '') {
		$('#err_msg').html('请选择城市');
		//layer.tips('请选择城市', "#city", { time: 1000, tips: [1,'#dd2727'] });
        return false;
    }
	if(address==''){
	$('#err_msg').html('请填写详细地址');
	$('#address').focus();
	 return false;
	}
    if (phone == '') {
		$('#err_msg').html('请填写手机号码');
		$('#phone').focus();
		///layer.tips('请填写手机号码', "#phone", { time: 1000, tips: [1,'#dd2727'] });
        return false;
    }
    if(phone !== '' && !is_phone(phone)) {
		$('#err_msg').html('手机号码格式不对');
		$('#phone').focus();
		//layer.tips('手机号码格式不对', "#phone", { time: 1000, tips: [1,'#dd2727'] });
        return false;
    }
	/*
    if(email !== '' && !is_email(email)) {
		layer.tips('邮箱格式不对', "#email", { time: 1000, tips: [1,'#dd2727'] });
        return false;
    }
	*/
	layer.load(2, {time: 10*1000});
    $.ajax({
        url: url_address_add,
        data: {realname: realname, province: province, city: city, area: area, address: address, zip: zip, phone: phone, is_default: is_default},
        cache: false,
        type: "POST",
        success: function(val) {
            if (val == "success") {
				//layer.alert('添加收货地址成功', function(index){
				  window.location.reload();
				
            }
            else {
				layer.msg('添加收货地址出错!');
    
            }
        },
        error: function() {
			layer.msg('系统出错!');
        }

    });
}



//提交订单
function checkout() {
    var sel_address_id = $.trim($("#sel_address_id").val());
    var sel_delivery_id = $.trim($("#sel_delivery_id").val());
    var delivery_time = $('#delivery_time').val();//$("input[name='delivery_time']:checked").val();
    var pay_type = $("#pay_type").val();//支付方式
	var payment = $("input[name='payment']:checked").val();//$("#payment").val();//支付途径
    var remark_member = $("#remark_member").val();//备注
    var total_pay_money = $("#total_pay_money").val();//使用余额
    var total_point = $("#total_point").val();//使用积分
    var yhq_ids = $("#yhq_ids").val();//使用的优惠券ids
    var id = $("#id").val();
	
	
	
	
    if(sel_address_id == 0 || sel_address_id == "") {
		 layer.msg('请先选择或者填写收货地址!');

    } else if(sel_delivery_id == 0 || sel_delivery_id == "") {
		layer.msg('请选择配送方式!');

    } else if(delivery_time == null || delivery_time == "") {
        layer.msg('请选择配送时间!');

    } else {
        $.ajax({
            beforeSend:function() {
           // layer.load(2, {time: 10*1000});
			var index = layer.load(2, {time: 10*1000});
            },
            url:url_checkout,
            cache:false,
            dataType: 'json',
            type:"POST",
            data:{id:id,sel_address_id:sel_address_id,sel_delivery_id:sel_delivery_id,delivery_time:delivery_time,pay_type:pay_type,payment:payment,total_pay_money:total_pay_money,total_point:total_point,remark_member:remark_member,yhq_ids:yhq_ids},
            success:function(val) {
                var json = eval(val);
                if(json.status == 'success') {
					 //layer.msg('订单提交成功!', {icon: 1});
                     layer.close(layer.index);
                    if(payment=='微信'){
                    document.location = url_success+'?order_id='+json.order_id;
					}else if(payment=='支付宝'){
						document.location = url_success2+'?order_id='+json.order_id;
					}else{
					document.location = url_success3+'?id='+json.order_id;
					}
                } else {
					 layer.msg(json.msg);
                }
            },
            error:function() {
				layer.msg("系统出错，请重试或联系客服！");
            }
            
        })
    }
}

//支付方式变更
function change_pay_type(pay_type_id) {
    $(".pay_type").removeClass("pay_type_sel");
    $("#pay_type_"+pay_type_id).addClass("pay_type_sel");
    $("#pay_type").val(pay_type_id);
    if(pay_type_id == 2) {
       $(".no_pay_last").hide();
    } else {
       $(".no_pay_last").show();
    }
    $("#sel_delivery_id").val("0");
}

//更新运费
function count_total_delivery() {
    var sel_address_id = $.trim($("#sel_address_id").val());
    var sel_delivery_id = $.trim($("#sel_delivery_id").val());
    var total_weight = $.trim($("#total_weight").val())*1000;//转换重量千克单位改为克
    if(sel_delivery_id != 0 && sel_delivery_id != ""){
        $.getJSON(url_count_delivery,{delivery_id:sel_delivery_id,weight:total_weight,address_id:sel_address_id},function(json) {
            if(json.status == 'success') {
                $("#total_delivery").val(json.total_delivery);
                count_total();
            } else {
				layer.msg("计算运费出错！");
   
            }
        })
    }
}

//更新结算金额
function count_total() {
    var total_cart = parseFloat($("#total_cart").val());
    var total_delivery = parseFloat($("#total_delivery").val());
    var total_pay_money = parseFloat($("#total_pay_money").val());
    var total_pay_point_money = parseFloat($("#total_pay_point_money").val());
	
    var total_yhq = parseFloat($("#yhq_money").val());
    var total_pay = total_cart + total_delivery - total_pay_money - total_pay_point_money - total_yhq;
    $("#span_total_delivery").html(total_delivery.toFixed(2));
    $("#span_total_pay").html(total_pay.toFixed(2));
    $("#total_pay").val(total_pay);
	
    $("#span_total_pay_point_money").html(total_pay_point_money.toFixed(2));
    //优惠券
    if(total_yhq > 0) {
        $("#li_total_yhq_money").show();
    } else {
        $("#li_total_yhq_money").hide();
    }
    $("#span_yhq_money").html(total_yhq.toFixed(2));
    //积分支付
    if(total_pay_point_money > 0) {
        $("#li_total_pay_point").show();
    } else {
        $("#li_total_pay_point").hide();
    }
    $("#span_total_pay_money").html(total_pay_money.toFixed(2));
    
    
}

////使用余额开关
//function use_money_on() {
//    var sel_address_id = $.trim($("#sel_address_id").val());
//    var sel_delivery_id = $.trim($("#sel_delivery_id").val());
//    var d = dialog({
//        title: "系统提示"
//    });
//    if(sel_address_id == 0 || sel_address_id == "") {
//        d.content("请选择或者填写收货地址！");
//        d.show();
//        $("input[name='use_money_on']").removeAttr('checked');
//        return;
//    } else if(sel_delivery_id == 0 || sel_delivery_id == "") {
//        d.content("请选择配送方式！");
//        d.show();
//        $("input[name='use_money_on']").removeAttr('checked');
//        return;
//    }
//    var use_money_on = $("input[name='use_money_on']:checked").val();
//    if(use_money_on == "1") {
//        $("#dl_money dd").show();
//        $("#li_total_pay_money").show();
//    } else {
//        $("#total_pay_money").val(0.00);
//        $("#txt_pay_money").val(0);
//        $("#dl_money dd").hide();
//        $("#li_total_pay_money").hide();
//        count_total();
//    }
//}
//
////使用余额
//function use_money() {
//    var txt_pay_money = parseFloat($("#txt_pay_money").val());
//    var member_money = parseFloat($("#member_money").val());
//    var total_pay = parseFloat($("#total_pay").val());
//    var total_pay_money = parseFloat($("#total_pay_money").val());
//    if(txt_pay_money > member_money) {
//        var d = dialog({
//            title: '系统提示',
//            content:'余额不足'
//        });
//        d.showModal();
//    }
//    if(txt_pay_money > (total_pay + total_pay_money)) {
//        txt_pay_money = total_pay + total_pay_money;
//        $("#txt_pay_money").val(total_pay_money);
//    }
//    if(txt_pay_money > 0) {
//        $("#total_pay_money").val(txt_pay_money.toFixed(2));
//    }
//    count_total();
//}

//使用积分开关
function use_point_on() {
    var sel_address_id = $.trim($("#sel_address_id").val());
    var sel_delivery_id = $.trim($("#sel_delivery_id").val());

    if(sel_address_id == 0 || sel_address_id == "") {
		layer.msg("请选择或者填写收货地址！");
        $("#use_point_on").val("0");
        return;
    } else if(sel_delivery_id == 0 || sel_delivery_id == "") {
		layer.msg("请选择配送方式！");
        $("#use_point_on").val("0");
        return;
    }
    var use_point_on = $("#use_point_on").val();
    if(use_point_on == "0") {
        //从未选中变成已选中
        $(".checked_point").addClass("on");
        $("#use_point_on").val("1");

        var total_cart = parseFloat($("#total_cart").val());
        var total_delivery = parseFloat($("#total_delivery").val());
        var total = total_cart + total_delivery;
        var point_1_to_money = $("#point_1_to_money").val();
        var point_pay_percent = $("#point_pay_percent").val();
        var span_max_point_money = total * point_pay_percent *0.01;
        var span_max_point = parseInt(span_max_point_money/point_1_to_money);
        
        var member_point =  parseInt($("#member_point").val());
		
		
		
        if(span_max_point > member_point) {
            span_max_point = member_point;
            span_max_point_money = point_1_to_money*span_max_point;
        }
        $("#span_max_point_money").html(span_max_point_money.toFixed(2));
        $("#span_max_point").html(span_max_point);
        $("#max_point").val(span_max_point);
		
     
       use_point()
    } else {

        $("#use_point_on").val("0");
        //$(".point_use").hide();
		 $(".checked_point").removeClass("on");
      
        $("#total_point").val(0);
        $("#txt_pay_point").val(0);
        //$("#dl_point dd").hide();
    
        $("#total_pay_point_money").val(0.00);
        count_total();
    }
}

//使用积分
function use_point() {
	
    var txt_pay_point = $("#span_max_point").html();

	if(txt_pay_point==''){
	txt_pay_point=0
	}
	txt_pay_point = parseInt(txt_pay_point);
	//alert(txt_pay_point)
    var member_point = parseFloat($("#member_point").val());
	
    var max_point = parseInt($("#max_point").val());
	
	
	
    var point_1_to_money = $("#point_1_to_money").val();
    if(txt_pay_point > member_point) {
	   layer.msg('积分不足!');
       txt_pay_point = member_point
	   $('#txt_pay_point').val(max_point)
    }
    if(txt_pay_point > max_point) {
        txt_pay_point = max_point;
    }
   
    $("#total_point").val(txt_pay_point);
    var txt_pay_point_money = txt_pay_point*point_1_to_money;

    $("#total_pay_point_money").val(txt_pay_point_money.toFixed(2));
   
    count_total();
}

//使用优惠券
function sel_yhq() {
	
    var yhq_ids = '';
    var yhq_money = 0;
    $("[name='yhq_ids']:checked").each(function(){ 
        yhq_ids += $(this).val()+",";
        yhq_money += parseFloat($(this).attr("money"));
    })
    if(yhq_ids != '') {
        yhq_ids = yhq_ids.substring(0,yhq_ids.length-1);
        $("#yhq_ids").val(yhq_ids);
    }
	
    if(yhq_money > 0) {
        $("#yhq_money").val(yhq_money);
    }else{
	  $("#yhq_money").val('0');
	}
  
	
    count_total();
}

//显示会员收货地址
/*
function get_address() {
    $("#address_list").html("<li><img src='"+fwshop_root+"/Public/system/images/ajax_loading.gif' /></li>");
    $.ajax({
        url: url_get_address,
        cache: false,
        success: function(val) {
            $("#address_list").html(val);
        }
    })
}
*/
//选择收货地址
function sel_address(id) {
    $("#address_item_" + id).addClass("on");
    $("#address_item_" + id).siblings().removeClass("on");
    $("#sel_address_id").val(id);
    count_total_delivery();
}

//删除收货地址
function del_address(id,obj) {
    $(obj).html("<img src='"+fwshop_root+"/Public/system/images/loading.gif' />");
    $.ajax({
        url: url_del_address + '?id=' + id,
        cache: false,
        success: function(val) {
            if (val == 'success') {
                $("#address_item_" + id).remove();
            } else {
				
				layer.msg('删除地址出错!');
            }
        }
    })
}

var dialog_add; 
    
//新增收货地址
function add_address() {    

    dialog_add = dialog({
        title: '新增收货地址',
        url:url_add_address,
        width:'700px',
        height:'400px'
    });
    dialog_add.showModal();
}

//修改收货地址
function edit_address(id) {    
    dialog_add = dialog({
        title: '修改收货地址',
        width:'700px',
        height:'400px',
        url:url_edit_address+'?id='+id
    });
    dialog_add.showModal();
}

//新增收货地址完成
function complet_add_address(){
   dialog_add.close().remove();
   get_address();
}

