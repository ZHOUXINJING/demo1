﻿/**
 * 找回密码JS
 * Created by www.szfangwei.cn on 2015-9-18
 */

$(function() {
    //打开页面输入焦点
    $("#email").focus();
    //首次加载验证码
    $("#validate").attr("src", url_verify_code + "time=" + (new Date()).getTime());
    //单击更新验证码事件
    $("#validate").bind("click", function() {
        this.src = url_verify_code + "time=" + (new Date()).getTime();
    });
    //邮箱找回按钮
    $("#btn_find").bind("click", function() {
        send_email();
    })
    //手机找回按钮
    $("#btn_find_phone").bind("click", function() {
        find_password_by_phone();
    })
    //发送手机短信验证码
    $("#get_phone_code").bind('click',function(){
        get_phone_code();
    });
    
    $("#phone").bind('keypress', function (event) {
        $("#get_phone_code").removeClass("txt21");
        $("#get_phone_code").addClass("txt2");
    });
});

//找回密码函数
function send_email() {
    var email = $.trim($("#email").val());
    var validate_code = $.trim($("#validate_code").val());
    var d = dialog({
            title: '系统提示'
        });
    //验证数据合法性
    if(email == '') {    
        $("#email").focus();
        d.content('请输入邮箱帐号！');
        d.show();
        return;
    }else if(!is_email(email)) {
        $("#email").focus();
        d.content('邮箱帐号格式不对！');
        d.show();
        return;
    }else if(validate_code == '') {
        $("#validate_code").focus();
        d.content('请输入验证码！');
        d.show();
        return;
    };
    d.content('<img src="'+fwshop_root+'/Public/system/images/ajax_loading.gif" />系统正在发送一封邮件到你的邮箱');
    d.show();
    $.ajax({
        url: url_forget_email,
        cache: false,
        type: "POST",
        data: {email: email, validate_code: validate_code},
        success: function(val) { 
             d.content(val);
           
        }
    })
}

var countdown = 60; 
function settime(id) { 
    if (countdown == 0) { 
       $("#"+id).bind('click',function(){
        get_phone_code();
        });
        $("#"+id).val("免费获取验证码");
        countdown = 60;
        $("#"+id).removeClass("txt21");
        $("#"+id).addClass("txt2");
        return;
    } else { 
        $("#"+id).unbind('click');
        $("#"+id).val("重新发送(" + countdown + ")"); 
        countdown--; 
    } 
    setTimeout(function(){settime(id)},1000);
}

//手机找回密码
function find_password_by_phone() {
    var phone = $.trim($("#phone").val());
    var phone_validate_code = $.trim($("#phone_validate_code").val());
    var password_new = $.trim($("#password_new").val());
    var d = dialog({
            title: '系统提示'
        });
    //验证数据合法性
    if(phone == '') {    
        $("#phone").focus();
        d.content('请输入注册手机！');
        d.show();
        return;
    }else if(!is_phone(phone)) {
        $("#email").focus();
        d.content('手机号码格式不对！');
        d.show();
        return;
    }else if(phone_validate_code == '') {
        $("#phone_validate_code").focus();
        d.content('请输入接收到的手机验证码！');
        d.show();
        return;
    }else if(password_new == '') {    
        $("#password_new").focus();
        d.content('请输入新密码！');
        d.show();
        return;
    }
    d.content('<img src="'+fwshop_root+'/Public/system/images/ajax_loading.gif" />请稍等...');
    d.show();
    $.ajax({
        url: url_forget_phone,
        cache: false,
        type: "POST",
        data: {phone: phone, phone_validate_code: phone_validate_code,password_new:password_new},
        success: function(val) { 
            if(val == 'success') {
                d.content('密码重置成功');
                document.location = url_login;
            } else {
               d.content(val);
           }
        }
    })
}

//发送手机短信
function get_phone_code() {
     var phone = $.trim($("#phone").val());
     if (phone == '' || !is_phone(phone)) {
        $("#phone").focus();
        alert('请输入手机号码或者手机号码不对！');
        return;
     } else {
         $.ajax({
            url: url_get_phone_code,
            type: "GET",
            cache: false,
            dataType:"json",
            data: {phone: phone},
            success: function (obj) {
                if (obj.code == '200') {
                    alert(obj.message);
                    $("#get_phone_code").removeClass("txt2");
                    $("#get_phone_code").addClass("txt21");
                    settime("get_phone_code");
                } else {
                    alert(obj.message);
                }
            },
            error: function () {
                alert("发送验证码失败！");
            }
        });
     }
}



