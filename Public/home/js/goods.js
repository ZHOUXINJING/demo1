﻿/**
 * 商品详情页JS
 * Created by www.szfangwei.cn on 2015-9-18
 */

function seturl(obj, url) {
    if (url !== '') {
        $(".country a").removeClass('sel')
        $(obj).addClass('sel');
        $("#buy_url").attr('href', url);
    }
}

$(function() {
    
    //商品数量增减
    $("#btn_jia_num").click(function() {
	
        var store_num = parseInt($("#hide_store_num").val());
        var num = parseInt($("#num").val());
        if(num >= store_num) {
			layer.msg('商品库存不足!', {icon: 5});
            return;
        }
        num++;
        $("#num").val(num);
    });
     $("#btn_jian_num").click(function() {
		var qi_sale = parseInt($(this).attr('data-qisale'));
		
        var num = parseInt($("#num").val());
		if(qi_sale>=num){
		layer.msg('起售数量为'+qi_sale+'个', {icon: 5});
          return
		}
        if(num == 1) {
			layer.msg('已经不能再少了!', {icon: 5});
            return;
        }
        num--;
        $("#num").val(num);
    });
	
	
});

/**修改数量**/
	function edit_num(n){
		
	  var num = $("#num").val();
	  var re =/^[1-9]+[0-9]*]*$/ 
	  if (!re.test(num)) {
　　　 $("#num").val(n);
　　　　return false;
　  　}else{
	  var num = parseInt($("#num").val());
	  if(n>=num){
		  alert('ddd');
		  $("#num").val(n);
          return
	   }else{
	   $("#num").val(num);
	   }
    }
	
	}
	

/*
 $(window).scroll(function() {
    var scroll_top = $(window).scrollTop();
	var element_top = $(".pro_detail_right").offset().top;
	var element_width = $(".pro_detail_right").width();
	var detail_1_top = $("#detail_con1").offset().top;
	var detail_2_top = $("#detail_con2").offset().top;
	var detail_3_top = $("#detail_con3").offset().top;
	var detail_4_top = $("#detail_con4").offset().top;
	//alert(scroll_top+"--"+element_top);
	if(scroll_top - element_top > -10) {
		$(".detail_tab").css('width',element_width+"px");
	    $(".detail_tab").addClass("detail_tab_fix");
		if(scroll_top - detail_4_top > -50) {
			$(".detail_tab li").removeClass("sel");
			$("#tab4").addClass("sel");
	    } else if (scroll_top - detail_3_top > -50) {
			$(".detail_tab li").removeClass("sel");
			$("#tab3").addClass("sel");
		} else if (scroll_top - detail_2_top > -50) {
			$(".detail_tab li").removeClass("sel");
			$("#tab2").addClass("sel");
		} else {
			$(".detail_tab li").removeClass("sel");
			$("#tab1").addClass("sel");
		}
		
	} else {
		$(".detail_tab").removeClass("detail_tab_fix");
	}
	
    
 })
*/
//
//选择规格
function select_spec(obj) {
	
    var attr_id = $(obj).attr('attr_id');
    var attr_val = $(obj).attr('attr_val');
    //清除同规格下已选
    $('#sel_span_' + attr_id).remove();
    //已经为选中状态时
    if ($(obj).attr('class') == 'sel') {
        $(obj).removeClass('sel');
        $('#sel_span_' + attr_id).remove();
    } else {
        //清除同行中其余规格选中状态
        $('#spec_list_' + attr_id).find('a.sel').removeClass('sel');
        $(obj).addClass('sel');
        var html_new_spec = "<span id='sel_span_" + attr_id + "'>“" + attr_val + "”</span>";
        $('#sel_span').append(html_new_spec);
    }
    var sel_spec = '';
    $(".spec_list").each(function() {
        var attr_t = $(this).attr('attr_t');
        var attr_s = $(this).find('a.sel').attr('attr_val');
        if (attr_s !== undefined)
            sel_spec += attr_t + ":" + attr_s + ",";
    })
    if (sel_spec.length > 0) {
        sel_spec = sel_spec.substring(0, sel_spec.length - 1);
    }
    //获取货品数据并进行渲染
    $.getJSON(url_get_item, {"goods_id": goods_id, "spec": sel_spec, "random": Math.random}, function(json) {
	
        if (json.flag == 'success') {
            $('#data_goods_sn').text(json.data.goods_sn);
            $('#data_store_num').text(json.data.store_num);
            $('#hide_store_num').val(json.data.store_num);
            //$('#hide_store_num').val(json.data.store_num);
            $('#data_price').text(json.data.price);
            $('#data_price_market').text(json.data.price_market);
            $('#data_weight').text(json.data.weight);
            $('#item_id').val(json.data.item_id);
			 $('.proDetails .hrefs a').attr('class','');
			 $('.proDetails .hrefs>a').eq(0).attr('class','on');
        } else if (json.flag == 'error') {
            //alert(json.message);
            $('#item_id').val('');
        } else {
			  $('.proDetails .hrefs a').attr('class','no-arrowed');
			  $('#data_store_num').text('0');
			   $('#hide_store_num').val('0');
            $('#item_id').val('');
        }
    });
}

//加入购物车
function add_cart() {
	/*
    var d = dialog({
        title: '系统提示'
    });
	*/
    var item_id = $("#item_id").val();
    var num = $("#num").val();
    var goods_id = $("#goods_id").val();
    var store_num = $("#hide_store_num").val();
    if (item_id == 0 || item_id == ''||store_num==0) {
		layer.msg('该商品库存不足!', {icon: 5});
		
    } else {
        if (parseInt(store_num) < parseInt(num)) {
			layer.msg('库存不足!', {icon: 5});
        } else {
            $.ajax({
                url: url_add_cart,
                type: "GET",
                data: {goods_id:goods_id,item_id:item_id,num:num,random:Math.random},
                cache: false,
                success: function(data) {
                     if (data >= 0) {
						 
	                var that = $(this)
	               var id = that.attr('data-id');
	               layer.confirm('加入购物车成功!', {
                    btn: ['去购物车','关闭'] //按钮
                  }, function(){
                    window.location.href= url_cart
                  //layer.closeAll();
		
                  }, function(){
		  
                    });  
						 
						 /*
                      	layer.alert('加入购物车成功', function(index){
                        window.location.reload();
                       layer.close(index);
                       });
					   */
					   
                    } else {
						layer.msg('加入购物车失败!', {icon: 5});

                    }
                },
                error: function() {
					layer.msg('系统出错,请重试!', {icon: 5});
                }
            });
        }
    }
}

//立即购买
function check_cart() {

    var item_id = $("#item_id").val();
    var num = $("#num").val();
    var goods_id = $("#goods_id").val();
    var store_num = $("#hide_store_num").val();
    if (item_id == 0 || item_id == ''||store_num==0) {
		 layer.msg('该商品库存不足!', {icon: 5});
    } else {
        if (parseInt(store_num) < parseInt(num)) {
           	layer.msg('库存不足', {icon: 5});

        } else {
            $.ajax({
                url: url_add_cart,
                type: "GET",
                data: {goods_id:goods_id,item_id:item_id,num:num,random:Math.random},
                cache: false,
                success: function(data) {
                    if (data >= 0) {
						layer.msg('加入购物车成功', {icon: 1});
                        document.location = fwshop_root+'/Check/index?id='+data;
                    } else {
						layer.msg('加入购物车失败', {icon: 5});
       
                    }
                },
                error: function() {
					layer.msg('系统出错', {icon: 5});
 
                }
            });
        }
    }
}

//收藏商品
function add_fav() {
    var goods_id = $("#goods_id").val();
    var item_id = $("#item_id").val();
    $.getJSON(url_add_fav, {goods_id: goods_id, item_id: item_id}, function(data) {
		if(data.status=='success'){
		if(data.msg=='1'){
		 layer.msg('收藏成功', {icon: 1});
		 $('#goods_fav>img').attr('src',theme_path+'/images/car3_2.png');
		}else if(data.msg=='-2'){
			$('#goods_fav>img').attr('src',theme_path+'/images/car3.png');	
		    layer.msg('你已移除此商品', {icon: 1});
			}
			
		}else{
			if(data.msg=='0'){
			//layer.msg('请先登录', {icon: 2});
			window.location.href = url_login_back
			}else if(data.msg=='-3'){
			   layer.msg('收藏失败', {icon: 2}); 
			}
		}
    });
}