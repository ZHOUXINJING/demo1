﻿/**
 * 会员中心-退换货JS
 * Created by www.szfangwei.cn on 2015-9-18
 */

//添加返修记录
function back_add() {
    //获取数据
    var type = $("input[name='type']:checked").val();
    var goods_num = $("#goods_num").val();
    var hid_goods_num = $("#hid_goods_num").val();
    var detail = $.trim($("#detail").val());
    var waiguan = $("input[name='waiguan']:checked").val();
    var baozhuang = $("input[name='baozhuang']:checked").val();
    var pinzheng = '';
    $("[name='pinzheng']:checked").each(function() {
        pinzheng += $(this).val() + ",";
    });

    var addr_realname = $.trim($("#addr_realname").val());
    var addr_tel = $.trim($("#addr_tel").val());
    var addr_address = $.trim($("#addr_address").val());
    var id = $("#id").val();
    var d = dialog({
        title: '系统提示'
    });
    if (type == null || type == '') {
        d.content('请选择类型');
        d.show();
        return false;
    } else if (goods_num > hid_goods_num) {
        $("#goods_num").focus();
        d.content('申请数量不能大于购买数量');
        d.show();
        return false;
    } else if (detail == '') {
        $("#detail").focus();
        d.content('请填写申请原因');
        d.show();
        return false;
    } else if (addr_realname == '') {
        $("#addr_realname").focus();
        d.content('请填写联系人');
        d.show();
        return false;
    } else if (addr_tel == '') {
        $("#addr_tel").focus();
        d.content('请填写联系电话');
        d.show();
        return false;
    } else if (addr_address == '') {
        $("#addr_address").focus();
        d.content('请填写联系地址');
        d.show();
        return false;
    }
    d.content('<img src="' + fwshop_root + '/Public/system/images/ajax_loading.gif" />正在提交中...，请稍等！');
    d.show();
    $.ajax({
        url: url_back_add + "id=" + id,
        data: {type: type, goods_num: goods_num, waiguan: waiguan, baozhuang: baozhuang, pinzheng: pinzheng, detail: detail, addr_realname: addr_realname, addr_tel: addr_tel, addr_address: addr_address},
        cache: false,
        type: "POST",
        success: function(val) {
            if (val == "success") {
                d.close().remove();
                var d2 = dialog({
                    title: '系统提示',
                    content: '申请成功，请等待客服审核！',
                    cancel: false,
                    ok: function() {
                        document.location = url_back_record;
                    }
                });
                d2.show();
            }
            else {
                d.content(val);
                d.show();
            }
        },
        error: function() {
            d.content('系统出错！');
            d.show();
        }

    });
}

//确认回寄商品
 function back_goods() {
    var id = $("#id").val();
    var d = dialog({
        title: '系统提示'
    });
    d.content('<img src="' + fwshop_root + '/Public/system/images/ajax_loading.gif" />正在提交中...，请稍等！');
    d.show();
    $.ajax({
        url: url_back_goods,
        data: {id: id},
        cache: false,
        type: "POST",
        success: function(val) {
            if (val == "success") {
                d.close().remove();
                var d2 = dialog({
                    title: '系统提示',
                    content: '操作成功，请等待处理！',
                    cancel: false,
                    ok: function() {
                        document.location.reload();
                    }
                });
                d2.show();
            }
            else {
                d.content(val);
                d.show();
            }
        },
        error: function() {
            d.content('系统出错！');
            d.show();
        }

    });
}
