﻿/**
 * 重置密码-找回密码JS
 * Created by www.szfangwei.cn on 2015-9-18
 */

$(function(){
    $("#btn_reset").bind('click',function(){
        change_password();
    });
})

function change_password() {
    var code = $("#code").val();
    var password = $.trim($("#password").val());
    var password2 = $.trim($("#password2").val());
    var d = dialog({
            title: '系统提示'
        });
    if (password == '') {
        d.content('请输入密码');
        d.show();
        return;
    }
    if (password !== password2) {
        d.content('两次输入的密码不一致');
        d.show();
        return;
    }
    $.ajax({
        url: url_forget_reset_ajax,
        cache: false,
        type: "POST",
        data: {code: code, password: password},
        success: function(val) {
            if (val == "success") {
                d.close().remove();
                var d2 = dialog({
                    title: '系统提示',
                    content: '密码重置成功！',
                    cancel: false,
                    ok: function () {
                        document.location = url_login;
                    }
                });
                d2.show();
            } else {
                d.content(val);
                d.show();
            }
        }
    })
}