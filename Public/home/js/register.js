﻿/**
 * 注册页面JS
 * Created by www.szfangwei.cn on 2015-9-18
 */

$(function () {
    //打开页面焦点放在手机输入框里
    $("#phone").focus();
    //首次加载
    $(".validate").attr("src", url_verify_code + "time=" + (new Date()).getTime());
    //单击验证码事件
    $(".validate").bind("click", function () {
        this.src = url_verify_code + "time=" + (new Date()).getTime();
    });

    //邮箱和手机TAG切换
    $(".reg_tab span").on("click", function () {
        $(this).addClass("on").siblings().removeClass("on");
        $(".login_menu").hide().eq($(this).index()).show();
    }).eq(0).click();

    //手机注册
    $("#btn_register_phone").bind('click', function () {
        register_phone();
    });
    $('#validate_code').bind('keypress', function (event) {
        if (event.keyCode == "13")
        {
            register_phone();
        }
    });

    //邮箱注册
    $("#btn_register_email").bind('click', function () {
        register_email();
    });
    $('#validate_code_email').bind('keypress', function (event) {
        if (event.keyCode == "13")
        {
            register_email();
        }
    });
})

//手机注册
function register_phone() {
    //获取值
    var phone = $.trim($("#phone").val());
    var password = $.trim($("#password").val());
    var password2 = $.trim($("#password2").val());
    var validate_code = $.trim($("#validate_code").val());
   

    //验证数据合法性
    if (phone == '') {
        $("#phone").focus();
		
		
        d.content('请输入手机号码！');
        d.show();
        return;
    } else if (!is_phone(phone)) {
        $("#phone").focus();
        d.content('手机号码格式不对！');
        d.show();
        return;
    } else if (password.length < 6) {
        $("#password").focus();
        d.content('密码最少为6个字符！');
        d.show();
        return;
    } else if (password != password2) {
        $("#password2").focus();
        d.content('两次密码输入不一致！');
        d.show();
        return;
    } else if (validate_code == '') {
        $("#validate_code").focus();
        d.content('请输入验证码！');
        d.show();
        return;
    }
    //提交数据
    d.content('<img src="' + fwshop_root + '/Public/system/images/ajax_loading.gif" />正在注册中...，请稍等！');
    d.show();
    $.ajax({
        url: url_register_submit,
        type: "POST",
        cache: false,
        data: {password: password, phone: phone, validate_code: validate_code},
        success: function (obj) {
            if (obj.title == 'success') {
                d.content("注册成功！");
                var login_url = $("#login_url").val();
                if(login_url == '') {
                    login_url == url_member_index ;
                }
                document.location = login_url;
            } else {
                d.content(obj.msg);
                if (obj.msg == '验证码不对！') {
                    $("#validate_code").focus();
                    $(".validate").attr("src", url_verify_code + "time=" + (new Date()).getTime());
                }
            }
        },
        error: function () {
            d.content("注册出现错误");
        }
    });
}

//邮箱注册
function register_email() {
    var email = $.trim($("#email").val());
    var password_email = $.trim($("#password_email").val());
    var password_email2 = $.trim($("#password_email2").val());
    var validate_code_email = $.trim($("#validate_code_email").val());
    var d = dialog({
        title: '注册提示'
    });
    //验证数据合法性
    if (email == '') {
        $("#email").focus();
        d.content('请输入邮箱帐号！');
        d.show();
        return;
    } else if (!is_email(email)) {
        $("#email").focus();
        d.content('邮箱帐号格式不对！');
        d.show();
        return;
    } else if (password_email.length < 6) {
        $("#password_email").focus();
        d.content('密码最少为6个字符！');
        d.show();
        return;
    } else if (password_email != password_email2) {
        $("#password_email2").focus();
        d.content('两次密码输入不一致！');
        d.show();
        return;
    } else if (validate_code_email == '') {
        $("#validate_code_email").focus();
        d.content('请输入验证码！');
        d.show();
        return;
    }
    //提交数据
    d.content('<img src="' + fwshop_root + '/Public/system/images/ajax_loading.gif" />正在注册中...，请稍等！');
    d.show();
    $.ajax({
        url: url_register_submit,
        type: "POST",
        cache: false,
        data: {email: email, password: password_email, validate_code: validate_code_email},
        success: function (obj) {
            if (obj.title == 'success') {
                d.content("注册成功！");
                var login_url = $("#login_url").val();
                if(login_url == '') {
                    login_url == url_member_index ;
                }
                document.location = login_url;
            } else {
                d.content(obj.msg);
                if (obj.msg == '验证码不对！') {
                    $("#validate_code_email").focus();
                    $(".validate").attr("src", url_verify_code + "time=" + (new Date()).getTime());
                }
            }
        },
        error: function () {
            d.content("注册出现错误");
        }
    });
}



