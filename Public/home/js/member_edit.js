function save() {
    $.ajax({
        url: url_member_edit,
        data: $("#frm").serialize(),
        cache: false,
        type: "POST",
        success: function(val) {
            if (val == "success") {
				layer.alert('修改成功', function(index){
                   window.location.reload();
                    layer.close(index);
                  });
				
               // $("#lbl_msg").html("<font color=green>资料修改成功</font>");
            }
            else {
				layer.msg(val, {icon: 5});
            }
        },
        error: function() {
			layer.msg('资料修改出现错误!', {icon: 5});
        }

    });
}
//用户地区选择
$(function() {
    var province = $("#hid_province").val();
    var city = $("#hid_city").val();
    var area = $("#hid_area").val();
    if (province != "") {
        $("#province").val(province);
        $("#span_city").load(url_city_sel + "province=" + encodeURI(province) + "&city=" + encodeURI(city));
        if (city != "") {
            $("#span_area").load(url_area_sel + "city=" + encodeURI(city) + "&area=" + encodeURI(area));
        }
    }
    $("#province").change(function() {
        change_province();
    });
})

function change_province() {
    var province = $("#province").val();
    $("#span_city").load(url_city_sel + "province=" + encodeURI(province) + "&city=");
    $("#span_area").html("");
}

function change_city() {
    var city = $("#city").val();
    $("#span_area").load(url_area_sel + "city=" + encodeURI(city) + "&area=");
}

function change_area() {
    var area = $("#area").val();
    $.get(url_get_zip + "area=" + encodeURI(area), function(zip) {
        if (zip != '') {
            $("#zip").val(zip);
        }
    });
}


