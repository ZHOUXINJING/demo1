$("#new_password").keyup(function() {
    AuthPasswd($("#new_password").val());
})
function change_password() {
    var old_password = $.trim($("#old_password").val());
    var new_password = $.trim($("#new_password").val());
    var new_password2 = $.trim($("#new_password2").val());

    if (old_password == "") {
        $("#old_password").focus();
		layer.msg('请输入旧密码!', {icon: 5});

        return false;
    } else if (new_password == "") {
        $("#new_password").focus();
		layer.msg('请输入新密码!', {icon: 5});

        return false;
    } else if (new_password == old_password) {
        $("#new_password").focus();
		layer.msg('新密码和旧密码一样!', {icon: 5});

        return false;
    }
    else if (new_password != new_password2) {
        $("#new_password2").focus();
		layer.msg('两次输入的密码不一致!', {icon: 5});
        return false;
    }
	layer.load(2, {time: 10*1000});
    $.ajax({
        url: url_member_change_pasword,
        data: {old_password: old_password, new_password: new_password},
        cache: false,
        type: "POST",
        success: function(val) {
            if (val == "success") {
                $("#old_password").val("");
                $("#new_password").val("");
                $("#new_password2").val("");
                   	layer.alert('密码修改成功！', function(index){
                       layer.close(index);
                    });
            }
            else {
				layer.msg(val, {icon: 5});
   
            }
        },
        error: function() {
			layer.msg('系统出错！', {icon: 5});
        }
    });
	layer.closeAll('loading'); //关闭加载层
}
function AuthPasswd(string) {
    if (string.length >= 6) {
        if (/[a-zA-Z]+/.test(string) && /[0-9]+/.test(string) && /\W+\D+/.test(string)) {
            noticeAssign(1);
        } else if (/[a-zA-Z]+/.test(string) || /[0-9]+/.test(string) || /\W+\D+/.test(string)) {
            if (/[a-zA-Z]+/.test(string) && /[0-9]+/.test(string)) {
                noticeAssign(-1);
            } else if (/\[a-zA-Z]+/.test(string) && /\W+\D+/.test(string)) {
                noticeAssign(-1);
            } else if (/[0-9]+/.test(string) && /\W+\D+/.test(string)) {
                noticeAssign(-1);
            } else {
                noticeAssign(0);
            }
        }
    } else {
        noticeAssign(null);
    }
}

function noticeAssign(num) {
    if (num == 1) {
        $('#weak').css({backgroundColor: '#009900'});
        $('#middle').css({backgroundColor: '#009900'});
        $('#strength').css({backgroundColor: '#009900'});
        $('#strength').html('很强');
        $('#middle').html('');
        $('#weak').html('');
    } else if (num == -1) {
        $('#weak').css({backgroundColor: '#ffcc33'});
        $('#middle').css({backgroundColor: '#ffcc33'});
        $('#strength').css({backgroundColor: ''});
        $('#weak').html('');
        $('#middle').html('中');
        $('#strength').html('');
    } else if (num == 0) {
        $('#weak').css({backgroundColor: '#dd0000'});
        $('#middle').css({backgroundColor: ''});
        $('#strength').css({backgroundColor: ''});
        $('#weak').html('弱');
        $('#middle').html('');
        $('#strength').html('');
    } else {
        $('#weak').html('&nbsp;');
        $('#middle').html('&nbsp;');
        $('#strength').html('&nbsp;');
        $('#weak').css({backgroundColor: ''});
        $('#middle').css({backgroundColor: ''});
        $('#strength').css({backgroundColor: ''});
    }
}