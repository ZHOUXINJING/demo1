//删除收藏商品
function del_fav(id) {
    $.getJSON(url_del_fav, {id: id}, function(data) {
      
	   layer.load(2, {time: 10*1000});
        document.location.reload();
    });
}

//加入购物车
function add_cart_to(goods_id, item_id) {
    var num = 1;
    var d = dialog({
        title: '系统提示'
    });
    $.ajax({
        url: url_add_cart,
        type: "GET",
        data: {goods_id: goods_id, item_id: item_id, num: num, random: Math.random},
        cache: false,
        success: function(data) {
            if (data >= 0) {
                d.content('加入购物车成功!');
                d.show();
            } else {
                d.content('加入购物车出错!');
                d.show();
            }
        },
        error: function() {
            alert('系统出错!');
        }
    })
}

function check_cart_to(goods_id,item_id) {
    var d = dialog({
        title: '系统提示'
    });
    var num = 1;
    
            $.ajax({
                url: url_add_cart,
                type: "GET",
                data: {goods_id:goods_id,item_id:item_id,num:num,random:Math.random},
                cache: false,
                success: function(data) {
                    if (data >= 0) {
                        d.content('加入购物车成功！');
                        d.show();
                        document.location = fwshop_root+'/Check/index?id='+data;
                    } else {
                        d.content('加入购物车失败！');
                        d.show();
                    }
                },
                error: function() {
                    d.content('系统出错！');
                    d.show();
                }
            });
     
}