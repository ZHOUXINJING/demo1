﻿/**
 * 帮助中心JS
 * Created by www.szfangwei.cn on 2015-9-18
 */
$(function(){
    //左侧菜单
	$(".help_left dt").click(function(){
	    if($(this).parent().find("dd").is(":hidden")) {
		    $(this).parent().find("dd").show();
			$(this).find("dt span").html("-");
		} else {
			$(this).parent().find("dd").hide();
			$(this).parent().find("dd span").html("-");
		}
	})	 
	 
})
