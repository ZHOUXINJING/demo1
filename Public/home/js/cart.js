﻿/**
 * 购物车页面JS
 * Created by www.szfangwei.cn on 2015-9-18
 */

//增加购物车数量
function cart_jia(cart_id) {
    var num = parseInt($("#num_" + cart_id).val());
    num++;
    $.ajax({
        url: url_cart_num,
        type:"GET",
        data:{"cart_id":cart_id,"num":num,"random":Math.random},
        cache: false,
        success: function(val) {
            if ($.trim(val) == '1') {
                $("#num_" + cart_id).val(num);
                $("#num2_" + cart_id).val(num);
                $("#old_num_" + cart_id).val(num);
                 //更新小计价格
                var price = $("#price_" + cart_id).val();
                var sub_total = price * num;
                $("#sub_total_" + cart_id).html(sub_total.toFixed(2));
                //更新总价
                cart_total();
                //库存检测
                var store_num = parseInt($("#store_num_"+cart_id).val());
                if(store_num <= 0) {
                    $("#store_num_msg_"+cart_id).html('<font color=red>无库存</font>');
                } else if(store_num < num) {
                    $("#store_num_msg_"+cart_id).html('<font color=red>仅剩'+store_num+'</font>');
                } else if(store_num <= 5) {
                     $("#store_num_msg_"+cart_id).html('<font color=gray>仅剩'+store_num+'</font>');
                } else {
                     $("#store_num_msg_"+cart_id).html('');
                }
            }
        }
    });
}

//减少购物车数量
function cart_jian(cart_id,qi_sale) {
    var num = parseInt($("#num_" + cart_id).val());
    num--;
   if(num < qi_sale) {
		 layer.tips('数量至少为'+qi_sale, "#num_" + cart_id, { time: 1000, tips: [1,'#dd2727'] });
        reutrn;
    }
    $.ajax({
        url: url_cart_num,
        type:"GET",
        data:{"cart_id":cart_id,"num":num,"random":Math.random},
        cache: false,
        success: function(val) {
            if ($.trim(val) == '1') {
                $("#num_" + cart_id).val(num);
                $("#num2_" + cart_id).val(num);
                $("#old_num_" + cart_id).val(num);
                //更新小计价格
                var price = $("#price_" + cart_id).val();
                var sub_total = price * num;
                $("#sub_total_" + cart_id).html(sub_total.toFixed(2));
                //更新总价
                cart_total();
                //库存检测
                var store_num = parseInt($("#store_num_"+cart_id).val());
                if(store_num <= 0) {
                    $("#store_num_msg_"+cart_id).html('<font color=red>无库存</font>');
                } else if(store_num < num) {
                    $("#store_num_msg_"+cart_id).html('<font color=red>仅剩'+store_num+'</font>');
                } else if(store_num <= 5) {
                     $("#store_num_msg_"+cart_id).html('<font color=gray>仅剩'+store_num+'</font>');
                } else {
                     $("#store_num_msg_"+cart_id).html('');
                }
            }
        }
    });
}

//修改购物车数量
function cart_num(cart_id,qi_sale) {
    var num = parseInt($("#num_" + cart_id).val());
    var old_num = $("#old_num_" + cart_id).val();
    if (!is_int(num)) {
		 layer.tips('商品数量必须为正整数', "#num_" + cart_id, { time: 1000, tips: [1,'#dd2727'] });
        $("#num_" + cart_id).val(old_num);
        $("#num2_" + cart_id).val(old_num);
        return;
    }
	if(num < qi_sale){
	  num = qi_sale
	}
	$("#num_" + cart_id).val(num)
    $.ajax({
        url: url_cart_num,
        type:"GET",
        data:{"cart_id":cart_id,"num":num,"random":Math.random},
        cache: false,
        success: function(val) {
            if ($.trim(val) == '1') {
                $("#old_num" + cart_id).val(num);
                 //更新小计价格
                var price = $("#price_" + cart_id).val();
                var sub_total = price * num;
                $("#sub_total_" + cart_id).html(sub_total.toFixed(2));
                //更新总价
                cart_total();
                //库存检测
                var store_num = parseInt($("#store_num_"+cart_id).val());
                if(store_num <= 0) {
                    $("#store_num_msg_"+cart_id).html('<font color=red>无库存</font>');
                } else if(store_num < num) {
                    $("#store_num_msg_"+cart_id).html('<font color=red>仅剩'+store_num+'</font>');
                } else if(store_num <= 5) {
                     $("#store_num_msg_"+cart_id).html('<font color=gray>仅剩'+store_num+'</font>');
                } else {
                     $("#store_num_msg_"+cart_id).html('');
                }
            }
        }
    });
}

//删除购物车商品
function cart_del(cart_id) {
    $.ajax({
        url: url_cart_del,
        type:"GET",
        data:{"cart_id":cart_id},
        cache: false,
        success: function(val) {
            if ($.trim(val) == '1') {
                $("#tr_cart_"+cart_id).remove();
				window.location.reload();
                cart_total();//更新总价
            }
        }

    });
}

//清空购物车
function cart_clear() {
    $.ajax({
        url: url_cart_clear,
        type:"GET",
        cache: false,
        success: function(val) {
            if ($.trim(val) == '1') {
                $("#cart_body").remove();
                cart_total();//更新总价
				//window.location.reload();
            }
        }
    });
}

//更新总价
function cart_total() {
	var sel_id_list='';
	var a = 0
	
  $("input[name='sel[]']:checked").each(function(){
        var cart_id = $(this).val();
        sel_id_list += cart_id + '-';
		a++;
    });
	
	 sel_id_list = sel_id_list.substring(0,sel_id_list.length - 1);
	//alert(sel_id_list)
	if(a==0){
		$("#cart_total").html('0.00');
		 $("#select_cart_num").html(a);
		return false;
	}
		
	url = url_cart_total+'?id='+sel_id_list;
    $.ajax({
        url: url,
        type:"GET",
        cache: false,
        success:function(val) {
            $("#cart_total").html(parseFloat(val).toFixed(2));
		    $("#select_cart_num").html(a);
        }
   });
}

 //全选
function sel_all(form)
{
    var elems = form.getElementsByTagName("input");
    for(var i=0;i<elems.length;i++) {
        if (elems[i].name == "sel[]") {
            elems[i].checked = form.selall.checked;
			
        }else{
		   elems[i].checked = form.selall.checked;
		}
    }
	cart_total()
}

//删除所选
function cart_del_sel() {
    $("#form_mycart").attr('action',url_cart_delsel);
    $("#form_mycart").submit();
}

//结算
function checkout() {
    var ok = true;
    var sel_cart_length = $("input[name='sel[]']:checked").length; //已选择购物车件数
    if(sel_cart_length <= 0) {
        ok = false;
		layer.msg('请选择需要购买的商品!', {icon: 5});
        return false;
    }
    var sel_id_list = '';
    //库存检测
    $("input[name='sel[]']:checked").each(function(){
        var cart_id = $(this).val();
        var num = parseInt($("#num_" + cart_id).val());
        var store_num = parseInt($("#store_num_"+cart_id).val());
        if(num > store_num) {
            ok = false;
			layer.msg('存在商品库存不足!', {icon: 5});
            return false;
        }
        sel_id_list += cart_id + '-';
    });
    if(ok) {
        sel_id_list = sel_id_list.substring(0,sel_id_list.length - 1);
        document.location = url_check+'?id='+sel_id_list; 
    }
}

function check_store_num() {
    $.ajax({
        url: url_check_store_num,
        type:"GET",
        cache: false,
        success:function(val) {
            if(val == "0") {
				layer.msg('存在商品库存不足!', {icon: 5});
            }
        }
    });
}

//收藏商品
function add_fav(goods_id,item_id) {
    $.getJSON(url_add_fav, {goods_id: goods_id, item_id: item_id}, function(data) {
		if(data.status=='success'){
		if(data.msg=='1'){
		 layer.msg('收藏成功', {icon: 1});
		}else if(data.msg=='-2'){	
		    layer.msg('你已收藏此商品', {icon: 1});
			}
			
		}else{
			if(data.msg=='0'){
			//layer.msg('请先登录', {icon: 2});
			window.location.href = url_login_back
			}else if(data.msg=='-3'){
			   layer.msg('收藏失败', {icon: 2}); 
			}
		}
    });
}