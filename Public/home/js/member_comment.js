﻿/**
 * 会员中心-评论JS
 * Created by www.szfangwei.cn on 2015-9-18
 */

//添加评论
function comment_add() {
    //获取数据
    //var star = $("input[name='star']:checked").val();
    var detail = $.trim($("#detail").val());
    var id = $("#comment_id").val();

     if (detail == '') {
        $("#detail").focus();
		layer.msg('请填写评价内容!', {icon: 5});
        return false;
    }
	layer.load(2, {time: 10*1000});

    $.ajax({
        url: url_comment_add + "id=" + id,
        data: {detail: detail},
        cache: false,
        type: "POST",
        success: function(val) {
            if (val == "success") {
				layer.msg('添加评价成功!', {icon: 1});
                 document.location = url_comment_record;
  
				
            }
            else {
				layer.msg(val, {icon: 5});
              
            }
        },
        error: function() {
			layer.msg('系统出错!', {icon: 5});
      
        }

    });
}
