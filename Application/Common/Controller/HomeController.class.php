<?php
namespace Common\Controller;
use Fwadmin\Model\ConfigModel;
use Fwadmin\Model\SeoPageModel;
use Think\Controller;
class HomeController extends Controller {
    function __construct() {
        parent::__construct();
         $url_action =  CONTROLLER_NAME.'/'.ACTION_NAME;
        //获取配置参数
        $config = S('config');
        if (empty($config)) {
            $ConfigObj = new ConfigModel;
            $config = $ConfigObj->getAll();
            S('config', $config);
        }
        $this->config = $config;
		
		//页面SEO自定义
        $seo = S('seo');
        if (empty($seo)) {
            $SeoObj = new SeoPageModel;
            $seo = $SeoObj->getAll();
            S('seo', $seo);
        }
		
	  $query_string = '';
	  if($_SERVER['QUERY_STRING']){
	  $query_string='?'.$_SERVER['QUERY_STRING'];
	  }
	  $current_url = curPageURL();
	  
	  $this->current_url = str_replace(get_host().__ROOT__,get_host().__ROOT__.'/phone',$current_url);
		
		
		
        $seo = $seo['Index/index'];
	
		$config['title'] = $seo['title'];
		$config['keywords'] = $seo['keywords'];
		$config['description]'] = $seo['description]'];
		
        $this->assign('config', $config);
		
        
        //网站维护
        if($config['site_close'] == "1") {
            $this->display('Index/weihu');
            exit();
        }
        

      
        //获取所有类别
        $all_cat = S('all_cat');
        if (empty($all_cat)) {
            $ModelGoodsCat = new \Home\Model\GoodsCatModel();
            $all_cat = $ModelGoodsCat->show_list(0);
            S('all_cat', $all_cat);
        }
	
        $this->all_cat = $all_cat;
	
        
        //获取导航菜单
        $menu_cat = S('menu_cat');
        if (empty($menu_cat)) {
            $ModelGoodsCat = new \Home\Model\GoodsCatModel();
            $menu_cat = $ModelGoodsCat->menu_list();
            S('menu_cat', $menu_cat);
        }
        $this->menu_cat = $menu_cat;
        
        //顶部搜索
        $hotsearch = S('hotsearch');
        if (empty($hotsearch)) {
            $HotsearchObj = new \Fwadmin\Model\HotsearchModel();
            $hotsearch = $HotsearchObj->field('title,url,color')->where('is_show=1')->order('order_id asc')->select();
            S('hotsearch', $hotsearch);
        }
        $this->assign('hotsearch', $hotsearch);
        
        //底部服务图标
        $config_tubiao = S('config_tubiao');
        if (empty($config_tubiao)) {
            $ConfigTubiaoObj = new \Fwadmin\Model\ConfigTubiaoModel();
            $config_tubiao = $ConfigTubiaoObj->field('title,picture,spec')->where('is_show=1')->order('order_id asc')->select();
            S('config_tubiao', $config_tubiao);
        }
        $this->assign('config_tubiao', $config_tubiao);
        
        //底部帮助菜单
        $footlink = S('foot_link');
        if (empty($footlink)) {
            $FootlinkObj = new \Home\Model\ConFootlinkModel();
            $footlink = $FootlinkObj->getList();
            S('foot_link', $footlink);
        }
		//print_r($footlink);
		$this->footlink = $footlink;
      
    
        $this->host = get_host();
        $this->theme_path = __ROOT__.'/Public/home';
		 $this->is_mobile = is_mobile()?1:0;
		/****获得cid****/
		
    }
}
?>