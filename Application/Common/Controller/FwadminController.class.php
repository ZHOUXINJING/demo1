<?php

namespace Common\Controller;

use Think\Controller;

class FwadminController extends Controller {

    function __construct() {
        parent::__construct();
        //获取配置参数
        $config = S('config');
        if (empty($config)) {
            $ConfigObj = new \Fwadmin\Model\ConfigModel;
            $config = $ConfigObj->getAll();
            S('config', $config);
        }
        //判断登录
        //echo ACTION_NAME;当前操作名
        //echo CONTROLLER_NAME;当前模块名
        $admin_id = I('session.admin_id', 0);
        if ($admin_id == 0) {
            $cookie_admin_id = I('cookie.admin_id', 0);
            $cookie_key_login_code = I('cookie.key_login_code', '');
            if ($cookie_admin_id > 0 && $cookie_key_login_code != '') {
                $AdminObj = new \Fwadmin\Model\AdminModel();
                $arr = $AdminObj->where(Array('admin_id' => $cookie_admin_id, 'is_enable'=>1, 'key_login_code' => $cookie_key_login_code))->find();
                if (is_array($arr)) {
                    session('admin_id', $arr['admin_id']);
                    session('admin_username', $arr['username']);
                    session('admin_group_id', $arr['group_id']);

                    $ARObj = new \Fwadmin\Model\SystemRecordModel();
                    $ARObj->addrecord("登陆维持");
                }
            }
            if (I('session.admin_id', 0) == 0) {
                $this->redirect("Login/index");
            }
        }

        $PurviewObj = new \Fwadmin\Model\AdminPurviewModel();
        $purviews = $PurviewObj->getNotPurviewStr(session('admin_group_id'));

        //判断权限
        $url = CONTROLLER_NAME . '/' . ACTION_NAME;
        if ($PurviewObj->checkByUrl($url, $purviews)) {
            $this->error('无权限！');
        }
    }

    //处理其他参数（获得内容，赋值到模板，整理条件数组）
    protected function paramValue(&$conditions, $param = array(), $method = 'GET') {
        if (!empty($param)) {
            foreach ($param as $key => $p) {
                if (is_array($p)) {
                    $temp = ($method == 'GET') ? I('get.' . $p['name']) : I('post.' . $p['name']);
                    $temp = iconvstr($temp);
                    $this->assign($p['name'], $temp);
                    if (!empty($temp)) {
                        switch ($p['type']) {
                            case 'string':
                                $conditions[$key] = $temp;
                                break;
                            case 'int':
                                $conditions[$key] = (int) $temp;
                                break;
                            case 'float':
                                $conditions[$key] = (float) $temp;
                                break;
                            default :
                                $conditions[$key] = $temp;
                                break;
                        }
                    }
                } else {
                    $temp = ($method == 'GET') ? I('get.' . $p) : I('post.' . $p);
                    $temp = iconvstr($temp);
                    $this->assign($p, $temp);
                    if (!empty($temp)) {
                        $conditions[$key] = $temp;
                    }
                }
            }
        }
    }

}

?>