<?php
namespace Common\Controller;
use Fwadmin\Model\ConfigModel;
use Fwadmin\Model\SeoPageModel;
use Think\Controller;
class PhoneController extends Controller {

    private $appid = 'wx93ac7736b1e564e1';
    private $serverUrl = 'http://www.7jbbk.com';
    private $appSecret = '040f4f1868f8d36426a8b750998cf9dc';

    function __construct() {
        parent::__construct();

        if (is_weixin()){
            $openid = session('openid');
            if (empty($openid)) {
                $this->getOpenidInfo();
            }
        }

        $url_action =  CONTROLLER_NAME.'/'.ACTION_NAME;
        //获取配置参数
        $config = S('config');
        if (empty($config)) {
            $ConfigObj = new ConfigModel;
            $config = $ConfigObj->getAll();
            S('config', $config);
        }
        $this->config = $config;
		
		//页面SEO自定义
        $seo = S('seo');
        if (empty($seo)) {
            $SeoObj = new SeoPageModel;
            $seo = $SeoObj->getAll();
            S('seo', $seo);
        }
		
        $seo = $seo['Index/index'];
	
		$config['title'] = $seo['title'];
		$config['keywords'] = $seo['keywords'];
		$config['description]'] = $seo['description]'];
		
        $this->assign('config', $config);

      
        //获取所有类别
        $all_cat = S('all_cat');
        if (empty($all_cat)) {
            $ModelGoodsCat = new \Phone\Model\GoodsCatModel();
            $all_cat = $ModelGoodsCat->show_list(0);
            S('all_cat', $all_cat);
        }
	
        $this->all_cat = $all_cat;
	     //print_r($all_cat);
        
        //获取导航菜单
        $menu_cat = S('menu_cat');
        if (empty($menu_cat)) {
            $ModelGoodsCat = new \Phone\Model\GoodsCatModel();
            $menu_cat = $ModelGoodsCat->menu_list();
            S('menu_cat', $menu_cat);
        }
        $this->menu_cat = $menu_cat;
		
        
        //顶部搜索
        $hotsearch = S('hotsearch');
        if (empty($hotsearch)) {
            $HotsearchObj = new \Fwadmin\Model\HotsearchModel();
            $hotsearch = $HotsearchObj->field('title,url,color')->where('is_show=1')->order('order_id asc')->select();
            S('hotsearch', $hotsearch);
        }
        $this->assign('hotsearch', $hotsearch);
        
        //底部服务图标
        $config_tubiao = S('config_tubiao');
        if (empty($config_tubiao)) {
            $ConfigTubiaoObj = new \Fwadmin\Model\ConfigTubiaoModel();
            $config_tubiao = $ConfigTubiaoObj->field('title,picture,spec')->where('is_show=1')->order('order_id asc')->select();
            S('config_tubiao', $config_tubiao);
        }
        $this->assign('config_tubiao', $config_tubiao);
        
        //底部帮助菜单
        $footlink = S('foot_link');
        if (empty($footlink)) {
            $FootlinkObj = new \Phone\Model\ConFootlinkModel();
            $footlink = $FootlinkObj->getList();
            S('foot_link', $footlink);
        }
		//print_r($footlink);
		$this->footlink = $footlink;
      
    
        $this->host = get_host();
        $this->theme_path = __ROOT__.'/Public/phone';
		$this->root_path = __ROOT__.'/Phone/';
		/****获得cid****/
		
    }


    // 获取微信code
    public function getOpenidInfo()
    {
        if (isset($_GET['code'])){
            $url = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid='.$this->appid.'&secret='.$this->appSecret.'&code='.$_GET['code'].'&grant_type=authorization_code';
            $result = $this->geturl($url);
            if (isset($result['openid'])) {
                session('openid', $result['openid']);
                $proUrl = session('proUrl');
                return redirect(empty($proUrl) ? '/Phone/Index/index' : $proUrl);
            }
            return false;
        }

        session('proUrl', $_SERVER['REQUEST_URI']);    // 记录上一次请求地址

        $shiftUrl = urlencode($this->serverUrl.'/Phone/Index/index.html');
        $url = 'https://open.weixin.qq.com/connect/oauth2/authorize?appid='.$this->appid.'&redirect_uri='.$shiftUrl.'&response_type=code&scope=snsapi_userinfo&state=1#wechat_redirect';
        echo '<script>window.location.href="'.$url.'"</script>';exit;
    }


    // curl
    public function geturl($url){
        $headerArray = array("Content-type:application/json;","Accept:application/json");
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($url,CURLOPT_HTTPHEADER,$headerArray);
        $output = curl_exec($ch);
        curl_close($ch);
        $output = json_decode($output,true);
        return $output;
    }




}
?>