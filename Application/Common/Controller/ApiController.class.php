<?php
namespace Common\Controller;
use Fwadmin\Model\ConfigModel;
use Think\Controller\RestController;
class ApiController extends RestController {
    function __construct() {
        parent::__construct();
        $url_action = 'Api/'.CONTROLLER_NAME.'/'.ACTION_NAME;
        //获取配置参数
        $config = S('config');
        if (empty($config)) {
            $ConfigObj = new ConfigModel;
            $config = $ConfigObj->getAll();
            S('config', $config);
        }
        $this->config = $config;
        $this->assign('config', $config);
        
        $this->theme_path = __ROOT__.'/Public/phone';
    }
    
    /**
     * 验证App Key
     * @param type $apptoken
     * @return boolean
     */
    protected function checkAppToken($token,$timestamp){
       return true;//测试期间暂不验证
        $token2 = md5($timestamp+C('APP_KEY'));
        if($token2 == $token){
            return true;
        }else{
            $data['code'] = '400';
            $data['msg'] = '密钥APP_KEY无效';
            $data['data'] = null;
            $this -> response($data, 'json');
            exit();
        }
    }
    
     // 验证 用户 Key
    protected function checkUserToken($userkey){
       return true;
    }
}
?>