<?php

return array(
    'MODULE_ALLOW_LIST' => array('Home', 'Phone', 'Fwadmin','Api', 'Inter', 'Inters'),
    'DEFAULT_MODULE' => 'Home', //默认模块
    'URL_MODEL' => '2', //URL模式 普通模式 0 PATHINFO模式 1 REWRITE模式 2 兼容模式 3 
    'SESSION_AUTO_START' => true, //是否开启session
    'DB_TYPE' => 'mysql', // 数据库类型
	/*'DB_HOST' => 'localhost', // 服务器地址
    'DB_NAME' => 'qijinshop', // 数据库名
    'DB_USER' => 'admin', // 用户名
    'DB_PWD' => 'admin123', // 密码
	*/
    'DB_HOST' => '203.195.236.176', // 服务器地址
    'DB_NAME' => 'demo', // 数据库名
    'DB_USER' => 'demo', // 用户名
    'DB_PWD' => 'HT7YGaWBf3bwiYcy', // 密码
	
    'DB_PORT' => '3306', // 端口
    'DB_PREFIX' => 'shop_' ,   // 数据库表前缀
	'WEB_URL'=>'http://www.7jbbk.com',

    'CUSTOMER' => 'http://cdn.7jbbk.com/Public/chat.html',   // 客服地址
   
   //支付宝参数配置
    'alipay_config' => array(
            'partner' => '2088431878352869',   //这里是你在成功申请支付宝接口后获取到的PID；
            'key' => 'hou245zmbhdhoo50r34mst66dxj49w49',//这里是你在成功申请支付宝接口后获取到的Key
            'seller_email' => 'qjzb777@sina.com',//这里是卖家的支付宝账号，也就是你申请接口时注册的支付宝账号
            'sign_type' =>strtoupper('MD5'),////签名方式 不需修改
            'input_charset' => strtolower('utf-8'),////字符编码格式 目前支持 gbk 或 utf-8
            'cacert' => getcwd().'\\cacert.pem',//ca证书路径地址，用于curl中ssl校验,请保证cacert.pem文件在当前文件夹目录中
            'transport' => 'http',//访问模式,根据自己的服务器是否支持ssl访问，若支持请选择https；若不支持请选择http
            'notify_url' => get_host().'/PayAlipay/notifyurl', //这里是异步通知页面url，提交到项目的Pay控制器的notifyurl方法；
			'notify_url2'=> get_host().'/Phone/PayAlipay/notifyurl', 
            'return_url' => get_host().'/PayAlipay/returnurl',//这里是页面跳转通知url，提交到项目的Pay控制器的returnurl方法；
			'return_url2' => get_host().'/Phone/PayAlipay/returnurl',//这里是页面跳转通知url，提交到项目的Pay控制器的returnurl方法；
            'successpage' => get_host().'/Member/myorder?pay=1',  //支付成功跳转到的页面，我这里跳转到项目的User控制器，myorder方法，并传参payed（已支付列表）,
			'successpage2' => get_host().'/Phone/Member/myorder?pay=1',  //手机支付成功跳转到的页面，我这里跳转到项目的User控制器，myorder方法，并传参payed（已支付列表）  
            'errorpage' => get_host().'/Member/myorder?pay=1',  //支付失败跳转到的页面，我这里跳转到项目的User控制器，myorder方法，并传参unpay（未支付列表）
			'errorpage2' => get_host().'/Phone/Member/myorder?pay=1',  //手机支付失败跳转到的页面，我这里跳转到项目的User控制器，myorder方法，并传参unpay（未支付列表）
        ),

   //微信参数配置
	    'weixin_config' => array(
            'APPID' => 'wx93ac7736b1e564e1',   //绑定支付的APPID；
            'MCHID' => '1523223211',//商户号
            'KEY' => 'aasd123qkzf812312451F1asdasdasda',//商户支付密钥
            'APPSECRET' =>'040f4f1868f8d36426a8b750998cf9dc',//// 公众帐号secert
            'notify_url' => get_host().'/PayWechat/wxnotify', //这里是PC异步通知页面url，提交到项目的Pay控制器的notifyurl方法；
			'notify_url2'=> get_host().'/Phone/PayWechat/wxnotify_h5', //这里是H5异步通知页面url，提交到项目的Pay控制器的notifyurl方法；
		    'successpage' => get_host().'/Member/myorder?pay=1',  //支付成功跳转到的页面，我这里跳转到项目的User控制器，myorder方法，并传参payed（已支付列表）,
			'successpage2' => get_host().'/Phone/Member/myorder?pay=1',  //手机支付成功跳转到的页面，我这里跳转到项目的User控制器，myorder方法，并传参payed（已支付列表）  
        ),
);
