<?php

namespace Inter\Controller;

use Inter\Model\MemberModel;
use Inter\Tool\Request;
use Inter\Tool\VerificationPhone;
use Phone\Service\QcloudsmsService;

class LoginController extends BaseController
{

    /**
     *  短信验证 : POST 提交: secret, code
     *  快捷登陆 : POST 提交: phone
     *  账号登录 : POST 提交: phone,password
     *  授权登录 : POST 提交: code, 相关用户信息
     *  登录
     */
    public function login()
    {
        if (I('post.code', false)) {
            $this->authorizationLogin();
        } else {
            if(I('post.password', false)){
                $this->accountLogin();
            } else {
                $this->phoneLogin();
            }
        }
    }


    /**
     *  授权登录
     */
    protected function authorizationLogin()
    {
        $code = I('post.code');
        $data = Request::Get(getOpenidUrl($code));
        if (!isset($data['openid'])) {
            echo $this->_returnJson(array(), 300, '获取openid失败'); exit;
        }

        $mm = new MemberModel();
        $user = $mm->where(array('login_weixin' => $data['openid']))->find();
        if (!empty($user)) {
            echo $this->pushToken($data['member_id']); exit;
        }

        $add = array(
            'username' => I('post.nickName'),
            'login_weixin' => $data['openid'],
            'create_time' => time(),
            'headpic' => I('post.avatarUrl'),
        );
        $mm->create($add);

        echo $this->pushToken($mm->add());
    }


    /**
     *  账号登录
     */
    protected function accountLogin()
    {
        $mm = new MemberModel();
        $phone = I('post.phone');
        $password = I('post.password');

        $data = $mm->where(array('phone' => $phone))->find();
        if (empty($data)) {
            echo $this->_returnJson(array(), 300, '查无次账号'); exit;
        }

        if ($data['password'] != strongmd5($password)) {
            echo $this->_returnJson(array(), 300, '密码错误'); exit;
        }

        if ($data['status'] == 2 || $data['status'] == 3) {
            echo $this->_returnJson(array(), 300, '账号已禁用'); exit;
        }

        echo $this->pushToken($data['member_id']);
    }


    /**
     *  快捷登录
     */
    public function phoneLogin()
    {
        $mm = new MemberModel();
        $phone = I('post.phone');
        $data = $mm->where(array('phone' => $phone))->find();
        if (empty($data)) {
            echo $this->_returnJson(array(), 300, '查无次账号'); exit;
        }
        if ($data['status'] == 2 || $data['status'] == 3) {
            echo $this->_returnJson(array(), 300, '账号已禁用'); exit;
        }
        $phoneData = VerificationPhone::createCheck($phone);

        $cloud  = new  QcloudsmsService();
        $result = $cloud->sendMsg($phone, $phoneData['code']);
        if ($result['result'] != 0) {
            echo $this->_returnJson(array(), 300, '服务器繁忙！请稍后再试'); exit;
        }

        echo $this->_returnJson(array('secret' => $phoneData['secret']), 200, '成功'); exit;
    }


    /**
     *  校验手机号
     */
    public function verificationCode()
    {
        $code   = I('post.code');
        $secret = I('post.secret');

        $phone = VerificationPhone::getCheck($code, $secret);
        if ($phone == false) {
            echo $this->_returnJson(array(), 300, '验证码有误'); exit;
        }

        $mm = new MemberModel();
        $id = $mm->where(array('phone' => $phone))->getField('member_id');

        echo $this->pushToken($id);
    }



    /**
     * 生成token
     * @param $member_id
     * @return false|string
     */
    private function pushToken($member_id)
    {
        $token = array(
            'token' => $this->create_token($member_id)
        );
        return $this->_returnJson($token, 200, '成功') ;
    }
}
