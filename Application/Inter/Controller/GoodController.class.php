<?php

namespace Inter\Controller;

use Inter\Model\GoodsModel;
use Inter\Model\GoodsCommentModel;

class GoodController extends BaseController
{
    // cid: 分类ID  type:1 综合 => 根据销量排序   2:新品 => 根据推荐排序  3: 价格高 => 根据售价 4: 价格低 => 根据售价
    public function goodTreeList()
    {
        $gm = new GoodsModel();
        $data = $gm->GoodList(I('get.cid'), I('get.type'));
        echo $this->_returnJson($data, 200,'成功');
    }

    /**
     *  good_id 商品id
     *  商品详情
     */
    public function GoodInfo()
    {
        $id = I('get.good_id');
        $gm = new GoodsModel();
        $gcm = new GoodsCommentModel();

        $data = $gm->field('goods_id,goods_name,cuxiao_msg,price,picture_list,count_sale,ex_share,detail,store_num')->find($id);

        $data['picture_list'] = goodPictureStrToArr($data['picture_list']);
        $data['detail'] = goodDetailReplace($data['detail']);
        $data['comment_count'] = $gcm->count_goods($data['goods_id']);
        $data['comment_avg'] = ($gcm->comment_star($data['goods_id']) * 20).'%';

        echo $this->_returnJson($data, 200,'成功');
    }

    /**
     *  good_id 商品id
     *  商品评价列表
     */
    public function GoodCommentCount()
    {
        $id = I('get.good_id');
        $gcm = new GoodsCommentModel();

        $data['high'] = $gcm->count_star_45($id);
        $data['in'] = $gcm->count_star_3($id);
        $data['low'] = $gcm->count_star_12($id);

        echo $this->_returnJson($data, 200,'成功');

    }


    /**
     *  good_id 商品id   type 1:全部  2:最新  3:好评 4:  中评  5:差评
     *  商品评价
     */
    public function GoodComment()
    {
        $id = I('get.good_id');
        $type = I('get.type');

        $gcm = new GoodsCommentModel();
        $data = $gcm->good_comment_list($id, $type);

        $data['data'] = spellUrl($data['data'], 'headpic');
        $data['data'] = formatTime($data['data'], 'create_time');
        $data['data'] = hiddenUsername($data['data'],'username');

        echo $this->_returnJson($data, 200,'成功');
    }
}