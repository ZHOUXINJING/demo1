<?php

namespace Inter\Controller;

use Inter\Model\GoodsCatModel;
use Inter\Model\GoodsModel;

class TreeController extends BaseController
{
    //    private $auth = array('index');

    private $auth = array();

    // 鉴权
    public function __construct()
    {
        parent::__construct();
        if(in_array(ACTION_NAME, $this->auth)) {
            $bool = $this->requestAuth();
            if (!$bool){
                echo $this->_returnJson(array(),400,'授权错误'); exit;
            }
            $this->member_id = $bool;
        }
    }

    /**
     *  分类列表
     */
    public function index()
    {
        $gcm  = new GoodsCatModel();
        $data = $gcm->show_list();
        echo $this->_returnJson(ergodicTree($data),200,'成功');
    }


}