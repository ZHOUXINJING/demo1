<?php

namespace Inter\Tool;

class VerificationPhone
{
    private static $expire = 300;

    /**
     * 生成手机验证
     * @param $phone
     * @return array
     */
    public static function createCheck($phone)
    {
        $code   = self::__createCode();
        $secret = self::__createSecret();

        S($secret, Array('phone' => $phone, 'code' => $code), self::$expire);

        return Array('code' => $code, 'secret' => $secret, 'expire' => (300/60));
    }


    /**
     * 返回手机验证
     * @param $code
     * @param $secret
     * @return bool
     */
    public static function getCheck($code, $secret)
    {
        $data = S($secret);
        if (empty($data)) {
            return false;
        }
        if ($data['code'] != $code) {
            return false;
        }
        S($secret, NULL);
        return $data['phone'];
    }



    // 生成code
    private static function __createCode($length = 6)
    {
        $code = '';
        for ($i = 0 ; $i < $length; $i++) {
            $code .= rand(0, 9);
        }
        return $code;
    }
    // 生成密钥
    private static function __createSecret()
    {
        return md5(rand(1000, 9999));
    }

}