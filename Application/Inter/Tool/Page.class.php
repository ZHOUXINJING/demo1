<?php

namespace Inter\Tool;

class Page
{
    // 分页
    public static function getPages($count  , $limit)
    {
        $page = I('get.page', 1);
        $count_pages   = ceil($count/$limit);
        $current_pages = $page;
        return array('count_pages' =>$count_pages, 'current_pages' => $current_pages);
    }
}