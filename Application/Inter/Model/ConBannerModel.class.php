<?php

namespace Inter\Model;

use Think\Model;

class ConBannerModel extends Model {

    /**
     * 获取banner
     * @param string $type 切换类别
     */
    public function get_list($type) {
        return $this->field('title as type,picture,url')->where(array('type'=>$type,'is_show'=>1))->order('order_id asc,id asc')->select();
    }



}
