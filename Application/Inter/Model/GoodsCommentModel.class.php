<?php

namespace Inter\Model;

use Think\Model;
use Inter\Tool\Page;

class GoodsCommentModel extends Model
{
    private $limit = 6;

    /**
     * 商品评价数
     * @param int $goods_id 商品ID
     * @return int 数量
     */
    public function count_goods($goods_id) {
        return $this->where(array('goods_id'=>$goods_id,'status'=>2))->count();
    }

    /**
     * 计算商品平均星级
     * @param int $goods_id 商品ID
     * @return int 星级
     */
    public function comment_star($goods_id) {
        $star = $this->where(array('goods_id'=>$goods_id,'status'=>2))->avg('star');
        return floor($star);
    }

    /**
     * 计算商品差评数
     * @param int $goods_id 商品ID
     * @return int 数量
     */
    public function count_star_12($goods_id) {
        return $this->where(array('goods_id'=>$goods_id,'star'=>array('in','1,2'),'status'=>2))->count();
    }

    /**
     * 计算商品中评数
     * @param int $goods_id 商品ID
     * @return int 数量
     */
    public function count_star_3($goods_id) {
        return $this->where(array('goods_id'=>$goods_id,'star'=>3,'status'=>2))->count();
    }

    /**
     * 计算商品好评数
     * @param int $goods_id 商品ID
     * @return int 数量
     */
    public function count_star_45($goods_id) {
        return $this->where(array('goods_id'=>$goods_id,'star'=>array('in','4,5'),'status'=>2))->count();
    }

    /**
     * 商品评价列表
     * @param $id
     * @param $type
     */
    public function good_comment_list($id, $type)
    {
        $map['shop_goods_comment.goods_id'] = $id;
        $map['shop_goods_comment.status'] = 2;

        switch ($type) {
            case 3:
                $map['shop_goods_comment.star'] = array('in','4,5');
                break;
            case 4:
                $map['shop_goods_comment.star'] = 3;
                break;
            case 5:
                $map['shop_goods_comment.star'] = array('in','1,2');
                break;
        }

        $count = $this->where($map)->count();
        $page_arr = Page::getPages($count, $this->limit);

        $list = $this->where($map)
                     ->join('shop_member ON shop_member.member_id = shop_goods_comment.member_id')
                     ->field('shop_goods_comment.detail,shop_goods_comment.count_up,shop_goods_comment.create_time,
                     shop_goods_comment.member_id,shop_member.username,shop_member.headpic')
                     ->order('shop_goods_comment.create_time desc')
                     ->limit($this->limit)
                     ->page($page_arr['current_pages'])
                     ->select();

        $data['data']          = empty($list) ? array() : $list;
        $data['count_pages']   = $page_arr['count_pages'];
        $data['current_pages'] = $page_arr['current_pages'];

        return $data;
    }
}