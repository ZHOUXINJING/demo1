<?php

namespace Inter\Model;

use Think\Model;
use Inter\Tool\Page;

class GoodsModel extends Model
{
    private $limit = 6;

    private $map = array();

    /**
     *  新品
     * @return mixed
     */
    public function newList()
    {
        return $this->where(array('is_index'=>1, 'status' => 1))
                    ->order('order_id asc')
                    ->field('goods_id, picture, goods_name, price')
                    ->limit(10)
                    ->select();
    }

    /**
     * 分类商品
     * @param $tree_id
     * @return mixed
     */
    public function treeList($tree_id)
    {
        return $this->where(array('is_hot'=>1, 'status' => 1, 'cat_ids' => ','.$tree_id.','))
            ->order('order_id asc')
            ->field('goods_id, picture, goods_name, price')
            ->limit(10)
            ->select();
    }

    /**
     * 搜索
     * @param $value
     * @return mixed
     */
    public function searchGood($value)
    {
        $map['goods_name']  = array('like','%'.$value.'%');
        $map['status']  = array('eq', 1);

        return $this->where($map)
                    ->field('goods_id, goods_name, price')
                    ->limit(10)
                    ->select();

    }

    /**
     *  分类下商品
     */
    public function GoodList($cid, $type)
    {
        $this->_map($cid, $type);

        $count = $this->where($this->map)->count();
        $page_arr = Page::getPages($count, $this->limit);

        $good  =  $this->_screen($type)
                      ->field('goods_id, picture, goods_name, price')
                      ->limit($this->limit)
                      ->page($page_arr['current_pages'])
                      ->select();

        $data['data']          = empty($good) ? array() : $good;
        $data['count_pages']   = $page_arr['count_pages'];
        $data['current_pages'] = $page_arr['current_pages'];

        return $data;
    }

    // 筛选
    public function _screen($type)
    {
        switch ($type) {
            case 1:
                return $this->where($this->map)->order('count_sale desc');
            case 2:
                return $this->where($this->map)->order('order_id asc');
            case 3:
                return $this->where($this->map)->order('price desc');
            case 4:
                return $this->where($this->map)->order('price asc');
        }
    }

    public function _map($cid ,$type)
    {
        $this->map['cat_ids'] =  ','.$cid.',';
        $this->map['status']  = 1;
        switch ($type) {
            case 1:
                break;
            case 2:
                $map['is_index'] = 1;
                break;
            case 3:
                break;
            case 4:
                break;
        }
    }

}