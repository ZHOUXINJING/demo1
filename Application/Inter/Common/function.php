<?php

/**
 * 拼接图片地址
 * @param $url
 * @param $filed
 * @return mixed
 */
function spellUrl($url, $filed) {
    $host = C('HOST');
    foreach ($url as &$v) {
        if (is_array($v[$filed])) {
            foreach ($v[$filed] as $k1=> $v1) {
                $v[$filed][$k1] = $host . $v1;
            }
        } else {
            $v[$filed] = $host . $v[$filed];
        }
    }
    return $url;
}

/**
 * 获取openid地址
 * @param $request
 * @return string
 */
function getOpenidUrl($code) {

    return  'https://api.weixin.qq.com/sns/jscode2session?appid='.C('APPID').
        '&secret='.C('SECRET').'&js_code='.$code.'&grant_type=authorization_code';
}


/**
 * 分类列表
 * @param $data
 * @return array
 */
function ergodicTree($data)
{
    $arr = array();
    foreach ($data as $k => $v) {
        $sub = array();
        foreach ($v['sub'] as $v1) {
            $v1['ico'] = C('HOST'). $v1['ico'];
            $sub[] = $v1;
        }
        $v['sub'] = $sub;
        $arr[] = $v;
    }
    return $arr;
}

/**
 *  商品图片拼接域名
 */
function goodPictureStrToArr($picture_list)
{
    $data = explode(',', $picture_list);
    $arr  = array();
    foreach ($data as $v) {
        $arr[] = C('HOST') . $v;
    }
    return $arr;
}

/**
 * 商品详情替换
 * @param $detail
 * @return string
 */
function goodDetailReplace($detail)
{
    $data = htmlspecialchars_decode($detail);
    $pattern = '/<img src="(.*)" alt="" \/>/';
    $replacement = '<img src="'.C('HOST').'${1}" alt="" />';
    return htmlspecialchars(preg_replace($pattern, $replacement, $data));
}

/**
 * 格式化时间
 * @param $time
 */
function formatTime($data, $filed)
{
    foreach ($data as $k => $v) {
        $data[$k][$filed] = Date('Y-m-d', $v[$filed]);
    }
    return $data;
}

/**
 *  隐藏用户名
 */
function hiddenUsername($data, $filed)
{
    foreach ($data as $k => $v) {
        $data[$k][$filed] = mb_substr($v[$filed], 1, 1).'***'.mb_substr($v[$filed], -1, 1);
    }
    return $data;
}
