<?php
namespace Fwadmin\Service;
/**
 * phpemail邮件发送服务类，下载的phpmail把src时的文件放入指定文件夹，同时修改去除PHPMailer.php和SMTP.php时的命名空间即可
 * 库下载地址：https://github.com/PHPMailer/PHPMailer
 * Class SendemailService
 * @package Fwadmin\Service
 */
class KuaidiService {
    function __construct() {
       // $fwconfig = D('Config')->getAll();
        $config = array(
            'EBusinessID' => '1259822',//控制台查看
            'AppKey' => '76bac43c-85f5-443e-928d-4dc5b888fd9c',//控制台查看
            'ReqURL' => 'http://api.kdniao.com/Ebusiness/EbusinessOrderHandle.aspx'
        );
        $this->config = $config;
    }

   
/**
 * Json方式 查询订单物流轨迹
 */
public function getOrderTracesByJson($OrderCode,$ShipperCode,$LogisticCode){
	
	$config = $this->config;
	
	$requestData= "{'OrderCode':'$OrderCode','ShipperCode':'$ShipperCode','LogisticCode':'$LogisticCode'}";
	
	$datas = array(
        'EBusinessID' => $config['EBusinessID'],
        'RequestType' => '1002',
        'RequestData' => urlencode($requestData) ,
        'DataType' => '2',
    );
    $datas['DataSign'] = $this->encrypt($requestData, $config['AppKey']);
	$result= $this->sendPost($config['ReqURL'], $datas);	
	
	//根据公司业务处理返回的信息......
	
	return $result;
}
 
/**
 *  post提交数据 
 * @param  string $url 请求Url
 * @param  array $datas 提交的数据 
 * @return url响应返回的html
 */
 public function sendPost($url, $datas) {
    $temps = array();	
    foreach ($datas as $key => $value) {
        $temps[] = sprintf('%s=%s', $key, $value);		
    }	
    $post_data = implode('&', $temps);
    $url_info = parse_url($url);
	if(empty($url_info['port']))
	{
		$url_info['port']=80;	
	}
    $httpheader = "POST " . $url_info['path'] . " HTTP/1.0\r\n";
    $httpheader.= "Host:" . $url_info['host'] . "\r\n";
    $httpheader.= "Content-Type:application/x-www-form-urlencoded\r\n";
    $httpheader.= "Content-Length:" . strlen($post_data) . "\r\n";
    $httpheader.= "Connection:close\r\n\r\n";
    $httpheader.= $post_data;
    $fd = fsockopen($url_info['host'], $url_info['port']);
    fwrite($fd, $httpheader);
    $gets = "";
	$headerFlag = true;
	while (!feof($fd)) {
		if (($header = @fgets($fd)) && ($header == "\r\n" || $header == "\n")) {
			break;
		}
	}
    while (!feof($fd)) {
		$gets.= fread($fd, 128);
    }
    fclose($fd);  
    
    return $gets;
}

/**
 * 电商Sign签名生成
 * @param data 内容   
 * @param appkey Appkey
 * @return DataSign签名
 */
public function encrypt($data, $appkey) {
	
    return urlencode(base64_encode(md5($data.$appkey)));
}

}