<?php

return array(

       'TMPL_PARSE_STRING' => array(
        '__LIB__' => __ROOT__ . '/Public/' . MODULE_NAME . '/lib',
        '__IMG__' => __ROOT__ . '/Public/' . MODULE_NAME . '/images',
        '__CSS__' => __ROOT__ . '/Public/' . MODULE_NAME . '/css',
        '__JS__' => __ROOT__ . '/Public/' . MODULE_NAME . '/js',
    ),
    
    'LAYOUT_ON' => true,
    'LAYOUT_NAME' => 'layout',
    'TMPL_ACTION_ERROR' => 'Public/msg', // 默认错误跳转对应的模板文件
    'TMPL_ACTION_SUCCESS' => 'Public/msg', // 默认错误跳转对应的模板文件
    'PAGE_NUM' => 15 //默认每页显示记录数
);
