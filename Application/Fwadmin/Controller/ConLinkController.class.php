<?php

namespace Fwadmin\Controller;

use Common\Controller\FwadminController;
use Fwadmin\Model\ConLinkModel;

class ConLinkController extends FwadminController {

    /*
     * 友情链接列表
     */
    public function index() {
        //获得数据       
        $ModelObj = new ConLinkModel();
        if (IS_AJAX) {
            $t = I('get.t');
            $v = I('get.v');
            $id = I('get.i');
            echo $ModelObj->where('id=' . $id)->setField($t, $v);
            exit();
        }
        $results = $ModelObj->search($conditions);
        $lists = $results['list'];
        $this->assign('list', $lists); // 赋值数据集
        $this->assign('page', $results['page']); // 赋值分页输出
        $this->display();
    }

    /*
     * 友情链接添加
     */
    public function add() {
        $ModelObj = new ConLinkModel();
        if (IS_POST) {
            if ($ModelObj->create()) {
                $id = $ModelObj->add();
                if ($id > 0)
                    $this->success('添加成功', U('ConLink/index'));
                else
                    $this->error($ModelObj->getError());
            } else {
                $this->error($ModelObj->getError());
            }
        } else {
            $this->display();
        }
    }

    /*
     * 友情链接编辑
     */
    public function edit($id) {
        $ModelObj = new ConLinkModel();
        if (IS_POST) {
            if ($ModelObj->create()) {
                $flag = $ModelObj->save();
                if ($flag !== false)
                    $this->success('修改成功', U('ConLink/index'));
                else
                    $this->error($ModelObj->getError());
            }
        } else {
            $model = $ModelObj->find($id);
            $this->model = $model;
            $this->display();
        }
    }

    /*
     * 友情链接删除
     */
    public function del($id) {
        $ModelObj = new ConLinkModel();
        $flag = $ModelObj->delete($id);
        if ($flag > 0)
            $this->success('删除成功');
        else
            $this->error($ModelObj->getError());
    }

}
