<?php

namespace Fwadmin\Controller;

use Common\Controller\FwadminController;
use Fwadmin\Model\GoodsCatModel;

class GoodsCatController extends FwadminController {

    /**
     * 类别列表
     */
    public function index() {
        //获得数据       
        $ModelObj = new GoodsCatModel();
        if (IS_AJAX) {
            $t = I('get.t');
            $v = I('get.v');
            $id = I('get.i');
            echo $ModelObj->where('cat_id=' . $id)->setField($t, $v);
            exit();
        }
        $conditions = array();
        $param = array('parent_id'=>'parent_id');
        $this->paramValue($conditions, $param);
        
        $list = $ModelObj->getSubList($this->parent_id,4,'|--', Array());
        $this->search_list = $ModelObj->getSubSelect(0, 4);
        $this->assign('list', $list); // 赋值数据集
        $this->display();
    }

    public function add() {
        $ModelObj = new GoodsCatModel();
        if (IS_POST) {
            if ($ModelObj->create()) {
                $ModelObj->path_id = $ModelObj->getPathId($ModelObj->parent_id);
                $ModelObj->cat_level = $ModelObj->getLevel($ModelObj->parent_id) + 1;
                $id = $ModelObj->add();
                if ($id > 0)
                    $this->success('添加成功', U('index'));
                else
                    $this->error($ModelObj->getLastSql());
            } else {
                $this->error($ModelObj->getError());
            }
        } else {
            $this->parentList = $ModelObj->getSubSelect(0, 4);
            $this->parent_id = I('get.parent_id',0);
            $this->display();
        }
    }

    public function edit($cat_id) {
        $ModelObj = new GoodsCatModel();
        if (IS_POST) {
            if ($ModelObj->create()) {
                $ModelObj->path_id = $ModelObj->getPathId($ModelObj->parent_id);
                $ModelObj->cat_level = $ModelObj->getLevel($ModelObj->parent_id) + 1;
                $flag = $ModelObj->save();
                if ($flag !== false)
                    $this->success('修改成功', U('index'));
                else
                    $this->error($ModelObj->getLastSql());
            } else {
                $this->error($ModelObj->getError());
            }
        } else {
            $model = $ModelObj->find($cat_id);
            $this->model = $model;
            $this->parentList = $ModelObj->getSubSelect(0, 3, $cat_id);
            $this->display();
        }
    }

    public function del($cat_id) {
        $ModelObj = new GoodsCatModel();
		$arr = $ModelObj->getSub2($cat_id);
		if(!empty($arr)){
		 $this->error('请先删除子类');
		}
		 $goods_count2 = M('goods')->where(Array('status'=>array('neq',3),'cat_path_ids'=>array('like','%,'.$cat_id.',%')))->count();
		if($goods_count2>0){
		 $this->error('请先删除类别下的商品');
		}
		
        $flag = $ModelObj->del($cat_id);
        if ($flag > 0)
            $this->success('删除成功');
        else
            $this->error($ModelObj->getError());
	
    }
	
	

	
	/****广告图片******/
	 public function adv_pic($cat_id) {
       $ModelObj = new ConBannerModel();
        if (IS_AJAX) {
            $t = I('get.t');
            $v = I('get.v');
            $id = I('get.i');
            echo $ModelObj->where('id=' . $id)->setField($t, $v);
            exit();
        }
        //获得数据
        $conditions['type'] = 'index';
        $results = $ModelObj->search($conditions);
        $lists = $results['list'];
        $this->assign('list', $lists); // 赋值数据集
        $this->assign('page', $results['page']); // 赋值分页输出
        $this->display();
    }

}

?>