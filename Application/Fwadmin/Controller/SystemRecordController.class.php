<?php

namespace Fwadmin\Controller;

use Fwadmin\Model\SystemRecordModel;
use Common\Controller\FwadminController;

class SystemRecordController extends FwadminController {

    public function index() {
        $ModelObj = new SystemRecordModel();
        //获得数据       
        $results = $ModelObj->search();
        $lists = $results['list'];
        $AdminObj = M('admin');
        foreach ($lists as $k => $v) {
            $lists[$k]['username'] = $AdminObj->getFieldByAdminId($v['admin_id'],'username');
        }
        $this->assign('list', $lists); // 赋值数据集
        $this->assign('page', $results['pageShow']); // 赋值分页输出
        $this->display();
    }

}
