<?php

namespace Fwadmin\Controller;

use Common\Controller\FwadminController;
use Fwadmin\Model\ConfigEmailModel;
//发送系统邮箱配置
class ConfigEmailController extends FwadminController {

    /*
     * 列表
     */
    public function index() {
        //获得数据       
        $ModelObj = new ConfigEmailModel();
        if (IS_AJAX) {
            $t = I('get.t');
            $v = I('get.v');
            $id = I('get.i');
            echo $ModelObj->where('id=' . $id)->setField($t, $v);
            exit();
        }
        $conditions = array();
        $result = $ModelObj->search($conditions);
        $this->assign('list', $result['list']); // 赋值数据集
        $this->assign('page', $result['page']);
        $this->display();
    }

    /*
     * 添加
     */
    public function add() {
        $ModelObj = new ConfigEmailModel();
        if (IS_POST) {
            if ($ModelObj->create()) {
                $ModelObj->password = encrypt($ModelObj->password);
                if(empty($ModelObj->ssl)) {
                    $ModelObj->ssl = 0;
                }
                $id = $ModelObj->add();
                if ($id > 0)
                    $this->success('添加成功', U('ConfigEmail/index'));
                else
                    $this->error($ModelObj->getLastSql());
            } else {
                $this->error($ModelObj->getError());
            }
        } else {
            $this->display();
        }
    }

    /*
     * 编辑
     */
    public function edit($id) {
        $ModelObj = new ConfigEmailModel();
        if (IS_POST) {
            if ($ModelObj->create()) {
                if(empty($ModelObj->ssl)) {
                    $ModelObj->ssl = 0;
                }
                if(trim(I('post.password2')) != '') {
                    $ModelObj->password = encrypt(trim(I('post.password2')));
                }
                $flag = $ModelObj->save();
                if ($flag !== false)
                    $this->success('修改成功', U('ConfigEmail/index'));
                else
                    $this->error($ModelObj->getLastSql());
            } else {
                $this->error($ModelObj->getError());
            }
        } else {
            $model = $ModelObj->find($id);
            $model['password'] = decrypt($model['password']);
            $this->model = $model; 
            $this->display();
        }
    }

    /*
     * 删除
     */
    public function del($id) {
        $ModelObj = new ConfigEmailModel();
        $flag = $ModelObj->del($id);
        if ($flag > 0)
            $this->success('删除成功');
        else
            $this->error($ModelObj->getError());
    }
    
    /*
     * 发送测试邮箱
     */
    public function sendMail() {
        $email = I('post.email');
        if($email != '') {
            $flag = sendMail($email,"测试发送邮件","测试发送邮件",false);
            if($flag) {
                $this->success('发送成功');
            } else {
                $this->success('发送失败'.$flag);
            }
        } else {
            $this->success('请填写邮箱');
        }
    }

}

?>