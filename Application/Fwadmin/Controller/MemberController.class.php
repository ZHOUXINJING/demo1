<?php

namespace Fwadmin\Controller;

use Common\Controller\FwadminController;
use Fwadmin\Model\MemberModel;
use Fwadmin\Model\MemberLevelModel;
use Fwadmin\Model\MemberInfoModel;

class MemberController extends FwadminController {

    /**
     * 会员列表
     */
    public function index() {
        //获得数据       
        $ModelObj = new MemberModel();
        $MemberLevelObj = new MemberLevelModel();
        if (IS_AJAX) {
            $t = I('get.t');
            $v = I('get.v');
            $member_id = I('get.i');
            echo $ModelObj->where('member_id=' . $member_id)->setField($t, $v);
            exit();
        }
        $conditions = array();
        $param = array('phone' => 'phone','level_id'=>'level_id','status'=>'status');
        $this->paramValue($conditions, $param);
        $results = $ModelObj->search($conditions);
        //会员等级数组 键是level_id
        $levels = $MemberLevelObj->getSel();
        $this->level_sel = $levels;

        $lists = $results['list'];
        foreach ($lists as $k => $v) {
            $lists[$k]['level_name'] = $levels[$v['level_id']];
        }
        $this->assign('list', $lists); // 赋值数据集
        $this->assign('page', $results['page']); // 赋值分页输出

        $this->display();
    }

    /**
     * 添加会员
     */
    public function add() {
        $ModelObj = new MemberModel();
        //保存操作
        if (IS_POST) {
            $email = I('post.email', '');
            $phone = I('post.phone', '');
            $username = I('post.username', '');
            $password = I('post.password', '');
            $result['title'] = 'success';
            if ($email == '' && $phone == '' && $username == '') {
                $result['title'] = 'false';
                $result['msg'] = '手机邮箱用户名必须填写一个！';
            } elseif ($email !== '' && !isEmail($email)) {
                $result['title'] = 'false';
                $result['msg'] = '邮箱有误！';
            } elseif ($email !== '' && $ModelObj->existEmail($email, 0) > 0) {
                $result['title'] = 'false';
                $result['msg'] = '此邮箱已被使用！';
            } elseif ($phone !== '' && !is_phone($phone)) {
                $result['title'] = 'false';
                $result['msg'] = '手机有误！';
            } elseif ($phone !== '' && $ModelObj->existPhone($phone, 0) > 0) {
                $result['title'] = 'false';
                $result['msg'] = '此手机已被使用！';
            } elseif ($username !== ''  && !isUsername($username)) {
                $result['title'] = 'false';
                $result['msg'] = '用户名有误！';
            } elseif ($username !== '' && $ModelObj->existUsername($nickname, 0) > 0) {
                $result['title'] = 'false';
                $result['msg'] = '此昵称已被使用！';
            } elseif ($password == '') {
                $result['title'] = 'false';
                $result['msg'] = '请填写密码！';
            } else {
                $member_id = $ModelObj->register( $phone, $username, $password);
                if ($member_id <= 0) {
                    $result['title'] = 'false';
                    $result['msg'] = '系统繁忙，注册失败！';
                }
            }
            if($result['title'] == 'false') {
                $this->error($result['msg']);
            } else {
                $this->success('新增成功');
            }
        } else {
            $this->display();
        }
    }

    /**
     * 修改会员
     */
    public function edit($member_id) {
        $ModelObj = new MemberModel();
        $MemberInfoObj = new MemberInfoModel();
        $MemberLevelObj = new MemberLevelModel();
        //保存操作
        if (IS_POST) {
            if ($MemberInfoObj->create()) {
                $year = I('post.year');
                $month = I('post.month');
                $day = I('post.day');
                if ($year != '' && $month != '' && $day != '') {
                    $MemberInfoObj->birthday = date('Y-m-d', strtotime($year . '-' . $month . '-' . $day));
                }
                $flag = $MemberInfoObj->save();
                if ($flag !== false) {
                    $data['level_id'] = I('post.level_id');
                    $data['status'] = I('post.status');
                    $data['member_id'] = I('post.member_id');
                    if(I('post.email','') !== '') {
                        $data['email'] = I('post.email','');
                    }
                    if(I('post.phone','') !== '') {
                        $data['phone'] = I('post.phone','');
                    }
                    if(I('post.username','') !== '') {
                        $data['username'] = I('post.username','');
                    }
                    $flag2 = $ModelObj->save($data);
                    if ($flag2 !== false) {
                        $this->success('修改成功',U('index'));
                    } else {
                        $this->error('部分内容修改失败');
                    }
                } else {
                    $this->error('修改失败');
                }
            }
        } else {
            //读取会员数据
            $model = $ModelObj->find($member_id);
            $model2 = $MemberInfoObj->find($member_id);

            //会员等级数组 键是level_id
            $levels = $MemberLevelObj->getSel();
            $this->level_sel = $levels;

            $model2['year'] = date('Y', strtotime($model2['birthday']));
            $model2['month'] = date('m', strtotime($model2['birthday']));
            $model2['day'] = date('d', strtotime($model2['birthday']));
            //调取省份
            $ProvinceObj = new \Fwadmin\Model\AddressProvinceModel();
            $this->province = $ProvinceObj->getSel();

            $this->model = $model;
            $this->model2 = $model2;
            $this->display();
        }
    }

    /**
     * 删除会员
     */
    public function del($member_id) {
        $ModelObj = new MemberModel();
        $flag = $ModelObj->del($member_id);
        if ($flag > 0)
            $this->success('删除成功');
        else
            $this->error($ModelObj->getError());
    }

}
