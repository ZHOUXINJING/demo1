<?php

namespace Fwadmin\Controller;

use Common\Controller\FwadminController;

class IndexController extends FwadminController {

    /**
     * 登陆后主界面
     */
    public function index() {
        layout(false);
        $SystemMenu = new \Fwadmin\Model\SystemMenuModel();
        $this->top_menu = $SystemMenu->getMenuTop();
        $this->left_menu = $SystemMenu->getMenuLeft();
        $this->menu = I('get.menu','');
        $this->url = I('get.url','');
        $this->redirect("Index/main");
    }

    /**
     * 管理主界面
     */
    public function main() {
        layout(false);
        $SystemMenu = new \Fwadmin\Model\SystemMenuModel();
        $this->top_menu = $SystemMenu->getMenuTop();
        $this->left_menu = $SystemMenu->getMenuLeft();
        $this->menu = I('get.menu','');
        $this->url = I('get.url','');
        $this->display();
    }
    
    /**
     * 更新缓存
     */
    public function update_cache() {
        $dirs = array('Application/Runtime/Temp/');
        //清理缓存
        foreach ($dirs as $value) {
            rmdirr($value);
        }
        echo '更新缓存成功';
        exit();
    }

    /**
     * 修改密码
     */
    public function changePassword() {
        if (IS_POST) {
            $AdminObj = new \Fwadmin\Model\AdminModel();
            if ($AdminObj->create($_POST, 4)) {
                $admin_id = I('session.admin_id', 0);
                $oldpassword = I('post.oldpassword', '', strongmd5);
                $AdminObj->password = strongmd5($AdminObj->password);
                if ($oldpassword == $AdminObj->password) {
                    $this->error('你输入的新密码和旧密码是一样的');
                }
                $flag = $AdminObj->where(Array('admin_id' => $admin_id, 'password' => $oldpassword))->setField('password', $AdminObj->password);
                if ($flag > 0) {
                    $ARObj = new \Fwadmin\Model\SystemRecordModel();
                    $ARObj->addrecord("修改密码");
                    $this->success('修改密码成功！');
                } else {
                    $this->error('旧密码错误！');
                }
            } else {
                $this->error($AdminObj->getError());
            }
            exit();
        }
        $this->display();
    }

    /**
     * 删除图片
     */
    public function delPic($filename) {
        //防止删除其它文件
        if (strpos($filename, 'Public/upload') !== false) {
            echo delfile($filename);
        } else {
            echo '0';
        }
    }
    
    /**
     * 弹出框上传图片
     */
    public function uploadimgframe() {
        layout(false);
        $this->path = I('get.path','');
        $this->thumbw = I('get.thumbw','');
        $this->thumbh = I('get.thumbh','');
        $this->parent_id = I('get.parent_id','');
        $this->display();
    }
    
}
