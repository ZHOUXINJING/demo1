<?php

namespace Fwadmin\Controller;

use Common\Controller\FwadminController;
use Fwadmin\Model\ConFootlinkModel;

class ConFootLinkController extends FwadminController {

    public function index() {
        //获得数据       
        $ModelObj = new ConFootlinkModel();
        if (IS_AJAX) {
            $t = I('get.t');
            $v = I('get.v');
            $id = I('get.i');
            echo $ModelObj->where('category_id=' . $id)->setField($t, $v);
            exit();
        }
        $list = $ModelObj->getList();
        $this->assign('list', $list); // 赋值数据集
        $this->display();
    }

    public function add() {
        $ModelObj = new ConFootlinkModel();
        if (IS_POST) {
            if ($ModelObj->create()) {
                $id = $ModelObj->add();
                if ($id > 0)
                    $this->success('添加成功', U('index'));
                else
                    $this->error($ModelObj->getLastSql());
            } else {
                $this->error($ModelObj->getError());
            }
        } else {
            $this->parentList = $ModelObj->getTop();
            $this->display();
        }
    }

    public function edit($category_id) {
        $ModelObj = new ConFootlinkModel();
        if (IS_POST) {
            if ($ModelObj->create()) {
                $flag = $ModelObj->save();
                if ($flag !== false)
                    $this->success('修改成功', U('index'));
                else
                    $this->error($ModelObj->getLastSql());
            } else {
                $this->error($ModelObj->getError());
            }
        } else {
            $model = $ModelObj->find($category_id);
            $this->model = $model; 
            $this->parentList = $ModelObj->getTop();
            $this->display();
        }
    }

    public function del($category_id) {
        $ModelObj = new ConFootlinkModel();
        $flag = $ModelObj->del($category_id);
        if ($flag > 0)
            $this->success('删除成功');
        else
            $this->error($ModelObj->getError());
    }

}

?>