<?php

namespace Fwadmin\Controller;

use Common\Controller\FwadminController;
use Fwadmin\Model\GoodsColorModel;

class GoodsColorController extends FwadminController {

    /**
     * 图片切换列表
     */
    public function index() {
        $ModelObj = new GoodsColorModel();
        if (IS_POST) {
		    $picture_id = I('post.picture_id');
			$count = count($picture_id);
			$goods_id = I('post.goods_id');
			$typename = I('post.typename');
			for($i=0;$i<$count;$i++){
			 $title = I('post.title_'.$i);
			 $picture = I('post.picture_list_'.$i);
			 $num = M('goods_color')->where(Array('goods_id'=>$goods_id,'title'=>$title))->count();
			 if($num>0){
			    M('goods_color')->where(Array('goods_id'=>$goods_id,'title'=>$title))->setField('picture_list',$picture); 
			  }else{
				 $data = array('goods_id'=>$goods_id,'title'=>$title,'picture_list'=>$picture);
			      M('goods_color')->data($data)->add();
			  }
			}
			  $this->success('设置成功', U('GoodsColor/index',Array('goods_id'=>$goods_id,'typename'=>$typename)));
			  exit();
		}
        //获得数据
		$goods_id = I('goods_id');
        $typename = I('typename');
		$this->typename = $typename;
	    $model = M('goods')->find($goods_id);
		$this->model = $model;
		
		$color_list = get_goods_color_list($model['spec_array'],$typename);
		foreach($color_list as $k =>$v){
		$color_list2[$k]['title'] = $v; 
		$color_list2[$k]['picture_list'] = M('goods_color')->where(Array('goods_id'=>$goods_id,'title'=>$v))->getField('picture_list');
		}
		$this->color_list2 = $color_list2;
		//print_r($color_list2);
		
	 

        $this->display();
    }

   

}
