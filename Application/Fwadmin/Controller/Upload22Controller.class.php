<?php

namespace Fwadmin\Controller;

use Common\Controller\FwadminController;

class UploadController extends FwadminController {

    /**
     * 更新uploadify session值，解决部分浏览器上传不了问题
     */
    public function _initialize() {
        if (I('get.sessionid', '') != '') {
            session_id(I('get.sessionid'));
            session('[pause]');
            session('[start]');
        }
    }

    /**
     * 编辑器上传文件
     */
    public function editor() {
        $admin_id = I('session.admin_id', 0);
        $config = array();
        $config['type'] = array("img", "flash", "file"); //上传允许type值
        $config['img'] = array("jpg", "bmp", "gif", "png"); //img允许后缀
        $config['flash'] = array("flv", "swf"); //flash允许后缀
        $config['file'] = array("rar", "zip", "doc", "docx", "xls", "xlsx", "pdf", "txt"); //文件允许后缀
        $config['flash_size'] = 3000; //上传flash大小上限 单位：KB
        $config['img_size'] = 5000; //上传img大小上限 单位：KB
        $config['file_size'] = 3000; //上传img大小上限 单位：KB
        $config['message'] = "上传成功"; //上传成功后显示的消息，若为空则不显示
        $config['name'] = time() . '_' . md5(mt_rand() . time() . $admin_id); //上传后的文件命名规则 这里以unix时间戳来命名
        $config['public'] = "/Public/upload/editor/";
        $config['publictemp'] = "/Public/upload/temp";
        $config['flash_dir'] = "flash"; //上传flash文件地址 采用绝对地址 方便upload.php文件放在站内的任何位置 后面不加"/"
        $config['img_dir'] = "img"; //上传img文件地址 采用绝对地址 采用绝对地址 方便upload.php文件放在站内的任何位置 后面不加"/"
        $config['file_dir'] = "file";
        $config['site_url'] = ""; //网站的网址 这与图片上传后的地址有关 最后不加"/" 可留空
        //文件上传
        //判断是否是非法调用
        if (empty($_GET['CKEditorFuncNum']))
            $this->mkhtml(1, "", "错误的功能调用请求");
        $fn = $_GET['CKEditorFuncNum'];

        if ($admin_id != 0) {
            if (!in_array(I("get.type"), $config['type']))
                $this->mkhtml(1, "", "错误的文件调用请求");
            $type = I("get.type");
            if (is_uploaded_file($_FILES['upload']['tmp_name'])) {
                //判断上传文件是否允许
                $filearr = pathinfo($_FILES['upload']['name']);
                $filetype = $filearr["extension"];
                if (!in_array($filetype, $config[$type]))
                    $this->mkhtml($fn, "", "错误的文件类型！");
                //判断文件大小是否符合要求
                if ($_FILES['upload']['size'] > $config[$type . "_size"] * 1024)
                    $this->mkhtml($fn, "", "上传的文件不能超过" . $config[$type . "_size"] . "KB！");
                //$filearr=explode(".",$_FILES['upload']['name']);
                //$filetype=$filearr[count($filearr)-1];
                $file_host = $config['public'] . $config[$type . "_dir"] . "/" . $config['name'] . "." . $filetype;
                $file_temp = $config['publictemp'] . "/" . $config['name'] . "." . $filetype;
                // $this->mkhtml($fn, "", "文件上传失败，请登录".$file_host);
                if (move_uploaded_file($_FILES['upload']['tmp_name'], '.' . $file_temp)) {
                    if ($type == 'img') {
                        // 生成缩略图
                        $image = new \Think\Image();
                        $image->open('.' . $file_temp);
                        $width = $image->width(); // 返回图片的宽度
                        $height = $image->height(); // 返回图片的高度
                        $max_width = I("get.maxwidth", 1100);
                        if ($width > $max_width) {
                            $image->thumb($max_width, $height, 1)->save('.' . $file_host);
                            delfile($file_temp);
                        } else {
                            if (!rename('.' . $file_temp, '.' . $file_host)) {
                                $this->mkhtml($fn, "", "文件上传失败，文件丢失");
                            }
                        }
                    } else {
                        if (!rename('.' . $file_temp, '.' . $file_host)) {
                            $this->mkhtml($fn, "", "文件上传失败，文件丢失");
                        }
                    }
                    $this->mkhtml($fn, $config['site_url'] . __ROOT__ . $file_host, $config['message']);
                } else {
                    $this->mkhtml($fn, "", "文件上传失败，请检查上传目录设置和目录读写权限");
                }
            } else {
                $this->mkhtml($fn, "", "文件上传失败，文件丢失");
            }
        } else {
            $this->mkhtml($fn, "", "文件上传失败，请登录");
        }
    }

    //输出js调用
    public function mkhtml($fn, $fileurl, $message) {
        $str = '<script type="text/javascript">window.parent.CKEDITOR.tools.callFunction(' . $fn . ', \'' . $fileurl . '\', \'' . $message . '\');</script>';
        exit($str);
    }

}
