<?php

namespace Fwadmin\Controller;

use Common\Controller\FwadminController;
use Fwadmin\Model\GoodsModModel;
//商品属性

class GoodsModController extends FwadminController {

    /**
     * 模型列表
     */
    public function index() {
        $ModelObj = new GoodsModModel();
        if (IS_AJAX) {
            $t = I('get.t');
            $v = I('get.v');
            $id = I('get.i');
            echo $ModelObj->where('model_id=' . $id)->setField($t, $v);
            exit();
        }
        //获得数据
        $this->list = $ModelObj->get_list();
        $this->display();
    }

    /**
     * 添加模型
     */
    public function add() {
        $ModelObj = new GoodsModModel();
        if (IS_POST) {
            if ($ModelObj->create()) {
                $id = $ModelObj->add();
                if ($id > 0)
                    $this->success('添加成功', U('index'));
                else
                    $this->error($ModelObj->getError());
            } else {
                $this->error($ModelObj->getError());
            }
        } else {
            $this->display();
        }
    }

     /**
     * 编辑模型
     */
    public function edit($model_id) {
        $ModelObj = new GoodsModModel();
        if (IS_POST) {
            if ($ModelObj->create()) {
                $flag = $ModelObj->save();
                if ($flag !== false)
                    $this->success('修改成功', U('index'));
                else
                    $this->error($ModelObj->getError());
            } else {
                $this->error($ModelObj->getError());
            }
        }else {
            $model = $ModelObj->find($model_id);
            $this->model = $model;
            $this->display();
        }
    }
    
     /**
     * 删除模型
     */
    public function del($model_id) {
        $ModelObj = new GoodsModModel();
        $flag = $ModelObj->del($model_id);
        if ($flag > 0)
            $this->success('删除成功');
        else
            $this->error('删除失败');
    }
    
    /**
     * 模型属性列表
     * @param int $model_id 模型ID
     */
    public function attr_index($model_id) {
        $this->model_id = $model_id;
        $MGoodsModAttr = M('GoodsModAttr');
        if (IS_AJAX) {
            $t = I('get.t');
            $v = I('get.v');
            $id = I('get.i');
            echo $MGoodsModAttr->where('attr_id=' . $id)->setField($t, $v);
            exit();
        }
        $ModelObj = new GoodsModModel();
        //获取模型列表，用于快速切换
        $this->model_list = $ModelObj->get_list();
        //获取模型属性值
        $list = $ModelObj->get_attr_list($model_id);
        foreach($list as $k => $v) {
            $list[$k]['attr_type_name'] = $ModelObj->get_attr_type($v['attr_type']);
            $list[$k]['is_search_name'] = $ModelObj->get_attr_search($v['is_search']);
            if($v['attr_type'] == 2 || $v['attr_type'] == 3 || $v['attr_type'] == 4) {
                $arr_sel = $ModelObj->get_attr_sel($v['attr_id']);
                foreach($arr_sel as $a_sel) {
                    $list[$k]['options'] .= $a_sel['attr_sel_name'].',';
                }
                $list[$k]['options'] = trim($list[$k]['options'],',');   
            } else {
                 $list[$k]['options'] = '';
            }
        }
        $this->list = $list;
        
        $this->display();
    }
    
    /**
     * 模型属性添加
     * @param int $model_id 模型ID
     */
    public function attr_add($model_id) {
        $this->model_id = $model_id;
        $ModelObj = new GoodsModModel();
        $MGoodsModAttr = M('GoodsModAttr');
        if (IS_POST) {
            if ($MGoodsModAttr->create()) {
                $attr_id = $MGoodsModAttr->add();
                if ($attr_id > 0) {
                    $attr_sel_name = I('post.attr_sel_name','','trim');
                    if($attr_sel_name !== '') {
                        $arr_attr_sel = explode(',',$attr_sel_name);
                        foreach($arr_attr_sel as $item) {
                           $ModelObj->add_attr_sel($item, $attr_id, $model_id);
                        }
                    }
                    $this->success('添加成功', U('attr_index',array('model_id'=>$model_id)));
                } else {
                    $this->error($MGoodsModAttr->getError());
                }
            } else {
                $this->error($MGoodsModAttr->getError());
            }
        } else {
            $this->display();
        }
    }
    
    /**
     * 修改模型属性
     * @param int $model_id 模型ID
     * @param int $attr_id 属性ID
     */
    public function attr_edit($model_id,$attr_id) {
        $this->model_id = $model_id;
        $this->attr_id = $attr_id;
        $MGoodsModSel = M('GoodsModSel');
        if (IS_AJAX) {
            $t = I('get.t');
            $v = I('get.v');
            $id = I('get.i');
            echo $MGoodsModSel->where('attr_sel_id=' . $id)->setField($t, $v);
            exit();
        }
        $ModelObj = new GoodsModModel();
        $MGoodsModAttr = M('GoodsModAttr');
        if (IS_POST) {
            if ($MGoodsModAttr->create()) {
                $flag = $MGoodsModAttr->save();
                if ($flag !== false)
                    $this->success('修改成功', U('attr_index',array('model_id'=>$model_id)));
                else
                    $this->error($MGoodsModAttr->getError());
            } else {
                $this->error($MGoodsModAttr->getError());
            }
        }else {
            //获取属性模型
            $model = $MGoodsModAttr->find($attr_id);
            $this->model = $model;
            //获取模型选项
            $this->sel_list = $ModelObj->get_attr_sel($attr_id);
            
            $this->display();
        }
    }
    
    /**
     * 删除模型属性
     */
    public function attr_del($model_id,$attr_id) {
        $ModelObj = new GoodsModModel();
        $flag = $ModelObj->del_attr($attr_id);
        if ($flag > 0)
            $this->success('删除成功');
        else
            $this->error('删除失败');
    }
    
    /**
     * 添加模型属性选项值
     * @param int $model_id 模型ID
     * @param int $attr_id 属性ID
     */
    public function attr_sel_add($model_id,$attr_id) {
        $MGoodsModSel = M('GoodsModSel');
        if (IS_POST) {
            if ($MGoodsModSel->create()) {
                $MGoodsModSel->model_id = $model_id;
                $MGoodsModSel->attr_id = $attr_id;
                $flag = $MGoodsModSel->add();
                if ($flag !== false)
                    $this->success('添加选项成功', U('attr_edit',array('model_id'=>$model_id,'attr_id'=>$attr_id)));
                else
                    $this->error($MGoodsModSel->getError());
            } else {
                $this->error($MGoodsModSel->getError());
            }
        }
    }
    
    /**
     * 删除模型属性选项值
     */
    public function attr_sel_del($attr_sel_id) {
        $ModelObj = new GoodsModModel();
        $flag = $ModelObj->del_attr_sel($attr_sel_id);
        if ($flag > 0)
            $this->success('删除成功');
        else
            $this->error('删除失败');
    }

}
