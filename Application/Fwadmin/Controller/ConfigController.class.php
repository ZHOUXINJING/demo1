<?php

namespace Fwadmin\Controller;

use Fwadmin\Model\ConfigModel;
use Common\Controller\FwadminController;

class ConfigController extends FwadminController {

    /**
     * 网站基本配置
     */
    public function site() {
        $ModelObj = new ConfigModel();
        if (IS_POST) {

            $ModelObj->set('site_name', I('post.site_name'));
            $ModelObj->set('site_logo', I('post.site_logo'));
            $ModelObj->set('site_tel', I('post.site_tel'));
            $ModelObj->set('site_weibo', I('post.site_weibo'));
            $ModelObj->set('site_weibo_qq', I('post.site_weibo_qq'));
            $ModelObj->set('site_weixin', I('post.site_weixin'));
            $ModelObj->set('site_copyright', I('post.site_copyright'));
            $ModelObj->set('site_icp', I('post.site_icp'));
            $ModelObj->set('editorValue', I('post.editorValue', ''));
	        $ModelObj->set('mingganci', I('post.mingganci', ''));
            $ModelObj->set('site_welcome', I('post.site_welcome', ''));
            S('config', null);
            $this->success('保存成功');
        } else {
            $this->site_name = $ModelObj->get('site_name');
            $this->site_logo = $ModelObj->get('site_logo');
            $this->site_tel = $ModelObj->get('site_tel');
            $this->site_weibo = $ModelObj->get('site_weibo');
            $this->site_weibo_qq = $ModelObj->get('site_weibo_qq');
            $this->site_weixin = $ModelObj->get('site_weixin');
            $this->site_copyright = $ModelObj->get('site_copyright');
            $this->site_icp = $ModelObj->get('site_icp');
            $this->editorValue = htmlspecialchars_decode($ModelObj->get('editorValue'));
	        $this->mingganci = $ModelObj->get('mingganci');
            $this->site_welcome = $ModelObj->get('site_welcome');
            $this->display();
        }
    }
    
    /*
     * 第三方代码配置
     */
    public function othercode() {
        $ModelObj = new ConfigModel();
        if (IS_POST) {
            if(get_magic_quotes_gpc() == 1) {
                $ModelObj->set('site_othercode', stripslashes(I('post.site_othercode', '', '')));
            } else {
                $ModelObj->set('site_othercode', I('post.site_othercode', '', ''));
            }
            S('config', null);
            $this->success('保存成功');
        } else {
            $this->site_othercode = $ModelObj->get('site_othercode');
            $this->display();
        }
    }


    /*
     * 积分配置
     */
    public function point() {
        $ModelObj = new ConfigModel();
        if (IS_POST) {
            $ModelObj->set('point_register', I('post.point_register'));
            $ModelObj->set('point_login_day', I('post.point_login_day'));
            $ModelObj->set('point_order_percent', I('post.point_order_percent'));
            $ModelObj->set('point_comment', I('post.point_comment'));
            $ModelObj->set('point_pay_can',I('post.point_pay_can'));
            $ModelObj->set('point_1_to_money',I('post.point_1_to_money'));
            $ModelObj->set('point_pay_percent',I('post.point_pay_percent'));
            S('config', null);
            $this->success('保存成功');
        } else {
            $this->point_register = $ModelObj->get('point_register');
            $this->point_login_day = $ModelObj->get('point_login_day');
            $this->point_order_percent = $ModelObj->get('point_order_percent');
            $this->point_comment = $ModelObj->get('point_comment');
            $this->point_pay_can = $ModelObj->get('point_pay_can');
            $this->point_1_to_money = $ModelObj->get('point_1_to_money');
            $this->point_pay_percent = $ModelObj->get('point_pay_percent');
            $this->display();
        }
    }
    
    /*
     * 服务费配置
     */
    public function fee() {
        $ModelObj = new ConfigModel();
        if (IS_POST) {
            $ModelObj->set('fee_dingzhi_add', I('post.fee_dingzhi_add'));
            $ModelObj->set('fee_dingzhi_see', I('post.fee_dingzhi_see'));
            $ModelObj->set('fee_qiugou_add', I('post.fee_qiugou_add'));
            $ModelObj->set('baozheng_qiugou', I('post.baozheng_qiugou'));
            $ModelObj->set('fee_order', I('post.fee_order'));
            $ModelObj->set('fee_ca_egt3', I('post.fee_ca_egt3'));
            $ModelObj->set('fee_ca_gt3', I('post.fee_ca_gt3'));
            $ModelObj->set('fee_hecheng', I('post.fee_hecheng'));
            $ModelObj->set('fee_jishu_free', I('post.fee_jishu_free'));
            $ModelObj->set('fee_jishu_more', I('post.fee_jishu_more'));
            $ModelObj->set('fee_jishu_day', I('post.fee_jishu_day'));
            $ModelObj->set('fee_jishu_day_tishi', I('post.fee_jishu_day_tishi'));
            S('config', null);
            $this->success('保存成功');
        } else {
            $this->fee_dingzhi_add = $ModelObj->get('fee_dingzhi_add');
            $this->fee_dingzhi_see = $ModelObj->get('fee_dingzhi_see');
            $this->fee_qiugou_add = $ModelObj->get('fee_qiugou_add');
            $this->baozheng_qiugou = $ModelObj->get('baozheng_qiugou');
            $this->fee_order = $ModelObj->get('fee_order');
            $this->fee_ca_egt3 = $ModelObj->get('fee_ca_egt3');
            $this->fee_ca_gt3 = $ModelObj->get('fee_ca_gt3');
            $this->fee_hecheng = $ModelObj->get('fee_hecheng');
            $this->fee_jishu_free = $ModelObj->get('fee_jishu_free');
            $this->fee_jishu_more = $ModelObj->get('fee_jishu_more');
            $this->fee_jishu_day = $ModelObj->get('fee_jishu_day');
            $this->fee_jishu_day_tishi = $ModelObj->get('fee_jishu_day_tishi');
            $this->display();
        }
    }
    
    /**
     * 修改找回密码邮件
     */
    public function forget() {
        $ModelObj = new ConfigModel();
        if (IS_POST) {
            $ModelObj->set('forget_email_title', I('post.forget_email_title'));
            $ModelObj->set('forget_email_detail', I('post.forget_email_detail'));
            S('config', null);
            $this->success('保存成功');
        } else {
            $this->forget_email_title = $ModelObj->get('forget_email_title');
            $this->forget_email_detail = $ModelObj->get('forget_email_detail');
            $this->display();
        }
    }
    
    /*
     * 网站维护开关
     */
    public function weihu() {
        $ModelObj = new ConfigModel();
        if (IS_POST) {
            $ModelObj->set('site_close', I('post.site_close'));
            $ModelObj->set('site_close_spec', I('post.site_close_spec'));
            S('config', null);
            $this->success('保存成功');
        } else {
            $this->site_close = $ModelObj->get('site_close');
            $this->site_close_spec = $ModelObj->get('site_close_spec');
            $this->display();
        }
    }
    
    /*网站售后配置*/
    public function orderback() {
        $ModelObj = new ConfigModel();
        if (IS_POST) {
            $ModelObj->set('orderback_day_tuihuo', I('post.orderback_day_tuihuo'));
            $ModelObj->set('orderback_day_huanhuo', I('post.orderback_day_huanhuo'));
            $ModelObj->set('orderback_day_weixiu', I('post.orderback_day_weixiu'));
            $ModelObj->set('orderback_address', I('post.orderback_address'));
            $ModelObj->set('orderback_realname', I('post.orderback_realname'));
            $ModelObj->set('orderback_tel', I('post.orderback_tel'));
            $ModelObj->set('orderback_zip', I('post.orderback_zip'));
            S('config', null);
            $this->success('保存成功');
        } else {
            $this->orderback_day_tuihuo = $ModelObj->get('orderback_day_tuihuo');
            $this->orderback_day_huanhuo = $ModelObj->get('orderback_day_huanhuo');
            $this->orderback_day_weixiu = $ModelObj->get('orderback_day_weixiu');
            $this->orderback_address = $ModelObj->get('orderback_address');
            $this->orderback_realname = $ModelObj->get('orderback_realname');
            $this->orderback_tel = $ModelObj->get('orderback_tel');
            $this->orderback_zip = $ModelObj->get('orderback_zip');
            $this->display();
        }
    }
    
    /*
     * 发送短信配置
     */
    public function sms() {
        $ModelObj = new ConfigModel();
        if (IS_POST) {
            $ModelObj->set('sms_name', I('post.sms_name'));
            $ModelObj->set('sms_url', I('post.sms_url'));
            $ModelObj->set('sms_username', I('post.sms_username'));
            $sms_password = I('post.sms_password','');
            if($sms_password != '') {
                $ModelObj->set('sms_password', I('post.sms_password'));
            }
            S('config', null);
            $this->success('保存成功');
        } else {
            $this->sms_url = $ModelObj->get('sms_url');
            $this->sms_name = $ModelObj->get('sms_name');
            $this->sms_username = $ModelObj->get('sms_username');
            $this->sms_password = $ModelObj->get('sms_password');
            $this->display();
        }
    }
    
    public function sendSMS() {
        $phone = I('post.phone');
        $content = "您的验证码为".rand(100000, 999999)."，在20分钟内有效。";
        $result = sendSMS($phone, $content);
        if($result == 'success') {
            $this->success('测试短信发送成功');
        } else {
            $this->error($result);
        }
    }
    

}
