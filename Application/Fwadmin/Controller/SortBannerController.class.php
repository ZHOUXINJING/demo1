<?php

namespace Fwadmin\Controller;

use Common\Controller\FwadminController;
use Fwadmin\Model\SortBannerModel;

class SortBannerController extends FwadminController {

    /**
     * 图片切换列表
     */
    public function index() {
        $ModelObj = new SortBannerModel();
        if (IS_AJAX) {
            $t = I('get.t');
            $v = I('get.v');
            $id = I('get.i');
            echo $ModelObj->where('id=' . $id)->setField($t, $v);
            exit();
        }
        //获得数据
		$typeid = I('typeid');
		$this->typeid = $typeid;
	
        $conditions['typeid'] = $typeid;
        $results = $ModelObj->search($conditions);
        $lists = $results['list'];
        $this->assign('list', $lists); // 赋值数据集
        $this->assign('page', $results['page']); // 赋值分页输出
        $this->display();
    }

    /**
     * 添加图片切换
     */
    public function add() {
        $ModelObj = new SortBannerModel();
        if (IS_POST) {
            if ($ModelObj->create()) {
				$typeid = I('typeid');
                $id = $ModelObj->add();
                if ($id > 0)
                    $this->success('添加成功', U('index',array('typeid'=>$typeid)));
                else
                    $this->error($ModelObj->getError());
            } else {
			
                $this->error($ModelObj->getError());
            }
        } else {
			$typeid = I('typeid');
		    $this->typeid = $typeid;
            $this->display();
        }
    }

     /**
     * 编辑图片切换
     */
    public function edit($id) {
        $ModelObj = new SortBannerModel();
        if (IS_POST) {
            if ($ModelObj->create()) {
                $flag = $ModelObj->save();
				$typeid = I('typeid');
                if ($flag !== false)
                    $this->success('修改成功', U('index',array('typeid'=>$typeid)));
                else
                    $this->error($ModelObj->getError());
            } else {
                $this->error($ModelObj->getError());
            }
        }else {
            $model = $ModelObj->find($id);
            $this->model = $model;
			$this->typeid = I('typeid');
            $this->display();
        }
    }
    
     /**
     * 删除图片切换
     */
    public function del($id) {
        $ModelObj = new SortBannerModel();
        $flag = $ModelObj->del($id);
        if ($flag > 0)
            $this->success('删除成功');
        else
            $this->error($ModelObj->getError());
    }

}
