<?php

namespace Fwadmin\Controller;

use Common\Controller\FwadminController;
use Fwadmin\Model\AdminGroupModel;

class AdminGroupController extends FwadminController {

    /**
     * 管理员组列表
     */
    public function index() {
        //获得数据       
        $ModelObj = new AdminGroupModel();
        if (IS_AJAX) {
            $t = I('get.t');
            $v = I('get.v');
            $id = I('get.i');
            echo $ModelObj->where('group_id=' . $id)->setField($t, $v);
            exit();
        }
        $results = $ModelObj->search();
        $this->assign('list', $results['list']); // 赋值数据集
        $this->assign('page', $results['page']); // 赋值数据集
        $this->display();
    }

    /**
     * 添加管理员组
     */
    public function add() {
        $ModelObj = new AdminGroupModel();
        if (IS_POST) {
            if ($ModelObj->create()) {
                $purview = I('post.purview');
                $ModelObj->purview_id_list = ',' . implode(",", $purview) . ',';//权限列表
                $group_name = $ModelObj->group_name;
                $group_id = $ModelObj->add();
                if ($group_id > 0) {
                    //记录操作
                    $RecordObj = new \Fwadmin\Model\SystemRecordModel();
                    $RecordObj->addrecord('新建管理员组:' . $group_name . '(ID:' . $group_id . ')');
                    $this->success('添加成功', U('index'));
                } else {
                    $this->error($ModelObj->getLastSql());
                }
            } else {
                $this->error($ModelObj->getError());
            }
        } else {
            //调取权限列表
            $PurviewObj = new \Fwadmin\Model\AdminPurviewModel();
            $this->purview = $PurviewObj->showList();
            $this->display();
        }
    }

    /**
     * 编辑管理员组
     * @param int $group_id 管理员组ID
     */
    public function edit($group_id) {
        $ModelObj = new AdminGroupModel();
        if (IS_POST) {
            if ($ModelObj->create()) {
                $purview = I('post.purview');
                $ModelObj->purview_id_list = ',' . implode(",", $purview) . ',';//权限列表
                $group_name = $ModelObj->group_name;
                $flag = $ModelObj->save();
                if ($flag !== false) {
                    //记录操作
                    $RecordObj = new \Fwadmin\Model\SystemRecordModel();
                    $RecordObj->addrecord('修改管理员组:' . $group_name . '(ID:' . $group_id . ')');
                    $this->success('修改成功', U('index'));
                } else {
                    $this->error($ModelObj->getLastSql());
                }
            } else {
                $this->error($ModelObj->getError());
            }
        } else {
            //调取权限列表
            $PurviewObj = new \Fwadmin\Model\AdminPurviewModel();
            $this->purview = $PurviewObj->showList();
            //调取组模型
            $model = $ModelObj->find($group_id);
            $this->model = $model;
            $this->display();
        }
    }

    /**
     * 删除管理员组
     * @param int $group_id 管理员组ID
     */
    public function del($group_id) {
        $ModelObj = new AdminGroupModel();
        $model = $ModelObj->find($group_id);
        $flag = $ModelObj->del($group_id);
        if($flag == -1) {
             $this->error("该管理员组存在用户，请先删除该组用户！");
        }elseif ($flag > 0) {
            //记录操作
            $RecordObj = new \Fwadmin\Model\SystemRecordModel();
            $RecordObj->addrecord('删除管理员组:' . $model['group_name'] . '(ID:' . $group_id . ')');
            $this->success('删除成功');
        } else {
            $this->error($ModelObj->getError());
        }
    }

}

?>