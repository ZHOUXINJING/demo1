<?php

namespace Fwadmin\Controller;

use Common\Controller\FwadminController;

class TempletController extends FwadminController {

    /* 
     * 列表 
     */
    public function index() {
        $this->display();
    }

    /* 
     * 添加 
     */
    public function add() {
        $this->display();
    }

    /* 
     * 编辑
     */
    public function edit($id) {
        $this->display();
    }
    
    /*
     * 删除
     */
    public function del($id) {
        $flag = 1;
        if ($flag > 0)
            $this->success('删除成功');
        else
            $this->error($ModelObj->getError());
    }

}
