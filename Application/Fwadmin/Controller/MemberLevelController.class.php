<?php

namespace Fwadmin\Controller;

use Common\Controller\FwadminController;
use Fwadmin\Model\MemberLevelModel;

class MemberLevelController extends FwadminController {

    public function index() {
        //获得数据       
        $ModelObj = new MemberLevelModel();
        if (IS_AJAX) {
            $t = I('get.t');
            $v = I('get.v');
            $level_id = I('get.i');
            echo $ModelObj->where('level_id=' . $level_id)->setField($t, $v);
            exit();
        }
        $results = $ModelObj->search($conditions);
        $lists = $results['list'];
        $this->assign('list', $lists); // 赋值数据集
        $this->assign('page', $results['page']); // 赋值分页输出
        $this->display();
    }

    public function add() {
        $ModelObj = new MemberLevelModel();
        if (IS_POST) {
            if ($ModelObj->create()) {
                $level_id = $ModelObj->add();
                if ($level_id > 0)
                    $this->success('添加成功', U('MemberLevel/index'));
                else
                    $this->error($ModelObj->getError());
            } else {
                $this->error($ModelObj->getError());
            }
        } else {
            $this->display();
        }
    }

    public function edit($level_id) {
        $ModelObj = new MemberLevelModel();
        if (IS_POST) {
            if ($ModelObj->create()) {
                $flag = $ModelObj->save();
                if ($flag !== false)
                    $this->success('修改成功', U('MemberLevel/index'));
                else
                    $this->error($ModelObj->getError());
            }
        } else {
            $model = $ModelObj->find($level_id);
            $this->model = $model;
            $this->display();
        }
    }

    public function del($level_id) {
        $ModelObj = new MemberLevelModel();
        $flag = $ModelObj->delete($level_id);
        if ($flag > 0)
            $this->success('删除成功');
        else
            $this->error($ModelObj->getError());
    }

}
