<?php

namespace Fwadmin\Controller;

use Common\Controller\FwadminController;
use Fwadmin\Model\ConfigDeliveryModel;
use Fwadmin\Model\ConfigDeliveryAreaModel;
use Fwadmin\Model\ConfigTakeselfModel;

class ConfigDeliveryController extends FwadminController {
    
    /*
     * 配送方式列表
     */
    public function index() {
        //获得数据       
        $ModelObj = new ConfigDeliveryModel();
        if (IS_AJAX) {
            $t = I('get.t');
            $v = I('get.v');
            $id = I('get.i');
            echo $ModelObj->where('delivery_id=' . $id)->setField($t, $v);
            exit();
        }
        $this->list = $ModelObj->get_list();
        $this->display();
    }
    
    /*
     * 添加配送方式
     */
    public function add() {
        $ModelObj = new ConfigDeliveryModel();
        if (IS_POST) {
            if ($ModelObj->create()) {
                if(empty($ModelObj->is_save_price)) {
                    $ModelObj->is_save_price = 0;
                }
                $id = $ModelObj->add();
                if ($id > 0)
                    $this->success('添加成功', U('index'));
                else
                    $this->error($ModelObj->getLastSql());
            } else {
                $this->error($ModelObj->getError());
            }
        } else {
            $this->display();
        }
    }
    
    /*
     * 修改配送方式
     */
    public function edit($delivery_id) {
        $ModelObj = new ConfigDeliveryModel();
        if (IS_POST) {
            if ($ModelObj->create()) {
                if(empty($ModelObj->is_save_price)) {
                    $ModelObj->is_save_price = 0;
                }
                $flag = $ModelObj->save();
                if ($flag !== false)
                    $this->success('修改成功', U('index'));
                else
                    $this->error($ModelObj->getLastSql());
            } else {
                $this->error($ModelObj->getError());
            }
        } else {
            $model = $ModelObj->find($delivery_id);
            $this->model = $model;
            $this->display();
        }
    }
    
    /*
     * 删除配送方式
     */
    public function del($delivery_id) {
        $ModelObj = new ConfigDeliveryModel();
        $flag = $ModelObj->del($delivery_id);
        if ($flag > 0)
            $this->success('删除成功');
        else
            $this->error($ModelObj->getError());
    }
    
     /*
     * 地区配送价格
     */
    public function area($delivery_id) {      
        //获得数据       
        $AreaObj = new ConfigDeliveryAreaModel();
        if (IS_AJAX) {
            $t = I('get.t');
            $v = I('get.v');
            $id = I('get.i');
            echo $AreaObj->where('id=' . $id)->setField($t, $v);
            exit();
        }
        //获取配送方式数据
        $ModelObj = new ConfigDeliveryModel();
        $this->delivery_id = $delivery_id;
        $this->delivery = $ModelObj->find($delivery_id);
        //获取省份
        $AddressObj = new \Fwadmin\Model\AddressProvinceModel();
        $this->province = $AddressObj->getSel();
        //获取数据
        $condition['delivery_id'] = $delivery_id;
        $result = $AreaObj->search($condition);
        $this->list = $result['list'];
        $this->page = $result['page'];
        $this->display();
    }
    
    /*
     * 设置地区配送默认
     */
    public function set_open_default() {
        $delivery_id = I('post.delivery_id');
        $open_default = I('post.open_default');
        $ModelObj = new ConfigDeliveryModel();
        $flag = $ModelObj->where('delivery_id='.$delivery_id)->setField('open_default',$open_default); 
        if($flag !== false) {
            echo '设置成功！';
        } else {
            echo '设置失败！';
        }
    }
    
    /*
     * 添加配送地区
     */
    public function area_add() {
        $delivery_id = I('post.delivery_id');
        $AreaObj = new ConfigDeliveryAreaModel();
        $province = I('post.province','');
        $city = I('post.city','');
        if($AreaObj->where(array('province'=>$province,'city'=>$city,'delivery_id'=>$delivery_id))->count()>0) {
            $this->error('添加的省份和城市已存在！');
        }
        if (IS_POST) {
            if ($AreaObj->create()) {
                $AreaObj->is_enable = 1;
                $id = $AreaObj->add();
                if ($id > 0)
                    $this->success('添加成功', U('area',array('delivery_id'=>$delivery_id)));
                else
                    $this->error($AreaObj->getError());
            } else {
                $this->error($AreaObj->getError());
            }
        } else {
            $this->display();
        }
    }
    
    /*
     * 删除配送地区
     */
    public function area_del($delivery_id,$id) {
        $AreaObj = new ConfigDeliveryAreaModel();
        $flag = $AreaObj->where(array('delivery_id'=>$delivery_id,'id'=>$id))->delete();
        if ($flag > 0)
            $this->success('删除成功');
        else
            $this->error($AreaObj->getError());
    }
    
    
    /*
     * 配送方式列表
     */
    public function takeself() {
        //获得数据       
        $ModelObj = new ConfigTakeselfModel();
        if (IS_AJAX) {
            $t = I('get.t');
            $v = I('get.v');
            $id = I('get.i');
            echo $ModelObj->where('delivery_id=' . $id)->setField($t, $v);
            exit();
        }
        $result = $ModelObj->search();
        $this->list = $result['list'];
        $this->page = $result['page'];
        $this->display();
    }
    
    /*
     * 添加配送方式
     */
    public function takeself_add() {
        $ModelObj = new ConfigTakeselfModel();
        if (IS_POST) {
            if ($ModelObj->create()) {
                $id = $ModelObj->add();
                if ($id > 0)
                    $this->success('添加成功', U('takeself'));
                else
                    $this->error($ModelObj->getLastSql());
            } else {
                $this->error($ModelObj->getError());
            }
        } else {
            //获取省份
            $AddressObj = new \Fwadmin\Model\AddressProvinceModel();
            $this->province = $AddressObj->getSel();
            $this->display();
        }
    }
    
    /*
     * 修改配送方式
     */
    public function takeself_edit($id) {
        $ModelObj = new ConfigTakeselfModel();
        if (IS_POST) {
            if ($ModelObj->create()) {
                $flag = $ModelObj->save();
                if ($flag !== false)
                    $this->success('修改成功', U('takeself'));
                else
                    $this->error($ModelObj->getLastSql());
            } else {
                $this->error($ModelObj->getError());
            }
        } else {
            //获取省份
            $AddressObj = new \Fwadmin\Model\AddressProvinceModel();
            $this->province = $AddressObj->getSel();
            
            $model = $ModelObj->find($id);
            $this->model = $model;
            $this->display();
        }
    }
    
    /*
     * 删除配送方式
     */
    public function takeself_del($id) {
        $ModelObj = new ConfigTakeselfModel();
        $flag = $ModelObj->del($id);
        if ($flag > 0)
            $this->success('删除成功');
        else
            $this->error($ModelObj->getError());
    }
    
    
    
    
}