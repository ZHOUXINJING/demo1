<?php
namespace Fwadmin\Controller;
use Think\Controller;
use Fwadmin\Model\GoodsCatModel;
use Common\Controller\FwadminController;
//调取全国省份城市地区邮政编码
class GoodCateController extends FwadminController {

    /**
     * 获取城市选择
     * @param string $province 所选城市名称
     * @param string $city 已选择城市名称
     */
    public function get_children($parent_id,$level_id) {
		
        $ModelObj = new GoodsCatModel();
         switch($level_id){
		 case 2:$level_name='二';break;
		 case 3:$level_name='三';break;
		  case 3:$level_name='四';break;
		 }
       
            $city_sel = $ModelObj->where(Array('cat_level'=>$level_id,'parent_id'=>$parent_id,'is_enable'=>1))->order('order_id asc,cat_id asc')->select();
			
			
           $str = '<select name="cat_id'.$level_id.'" id="cat_id'.$level_id.'" dir="'.($level_id+1).'" class="sell"  onchange="get_children(\'cat_id\','.$level_id.');">';
            $str .= '<option value="">选择'.$level_name.'级类别</option>';
            foreach($city_sel as $c) {
          
               $str .= '<option value="'.$c['cat_id'].'">'.$c['cat_name'].'</option>';
              
            }
            $str .= '</select>';
       
        echo $str;
        exit();
    }
    
	
	    public function get_children2($parent_id,$level_id) {
		
        $ModelObj = new GoodsCatModel();
         switch($level_id){
		 case 2:$level_name='二';break;
		 case 3:$level_name='三';break;
		  case 3:$level_name='四';break;
		 }
       
            $city_sel = $ModelObj->where(Array('cat_level'=>$level_id,'parent_id'=>$parent_id,'is_enable'=>1))->order('order_id asc,cat_id asc')->select();
			
			
           $str = '<select name="ccat_id'.$level_id.'" id="ccat_id'.$level_id.'" dir="'.($level_id+1).'" class="sell"  onchange="get_children2(\'ccat_id\','.$level_id.');">';
            $str .= '<option value="">选择'.$level_name.'级类别</option>';
            foreach($city_sel as $c) {
          
               $str .= '<option value="'.$c['cat_id'].'">'.$c['cat_name'].'</option>';
              
            }
            $str .= '</select>';
       
        echo $str;
        exit();
    }
    
    
    /**
     * AJAX获取地区选择
     * @param string $city 城市  修正，需要获取省份，不然所有的“市辖区”都只能获取到北京市的
     * @param string $area 已选择地区
     */
    public function area_sel($province,$city,$area) {
        $ProvinceObj = M('AddressProvince');
        $CityObj = M('AddressCity');
        $AreaObj = M('AddressArea');
        if($province == '') exit();
        $provinceid = $ProvinceObj->getFieldByProvince($province,'provinceid'); //获取省份ID
        if($city == "") exit();
        $cityid = $CityObj->where("provinceid=".$provinceid." and city='".$city."'")->getField('cityid'); //获取城市ID

        $str = '';
        if(!empty($cityid)) {
            $area_sel = $AreaObj->field('area')->where('cityid='.$cityid)->select();
            $str = '<select name="area" id="area" onchange="change_area()">';
            $str .= '<option value="">选择地区</option>';
            foreach($area_sel as $a) {
                if($a['area'] == $area) {
                    $str .= '<option selected="selected" value="'.$a['area'].'">'.$a['area'].'</option>';
                } else {
                    $str .= '<option value="'.$a['area'].'">'.$a['area'].'</option>';
                }
            }
            $str .= '</select>';
        }
        echo $str;
        exit();
    }
    /*
     * AJAX获取邮政编码
     * @param string $area 地区名称
     */
    public function get_zip($area) {
        $AreaObj = M('AddressArea');
        $zipcode = $AreaObj->getFieldByArea($area,'zipcode');
        echo $zipcode;
        exit();
    }


}
