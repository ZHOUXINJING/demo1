<?php

namespace Fwadmin\Controller;

use Common\Controller\FwadminController;
use Fwadmin\Model\AdminPurviewModel;

class AdminPurviewController extends FwadminController {

    public function index() {
        //获得数据       
        $ModelObj = new AdminPurviewModel();
        if (IS_AJAX) {
            $t = I('get.t');
            $v = I('get.v');
            $id = I('get.i');
            echo $ModelObj->where('purview_id=' . $id)->setField($t, $v);
            exit();
        }
        $results = $ModelObj->getList();
        $this->assign('list', $results); // 赋值数据集
        $this->display();
    }

    public function add() {
        $ModelObj = new AdminPurviewModel();
        if (IS_POST) {
            if ($ModelObj->create()) {
                $ModelObj->check_str = strtolower($ModelObj->check_str);
                $purview_id = $ModelObj->add();
                if ($purview_id > 0) {
                    $this->success('添加成功', U('index'));
                } else {
                    $this->error($ModelObj->getLastSql());
                }
            } else {
                $this->error($ModelObj->getError());
            }
        } else {
            $this->parentList = $ModelObj->getSub(0);
            $this->display();
        }
    }

    public function edit($purview_id) {
        $ModelObj = new AdminPurviewModel();
        if (IS_POST) {
            if ($ModelObj->create()) {
                $ModelObj->check_str = strtolower($ModelObj->check_str);
                $flag = $ModelObj->save();
                if ($flag !== false) {
                    $this->success('修改成功', U('index'));
                } else {
                    $this->error($ModelObj->getLastSql());
                }
            } else {
                $this->error($ModelObj->getError());
            }
        } else {
            $this->parentList = $ModelObj->getSub(0);
            $model = $ModelObj->find($purview_id);
            $this->model = $model;
            $this->display();
        }
    }

    public function del($purview_id) {
        $ModelObj = new AdminPurviewModel();
        $flag = $ModelObj->delete($purview_id);
        if ($flag > 0)
            $this->success('删除成功');
        else
            $this->error($ModelObj->getError());
    }

}

?>