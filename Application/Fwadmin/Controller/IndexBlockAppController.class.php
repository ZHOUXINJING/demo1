<?php

namespace Fwadmin\Controller;

use Common\Controller\FwadminController;

class IndexBlockAppController extends FwadminController {
    public $client_type = 3;
    /*
     * 模型列表 
     */
    public function index() {
        $IndexBlockObj = new \Fwadmin\Model\IndexBlockModel();
        if (IS_AJAX) {
            $t = I('get.t');
            $v = I('get.v');
            $id = I('get.i');
            echo $IndexBlockObj->where('block_id=' . $id)->setField($t, $v);
            exit();
        }
        $this->list = $IndexBlockObj->get_list($this->client_type);
        $this->display();
    }

    /*
     * 添加模型 
     */
    public function add() {
        $ModelObj = new \Fwadmin\Model\IndexBlockModel();
        if (IS_POST) {
            if ($ModelObj->create()) {
                $spec_t = I('post.spec_t');
                $spec_v = I('post.spec_v');
                $spec_arr = array();
                for ($i = 0; $i < count($spec_t); $i++) {
                    if ($spec_t[$i] !== '' || $spec_v[$i] !== '') {
                        $spec_arr[$i]['k'] = $spec_t[$i];
                        $spec_arr[$i]['v'] = $spec_v[$i];
                    }
                }
                $ModelObj->client_type = $this->client_type;
                $ModelObj->spec = serialize($spec_arr);

                $id = $ModelObj->add();
                if ($id > 0)
                    $this->success('添加成功', U('index'));
                else
                    $this->error($ModelObj->getError());
            } else {
                $this->error($ModelObj->getError());
            }
        } else {
            //类别
            $GoodsCatObj = new \Fwadmin\Model\GoodsCatModel();
            $this->goods_cat = $GoodsCatObj->getSub(0);
            //风格
            $this->style_list = $ModelObj->get_style($this->client_type);
            $this->display();
        }
    }

    /*
     * 编辑模型
     */
    public function edit($block_id) {
        $ModelObj = new \Fwadmin\Model\IndexBlockModel();
        if (IS_POST) {
            if ($ModelObj->create()) {
                $spec_t = I('post.spec_t');
                $spec_v = I('post.spec_v');
                $spec_arr = array();
                for ($i = 0; $i < count($spec_t); $i++) {
                    if ($spec_t[$i] !== '' || $spec_v[$i] !== '') {
                        $spec_arr[$i]['k'] = $spec_t[$i];
                        $spec_arr[$i]['v'] = $spec_v[$i];
                    }
                }
                $ModelObj->spec = serialize($spec_arr);
         
                $flag = $ModelObj->save();
                if ($flag !== false)
                    $this->success('修改成功', U('index'));
                else
                    $this->error($ModelObj->getError());
            } else {
                $this->error($ModelObj->getError());
            }
        } else {
            $model = $ModelObj->find($block_id);
            $model['spec_arr'] = unserialize($model['spec']);
            $this->model = $model;

            //类别
            $GoodsCatObj = new \Fwadmin\Model\GoodsCatModel();
            $this->goods_cat = $GoodsCatObj->getSub(0);
            //风格
            $this->style_list = $ModelObj->get_style($this->client_type);

            $this->display();
        }
    }

    /*
     * 添加模型 
     */

    public function adv_add() {
        $ModelObj = new \Fwadmin\Model\IndexBlockModel();
        if (IS_POST) {
            if ($ModelObj->create()) {
                $id = $ModelObj->add();
                if ($id > 0)
                    $this->success('添加成功', U('index'));
                else
                    $this->error($ModelObj->getError());
            } else {
                $this->error($ModelObj->getError());
            }
        } else {
            $this->display();
        }
    }

    /*
     * 编辑模型
     */

    public function adv_edit($block_id) {
        $ModelObj = new \Fwadmin\Model\IndexBlockModel();
        if (IS_POST) {
            if ($ModelObj->create()) {
                $flag = $ModelObj->save();
                if ($flag !== false)
                    $this->success('修改成功', U('index'));
                else
                    $this->error($ModelObj->getError());
            } else {
                $this->error($ModelObj->getError());
            }
        } else {
            $model = $ModelObj->find($block_id);
            $this->model = $model;
            $this->display();
        }
    }

    /*
     * 删除模型
     */

    public function del($block_id) {
        $ModelObj = new \Fwadmin\Model\IndexBlockModel();
        $flag = $ModelObj->del($block_id);
        if ($flag > 0)
            $this->success('删除成功');
        else
            $this->error($ModelObj->getError());
    }

    /**
     * 模型内容管理
     * @param type $block_id
     */
    public function content($block_id) {
        $IndexBlockObj = new \Fwadmin\Model\IndexBlockModel();
        $IndexBlockCon = M('index_block_con');
        if (IS_AJAX) {
            $t = I('get.t');
            $v = I('get.v');
            $id = I('get.i');
            echo $IndexBlockCon->where('id=' . $id)->setField($t, $v);
            exit();
        }

        $IndexStyleObj = M('index_style');
        $block = $IndexBlockObj->find($block_id);
        //样式风格
        $style_id = $block['style_id'];
        $style = $IndexStyleObj->find($style_id);
        //样式项目列表
        $StyleItemObj = M('index_style_item');
        $style_item = $StyleItemObj->where(array('style_id' => $style_id))->order('order_id asc')->select();
        foreach ($style_item as $k => $v) {
            $style_item[$k]['num_add'] = $IndexBlockCon->where(array('style_item_id' => $v['style_item_id'], 'is_show' => 1))->count();
        }

        $this->block = $block;
        $this->style = $style;
        $this->style_item = $style_item;

        //已选择内容列表
        $this->list = $IndexBlockObj->get_block_con($block_id);

        $this->display();
    }

    public function content_del($id) {
        $IndexBlockCon = M('index_block_con');
        $flag = $IndexBlockCon->delete($id);
        if ($flag > 0)
            $this->success('删除成功');
        else
            $this->error('删除失败');
    }

    public function content_edit($block_id, $id) {
        $IndexBlockCon = M('index_block_con');
        if (IS_POST) {
            if ($IndexBlockCon->create()) {
                $flag = $IndexBlockCon->save();
                if ($flag !== false)
                    $this->success('修改成功', U('content', array('block_id' => $block_id)));
                else
                    $this->error($IndexBlockCon->getError());
            } else {
                $this->error($IndexBlockCon->getError());
            }
        } else {
            $model = $IndexBlockCon->find($id);
            $this->model = $model;
            $this->block_id = $block_id;
            $this->display();
        }
    }

    /**
     * 添加模型商品
     * @param int $block_id 模型ID
     * @param int $style_id 样式ID 
     * @param int $style_item_id 样式项目ID
     * @param int $goods_ids 商品ID列表
     */
    public function content_add_pro() {
        $block_id = I('post.block_id');
        $style_id = I('post.style_id');
        $style_item_id = I('post.style_item_id');
        $goods_ids = I('post.goods_ids');
        $IndexBlockObj = new \Fwadmin\Model\IndexBlockModel();
        $goods_ids = trim($goods_ids, ',');
        $goods_ids = explode(',', $goods_ids);
        $flag = 0;
        foreach ($goods_ids as $goods_id) {
            if ($goods_id > 0) {
                $flag += $IndexBlockObj->add_block_goods($block_id, $style_id, $style_item_id, $goods_id);
            }
        }
        if ($flag > 0) {
            echo 'success';
        } else {
            echo 'error';
        }
    }

    /**
     * 添加模型广告
     * @param int $block_id 模型ID
     * @param int $style_id 风格ID
     * @param int $style_item_id 风格栏目ID
     */
    public function content_add_adv($block_id, $style_id, $style_item_id) {
        if (IS_POST) {
            $IndexBlockCon = M('index_block_con');
            if ($IndexBlockCon->create()) {
                $flag = $IndexBlockCon->add();
                if ($flag > 0)
                    $this->success('修改成功', U('content', array('block_id' => $block_id)));
                else
                    $this->error($IndexBlockCon->getError());
            } else {
                $this->error($IndexBlockCon->getError());
            }
        } else {
            $IndexStyleItem = M('index_style_item');
            $style_item = $IndexStyleItem->find($style_item_id);
            $this->style_item = $style_item;
            $this->block_id = $block_id;
            $this->style_id = $style_id;
            $this->style_item_id = $style_item_id;
            $this->display();
        }
    }

    /**
     * 添加模型广告
     * @param int $block_id 模型ID
     * @param int $style_id 风格ID
     * @param int $style_item_id 风格栏目ID
     */
    public function content_edit_adv($block_id, $id) {
        $IndexBlockCon = M('index_block_con');
        if (IS_POST) {
            if ($IndexBlockCon->create()) {
                $flag = $IndexBlockCon->save();
                if ($flag !== false)
                    $this->success('修改成功', U('content', array('block_id' => $block_id)));
                else
                    $this->error($IndexBlockCon->getError());
            } else {
                $this->error($IndexBlockCon->getError());
            }
        } else {
            $model = $IndexBlockCon->find($id);
            $this->model = $model;
            $this->block_id = $block_id;
            $this->display();
        }
    }

}
