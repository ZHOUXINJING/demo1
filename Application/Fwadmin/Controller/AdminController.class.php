<?php

namespace Fwadmin\Controller;

use Common\Controller\FwadminController;
use Fwadmin\Model\AdminModel;

class AdminController extends FwadminController {
    /*
     * 管理员列表
     */
    public function adminList() {
        //获得数据       
        $ModelObj = new AdminModel();
        if (IS_AJAX) {
            $t = I('get.t');
            $v = I('get.v');
            $id = I('get.i');
            echo $ModelObj->where('admin_id=' . $id)->setField($t, $v);
            exit();
        }
        $results = $ModelObj->search($conditions);
        $lists = $results['list'];
        $this->assign('list', $lists); // 赋值数据集
        $this->assign('page', $results['pageShow']); // 赋值分页输出
        $this->display();
    }

    /*
     * 添加管理员
     */
    public function adminAdd() {
        $ModelObj = new AdminModel();
        if (IS_POST) {
            if ($ModelObj->create()) {
                $ModelObj->password = strongmd5($ModelObj->password);
                $username = $ModelObj->username;
                $admin_id = $ModelObj->add();
                if ($admin_id > 0) {
                    $RecordObj = new \Fwadmin\Model\SystemRecordModel();
                    $RecordObj->addrecord('新增管理员:' . $username .'(ID:' . $admin_id . ')');
                    $this->success('添加成功', U('Admin/adminList'));
                } else {
                    $this->error($ModelObj->getError());
                }
            } else {
                $this->error($ModelObj->getError());
            }
        } else {
            $AGObj = new \Fwadmin\Model\AdminGroupModel();
            $this->groupList = $AGObj->select();
            $this->display();
        }
    }

    /*
     * 编辑管理员
     */
    public function adminEdit($admin_id) {
        $ModelObj = new AdminModel();
        if (IS_POST) {
            if ($ModelObj->create()) {
                if ($ModelObj->password != '')
                    $ModelObj->password = strongmd5($ModelObj->password);
                else
                    unset($ModelObj->password);
                $username = $ModelObj->username;
                $flag = $ModelObj->save();
                if ($flag !== false) {
                    $RecordObj = new \Fwadmin\Model\SystemRecordModel();
                    $RecordObj->addrecord('修改管理员:' . $username .'(ID:' . $admin_id . ')');
                    $this->success('修改成功', U('Admin/adminList'));
                } else {
                    $this->error($ModelObj->getError());
                }
            } else {
                $this->error($ModelObj->getError());
            }
        } else {
            $model = $ModelObj->find($admin_id);
            $this->model = $model;
            $AGObj = new \Fwadmin\Model\AdminGroupModel();
            $this->groupList = $AGObj->select();
            $this->display();
        }
    }

    /*
     * 删除管理员
     */
    public function adminDel($admin_id) {
        $ModelObj = new AdminModel();
        $model = $ModelObj->find($admin_id);
        $flag = $ModelObj->delete($admin_id);
        if ($flag > 0) {
            $RecordObj = new \Fwadmin\Model\SystemRecordModel();
            $RecordObj->addrecord('删除管理员:' . $model['username'] .'(ID:' . $admin_id . ')');
            $this->success('删除成功');
        } else {
            $this->error($ModelObj->getError());
        }
    }

}

?>