<?php

namespace Fwadmin\Controller;

use Common\Controller\FwadminController;

class IndexStyleController extends FwadminController {
    
    /*
     * 模块风格列表 
     */
    public function index() {
        $IndexStyleObj = new \Fwadmin\Model\IndexStyleModel();
        if (IS_AJAX) {
            $t = I('get.t');
            $v = I('get.v');
            $id = I('get.i');
            echo $IndexStyleObj->where('style_id=' . $id)->setField($t, $v);
            exit();
        }
        $this->list = $IndexStyleObj->get_list();
        $this->display();
    }

    /*
     * 添加模块风格
     */
    public function add() {
        $ModelObj = new \Fwadmin\Model\IndexStyleModel();
        if (IS_POST) {
            if ($ModelObj->create()) {
                $style_id = $ModelObj->add();
                if ($style_id > 0) {
                    $arr_style_item_name = I('post.style_item_name');
                    $arr_type = I('post.type');
                    $arr_pic_width = I('post.pic_width');
                    $arr_pic_height = I('post.pic_height');
                    $arr_num = I('post.num');
                    for ($i = 0; $i < count($arr_style_item_name); $i++) {
                        if($arr_style_item_name[$i] != "") {
                            $this->add_style_item($style_id, $arr_style_item_name[$i], $arr_type[$i], $arr_pic_width[$i], $arr_pic_height[$i], $arr_num[$i], $i);
                        }
                    }
                    $this->success('添加成功', U('index'));
                } else {
                    $this->error($ModelObj->getError());
                }
            } else {
                $this->error($ModelObj->getError());
            }
        } else {
            $this->display();
        }
    }

    /*
     * 编辑模块风格
     */
    public function edit($style_id) {
        $ModelObj = new \Fwadmin\Model\IndexStyleModel();
        if (IS_POST) {
            if ($ModelObj->create()) {
                $flag = $ModelObj->save();
                if ($flag !== false) {
                    $arr_style_item_name = I('post.style_item_name');
                    $arr_type = I('post.type');
                    $arr_pic_width = I('post.pic_width');
                    $arr_pic_height = I('post.pic_height');
                    $arr_num = I('post.num');
                    for ($i = 0; $i < count($arr_style_item_name); $i++) {
                        $style_item_id = I('post.style_item_id_'.($i+1),0);
                        if($style_item_id==0) {
                            if($arr_style_item_name[$i] != "") {
                                $this->add_style_item($style_id, $arr_style_item_name[$i], $arr_type[$i], $arr_pic_width[$i], $arr_pic_height[$i], $arr_num[$i], $i);
                            }
                        } else {
                            if($arr_style_item_name[$i] == "") {
                                $this->del_style_item($style_item_id);
                            } else {
                                $this->edit_style_item($style_item_id, $arr_style_item_name[$i], $arr_type[$i], $arr_pic_width[$i], $arr_pic_height[$i], $arr_num[$i]);
                            }
                        }
                    }
                    
                    
                    $this->success('修改成功', U('index'));
                } else {
                    $this->error($ModelObj->getError());
                }
            } else {
                $this->error($ModelObj->getError());
            }
        } else {
            $model = $ModelObj->find($style_id);
            $this->model = $model;
            
            $ItemObj = M('index_style_item');
            $this->style_item = $ItemObj->where(array('style_id'=>$style_id))->select(); 
            
             
            $this->display();
        }
    }
    
    /**
     * 删除模块风格
     * @param int $style_id 模型ID
     */
    public function del($style_id) {
        $ModelObj = new \Fwadmin\Model\IndexStyleModel();
        $flag = $ModelObj->del($style_id);
        if ($flag > 0)
            $this->success('删除成功');
        else
            $this->error($ModelObj->getError());
    }
    
    /**
     * 增加模块风格内容项 
     * @param int $style_id 风格模块ID
     * @param string $style_item_name 内容项名称
     * @param string $type 类型
     * @param int $pic_width 图片宽度
     * @param int $pic_height 图片高度
     * @param int $num 数量
     * @param int $order_id 顺序编号
     */
    public function add_style_item($style_id,$style_item_name,$type,$pic_width,$pic_height,$num,$order_id) {
        $ItemObj = M('index_style_item');
        $data['style_id'] = $style_id;
        $data['style_item_name'] = $style_item_name;
        $data['type'] = $type;
        $data['pic_width'] = $pic_width;
        $data['pic_height'] = $pic_height;
        $data['num'] = $num;
        $data['order_id'] = $order_id;
        return $ItemObj->data($data)->add();
    }
    
    /**
     * 修改模块风格内容项
     * @param int $style_item_id 风格内容项ID
     * @param string $style_item_name 内容项名称
     * @param string $type 类型
     * @param int $pic_width 图片宽度
     * @param int $pic_height 图片高度
     * @param int $num 数量
     */
    public function edit_style_item($style_item_id,$style_item_name,$type,$pic_width,$pic_height,$num) {
        $ItemObj = M('index_style_item');
        $data['style_item_id'] = $style_item_id;
        $data['style_item_name'] = $style_item_name;
        $data['type'] = $type;
        $data['pic_width'] = $pic_width;
        $data['pic_height'] = $pic_height;
        $data['num'] = $num;
        return $ItemObj->data($data)->save();
    }
    
    /**
     * 删除模块风格内容项
     * @param type $style_item_id 风格内容项ID
     * @return int 是否成功
     */
    public function del_style_item($style_item_id) {
        $ItemObj = M('index_style_item');
        return $ItemObj->delete($style_item_id);
    }
    

    

}
