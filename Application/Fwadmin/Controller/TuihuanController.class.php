<?php
namespace Fwadmin\Controller;
use Common\Controller\FwadminController;
use Fwadmin\Model\MemberModel;
use Fwadmin\Model\TuihuanModel;

class TuihuanController extends FwadminController {
	public function index() {
		//获得数据       
		$ThObj=new TuihuanModel();
		$th_status=I('get.th_status','');
		$conditions = array();
		if($th_status){
		$conditions['th_status']= $th_status;
		}
		$conditions = array();
		$this->th_status =$th_status ;
	    $conditions['typeid']= 1;
        $param = array('th_status'=>'th_status');
        $this->paramValue($conditions,$param,'GET');
        $results = $ThObj->search($conditions,'id desc',true,$export=false,$page=8);
        $lists = $results['list'];
		$count = count($lists);
		for($i=0;$i<$count;$i++){
		 $lists[$i]['Cart']= M('order_goods')->where(array('id'=>$lists[$i]['cart_id']))->find();
		}
		//print_r($lists);
		$this->tuihuan_array = tuihuan_status_array(); 
        $this->assign('list', $lists); // 赋值数据集
        $this->assign('page', $results['pageShow']); // 赋值分页输出
		$this->list_url = urlencode(__SELF__);
		$this->display();
	}

	

	public function info($id){
    	$ThObj=new TuihuanModel();
		$model=$ThObj->where('id='.$id)->find();
		$this->picture_list = stringToarray($model['picture']);
		$model2 = M('order_goods')->where('id='.$model['cart_id'])->find();
		$this->model=$model;
		$this->display();

	}

	public function tuihuan_action(){
		if (IS_POST) {
		 $id=I('post.id');
		 $data['th_status']=I('post.th_status');
		$ThObj=new TuihuanModel();
		  $flag=$ThObj->where('id='.$id)->save($data);
		   if($flag!== false)
		    $this->success('修改成功');
			else
			$this->error($ThObj->getError());
		}
	}
   
   //换货列表
   	public function huan() {
		//获得数据       
		$ThObj=new TuihuanModel();
        $CartObj=new OrderCartModel();

		$th_status=I('get.th_status','');
		$conditions = array();
		if($th_status){
		$conditions['th_status']= $th_status;
		}
		$conditions = array();
		$this->th_status =$th_status ;
		 $conditions['typeid']= 2;
        $param = array('th_status'=>'th_status');
        $this->paramValue($conditions,$param,'GET');
        $results = $ThObj->search($conditions,'id desc',true,$export=false,$page=8);
        $lists = $results['list'];
		$count = count($lists);
		for($i=0;$i<$count;$i++){
		 $lists[$i]['Cart']=$CartObj->find($lists[$i]['cart_id']);
		}
        $this->assign('list', $lists); // 赋值数据集
        $this->assign('page', $results['pageShow']); // 赋值分页输出
		$this->display();
	}

	

	public function info2($id){
   	  $ThObj=new TuihuanModel();
		$CartObj = new OrderCartModel(); 
	    if(IS_AJAX) {
           $t = I('get.t');
           $v = I('get.v');
		   $id= I('get.id');
            echo $ThObj->where('id='.$id)->setField($t,$v);
			exit();
		}
		$model=$ThObj->where('id='.$id)->find();
		$model2 = $CartObj->where('id='.$model['cart_id'])->find();
		$this->model=$model;
		$this->display();

	}

	

}