<?php

namespace Fwadmin\Controller;

use Common\Controller\FwadminController;
use Fwadmin\Model\HotsearchModel;

class HotsearchController extends FwadminController {

    public function index() {
        //获得数据       
        $ModelObj = new HotsearchModel();
        if (IS_AJAX) {
            $t = I('get.t');
            $v = I('get.v');
            $id = I('get.i');
            echo $ModelObj->where('id=' . $id)->setField($t, $v);
            exit();
        }
        $results = $ModelObj->search($conditions);
        $lists = $results['list'];
        $this->assign('list', $lists); // 赋值数据集
        $this->assign('page', $results['pageShow']); // 赋值分页输出
        $this->display();
    }

    public function add() {
        $ModelObj = new HotsearchModel();
        if (IS_POST) {
            if ($ModelObj->create()) {
                $id = $ModelObj->add();
                if ($id > 0)
                    $this->success('添加成功', U('Hotsearch/index'));
                else
                    $this->error($ModelObj->getError());
            } else {
                $this->error($ModelObj->getError());
            }
        } else {
            $this->display();
        }
    }

    public function edit($id) {
        $ModelObj = new HotsearchModel();
        if (IS_POST) {
            if ($ModelObj->create()) {
                $flag = $ModelObj->save();
                if ($flag !== false)
                    $this->success('修改成功', U('Hotsearch/index'));
                else
                    $this->error($ModelObj->getError());
            }
        } else {
            $model = $ModelObj->find($id);
            $this->model = $model;
            $this->display();
        }
    }

    public function del($id) {
        $ModelObj = new HotsearchModel();
        $flag = $ModelObj->delete($id);
        if ($flag > 0)
            $this->success('删除成功');
        else
            $this->error($ModelObj->getError());
    }

}
