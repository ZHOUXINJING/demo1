<?php

namespace Fwadmin\Controller;

use Fwadmin\Model\SystemErrorModel;
use Common\Controller\FwadminController;

class SystemErrorController extends FwadminController {

    /**
     * 系统错误记录列表
     */
    public function index() {
        $ModelObj = new SystemErrorModel();
        //获得数据       
        $results = $ModelObj->search();
        $lists = $results['list'];
        $AdminObj = M('admin');
        foreach ($lists as $k => $v) {
            $lists[$k]['adminname'] = $AdminObj->getFieldByAdminId($v['admin_id'],'username');
        }
        $this->assign('list', $lists); // 赋值数据集
        $this->assign('page', $results['pageShow']); // 赋值分页输出
        $this->display();
    }

}
