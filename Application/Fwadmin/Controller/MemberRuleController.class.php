<?php

namespace Fwadmin\Controller;

use Common\Controller\FwadminController;
use Fwadmin\Model\MemberRuleModel;

class MemberRuleController extends FwadminController {

    public function index() {
        //获得数据       
        $ModelObj = new MemberRuleModel();
        if (IS_AJAX) {
            $t = I('get.t');
            $v = I('get.v');
            $rule_id = I('get.i');
            echo $ModelObj->where('rule_id=' . $rule_id)->setField($t, $v);
            exit();
        }
        $this->list = $ModelObj->select();
        $this->display();
    }
    
    public function edit($rule_id) {
        $ModelObj = new MemberRuleModel();
        if (IS_POST) {
            if ($ModelObj->create()) {
                $flag = $ModelObj->save();
                if ($flag !== false)
                    $this->success('修改成功', U('MemberRule/index'));
                else
                    $this->error($ModelObj->getError());
            }
        } else {
            $model = $ModelObj->find($rule_id);
            $this->model = $model;
            $this->display();
        }
    }

    

}
