<?php

namespace Fwadmin\Controller;

use Think\Controller;
use Common\Controller\FwadminController;
use Fwadmin\Model\ExtensionModel;

// 推广模块         ( 扩展 )
class ExtensionController extends FwadminController
{
    /**
     *  上传图片
     */
    public function upload($string)
    {
        $upload = new \Think\Upload();
        $upload->exts      =     array('jpg', 'gif', 'png', 'jpeg');
        $upload->rootPath  =     './Uploads/detail/';
        $info   =   $upload->uploadOne($_FILES[$string]);

        return '/Uploads/detail/'.$info['savepath'].$info['savename'];
    }


    /**
     *  推广介绍
     */
    public function information()
    {
        $em = new ExtensionModel();
        if (IS_POST) {
            $em->set('ex_register', I('post.ex_register'));
            $em->set('ex_divide', I('post.ex_divide'));
            $em->set('ex_profit', I('post.ex_profit'));
            $em->set('ex_level_1', I('post.ex_level_1'));
            $em->set('ex_profit_1', I('post.ex_profit_1'));
            $em->set('ex_level_2', I('post.ex_level_2'));
            $em->set('ex_profit_2', I('post.ex_profit_2'));
            $em->set('ex_condition_2', I('post.ex_condition_2'));
            $em->set('ex_level_3', I('post.ex_level_3'));
            $em->set('ex_profit_3', I('post.ex_profit_3'));
            $em->set('ex_condition_3', I('post.ex_condition_3'));
            $em->set('editorValue', $_POST['editorValue']);
            $em->set('ex_coupon', I('post.ex_coupon'));
            $em->set('ex_coupon_time', I('post.ex_coupon_time'));
            $em->set('ex_all_coupon', I('post.ex_all_coupon'));
            $em->set('ex_good_push', I('post.ex_good_push'));
            $em->set('ex_share_info', I('post.ex_share_info'));
            if (empty($_FILES['ex_show_img']['error'])) {
                $em->set('ex_show_img', $this->upload('ex_show_img'));
            }
            if (empty($_FILES['ex_reg_img']['error'])) {
                $em->set('ex_reg_img', $this->upload('ex_reg_img'));
            }
            if (empty($_FILES['ex_share_img']['error'])) {
                $em->set('ex_share_img', $this->upload('ex_share_img'));
            }
        }

        $this->ex_register    = $em->get('ex_register');
        $this->ex_divide      = $em->get('ex_divide');
        $this->ex_level_1     = $em->get('ex_level_1');
        $this->ex_profit_1    = $em->get('ex_profit_1');
        $this->ex_level_2     = $em->get('ex_level_2');
        $this->ex_profit_2    = $em->get('ex_profit_2');
        $this->ex_condition_2 = $em->get('ex_condition_2');
        $this->ex_level_3     = $em->get('ex_level_3');
        $this->ex_profit_3    = $em->get('ex_profit_3');
        $this->ex_condition_3 = $em->get('ex_condition_3');
        $this->editorValue    = $em->get('editorValue');
        $this->ex_show_img    = $em->get('ex_show_img');
        $this->ex_coupon      = $em->get('ex_coupon');
        $this->ex_coupon_time = $em->get('ex_coupon_time');
        $this->ex_all_coupon  = $em->get('ex_all_coupon');
        $this->ex_good_push   = $em->get('ex_good_push');
        $this->ex_reg_img     = $em->get('ex_reg_img');
        $this->ex_profit     = $em->get('ex_profit');
        $this->ex_share_img  = $em->get('ex_share_img');
        $this->ex_share_info = $em->get('ex_share_info');
        $this->display();
    }
}