<?php

namespace Fwadmin\Controller;

use Common\Controller\FwadminController;
use Fwadmin\Model\GoodsBrandModel;
//商品品牌

class GoodsBrandController extends FwadminController {

    /**
     * 品牌列表
     */
    public function index() {
        $ModelObj = new GoodsBrandModel();
        if (IS_AJAX) {
            $t = I('get.t');
            $v = I('get.v');
            $id = I('get.i');
            echo $ModelObj->where('brand_id=' . $id)->setField($t, $v);
            exit();
        }
        //获得数据
        $result = $ModelObj->search(array());
        $this->list = $result['list'];
        $this->page = $result['page'];
        $this->display();
    }

    /**
     * 添加品牌
     */
    public function add() {
        $ModelObj = new GoodsBrandModel();
        if (IS_POST) {
            if ($ModelObj->create()) {
                $brand_id = $ModelObj->add();
                if ($brand_id > 0)
                    $this->success('添加成功', U('index'));
                else
                    $this->error($ModelObj->getError());
            } else {
                $this->error($ModelObj->getError());
            }
        } else {
            $this->display();
        }
    }

     /**
     * 编辑品牌
     */
    public function edit($brand_id) {
        $ModelObj = new GoodsBrandModel();
        if (IS_POST) {
            if ($ModelObj->create()) {
                $flag = $ModelObj->save();
                if ($flag !== false)
                    $this->success('修改成功', U('index'));
                else
                    $this->error($ModelObj->getError());
            } else {
                $this->error($ModelObj->getError());
            }
        }else {
            $model = $ModelObj->find($brand_id);
            $this->model = $model;
            $this->display();
        }
    }
    
     /**
     * 删除品牌
     */
    public function del($brand_id) {
        $ModelObj = new GoodsBrandModel();
        $flag = $ModelObj->del($brand_id);
        if ($flag > 0)
            $this->success('删除成功');
        else
            $this->error('删除失败');
    }
}