<?php

namespace Fwadmin\Controller;

use Common\Controller\FwadminController;
use Fwadmin\Model\MemberModel;
use Fwadmin\Model\MemberMessageModel;
use Fwadmin\Model\MemberLevelModel;

class MemberMessageController extends FwadminController {

    /**
     * 已发放优惠券
     */
    public function index() {
        
        $MemberMessageObj = new MemberMessageModel();
       
        $conditions = array();
        $param = array('username'=>'username','time_begin'=>'time_begin','time_end'=>'time_end','title' => 'title');
        $this->paramValue($conditions, $param);
        $results = $MemberMessageObj->search($conditions);
       
        $lists = $results['list'];
        $this->assign('list', $lists); // 赋值数据集
        $this->assign('page', $results['page']); // 赋值分页输出
        
        $this->display();
    }
    
    /**
     * 发放优惠券
     */
    public function add() {
        if(IS_POST) {
            $give_type = I('post.give_type','');
            $level_ids = I('post.level_ids');
            $member_list = I('post.member_list','');
            $title = I('post.title','');
			$detail = I('post.detail','');
			$url = I('post.url','');
            $map['status'] = 1;//只给正常的会员发消息
            $MemberObj = new MemberModel();
            $MemberMessageObj = new MemberMessageModel();
            if($give_type == "level") {//按会员等级
                $level_ids_str = implode(',', $level_ids);
                $map['level_id'] = array('in',$level_ids_str);
            } elseif($give_type == "single") {//指定会员
                $map['phone|email'] = array('in',$member_list);
            }
            $member_list = $MemberObj->field('member_id')->where($map)->select();
            $count = 0;
            foreach($member_list as $k => $v) {
                $flag =  $MemberMessageObj->add_record($v['member_id'], $title, $detail,$url);
                if($flag > 0) $count++;
            }
            $this->success('发送'.$count.'条消息成功！');
            
            
        } else {
            //会员等级
            $MemberLevelObj = new MemberLevelModel();
            $levels = $MemberLevelObj->getSel();
            $this->level_sel = $levels;

            $this->display();
        }
    }
    
     /**
     * 批量删除优惠券
     * @return int 返回删除数目
     */
    public function delsel() {
        $MemberMessageObj = new MemberMessageModel();
        $selid = I('post.selid');
        $count = 0;
        foreach($selid as $id) {
            $count += $MemberMessageObj->delete($id);
        }
        if($count > 0) {
            $this->success('删除'.$count.'条消息成功');
        } else {
            $this->success('删除失败');
        }
    }

    

}
