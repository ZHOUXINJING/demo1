<?php

namespace Fwadmin\Controller;

use Fwadmin\Model\SeoPageModel;
use Common\Controller\FwadminController;

class SeoPageController extends FwadminController {

    /*
     * SEO页面配置列表
     */
    public function index() {
        $ModelObj = new SeoPageModel();
        if (IS_AJAX) {
            $t = I('get.t');
            $v = I('get.v');
            $id = I('get.i');
            echo $ModelObj->where('id=' . $id)->setField($t, $v);
            exit();
        }
        //获得数据       
        $results = $ModelObj->search();
        $lists = $results['list'];
        $this->assign('list', $lists); // 赋值数据集
        $this->assign('page', $results['pageShow']); // 赋值分页输出
        $this->display();
    }

    /*
     * 添加
     */
    public function add() {
        $ModelObj = new SeoPageModel();
        if (IS_POST) {
            if ($ModelObj->create()) {
                $id = $ModelObj->add();
                if ($id > 0)
                    $this->success('添加成功', U('SeoPage/index'));
                else
                    $this->error($ModelObj->getError());
            } else {
                $this->error($ModelObj->getError());
            }
        } else {
            $this->display();
        }
    }

    /*
     * 修改
     */
    public function edit($id) {
        $ModelObj = new SeoPageModel();
        if (IS_POST) {
            if ($ModelObj->create()) {
                $flag = $ModelObj->save();
                if ($flag !== false)
                    $this->success('修改成功', U('SeoPage/index'));
                else
                    $this->error($ModelObj->getError());
            }
        }else {
            $model = $ModelObj->find($id);
            $this->model = $model;
            $this->display();
        }
    }
    
    /*
     * 删除
     */
    public function del($id) {
        $ModelObj = new SeoPageModel();
        $flag = $ModelObj->del($id);
        if ($flag > 0)
            $this->success('删除成功');
        else
            $this->error($ModelObj->getError());
    }

}
