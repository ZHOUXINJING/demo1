<?php

namespace Fwadmin\Controller;

use Common\Controller\FwadminController;

class TongjiController extends FwadminController {
    
    /**
     * 订单销售额统计
     */
    public function order() {
        $conditions = array();
        $param = array('btime' => 'btime', 'etime' => 'etime', 'type' => 'type');
        $this->paramValue($conditions, $param, 'GET');
        //默认最近30天
        if(empty($conditions['btime']) && empty($conditions['etime']) && empty($conditions['type'])) {
            $conditions['etime'] = date('Y-m-d',time());
            $conditions['btime'] = date('Y-m-d',strtotime('-30 day', strtotime($conditions['etime'])));
            $this->quick = 30;
            $this->btime = $conditions['btime'];
            $this->etime = $conditions['etime'];
            $this->type = '日'; 
        }
        if (I('get.submit','') == '导出报表') {
            $this->order_export($conditions);
        }
        $OrderObj = new \Fwadmin\Model\OrderModel();
        $result = $OrderObj->tongji_total($conditions);
        $list = $result['list'];
        $this->total = $result['total'];
        $this->list = $list;
        
        //统计图表
        $chart_array = array();
        foreach($list as $k => $v) {
            $chart_array[$v['day']] = $v['total'];
        }
        //补全日期
        if(!empty($conditions['btime'])) {
            if(empty($conditions['etime'])) {
                $conditions['etime'] = date('Y-m-d',time());
            }
            $dt_start = strtotime($conditions['btime']);
            $dt_end = strtotime($conditions['etime']);
            if($conditions['type'] == '月') {
                $str_date_1 = 'month';
                $str_date_format = 'Y-m';
             } elseif ($conditions['type'] == '年') {
                $str_date_1 = 'year';
                $str_date_format = 'Y';
             } else {
                $str_date_1 = 'day';
                $str_date_format = 'Y-m-d';
             }
             while ($dt_start <= $dt_end) {
                $date_start = date($str_date_format,$dt_start);
                if(empty($chart_array[$date_start])) {
                    $chart_array[$date_start] = 0;
                }
                $dt_start = strtotime('+1 '.$str_date_1, $dt_start);
            }
        }
        //赋值数据图表
        $chart_x = '';
        $chart_y = '';
        ksort($chart_array);
        foreach($chart_array as $k2 => $v2) {
            $chart_x .= "'".$k2."',";
            $chart_y .= $v2 .',';
        }
        //赋值统计图表数据
        $this->chart_x = trim($chart_x,',');
        $this->chart_y = trim($chart_y,',');
        
        $this->display(); 
    }
    /**
     * 订单销售额统计-导出报表
     */
    public function order_export($conditions) {
        $OrderObj = new \Fwadmin\Model\OrderModel();
        $result = $OrderObj->tongji_total($conditions);
        $total = $result['total'];
        $list = $result['list'];
        $type = 'day';
        if($conditions == '月') {
            $type = 'month';
        } elseif ($conditions['type'] == '年') {
            $type = 'year';
        }
        $filename = 'tongji_order_money_' . $conditions['btime'] .'_'. $conditions['etime'] .'_'.$type;
        $sheet1 = '订单销售额统计';
        $data_title = array();
        $data_title['A1'] = '编号';
        $data_title['B1'] = '日期';
        $data_title['C1'] = '销售额';

        $data_content = array();
        foreach ($list as $k => $v) {
            $num = $k + 2;
            $data_content['A' . $num] = $k + 1;
            $data_content['B' . $num] = $v['day'];
            $data_content['C' . $num] = $v['total'];
        }
        excelExport($filename, $data_title, $data_content, $sheet1);
    }
    
    /**
     * 订单数统计
     */
    public function order_num() {
        $conditions = array();
        $param = array('btime' => 'btime', 'etime' => 'etime', 'type' => 'type');
        $this->paramValue($conditions, $param, 'GET');
         //默认最近30天
        if(empty($conditions['btime']) && empty($conditions['etime']) && empty($conditions['type'])) {
            $conditions['etime'] = date('Y-m-d',time());
            $conditions['btime'] = date('Y-m-d',strtotime('-30 day', strtotime($conditions['etime'])));
            $this->quick = 30;
            $this->btime = $conditions['btime'];
            $this->etime = $conditions['etime'];
            $this->type = '日'; 
        }
        if (I('get.submit','') == '导出报表') {
            $this->order_num_export($conditions);
        }
        $OrderObj = new \Fwadmin\Model\OrderModel();
        $result = $OrderObj->tongji_total_num($conditions);
        $list = $result['list'];
        $this->total = $result['total'];
        $this->list = $list;
        
        //统计图表
        $chart_array = array();
        foreach($list as $k => $v) {
            $chart_array[$v['day']] = $v['total'];
        }
        //补全日期
        if(!empty($conditions['btime'])) {
            if(empty($conditions['etime'])) {
                $conditions['etime'] = date('Y-m-d',time());
            }
            $dt_start = strtotime($conditions['btime']);
            $dt_end = strtotime($conditions['etime']);
            if($conditions['type'] == '月') {
                $str_date_1 = 'month';
                $str_date_format = 'Y-m';
             } elseif ($conditions['type'] == '年') {
                $str_date_1 = 'year';
                $str_date_format = 'Y';
             } else {
                $str_date_1 = 'day';
                $str_date_format = 'Y-m-d';
             }
             while ($dt_start <= $dt_end) {
                $date_start = date($str_date_format,$dt_start);
                if(empty($chart_array[$date_start])) {
                    $chart_array[$date_start] = 0;
                }
                $dt_start = strtotime('+1 '.$str_date_1, $dt_start);
            }
        }
        //赋值数据图表
        $chart_x = '';
        $chart_y = '';
        ksort($chart_array);
        foreach($chart_array as $k2 => $v2) {
            $chart_x .= "'".$k2."',";
            $chart_y .= $v2 .',';
        }
        //赋值统计图表数据
        $this->chart_x = trim($chart_x,',');
        $this->chart_y = trim($chart_y,',');
        
        $this->display(); 
    }
    /**
     * 订单数统计-导出报表
     */
    public function order_num_export($conditions) {
        $OrderObj = new \Fwadmin\Model\OrderModel();
        $result = $OrderObj->tongji_total_num($conditions);
        $total = $result['total'];
        $list = $result['list'];
        $type = 'day';
        if($conditions == '月') {
            $type = 'month';
        } elseif ($conditions['type'] == '年') {
            $type = 'year';
        }
        $filename = 'tongji_order_num_' . $conditions['btime'] .'_'. $conditions['etime'] .'_'.$type;
        $sheet1 = '订单数统计';
        $data_title = array();
        $data_title['A1'] = '编号';
        $data_title['B1'] = '日期';
        $data_title['C1'] = '订单数';

        $data_content = array();
        foreach ($list as $k => $v) {
            $num = $k + 2;
            $data_content['A' . $num] = $k + 1;
            $data_content['B' . $num] = $v['day'];
            $data_content['C' . $num] = $v['total'];
        }
        excelExport($filename, $data_title, $data_content, $sheet1);
    }
    
    /**
     * 会员注册量统计
     */
    public function member() {
        $conditions = array();
        $param = array('btime' => 'btime', 'etime' => 'etime', 'type' => 'type');
        $this->paramValue($conditions, $param, 'GET');
        //默认最近30天
        if(empty($conditions['btime']) && empty($conditions['etime']) && empty($conditions['type'])) {
            $conditions['etime'] = date('Y-m-d',time());
            $conditions['btime'] = date('Y-m-d',strtotime('-30 day', strtotime($conditions['etime'])));
            $this->quick = 30;
            $this->btime = $conditions['btime'];
            $this->etime = $conditions['etime'];
            $this->type = '日'; 
        }
        if (I('get.submit','') == '导出报表') {
            $this->member_export($conditions);
        }
        $MemberObj = new \Fwadmin\Model\MemberModel();
        $result = $MemberObj->tongji_zhuce($conditions);
        $this->total = $result['total'];
        $list = $result['list'];
        $this->list = $list;
        //统计图表
        $chart_array = array();
        foreach($list as $k => $v) {
            $chart_array[$v['day']] = $v['num'];
        }
        //补全日期
        if(!empty($conditions['btime'])) {
            if(empty($conditions['etime'])) {
                $conditions['etime'] = date('Y-m-d',time());
            }
            $dt_start = strtotime($conditions['btime']);
            $dt_end = strtotime($conditions['etime']);
            if($conditions['type'] == '月') {
                $str_date_1 = 'month';
                $str_date_format = 'Y-m';
             } elseif ($conditions['type'] == '年') {
                $str_date_1 = 'year';
                $str_date_format = 'Y';
             } else {
                $str_date_1 = 'day';
                $str_date_format = 'Y-m-d';
             }
             while ($dt_start <= $dt_end) {
                $date_start = date($str_date_format,$dt_start);
                if(empty($chart_array[$date_start])) {
                    $chart_array[$date_start] = 0;
                }
                $dt_start = strtotime('+1 '.$str_date_1, $dt_start);
            }
        }
        //赋值数据图表
        $chart_x = '';
        $chart_y = '';
        ksort($chart_array);
        foreach($chart_array as $k2 => $v2) {
            $chart_x .= "'".$k2."',";
            $chart_y .= $v2 .',';
        }
        //赋值统计图表数据
        $this->chart_x = trim($chart_x,',');
        $this->chart_y = trim($chart_y,',');
        
        $this->display(); 
    }

    /**
     * 会员注册量统计-导出报表
     */
    public function member_export($conditions) {
        $MemberObj = new \Fwadmin\Model\MemberModel();
        $result = $MemberObj->tongji_zhuce($conditions);
        $total = $result['total'];
        $list = $result['list'];
        $type = 'day';
        if($conditions == '月') {
            $type = 'month';
        } elseif ($conditions['type'] == '年') {
            $type = 'year';
        }
        $filename = 'tongji_member_' . $conditions['btime'] .'_'. $conditions['etime'] .'_'.$type;
        $sheet1 = '会员注册量统计';
        $data_title = array();
        $data_title['A1'] = '编号';
        $data_title['B1'] = '日期';
        $data_title['C1'] = '注册会员数';

        $data_content = array();
        foreach ($list as $k => $v) {
            $num = $k + 2;
            $data_content['A' . $num] = $k + 1;
            $data_content['B' . $num] = $v['day'];
            $data_content['C' . $num] = $v['num'];
        }
        excelExport($filename, $data_title, $data_content, $sheet1);
    }

    /* 
     * 商品收藏排行榜 
     */
    public function fav() {
        $conditions = array();
        $param = array('btime' => 'btime', 'etime' => 'etime');
        $this->paramValue($conditions, $param, 'GET');
        if (I('get.submit','') == '导出报表') {
            $this->fav_export($conditions);
        }
        $MemberFavObj = new \Fwadmin\Model\MemberFavModel();
        $this->list = $MemberFavObj->tongji_goods($conditions);
        $this->display();
    }
    /**
     * 商品收藏排行榜-导出报表
     */
    public function fav_export($conditions) {
        $MemberFavObj = new \Fwadmin\Model\MemberFavModel();
        $list = $MemberFavObj->tongji_goods($conditions);
        $filename = 'tongji_fav_' . $conditions['btime'] .'_'. $conditions['etime'];
        $sheet1 = '商品收藏排行榜';
        $data_title = array();
        $data_title['A1'] = '编号';
        $data_title['B1'] = '商品名称';
        $data_title['C1'] = '收藏数量';

        $data_content = array();
        foreach ($list as $k => $v) {
            $num = $k + 2;
            $data_content['A' . $num] = $k + 1;
            $data_content['B' . $num] = $v['goods_name'];
            $data_content['C' . $num] = $v['num'];
        }
        excelExport($filename, $data_title, $data_content, $sheet1);
    }
    
    /* 
     * 商品销售排行榜 
     */
    public function goods() {
        $conditions = array();
        $param = array('btime' => 'btime', 'etime' => 'etime');
        $this->paramValue($conditions, $param, 'GET');
        if (I('get.submit','') == '导出报表') {
            $this->goods_export($conditions);
        }
        $OrderObj = new \Fwadmin\Model\OrderModel();
        $this->list = $OrderObj->tongji_goods($conditions);
        $this->display();
    }
     /**
     * 商品销售排行榜-导出报表
     */
    public function goods_export($conditions) {
        $OrderObj = new \Fwadmin\Model\OrderModel();
        $list = $OrderObj->tongji_goods($conditions);
        $filename = 'tongji_goods_' . $conditions['btime'] .'_'. $conditions['etime'];
        $sheet1 = '商品销售排行榜';
        $data_title = array();
        $data_title['A1'] = '编号';
        $data_title['B1'] = '商品名称';
        $data_title['C1'] = '销售数量';

        $data_content = array();
        foreach ($list as $k => $v) {
            $num = $k + 2;
            $data_content['A' . $num] = $k + 1;
            $data_content['B' . $num] = $v['goods_name'];
            $data_content['C' . $num] = $v['num'];
        }
        excelExport($filename, $data_title, $data_content, $sheet1);
    }
    

   

}
