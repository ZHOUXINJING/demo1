<?php

namespace Fwadmin\Controller;

use Common\Controller\FwadminController;
use Fwadmin\Model\SystemMenuModel;

class SystemMenuController extends FwadminController {

    /**
     * 系统菜单列表
     */
    public function index() {
        //获得数据       
        $ModelObj = new SystemMenuModel();
        if (IS_AJAX) {
            $t = I('get.t');
            $v = I('get.v');
            $id = I('get.i');
            echo $ModelObj->where('menu_id=' . $id)->setField($t, $v);
            exit();
        }
        
        //获取当前选择类别
        $current_id = I('get.parent_id',0);
        $this->current_id = $current_id;
        
        //获取顶级菜单并赋值
        $top = $ModelObj->getList(0,0);
        $this->top = $top;
        
         //获取所有菜单并赋值
        $results = $ModelObj->getList($current_id,2);
        $this->list = $results;
        
        $this->display();
    }

    /**
     * 添加系统菜单
     */
    public function add() {
        $ModelObj = new SystemMenuModel();
        if (IS_POST) {
            if ($ModelObj->create()) {
                $ModelObj->top_menu_id = $ModelObj->getTopMenuId($ModelObj->parent_id);
                $menu_id = $ModelObj->add();
                if ($menu_id > 0) {
                    $this->success('添加成功', U('index'));
                } else {
                    $this->error($ModelObj->getLastSql());
                }
            } else {
                $this->error($ModelObj->getError());
            }
        } else {
            $this->parentList = $ModelObj->getList(0,0);
            $this->display();
        }
    }

    /**
     * 编辑系统菜单
     */
    public function edit($menu_id) {
        $ModelObj = new SystemMenuModel();
        if (IS_POST) {
            if ($ModelObj->create()) {
                $ModelObj->top_menu_id = $ModelObj->getTopMenuId($ModelObj->parent_id);
                $flag = $ModelObj->save();
                if ($flag !== false) {
                    $this->success('修改成功', U('index'));
                } else {
                    $this->error($ModelObj->getLastSql());
                }
            } else {
                $this->error($ModelObj->getError());
            }
        } else {
            $this->parentList = $ModelObj->getList(0,0,$menu_id);
            $model = $ModelObj->find($menu_id);
            $this->model = $model;
            $this->display();
        }
    }

    /*
     * 删除系统菜单
     */
    public function del($menu_id) {
        $ModelObj = new SystemMenuModel();
        $flag = $ModelObj->delete($menu_id);
        if ($flag > 0)
            $this->success('删除成功');
        else
            $this->error($ModelObj->getError());
    }
    
    /**
     * 选择图标
     */
    public function ico(){
        $ModelIcoObj = M('system_ico');
        $this->list = $ModelIcoObj->select();
        $this->display();
    }
            

}

?>