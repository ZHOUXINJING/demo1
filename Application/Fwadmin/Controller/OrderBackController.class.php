<?php

namespace Fwadmin\Controller;

use Common\Controller\FwadminController;
use Fwadmin\Model\OrderBackModel;

class OrderBackController extends FwadminController {

    /**
     * 售后列表页
     */
    public function index() {
        //获得数据       
        $ModelObj = new OrderBackModel();
        $conditions = array();
        $param = array('back_no' => 'back_no','order_no' => 'order_no','member_name' => 'member_name','status' => 'status');
        $this->paramValue($conditions, $param);
        $results = $ModelObj->search($conditions);

        $lists = $results['list'];
        foreach($lists as $k => $v) {
            $lists[$k]['status_str'] = $ModelObj->get_status($v['status']);
        }
        $this->assign('list', $lists); // 赋值数据集
        $this->assign('page', $results['page']); // 赋值分页输出

        $this->list_url = urlencode(__SELF__);
        $this->display();
    }
    
     /**
     * 售后待退款
     */
    public function pay() {
        //获得数据       
        $ModelObj = new OrderBackModel();
        $conditions = array();
        $param = array('back_no' => 'back_no','order_no' => 'order_no','member_name' => 'member_name','status_pay' => 'status_pay');
        $this->paramValue($conditions, $param);
        $conditions['status'] = 5;
        $results = $ModelObj->search($conditions);

        $lists = $results['list'];
        foreach($lists as $k => $v) {
            $lists[$k]['status_str'] = $ModelObj->get_status($v['status']);
            $lists[$k]['status_pay_str'] = $ModelObj->get_status_pay($v['status_pay']);
        }
        $this->assign('list', $lists); // 赋值数据集
        $this->assign('page', $results['page']); // 赋值分页输出

        $this->list_url = urlencode(__SELF__);
        $this->display();
    }
    
    /**
     * 售后详细页
     * @param int $id 售后ID
     */
    public function info($id) {
        $OrderBackObj = new OrderBackModel();
        $model = $OrderBackObj->find($id);
        
        $OrderGoods = M('order_goods');
        $order_goods = $OrderGoods->where(array('member_id'=>$this->member_id))->find($model['cart_id']);
        
        $model['str_status'] = $OrderBackObj->get_status($model['status']);
        $model['str_status_pay'] = $OrderBackObj->get_status_pay($model['status_pay']);
        
        $this->model = $model;
        $this->order_goods = $order_goods;
        
        //列表地址
        $this->list_url = I('get.list_url','');
                
        $this->display();
    }
    
    /**
     * 获取操作按钮
     * @param int $order_id 订单ID
     */
    public function get_btn($id) {
        $OrderBackObj = new OrderBackModel();
        $model = $OrderBackObj->field('status,status_pay')->find($id);
        if($model['status'] == '1'){
            echo '&nbsp;&nbsp;<input type="button" class="btn1" id="btn_confirm_2" onclick="confirm_2()" value="审核通过" />&nbsp;&nbsp;';
            echo '&nbsp;&nbsp;<input type="button" class="btn2" id="btn_confirm_6" onclick="confirm_6()" value="审核不通过" />&nbsp;&nbsp;';
        } elseif ($model['status'] == '3') {
            echo '&nbsp;&nbsp;<input type="button" class="btn2" id="btn_confirm_4" onclick="confirm_4()" value="确定收到回寄商品" />&nbsp;&nbsp;';
        } elseif ($model['status'] == '4') {
            echo '&nbsp;&nbsp;<input type="button" class="btn4" id="btn_confirm_5" onclick="confirm_5()" value="已处理完成" />&nbsp;&nbsp;';
        } elseif($model['status'] == '5') {
            if($model['status_pay'] == '1') {
                echo '&nbsp;&nbsp;<input type="button" class="btn1" id="btn_pay_2" onclick="pay_2()"  value="已退相应款项给客户" />&nbsp;&nbsp;';
            }
        }
    }
    
     //审核通过
    public function confirm_2($id) {
        $OrderBackObj = new OrderBackModel();
        $flag = $OrderBackObj->confirm_2($id);
        if($flag > 0) {
            $data['status'] = 'success';
            $data['msg'] = '';
            $this->ajaxReturn($data);
        } elseif ($flag == 0) {
            $data['status'] = 'error';
            $data['msg'] = '操作失败';
            $this->ajaxReturn($data);
        } else {
            $data['status'] = 'error';
            $data['msg'] = '非法操作';
            $this->ajaxReturn($data);
        }
    }
    
     //确认收到已回寄商品
    public function confirm_4($id) {
        $OrderBackObj = new OrderBackModel();
        $flag = $OrderBackObj->confirm_4($id);
        if($flag > 0) {
            $data['status'] = 'success';
            $data['msg'] = '';
            $this->ajaxReturn($data);
        } elseif ($flag == 0) {
            $data['status'] = 'error';
            $data['msg'] = '操作失败';
            $this->ajaxReturn($data);
        } else {
            $data['status'] = 'error';
            $data['msg'] = '非法操作';
            $this->ajaxReturn($data);
        }
    }
    
    //审核不通过
    public function confirm_6($id) {
        $OrderBackObj = new OrderBackModel();
        $flag = $OrderBackObj->confirm_6($id);
        if($flag > 0) {
            $data['status'] = 'success';
            $data['msg'] = '';
            $this->ajaxReturn($data);
        } elseif ($flag == 0) {
            $data['status'] = 'error';
            $data['msg'] = '操作失败';
            $this->ajaxReturn($data);
        } else {
            $data['status'] = 'error';
            $data['msg'] = '非法操作';
            $this->ajaxReturn($data);
        }
    }
    
    //处理完成
    public function confirm_5($id) {
        $OrderBackObj = new OrderBackModel();
        $flag = $OrderBackObj->confirm_5($id);
        if($flag > 0) {
            $data['status'] = 'success';
            $data['msg'] = '';
            $this->ajaxReturn($data);
        } elseif ($flag == 0) {
            $data['status'] = 'error';
            $data['msg'] = '操作失败';
            $this->ajaxReturn($data);
        } else {
            $data['status'] = 'error';
            $data['msg'] = '非法操作';
            $this->ajaxReturn($data);
        }
    }
    
    //处理完成
    public function pay_2($id) {
        $OrderBackObj = new OrderBackModel();
        $flag = $OrderBackObj->pay_2($id);
        if($flag > 0) {
            $data['status'] = 'success';
            $data['msg'] = '';
            $this->ajaxReturn($data);
        } elseif ($flag == 0) {
            $data['status'] = 'error';
            $data['msg'] = '操作失败';
            $this->ajaxReturn($data);
        } else {
            $data['status'] = 'error';
            $data['msg'] = '非法操作';
            $this->ajaxReturn($data);
        }
    }
    
    
    

    

}
