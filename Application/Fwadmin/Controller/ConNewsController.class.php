<?php

namespace Fwadmin\Controller;

use Common\Controller\FwadminController;
use Fwadmin\Model\ConNewsModel;
use Fwadmin\Model\ConHelpModel;

class ConNewsController extends FwadminController {

    /**
     * 资讯列表
     */
    public function index() {
        //获得数据
        $ModelObj = new ConNewsModel();
        $HelpObj =  new ConHelpModel();
        if (IS_AJAX) {
            $t = I('get.t');
            $v = I('get.v');
            $id = I('get.i');
            echo $ModelObj->where('news_id=' . $id)->setField($t, $v);
            exit();
        }
	//删除所选
     if (IS_POST) {
            if (I('post.action') == 'delsel') {
                $sel = I('post.sel');
                if (is_array($sel)) {
                    $flag = 0;
                    foreach ($sel as $id) {
                        $flag += $ModelObj->delete($id);
                    }
                    $this->success('删除' . $flag . '条数据成功！');
                    exit();
                }
            }
        }
        //搜索条件
        $conditions = array();
        $param = array('title' => 'title', 'help_id' => 'help_id', 'time_begin' => 'time_begin', 'time_end' => 'time_end');
        $this->paramValue($conditions, $param, 'GET');
        $results = $ModelObj->search($conditions);
        $lists = $results['list'];
		foreach($lists as $k=>$v){
		$lists[$k]['cate_name'] = M('con_help')->getFieldByHelpId($v['help_id'],'help_name');
		}
        $this->assign('list', $lists); // 赋值数据集
        $this->assign('page', $results['page']); // 赋值分页输出
        //类别
        $this->news_cat = $HelpObj->get_news_list();
        $this->display();
    }

    /**
     * 添加
     */
    public function add() {
        $ModelObj = new ConNewsModel();
        $HelpObj =  new ConHelpModel();
        if (IS_POST) {
            if ($ModelObj->create()) {
                $ModelObj->create_time = strtotime($ModelObj->create_time);
                $id = $ModelObj->add();
                if ($id > 0)
                    $this->success('添加成功', U('ConNews/index'));
                else
                    $this->error($ModelObj->getError());
            } else {
                $this->error($ModelObj->getError());
            }
        } else {
             //类别
            $this->news_cat = $HelpObj->get_news_list();
            $this->display();
        }
    }

     /**
     * 编辑
     */
    public function edit($news_id) {
        $ModelObj = new ConNewsModel();
        $HelpObj =  new ConHelpModel();
        if (IS_POST) {
            if ($ModelObj->create()) {
                $ModelObj->create_time = strtotime($ModelObj->create_time);
                $flag = $ModelObj->save();
                if ($flag !== false)
                    $this->success('修改成功', U('ConNews/index'));
                else
                    $this->error($ModelObj->getError());
            } else {
                $this->error($ModelObj->getError());
            }
        }else {
            $model = $ModelObj->find($news_id);
            $this->model = $model;
              //类别
            $this->news_cat = $HelpObj->get_news_list();
            $this->display();
        }
    }
    
     /**
     * 删除图片切换
     */
    public function del($news_id) {
        $ModelObj = new ConNewsModel();
        $flag = $ModelObj->del($news_id);
        if ($flag > 0)
            $this->success('删除成功');
        else
            $this->error($ModelObj->getError());
    }

}
