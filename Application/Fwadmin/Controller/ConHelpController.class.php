<?php

namespace Fwadmin\Controller;

use Common\Controller\FwadminController;
use Fwadmin\Model\ConHelpModel;

class ConHelpController extends FwadminController {

    /**
     * 帮助中心栏目
     */
    public function index() {
        $ModelObj = new ConHelpModel();
        if (IS_AJAX) {
            $t = I('get.t');
            $v = I('get.v');
            $id = I('get.i');
            echo $ModelObj->where('help_id=' . $id)->setField($t, $v);
            exit();
        }
        //获得数据
        $this->list = $ModelObj->get_list(0);
       
        $this->display();
    }

    /**
     * 添加栏目
     */
    public function add() {
        $ModelObj = new ConHelpModel();
        if (IS_POST) {
            if ($ModelObj->create()) {
                $id = $ModelObj->add();
                if ($id > 0)
                    $this->success('添加成功', U('ConHelp/index'));
                else
                    $this->error($ModelObj->getError());
            } else {
                $this->error($ModelObj->getError());
            }
        } else {
            $this->parent_list = $ModelObj->get_category();
            $this->display();
        }
    }

     /**
     * 编辑栏目
     */
    public function edit($help_id) {
        $ModelObj = new ConHelpModel();
        if (IS_POST) {
            if ($ModelObj->create()) {
                $flag = $ModelObj->save();
                if ($flag !== false)
                    $this->success('修改成功', U('ConHelp/index'));
                else
                    $this->error($ModelObj->getError());
            } else {
                $this->error($ModelObj->getError());
            }
        }else {
            $model = $ModelObj->find($help_id);
            $this->model = $model;
            $this->parent_list = $ModelObj->get_category();
            $this->display();
        }
    }
    
     /**
     * 删除图片切换
     */
    public function del($help_id) {
        $ModelObj = new ConHelpModel();
        $flag = $ModelObj->del($help_id);
        if ($flag > 0)
            $this->success('删除成功');
        else
            $this->error($ModelObj->getError());
    }

}
