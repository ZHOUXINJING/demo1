<?php

namespace Fwadmin\Controller;

use Common\Controller\FwadminController;
use Fwadmin\Model\OrderModel;

class OrderController extends FwadminController {

    /**
     * 订单列表
     */
    public function index() {
        $status = I('get.status',0);
        //获得数据       
        $ModelObj = new OrderModel();
        $conditions = array();
        $param = array('order_no' => 'order_no','member_name' => 'member_name','addr_phone'=>'addr_phone','addr_realname'=>'addr_realname','addr_province'=>'addr_province');
        $this->paramValue($conditions, $param);
        if(!empty($status)) {
            $conditions['status'] = $status;
            $this->status = $status;
        }
        $results = $ModelObj->search($conditions);

        $lists = $results['list'];
        foreach($lists as $k => $v) {
            $lists[$k]['pay_type_str'] = $ModelObj->get_pay_type($v['pay_type']);//支付方式
            $lists[$k]['status_str'] = $ModelObj->show_status($v['status'], $v['status_pay'], $v['status_delivery']);
        }
        $this->assign('list', $lists); // 赋值数据集
        $this->assign('page', $results['page']); // 赋值分页输出

        //调取省份
        $ProvinceObj = new \Fwadmin\Model\AddressProvinceModel();
        $this->province = $ProvinceObj->getSel();

        $this->list_url = urlencode(__SELF__);
        $this->display();
    }
    
    /**
     * 订单列表
     */
    public function pay() {
        $status_pay = I('get.status_pay',0);
        //获得数据       
        $ModelObj = new OrderModel();
        $conditions = array();
        $param = array('order_no' => 'order_no','member_name' => 'member_name','addr_realname'=>'addr_realname');
        $this->paramValue($conditions, $param);
        if(!empty($status_pay)) {
            $conditions['status_pay'] = $status_pay;
            $this->status_pay = $status_pay;
        }
        $results = $ModelObj->search($conditions);

        $lists = $results['list'];
        foreach($lists as $k => $v) {
            $lists[$k]['pay_type_str'] = $ModelObj->get_pay_type($v['pay_type']);//支付方式
            $lists[$k]['status_str'] = $ModelObj->show_status($v['status'], $v['status_pay'], $v['status_delivery']);
        }
        $this->assign('list', $lists); // 赋值数据集
        $this->assign('page', $results['page']); // 赋值分页输出

        $this->list_url = urlencode(__SELF__);
        $this->display();
    }
    
    /**
     * 订单配送列表
     */
    public function delivery() {
        $status_delivery = I('get.status_delivery',0);
        //获得数据       
        $ModelObj = new OrderModel();
        $conditions = array();
        $param = array('order_no' => 'order_no','member_name' => 'member_name','addr_realname'=>'addr_realname');
        $this->paramValue($conditions, $param);
        if(!empty($status_delivery)) {
            $conditions['status_delivery'] = $status_delivery;
            $this->status_delivery = $status_delivery;
        }
        $results = $ModelObj->search($conditions);

        $lists = $results['list'];
        foreach($lists as $k => $v) {
            $lists[$k]['pay_type_str'] = $ModelObj->get_pay_type($v['pay_type']);//支付方式
            $lists[$k]['status_str'] = $ModelObj->show_status($v['status'], $v['status_pay'], $v['status_delivery']);
        }
        $this->assign('list', $lists); // 赋值数据集
        $this->assign('page', $results['page']); // 赋值分页输出

        $this->list_url = urlencode(__SELF__);
        $this->display();
    }
    
    /**
     * 订单详细页
     * @param int $order_id 订单ID
     */
    public function info($order_id) {
        $OrderObj = new OrderModel();
        $order = $OrderObj->find($order_id);
        $order['status_str'] = $OrderObj->show_status($order['status'], $order['status_pay'], $order['status_delivery']);
        $total_need_pay = $OrderObj->get_total_need_pay($order);
		$order['total_need_pay'] = abs(round($total_need_pay,2));
		
        $order['str_pay_type'] = $OrderObj->get_pay_type($order['pay_type']);
        $this->order = $order;
		  if($order['delivery_code']!=''&&$order['delivery_no']!=''){
		 $data = D('Kuaidi', 'Service')->getOrderTracesByJson($order['order_no'],$order['delivery_code'],$order['delivery_no']);
		 $this->kuaiid_list = json_decode($data,true); 
		
		 }
        
        //订单跟踪信息
        $this->record = $OrderObj->get_record($order_id);
        //订单购物车信息
        $this->goods = $OrderObj->get_goods($order_id);
        
        //订单支付记录
        $PayRecordObj = new \Fwadmin\Model\PayRecordModel();
        $this->record_pay = $PayRecordObj->get_record($order_id);
        
        //列表地址
         $list_url = I('get.list_url','');
		 session('shop_order_list_url',$list_url);
        if(empty($list_url)) {
            $list_url = U('Order/index');
        }
       $this->list_url = $list_url;
		 
        
        //订单来源
        $this->source_str = $OrderObj->get_source($order['source']);
      
        $this->display();
    }
    
    /**
     * 订单修改
     * @param int $order_id 订单ID
     */
    public function edit($order_id) {
        $OrderObj = new OrderModel();
        $list_url = I('get.list_url','');
        if (IS_POST) {
            if ($OrderObj->create()) {
                $flag = $OrderObj->save();
                if ($flag !== false)
                    $this->success('修改成功', U('edit',array('order_id'=>$order_id,'list_url'=>$list_url)));
                else
                    $this->error($OrderObj->getError());
            } else {
                $this->error($OrderObj->getError());
            }
        }else {
            $order = $OrderObj->find($order_id);
            $order['status_str'] = $OrderObj->show_status($order['status'], $order['status_pay'], $order['status_delivery']);
            $order['total_need_pay'] = $OrderObj->get_total_need_pay($order);// $order['total'] - $order['total_discount'] - $order['total_pay'] - $order['total_pay_point'] ;
            $order['str_pay_type'] = $OrderObj->get_pay_type($order['pay_type']);
            $this->order = $order;
			
		$list_url = session('shop_order_list_url');
        if(empty($list_url)) {
            $list_url = U('ShopOrder/index');
        }
        $this->list_url = $list_url;
         $this->display();
        }
    }
    
    /**
     * 获取订单操作按钮
     * @param int $order_id 订单ID
     */
    public function get_btn($order_id,$list_url) {
        $PurviewObj = new \Fwadmin\Model\AdminPurviewModel();
        $purviews = $PurviewObj->getNotPurviewStr(session('admin_group_id'));
        $pur_order_confirm = $PurviewObj->checkByUrl('order_confirm', $purviews);//是否禁止审核
        $pur_order_delivery = $PurviewObj->checkByUrl('order_delivery', $purviews);//是否禁止配送
        $pur_order_pay = $PurviewObj->checkByUrl('order_pay', $purviews);//是否禁止支付
        $pur_order_complete = $PurviewObj->checkByUrl('order_complete', $purviews);//是否禁止确认收货
        $pur_order_edit = $PurviewObj->checkByUrl('order_edit', $purviews);//是否禁止修改
         
        $OrderObj = new OrderModel();
        $order = $OrderObj->where('status,status_pay,status_delivery,pay_type')->find($order_id);
        if($order['status'] == '3'){
            echo '订单已完成';
        } elseif ($order['status'] == '4') {
            echo '用户已取消订单';
            if($order['status_pay'] == '3') {
                if(!$pur_order_pay) {
                    //echo '&nbsp;&nbsp;<input type="button" class="btn1" id="btn_pay_4" onclick="pay_4()"  value="" />&nbsp;&nbsp;';
                }
            }
        } elseif ($order['status'] == '5') {
            echo '订单已作废';
            if(!$pur_order_confirm) {
                echo '&nbsp;&nbsp;<input type="button" class="btn4" id="btn_confirm_1" onclick="confirm_1()" value="取消作废" />&nbsp;&nbsp;';
            }
            if($order['status_pay'] == '3') {
                if(!$pur_order_pay) {
                   // echo '&nbsp;&nbsp;<input type="button" class="btn1" id="btn_pay_4" onclick="pay_4()"  value="" />&nbsp;&nbsp;';
                }
            }
        } else {
            if($order['pay_type'] == 2) {//货到付款
                if($order['status'] == '1') {
                    if(!$pur_order_confirm) {
                        echo '&nbsp;&nbsp;<input type="button" class="btn1" id="btn_confirm_2" onclick="confirm_2()" value="审核通过" />&nbsp;&nbsp;';
                        echo '&nbsp;&nbsp;<input type="button" class="btn2" id="btn_confirm_5" onclick="confirm_5()" value="订单作废" />&nbsp;&nbsp;';
                    }
                } elseif($order['status'] == '2' && $order['status_delivery'] == '1') {
                    if(!$pur_order_delivery) {
                        echo '&nbsp;&nbsp;<input type="button" class="btn1" id="btn_delivery_2" onclick="delivery_2()" value="发货" />&nbsp;&nbsp;';
                    }
                    if(!$pur_order_confirm) {
                        echo '&nbsp;&nbsp;<input type="button" class="btn4" id="btn_confirm_1" onclick="confirm_1()" value="取消审核" />&nbsp;&nbsp;';
                    }
                } elseif($order['status'] == '2' && $order['status_delivery'] == '2') {
                    if(!$pur_order_complete) {
                        echo '&nbsp;&nbsp;<input type="button" class="btn1" id="btn_delivery_4" onclick="delivery_4()" value="确认收货" />&nbsp;&nbsp;'; 
                    }
                }
                if($order['status'] == "1" || $order['status'] == "2") {
                    if($order['status_pay'] == '1' || $order['status_pay'] == '2') {
                        if(!$pur_order_pay) {
                            echo '&nbsp;&nbsp;<input type="button" class="btn1" id="btn_pay_3" onclick="pay_3()"  value="确认支付" />&nbsp;&nbsp;';
                        }
                    } 
                }
            } else {//在线支付
                if($order['status_pay'] == '1' || $order['status_pay'] == '2') {
                    if(!$pur_order_pay) {
                        echo '&nbsp;&nbsp;<input type="button" class="btn1" id="btn_pay_3" onclick="pay_3()"  value="确认支付" />&nbsp;&nbsp;';
                    }
                } else {
                    if($order['status'] == '1') {
                        if(!$pur_order_confirm) {
                            echo '&nbsp;&nbsp;<input type="button" class="btn1" id="btn_confirm_2" onclick="confirm_2()"  value="审核通过" />&nbsp;&nbsp;';
                            echo '&nbsp;&nbsp;<input type="button" class="btn2" id="btn_confirm_5" onclick="confirm_5()"  value="订单作废" />&nbsp;&nbsp;';
                        }
                    } elseif($order['status'] == '2' && $order['status_delivery'] == '1') {
                        if(!$pur_order_delivery) {
                            echo '&nbsp;&nbsp;<input type="button" class="btn1" id="btn_delivery_2" onclick="delivery_2()"  value="发货" />&nbsp;&nbsp;';
                        }
                        if(!$pur_order_confirm) {
                            echo '&nbsp;&nbsp;<input type="button" class="btn4" id="btn_confirm_1" onclick="confirm_1()"  value="取消审核" />&nbsp;&nbsp;';
                        }
                    } elseif($order['status'] == '2' && $order['status_delivery'] == '2') {
                        if(!$pur_order_complete) {
                            echo '&nbsp;&nbsp;<input type="button" class="btn1" id="btn_delivery_4" onclick="delivery_4()"  value="确认收货" />&nbsp;&nbsp;'; 
                        }
                    }
                }
            }
        }
        if(!$pur_order_edit) {
             echo '&nbsp;&nbsp;<a class="btn_a3" href="'.U('edit',array('order_id'=>$order['order_id'])).'?list='.$list_url.'">修改订单</a>&nbsp;&nbsp;'; 
        }
    }
    
    /**
     * 订单取消审核
     * @param int $order_id 订单ID
     */
    public function confirm_1($order_id) {
        $OrderObj = new OrderModel();
        $flag = $OrderObj->confirm_1($order_id);
        if($flag > 0) {
            $data['status'] = 'success';
            $data['msg'] = '';
            $this->ajaxReturn($data);
        } elseif ($flag == 0) {
            $data['status'] = 'error';
            $data['msg'] = '操作失败';
            $this->ajaxReturn($data);
        } else {
            $data['status'] = 'error';
            $data['msg'] = '非法操作';
            $this->ajaxReturn($data);
        }
    }
    
    /**
     * 订单审核
     * @param int $order_id 订单ID
     */
    public function confirm_2($order_id) {
        $OrderObj = new OrderModel();
        $flag = $OrderObj->confirm_2($order_id);
        if($flag > 0) {
            $data['status'] = 'success';
            $data['msg'] = '';
            $this->ajaxReturn($data);
        } elseif ($flag == 0) {
            $data['status'] = 'error';
            $data['msg'] = '操作失败';
            $this->ajaxReturn($data);
        } else {
            $data['status'] = 'error';
            $data['msg'] = '非法操作';
            $this->ajaxReturn($data);
        }
    }
    
    /**
     * 订单作废
     * @param int $order_id 订单ID
     */
    public function confirm_5($order_id) {
        $OrderObj = new OrderModel();
        $flag = $OrderObj->confirm_5($order_id);
        if($flag > 0) {
            $data['status'] = 'success';
            $data['msg'] = '';
            $this->ajaxReturn($data);
        } elseif ($flag == 0) {
            $data['status'] = 'error';
            $data['msg'] = '操作失败';
            $this->ajaxReturn($data);
        } else {
            $data['status'] = 'error';
            $data['msg'] = '非法操作';
            $this->ajaxReturn($data);
        }
    }
    
    /**
     * 订单支付
     * @param int $order_id 订单ID
     */
    public function pay_3($order_id) {
        $payment = I('get.payment','');
        $money = I('get.money',0);
        $OrderObj = new OrderModel();
        $flag = $OrderObj->pay_3($order_id,$payment,$money);
        if($flag > 0) {
            $data['status'] = 'success';
            $data['msg'] = '';
            $this->ajaxReturn($data);
        } elseif ($flag == 0) {
            $data['status'] = 'error';
            $data['msg'] = '操作失败';
            $this->ajaxReturn($data);
        } else {
            $data['status'] = 'error';
            $data['msg'] = '非法操作'.$flag;
            $this->ajaxReturn($data);
        }
    }
    
    /*
     * 发货
     * @param int @order_id 订单ID
     */
    public function delivery_2($order_id) {
        $OrderObj = new OrderModel();
        $delivery_company = I('get.delivery_company','');
		$delivery_code = I('get.delivery_code','');
        $delivery_no = I('get.delivery_no','');
        $flag = $OrderObj->delivery_2($order_id,$delivery_company,$delivery_no,$delivery_code);
        if($flag > 0) {
            $data['status'] = 'success';
            $data['msg'] = '';
            $this->ajaxReturn($data);
        } elseif ($flag == 0) {
            $data['status'] = 'error';
            $data['msg'] = '操作失败';
            $this->ajaxReturn($data);
        } else {
            $data['status'] = 'error';
            $data['msg'] = '非法操作';
            $this->ajaxReturn($data);
        }
    }     
    
    /*
     * 确认收货
     * @param int @order_id 订单ID
     */
    public function delivery_4($order_id) {
        $OrderObj = new OrderModel();
        $flag = $OrderObj->delivery_4($order_id);
        if($flag > 0) {
            $data['status'] = 'success';
            $data['msg'] = '';
            $this->ajaxReturn($data);
        } elseif ($flag == 0) {
            $data['status'] = 'error';
            $data['msg'] = '操作失败';
            $this->ajaxReturn($data);
        } else {
            $data['status'] = 'error';
            $data['msg'] = '非法操作';
            $this->ajaxReturn($data);
        }
    }     
    

    

}
