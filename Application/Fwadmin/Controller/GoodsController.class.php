<?php



namespace Fwadmin\Controller;



use Common\Controller\FwadminController;

use Fwadmin\Model\GoodsModel;

use Fwadmin\Model\GoodsCatModel;

use Fwadmin\Model\GoodsBrandModel;

use Fwadmin\Model\GoodsSpecModel;

use Fwadmin\Model\GoodsModModel;



class GoodsController extends FwadminController {

    

    /**

     * 商品列表

     */

    public function index() {

        //获得数据       

        $ModelObj = new GoodsModel();

        if (IS_AJAX) {

            $t = I('get.t');

            $v = I('get.v');

            $id = I('get.i');

            echo $ModelObj->where('goods_id=' . $id)->setField($t, $v);

            exit();

        }

        

        //搜索筛选参数

        $conditions = array();

        $param = array('title'=>'title','cat_id'=>'cat_id','brand_id'=>'brand_id','status'=>'status');

        $this->paramValue($conditions, $param);

        $result = $ModelObj->search($conditions, 'goods_id,goods_name,goods_sn,cat_ids,cat_path_ids,brand_id,order_id,status,time_create,picture,spec_array');

        $list = $result['list'];

        $CatObj = new GoodsCatModel();

        $BrandObj = new GoodsBrandModel();

        foreach($list as $k => $r) {

             $list[$k]['cat_name'] = $CatObj->get_all_cate($r['cat_path_ids']);

            $list[$k]['brand_name'] = $BrandObj->getFieldByBrandId($r['brand_id'],'brand_name');

			$list[$k]['color_array'] = get_goods_color_list($r['spec_array'],'颜色');
			$list[$k]['kuanshi_array'] = get_goods_color_list($r['spec_array'],'款式');

			

			 // $color_array = array_column($arr_par, 'name');

			  

        }

		//print_r($list);

        //搜索类别列表

	   $this->cat_list = $CatObj->getSubSelect(0, 5);

        //搜索品牌列表

        $this->brand_list = $BrandObj->get_sel();


        $this->list = $list; // 赋值数据集

        $this->page = $result['page'];

        $this->display();

    }

    

    /**

     * 添加商品

     */

    public function add() {

        $ModelObj = new GoodsModel();

        $CatObj = new GoodsCatModel();

        $BrandObj = new GoodsBrandModel();

        $GoodsModObj = new GoodsModModel();

        if (IS_POST) {

            if ($ModelObj->create()) {

				/*

                //如果首图没有上传，则采用大图第一张

                if(empty($ModelObj->picture)) {

                    $picture_list = $ModelObj->picture_list;

                    $picture_first_index = strpos($picture_list,",");

                    $ModelObj->picture = substr($picture_list,0,$picture_first_index);

                }

				*/

                //模型值

                $model_id = $ModelObj->model_id;

                //类别

               // $cat_id = I('post.cat_id');

               // $cat_ids = implode(',',$cat_id);

			   

			   //修改20170712

				for($i=4;$i>0;$i--){

				  $cat_ids = I('post.cat_id'.$i);

				  if($cat_ids>0){

				      break;

				   }

				}

				$cat_ids=','.$cat_ids;

			   

                $cat_ids = str_replace(",,",",",$cat_ids);//去掉空的值

                $ModelObj->cat_ids = $cat_ids.',';//类别

                $ModelObj->cat_path_ids = $CatObj->getPathIds($cat_ids); //类别路径

                $ModelObj->time_create = strtotime($ModelObj->time_create);//发布时间

                $ModelObj->tuijian = trim($ModelObj->tuijian,',');//推荐商品

                $ModelObj->spec_array = I('post.sel_spec_value','','');

                $goods_id = $ModelObj->add();

                if ($goods_id > 0) {

                    $search_str = ',';

                    //保存模型参数值

                    if($model_id > 0) {

                        $attr_list = $GoodsModObj->get_attr_list($model_id, 1);

                        foreach ($attr_list as $attr) {

                            $attr_type = $attr['attr_type'];

                            $attr_id = $attr['attr_id'];

                            $attr_value = I('post.attr_'.$attr_id);

                            //如果是单选框和选择框，放入辅助搜索参数

                            if($attr_type == 2 || $attr_type ==3) {

                                $search_str.= 'attr_'.$attr_id.'_'.$attr_value.',';

                            } elseif ($attr_type == 4) { //多选框

                                foreach($attr_value as $attr_value_item) {

                                    $search_str.= 'attr_'.$attr_id.'_'.$attr_value_item.',';

                                }

                            }

                            if($attr_type == 4) { //多选框

                                $attr_value = ','.implode(',',$attr_value).',';

                            }

                            $GoodsModObj->set_attr_value($attr_id, $goods_id, $attr_value, $model_id);

                        }

                    }

                    

                    //保存搜索参数属性

                    $ModelObj->where('goods_id='.$goods_id)->setField('search_str',$search_str);

                    

                    //保存规格参数值

                    $arr_spec_id = I('post.spec_id');

                    if(count($arr_spec_id) > 0) {

                        $is_first = 1;//判断是不是第一个

                        $data2 = array();//主表更新数据

                        foreach($arr_spec_id as $k=>$i) {

                            $data['goods_sn'] = I('post.goods_sn_'.$i);

                            if($is_first == 1 || $data['goods_sn'] !== '') {

                                $data['spec_array'] = I('post.spec_'.$i);

                                $data['price_market'] = I('post.price_market_'.$i,0);

                                $data['price'] = I('post.price_'.$i,0);

                                $data['price_cost'] = I('post.price_cost_'.$i,0);

                                $data['weight'] = I('post.weight_'.$i,0);

                                $data['store_num'] = I('post.num_'.$i,0);

                                $data['order_id'] = $k;

                                $data['goods_id'] = $goods_id;

                                $MGoodsItem = M('GoodsItem');

                                $MGoodsItem->add($data);

                                //如果是第一个，主表更新规格数据

                                if($is_first == 1) {

                                   $data2 = $data;

                                   $is_first = 0;

                                }

                            }

                        }

                        //更新主表规格数据

                        unset($data2['order_id']);

                        unset($data2['spec_array']);

                        $ModelObj->save($data2);

                    }

                    $this->success('添加成功', U('Goods/index'));

                } else {

                    $this->error($ModelObj->getError());

                }

            } else {

                $this->error($ModelObj->getError());

            }

        } else {

            $this->cat_sel = $CatObj->getSubSelect(0, 5); //父类别

            $this->brand_sel = $BrandObj->get_sel(); //品牌选择

            $this->model_sel = $GoodsModObj->get_list(1);//启用模型

			/******20170712修改***/

			//获取大类

			$this->parent_cat = $CatObj->where(Array('cat_level'=>1,'is_enable'=>1))->order('order_id asc,cat_id asc')->select();

            $this->display();

        }

    }



    /**

     * 修改商品

     */

    public function edit($goods_id) {

        $ModelObj = new GoodsModel();

        $CatObj = new GoodsCatModel();

        $BrandObj = new GoodsBrandModel();

        $GoodsModObj = new GoodsModModel();

        if (IS_POST) {

            if ($ModelObj->create()) {

				/*

                //如果首图没有上传，则采用大图第一张

                if(empty($ModelObj->picture)) {

                    $picture_list = $ModelObj->picture_list;

                    $picture_first_index = strpos($picture_list,",");

                    $ModelObj->picture = substr($picture_list,0,$picture_first_index);

                }

				*/

                //模型值

                $model_id = $ModelObj->model_id;

				

			   /*

                //类别

                $cat_id = I('post.cat_id');

                $cat_ids = implode(',',$cat_id);

                $cat_ids = str_replace(",,",",",$cat_ids);//去掉空的值

                $ModelObj->cat_ids = $cat_ids.',';//类别

                $ModelObj->cat_path_ids = $CatObj->getPathIds($cat_ids); //类别路径

				*/

				

				//类别

				for($i=4;$i>0;$i--){

				  $cat_ids = I('post.cat_id'.$i);

				  if($cat_ids>0){

				      break;

				   }

				}

				$cat_ids=','.$cat_ids;

                $ModelObj->cat_ids = $cat_ids.',';//类别

				

                $ModelObj->cat_path_ids = $CatObj->getPathIds($cat_ids); //类别路径

                $ModelObj->time_create = strtotime($ModelObj->time_create);//发布时间

                $ModelObj->tuijian = trim($ModelObj->tuijian,',');//推荐商品

                if(I('post.sel_spec_value','','') !== '') {

                    $ModelObj->spec_array = I('post.sel_spec_value','','');

                }

                $flag = $ModelObj->save();

                if($flag !== false) {

                    $search_str = ',';

                    //保存模型参数值

                    if($model_id > 0) {

                        $attr_list = $GoodsModObj->get_attr_list($model_id, 1);

                        foreach ($attr_list as $attr) {

                            $attr_type = $attr['attr_type'];

                            $attr_id = $attr['attr_id'];

                            $attr_value = I('post.attr_'.$attr_id);

                            //如果是单选框和选择框，放入辅助搜索参数

                            if($attr_type == 2 || $attr_type ==3) {

                                $search_str.= 'attr_'.$attr_id.'_'.$attr_value.',';

                            } elseif ($attr_type == 4) { //多选框

                                foreach($attr_value as $attr_value_item) {

                                    $search_str.= 'attr_'.$attr_id.'_'.$attr_value_item.',';

                                }

                            }

                            if($attr_type == 4) { //多选框

                                $attr_value = ','.implode(',',$attr_value).',';

                            }

                            $GoodsModObj->set_attr_value($attr_id, $goods_id, $attr_value, $model_id);

                        }

                    }

                    //保存搜索参数属性

                    $ModelObj->where('goods_id='.$goods_id)->setField('search_str',$search_str);

                    

                    //保存规格参数值

                    $arr_spec_id = I('post.spec_id');

                    if(count($arr_spec_id) > 0) {

                        $is_first = 1;//判断是不是第一个

                        $data2 = array();//主表更新数据

                        $item_ids = array();//原有ID列表                     

                        foreach($arr_spec_id as $k=>$i) {

                            $data['goods_sn'] = I('post.goods_sn_'.$i);

                            if($data['goods_sn'] !== '') {

                                $data['spec_array'] = I('post.spec_'.$i);

                                $item_id = I('post.item_id_'.$i,0);

                                $data['price_market'] = I('post.price_market_'.$i);

                                $data['price'] = I('post.price_'.$i);

                                $data['price_cost'] = I('post.price_cost_'.$i);

                                $data['weight'] = I('post.weight_'.$i);

                                $data['store_num'] = I('post.num_'.$i);

                                $data['order_id'] = $k;

                                $data['goods_id'] = $goods_id;

                                $MGoodsItem = M('GoodsItem');

                                

                                if($item_id > 0) {

                                    $MGoodsItem->where('item_id='.$item_id)->save($data);

                                } else {

                                    $item_id = $MGoodsItem->add($data);

                                }

                                $item_ids[] = $item_id;

                                //如果是第一个，更新到主表

                                if($is_first == 1) {

                                   $data2 = $data;

                                   $is_first = 0;

                                }

                            }

                        }

                        //更新主表单价参数数据

                        unset($data2['order_id']);

                        unset($data2['spec_array']);

                        $ModelObj->save($data2);

                        //删除页面已删除的单件

                        $ModelObj->del_item_notin($goods_id,$item_ids);

                    }

                    $this->success('修改成功', U('Goods/index'));

                } else {

                    $this->error($ModelObj->getError());

                }

            } else {

                $this->error($ModelObj->getError());

            }

        } else {

            $model = $ModelObj->find($goods_id);

            $model['spec_array'] = stripslashes($model['spec_array']);//去掉引号前面的斜杠

            $this->model = $model;

            $this->items = $ModelObj->get_item($goods_id);

            $this->cat_sel = $CatObj->getSubSelect(0, 5); //父类别

            $this->brand_sel = $BrandObj->get_sel(); //品牌选择

            $this->model_sel = $GoodsModObj->get_list(1);//启用模型

			

			 $this->return_url =$_SERVER["HTTP_REFERER"];

			/******20170712修改***/

			//获取大类

			$this->parent_cat = $CatObj->where(Array('cat_level'=>1,'is_enable'=>1))->order('order_id asc,cat_id asc')->select();

			$cat_id=stringToarray($model['cat_path_ids']);

			$cat_id =  array_reverse($cat_id);//元素反过来

			$cat_count =count($cat_id);

			foreach($cat_id as $k =>$v){

			 if($v>0){

			    $cat_list[$k+1] = $CatObj->where(Array('cat_level'=>$k+1,'parent_id'=>$v,'is_enable'=>1))->order('order_id asc,cat_id asc')->select();

				$cat_current_id[$k] = $v;

			  }

			}

			$this->cat_current_id = $cat_current_id;

			$this->cat_list = $cat_list;

			 //end

			

			

            $this->display();

        }

    }



    /**

     * 删除商品

     * @param int $goods_id 商品ID

     */

    public function move_recycle($goods_id) {

        $ModelObj = new GoodsModel();

        $flag = $ModelObj->move_recycle($goods_id);

        if ($flag > 0) {

            $this->success('移到回收站成功');

        } else {

            $this->error($ModelObj->getError());

        }

    }

    

    /**

     * 批量删除商品

     * @return int 返回商品数目

     */

    public function delsel() {

        $ModelObj = new GoodsModel();

        $selid = I('post.selid');

        $count = 0;

        foreach($selid as $id) {

            $count += $ModelObj->move_recycle($id);

        }

        if($count > 0) {

            $this->success('移到'.$count.'件商品到回收站成功');

        } else {

            $this->success('移到所选商品到回收站失败');

        }

    }

    

    /**

     * 批量上架商品

     * @return int 返回商品数目

     */

    public function set_status_1() {

        $ModelObj = new GoodsModel();

        $selid = I('post.selid');

        $count = 0;

        foreach($selid as $id) {

            $count += $ModelObj->set_status_1($id);

        }

        if($count > 0) {

            $this->success($count.'件商品上架成功');

        } else {

            $this->success('商品上架失败');

        }

    }

    

    /**

     * 批量下架商品

     * @return int 返回商品数目

     */

    public function set_status_2() {

        $ModelObj = new GoodsModel();

        $selid = I('post.selid');

        $count = 0;

        foreach($selid as $id) {

            $count += $ModelObj->set_status_2($id);

        }

        if($count > 0) {

            $this->success($count.'件商品下架成功');

        } else {

            $this->success('商品下架失败');

        }

    }





    /**

     * 商品回收站

     */

    public function recycle() {

        if (I('get.submit','') == '一键清空回收站') {

            $this->del_all();

            exit();

        }

        //获得数据  

        $ModelObj = new GoodsModel();

        $conditions = array();

        $param = array('title'=>'title','cat_id'=>'cat_id','brand_id'=>'brand_id');

        $this->paramValue($conditions, $param);

        $conditions['status'] = 3;

        $result = $ModelObj->search($conditions, 'goods_id,goods_name,goods_sn,cat_ids,brand_id,order_id,status,time_create,picture');

        $list = $result['list'];

        $CatObj = new GoodsCatModel();

        $BrandObj = new GoodsBrandModel();

        foreach($list as $k => $r) {

            $list[$k]['cat_name'] = $CatObj->getAllPathName($r['cat_ids']);

            $list[$k]['brand_name'] = $BrandObj->getFieldByBrandId($r['brand_id'],'brand_name');

        }

        //搜索类别列表

	$this->cat_list = $CatObj->getSubSelect(0, 5);

        //搜索品牌列表

        $this->brand_list = $BrandObj->get_sel();

        

        $this->list = $list; // 赋值数据集

        $this->page = $result['page'];

        $this->display();

    }

    

    /*

     * 新增规格 2016-08-06 更新

     */

    public function add_spec($spec_array) {

        layout(false);

        $GoodsSpecObj = new GoodsSpecModel();

        $spec_list = $GoodsSpecObj->get_list(1);//获得规格列表

        foreach($spec_list as $k => $v) {

            $arr_spec_value = unserialize($v['spec_value']);

            $str = '';

            foreach($arr_spec_value as $v) {

                 $str .= $v['name'].',';

            }

            $str = trim($str,',');

            $spec_list[$k]['spec_value'] = $str;

        }

        $this->spec_list = $spec_list;

        

        $arr_par = json_decode($spec_array,true);

        if(!empty($arr_par)) {

            foreach($arr_par as $k2 => $v2) {

                //$arr_par[$k2]['sub'] = explode('|', $v2['val']);

                //过滤不存在选项

                foreach($arr_par[$k2]['sub'] as $k3 => $v3) {

                    $par = $v2['name'].':'.$v3;

                    

                }

            }

            $this->arr_par = $arr_par;

           // print_r($this->arr_par);

        }

        

        

        $this->display();

    }

    

    /*

     * 设置规格

     */

    public function set_spec() {

        layout(false);

        $GoodsSpecObj = new GoodsSpecModel();

        $spec_list = $GoodsSpecObj->get_list(1);//获得规格列表

        foreach($spec_list as $k => $v) {

             $arr_spec_value = unserialize($v['spec_value']);

            $str = '';

            foreach($arr_spec_value as $v) {

                 $str .= $v['name'].',';

            }

            $str = trim($str,',');

            $spec_list[$k]['spec_value'] = $str;

        }

        $this->spec_list = $spec_list;

        $this->display();

    }

    

    /**

     * 获得商品模型参数

     * @param int $model_id 模型ID

     * @param int $goods_id 商品ID

     */

    public function get_model_par($model_id, $goods_id = 0) {

        $ret = '';

        $GoodsModObj = new GoodsModModel();

        $attr_list = $GoodsModObj->get_attr_list($model_id, 1);

        foreach($attr_list as $attr) {

            $attr_type = $attr['attr_type'];

            $attr_name = $attr['attr_name'];

            $attr_id = $attr['attr_id'];

            $attr_value = '';

            if($goods_id > 0) {

                $attr_value = $GoodsModObj->get_attr_value($attr_id,$goods_id);//获取已选值

            }

            switch($attr_type) {

                case '1'://输入框

                    $ret .= '<div class="attr_item"><span class="attr_t">'.$attr_name.':</span><input type="text" id="attr_'.$attr_id.'" name="attr_'.$attr_id.'" value="'.$attr_value.'" />';

                    break;

                case '2'://选择框

                    $ret .= '<div class="attr_item"><span class="attr_t">'.$attr_name.':</span><select id="attr_'.$attr_id.'" name="attr_'.$attr_id.'">';

                    $ret .= '<option value="0">请选择</option>';

                    $sel = $GoodsModObj->get_attr_sel($attr_id);

                    foreach($sel as $s) {

                        if($attr_value == $s['attr_sel_id']) {

                            $ret .= '<option value="'.$s['attr_sel_id'].'" selected="selected">'.$s['attr_sel_name'].'</option>';

                        } else {

                            $ret .= '<option value="'.$s['attr_sel_id'].'">'.$s['attr_sel_name'].'</option>';

                        }

                    }

                    $ret .= '</select></div>';

                    break;

                case '3'://单选框

                    $ret .= '<div class="attr_item"><span class="attr_t">'.$attr_name.':</span>';

                    $sel = $GoodsModObj->get_attr_sel($attr_id);

                    foreach($sel as $s) {

                        if($attr_value == $s['attr_sel_id']) {

                            $ret .= '<label><input type="radio" checked="checked"  name="attr_'.$attr_id.'" value="'.$s['attr_sel_id'].'" />'.$s['attr_sel_name'].'</label> ';

                        } else {

                            $ret .= '<label><input type="radio" name="attr_'.$attr_id.'" value="'.$s['attr_sel_id'].'" />'.$s['attr_sel_name'].'</label> ';

                        }

                    }

                    $ret .= '</div>';

                    break;

                case '4'://多选框

                    $ret .= '<div class="attr_item"><span class="attr_t">'.$attr_name.':</span>';

                    $sel = $GoodsModObj->get_attr_sel($attr_id);

                    foreach($sel as $s) {

                        $arr_attr_value = explode(',', $attr_value);

                        if(in_array($s['attr_sel_id'], $arr_attr_value)) {

                            $ret .= '<label><input type="checkbox" checked="checked" name="attr_'.$attr_id.'[]" value="'.$s['attr_sel_id'].'" />'.$s['attr_sel_name'].'</label> ';

                        } else {

                            $ret .= '<label><input type="checkbox" name="attr_'.$attr_id.'[]" value="'.$s['attr_sel_id'].'" />'.$s['attr_sel_name'].'</label> ';

                        }

                    }

                    $ret .= '</div>';

                    break;

            }

        }

        echo $ret;

        exit();

    }



    //商品还原

    public function reduction($goods_id){

        $ModelObj = new GoodsModel();

        $flag = $ModelObj->where("goods_id='$goods_id'")->setField('status',2);

        if ($flag > 0) {

            $this->success('还原成功');

        } else {

            $this->error($ModelObj->getError());

        }

    }

    

    //商品彻底删除

    public function del($goods_id) {

        $ModelObj = new GoodsModel();

        $flag = $ModelObj->del($goods_id);

        if ($flag > 0) {

            $this->success('彻底删除成功');

        } else {

            $this->error($ModelObj->getError());

        }

    }

    

    //一键情况回收站商品

    public function del_all() {

        $GoodsObj = new GoodsModel();

        $del_goods = $GoodsObj->field('goods_id')->where('status=3')->select();

        foreach($del_goods as $k => $v) {

            $GoodsObj->del($v['goods_id']);

        }

        $this->success('商品回收站已清空！');

    }

    

    /**

     * 选择推荐商品

     */

     public function select_tuijian() {

        $ModelObj = new GoodsModel();

        layout(false);

        //已选择

        $tuijian = I('get.tuijian','');

        $this->arr_tuijian = explode(',', $tuijian);

        //搜索筛选参数

        $conditions = array();

        $param = array('title'=>'title','cat_id'=>'cat_id','brand_id'=>'brand_id','status'=>'status');

        $this->paramValue($conditions, $param);

        $result = $ModelObj->search($conditions, 'goods_id,goods_name,goods_sn,cat_ids,brand_id,order_id,status,time_create,picture');

        $list = $result['list'];

        $CatObj = new GoodsCatModel();

        $BrandObj = new GoodsBrandModel();

        foreach($list as $k => $r) {

            $list[$k]['cat_name'] = $CatObj->getAllPathName($r['cat_ids']);

            $list[$k]['brand_name'] = $BrandObj->getFieldByBrandId($r['brand_id'],'brand_name');

        }

        //搜索类别列表

	$this->cat_list = $CatObj->getSubSelect(0, 5);

        //搜索品牌列表

        $this->brand_list = $BrandObj->get_sel();

        

        $this->list = $list; // 赋值数据集

        $this->page = $result['page'];

        

        $this->display();

     }

     

     /**

     * 显示推荐商品

     */

     public function show_tuijian($tuijian) {

        layout(false);

          //获得数据       

        $ModelObj = new GoodsModel();

        $map['goods_id'] = array('in',$tuijian);

        $this->list = $ModelObj->field('goods_id,picture,goods_name')->where($map)->select();

        $this->display();

     }

     

     /**

     * 首页选择推荐商品

     */

     public function select_index_tuijian($block_id,$style_id,$style_item_id,$cat_id = 0) {

        $ModelObj = new GoodsModel();

        layout(false);

        //已选择

        $tuijian = I('get.tuijian','');

        $this->arr_tuijian = explode(',', $tuijian);

        $this->block_id = $block_id;

        $this->style_id = $style_id;

        $this->style_item_id = $style_item_id;

        $this->cat_id = $cat_id;

        //搜索筛选参数

        $conditions = array();

        $param = array('title'=>'title','cat_id'=>'cat_id','brand_id'=>'brand_id','status'=>'status');

        $this->paramValue($conditions, $param);

        $result = $ModelObj->search($conditions, 'goods_id,goods_name,goods_sn,cat_ids,brand_id,order_id,status,time_create,picture');

        $list = $result['list'];

        $CatObj = new GoodsCatModel();

        $BrandObj = new GoodsBrandModel();

        foreach($list as $k => $r) {

            $list[$k]['cat_name'] = $CatObj->getAllPathName($r['cat_ids']);

            $list[$k]['brand_name'] = $BrandObj->getFieldByBrandId($r['brand_id'],'brand_name');

        }

        //搜索类别列表

	$this->cat_list = $CatObj->getSubSelect(0, 5);

        //搜索品牌列表

        $this->brand_list = $BrandObj->get_sel();

        

        $this->list = $list; // 赋值数据集

        $this->page = $result['page'];

        

        $this->display();

     }

     

     /**

     * 商品导入

     */

    public function export() {

        $CatObj = new GoodsCatModel();

        $BrandObj = new GoodsBrandModel();

        $this->cat_sel = $CatObj->getSubSelect(0, 5); //父类别

        $this->brand_sel = $BrandObj->get_sel(); //品牌选择

        $this->display();

    }

    

    /**

     * 商品csv导入AJAX操作

     */

    public function export_ajax() {

        $filename = I('get.filename');

        $cat_id = I('get.cat_id',0);

        $brand_id = I('get.brand_id',0);

        $status = I('get.status',2);

        $filename = $_SERVER['DOCUMENT_ROOT'].__ROOT__.$filename;

        if (!file_exists($filename)) {

            echo '文件不存在，请检查文件路径！';

            exit();

        }

        

        $file = fopen($filename,'r'); 

        //循环读取CSV里面的每一行内容存入$goods_list数组

        $row_index = 0;

        while ($data = fgetcsv($file)) {

            //检查文件编码

            if($row_index == 0) {

                if(trim($data[0]) == '﻿商品名称') {

                   echo '文件编码格式检查通过！<br>';

                } else {

                    echo '文件编码格式必须为UTF-8，请用记事本打开文件另存为UTF-8格式文件再重新上传导入！'.trim($data[0]);

                    fclose($file);

                    exit();

                }

            }

            $row_index++;

            if(count($data)<16) {

                echo '第'.$row_index.'行数据内容不完整('.count($data).')';

                fclose($file);

                exit();

            }

            if($data[0] != '') {

                $goods_list[] = $data;

            }

        }

        fclose($file);

        unset($goods_list[0]);//去掉第一行表头数据

        $total_row = count($goods_list);

        echo '共有商品数据'.$total_row.'行<br>';

        

        $count_success = 0;

        $GoodsObj = new GoodsModel();

        $MGoodsItem = M('GoodsItem');

        $CatObj = new GoodsCatModel();

        $cat_ids = ','.$cat_id.',';

        $goods_data['cat_ids'] = $cat_ids;

        $goods_data['model_id'] = 0;

        $goods_data['brand_id'] = $brand_id;

        $goods_data['status'] = $status;

        $goods_data['cat_path_ids'] = $CatObj->getPathIds($cat_ids);

        $goods_data['time_create'] = time();

        $goods_data['tuijian'] = '';

        $goods_data['spec_array'] = '';

        $goods_data['search_str'] = '';

        $goods_data['point'] = 0;

        $goods_data['price_cost'] = 0;

        $goods_data['is_hot'] = 0;

        $goods_data['is_index'] = 0;

        $goods_data['count_fav'] = 0;

        $goods_data['count_hits'] = 0;

        $goods_data['count_sale'] = 0;

        $goods_data['count_comment'] = 0;

        $goods_data['comment_grade'] = 5;

        foreach($goods_list as $k => $goods) {

            $goods_d = $goods_data;

            $goods_d['goods_name'] = $goods[0];

            $goods_d['cuxiao_msg'] = $goods[1];

            $goods_d['picture'] = $goods[2];

            $goods_d['picture_list'] = $goods[3];

            $goods_d['goods_sn'] = $goods[4];

            $goods_d['price'] = $goods[5];

            $goods_d['price_market'] = $goods[6];

            $goods_d['store_num'] = $goods[7];

            $goods_d['weight'] = $goods[8];

            $goods_d['unit'] = $goods[9];

            $goods_d['detail'] = $goods[10];

            $goods_d['detail2'] = $goods[11];

            $goods_d['order_id'] = $goods[12];

            $goods_d['seo_title'] = $goods[13];

            $goods_d['seo_keywords'] = $goods[14];

            $goods_d['seo_description'] = $goods[15];

            $goods_id = $GoodsObj->add($goods_d);

            if($goods_id > 0) {

                $goods_d_item['goods_id'] = $goods_id;

                $goods_d_item['goods_sn'] = $goods_d['goods_sn'];

                $goods_d_item['price'] =  $goods_d['price'];

                $goods_d_item['price_cost'] =  $goods_d['price_cost'];

                $goods_d_item['price_market'] =  $goods_d['price_market'];

                $goods_d_item['store_num'] =  $goods_d['store_num'];

                $goods_d_item['weight'] = $goods_d['weight'];

                $goods_d_item['spec_array'] = '';

                $goods_d_item['order_id'] = 0;

                $MGoodsItem->add($goods_d_item);

                $count_success++;

            }

            

        }

        echo '成功导入商品'.$count_success.'条！';

           

    }



}

