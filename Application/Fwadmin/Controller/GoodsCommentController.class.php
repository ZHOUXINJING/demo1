<?php

namespace Fwadmin\Controller;

use Fwadmin\Model\GoodsCommentModel;
use Common\Controller\FwadminController;

class GoodsCommentController extends FwadminController {

    public function index() {
        $ModelObj = new GoodsCommentModel();
		
		 if (IS_AJAX) {
            $t = I('get.t');
            $v = I('get.v');
            $id = I('get.i');
            echo $ModelObj->where('id=' . $id)->setField($t, $v);
            exit();
        }
		
        //获得数据       
        $conditions = array();
        $param = array('title'=>'title','goods_id'=>'goods_id','member_id'=>'member_id');
        $this->paramValue($conditions, $param);
        $results = $ModelObj->search($conditions);
        $lists = $results['list'];

        $this->assign('list', $lists); // 赋值数据集
        $this->assign('page', $results['page']); // 赋值分页输出
        $this->display();
    }
    
    //商品彻底删除
    public function del($id) {
        $ModelObj = new GoodsCommentModel();
        $flag = $ModelObj->del($id);
        if ($flag > 0) {
            $this->success('删除成功');
        } else {
            $this->error($ModelObj->getError());
        }
    }
    
    /**
     * 批量删除商品
     * @return int 返回商品数目
     */
    public function delsel() {
        $ModelObj = new GoodsCommentModel();
        $selid = I('post.selid');
        $count = 0;
        foreach($selid as $id) {
            $count += $ModelObj->del($id);
        }
        if($count > 0) {
            $this->success('删除'.$count.'个评论内容成功');
        } else {
            $this->error('删除评论失败');
        }
    }
	
	public function reply(){
	 $id = I('id');
	 $ModelObj = new GoodsCommentModel();
	 $model = $ModelObj->where(Array('id'=>$id))->find();
	 $this->model2 = M('goods')->where(Array('goods_id'=>$model['goods_id']))->find();
	 $this->model = $model;
	 $this->display();
	}
	
	/*****提交回复****/
	
   public function submit_reply(){
      if(IS_AJAX){
		  $ModelObj = new GoodsCommentModel();
	    $result['title']='success';
		 $id = I('post.id');
		 $status = I('post.status');
		 $reply_detail = I('post.reply_detail');//回复内容
		$data['status'] = $status;
		$data['reply_detail'] = $reply_detail;
		$data['reply_time'] = time();
		
		$flag = $ModelObj->where(Array('id'=>$id))->data($data)->save();
		if($flag>-1){
		$result['msg']='回复成功';
		}else{
		$result['title']='success';
		$result['msg']='回复失败';
		}
		 
		$this->ajaxReturn($result);
	  
	  }
     
   }
}
