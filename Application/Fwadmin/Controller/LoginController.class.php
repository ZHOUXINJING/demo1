<?php

namespace Fwadmin\Controller;

use Think\Controller;

class LoginController extends Controller {

    public function _initialize() {
        if (I('get.sessionid', '') != '') {
            session_id(I('get.sessionid'));
            session('[pause]');
            session('[start]');
        }
    }

    /**
     * 登录页面
     */
    public function index() {
        layout(false);
        if (IS_POST) {
            $AdminObj = new \Fwadmin\Model\AdminModel();
            if ($AdminObj->create($_POST, 5)) {
                $AdminObj->password = strongmd5($AdminObj->password);
                $verify = new \Think\Verify();
                if (!$verify->check(I('post.code'), '')) {
                    $this->error('验证码错误！' . $AdminObj->code, U('index'));
                } else {
                    $arr = $AdminObj->where(Array('username' => $AdminObj->username, 'password' => $AdminObj->password))->find();
                    if (is_array($arr)) {
                        if ($arr['is_enable'] != 1) {
                            $this->error('你的账号已被禁用！');
                        }
                        session('admin_id', $arr['admin_id']);
                        session('admin_username', $arr['username']);
                        session('admin_group_id', $arr['group_id']);
                        $key_login_code = $arr['admin_id'] . $arr['group_id'] . randomname(30);
                        $arr['key_login_code'] = $key_login_code;
                        $arr['last_login_time'] = time();
                        $arr['last_login_ip'] = get_client_ip();
                        $arr['login_times'] = $arr['login_times'] + 1;
                        $AdminObj->save($arr);
                        if (I('post.remember') == '1') {
                            $expire = 604800;
                            cookie('admin_id', $arr['admin_id'], $expire);
                            cookie('key_login_code', $key_login_code, $expire);
                        }
                        $ARObj = new \Fwadmin\Model\SystemRecordModel();
                        $ARObj->addrecord("登陆");
                        $this->redirect("Index/main");
                    } else {
                        $this->error('用户名或密码不对！');
                    }
                }
            } else {
                $this->error($AdminObj->getError());
            }
        }
        $this->display();
    }

    /**
     * 退出登录页面
     */
    public function logout() {
        $admin_id = I('session.admin_id', 0);
        $AdminObj = new \Fwadmin\Model\AdminModel();
        $AdminObj->where(Array('admin_id' => $admin_id))->setField('key_login_code', '');
        $ARObj = new \Fwadmin\Model\SystemRecordModel();
        $ARObj->addrecord("退出");
        cookie('admin_id', null);
        cookie('key_login_code', null);
        cookie(null);
        session(null);
        $this->success('退出成功！', U('index'));
        exit();
    }

    /**
     * 显示验证码
     */
    public function verify() {
        layout(false);
        $Verify = new \Think\Verify();
        $Verify->fontSize = 16;
        $Verify->useImgBg = false;
        $Verify->length = 4;
        $Verify->useNoise = false;
        $Verify->useCurve = false;
        $Verify->codeSet = '0123456789';
        $Verify->entry();
    }

    /**
     * uploadify上传图片
     */
    public function uploadPic($path) {
        //$admin_id = I('session.admin_id', 0);
        //if ($admin_id != 0) {
            $upload = new \Think\Upload(); // 实例化上传类
            $upload->exts = array('jpg', 'bmp', 'gif', 'png', 'jpeg'); // 设置附件上传类型
            $upload->saveName = array('randomname', '6');
            $upload->maxSize = 2024000;
            $upload->rootPath = 'Public'; // 设置附件上传目录
            $upload->savePath = '/upload/' . $path . '/';
            $upload->autoSub = false;
            $info = $upload->upload();
            if (!$info) {// 上传错误提示错误信息
                $data['Message'] = $upload->getError();
                $data['Success'] = false;
            } else {// 上传成功
                foreach ($info as $file) {
                    $filepath = "/" . $upload->rootPath . $file['savepath'] . $file['savename'];
                    $data['Success'] = true;
                    $data['SaveName'] = $filepath;
                    $thumbw = I('get.thumbw', '');
                    $thumbh = I('get.thumbh', '');
                    if ($thumbw != '' || $thumbh != '') {
                        $thumbfolder = './' . $upload->rootPath . $file['savepath'] . 'thumb/';
                        if (!is_dir($thumbfolder)) {
                            mkdir($thumbfolder, 0777, true);
                        }
                        $thumbpath =  $thumbfolder . $file['savename'];
                        $image = new \Think\Image();
                        $image->open('./' . $filepath);
                        if($path == 'products') {
                            $image->thumb($thumbw, $thumbh, \Think\Image::IMAGE_THUMB_CENTER)->save($thumbpath);
                        } else {
                            $image->thumb($thumbw, $thumbh, \Think\Image::IMAGE_THUMB_FILLED)->save($thumbpath);
                        }
                    }
                }
            }
            $this->ajaxReturn($data);
       // } else {
       //     echo $admin_id;
       // }
    }

    /**
     * uploadify上传多张图片
     */
    public function uploadMorePic($path) {
       // $admin_id = I('session.admin_id', 0);
       // if ($admin_id != 0) {
            $upload = new \Think\Upload(); // 实例化上传类
            $upload->exts = array('jpg', 'bmp', 'gif', 'png', 'jpeg'); // 设置附件上传类型
            $upload->saveName = array('randomname', '6');
            $upload->maxSize = 2024000;
            $upload->rootPath = 'Public'; // 设置附件上传目录
            $upload->savePath = '/upload/' . $path . '/';
            $upload->autoSub = false;
            $info = $upload->upload();
            if (!$info) {// 上传错误提示错误信息
                $data['Message'] = $upload->getError();
                $data['Success'] = false;
            } else {// 上传成功
                foreach ($info as $file) {
                    $filepath = "/" . $upload->rootPath . $file['savepath'] . $file['savename'];
                    $data['Success'] = true;
                    $data['SaveName'] = $filepath;
                    $thumbw = I('get.thumbw', '');
                    $thumbh = I('get.thumbh', '');
                    if ($thumbw != '' || $thumbh != '') {
                        $thumbpath = './' . $upload->rootPath . $file['savepath'] . 'thumb/' . $file['savename'];
                        $image = new \Think\Image();
                        $image->open('./' . $filepath);
                        $image->thumb($thumbw, $thumbh, \Think\Image::IMAGE_THUMB_FILLED)->save($thumbpath);
                    }
                   
                }
            }
            $this->ajaxReturn($data);
       // } else {
       //     echo $admin_id;
       // }
    }

    /**
     * uploadify上传文件
     */
    public function uploadFile($path) {
       // $admin_id = I('session.admin_id', 0);
       // if ($admin_id != 0) {
            $upload = new \Think\Upload(); // 实例化上传类
            $upload->exts = array('doc', 'txt', 'pdf', 'xls', 'ppt', 'docx', 'pptx', 'xlsx', 'rar', 'zip', 'jpg', 'bmp', 'gif', 'png', 'jpeg','csv'); // 设置附件上传类型
            $upload->saveName = array('randomname', '6');
            $upload->maxSize = 2024000;
            $upload->rootPath = 'Public'; // 设置附件上传目录
            $upload->savePath = '/upload/' . $path . '/';
            $upload->autoSub = false;
            $info = $upload->upload();
            if (!$info) {// 上传错误提示错误信息
                $data['Message'] = $upload->getError();
                $data['Success'] = false;
            } else {// 上传成功
                foreach ($info as $file) {
                    $filepath = "/" . $upload->rootPath . $file['savepath'] . $file['savename'];
                    $data['Success'] = true;
                    $data['SaveName'] = $filepath;
                }
            }
            $this->ajaxReturn($data);
       // } else {
       //     echo $admin_id;
       // }
    }

}
