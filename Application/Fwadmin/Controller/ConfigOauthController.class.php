<?php

namespace Fwadmin\Controller;

use Common\Controller\FwadminController;
use Fwadmin\Model\ConfigOauthModel;

class ConfigOauthController extends FwadminController {
    /*
     * 配送方式列表
     */

    public function index() {
        //获得数据       
        $ModelObj = new ConfigOauthModel();
        if (IS_AJAX) {
            $t = I('get.t');
            $v = I('get.v');
            $id = I('get.i');
            echo $ModelObj->where('id=' . $id)->setField($t, $v);
            exit();
        }
        $this->list = $ModelObj->get_list();
        $this->display();
    }

    /*
     * 添加配送方式
     */

    public function add() {
        $ModelObj = new ConfigOauthModel();
        if (IS_POST) {
            if ($ModelObj->create()) {
                $par['par_id'] = I('post.par_id', '');
                $par['par_key'] = I('post.par_key', '');
                $ModelObj->par_array = json_encode($par);
                $id = $ModelObj->add();
                if ($id > 0)
                    $this->success('添加成功', U('index'));
                else
                    $this->error($ModelObj->getLastSql());
            } else {
                $this->error($ModelObj->getError());
            }
        } else {
            $this->display();
        }
    }

    /*
     * 修改配送方式
     */

    public function edit($id) {
        $ModelObj = new ConfigOauthModel();
        if (IS_POST) {
            if ($ModelObj->create()) {
                $par['par_id'] = I('post.par_id', '');
                $par['par_key'] = I('post.par_key', '');
                $ModelObj->par_array = json_encode($par);
                $flag = $ModelObj->save();
                if ($flag !== false)
                    $this->success('修改成功', U('index'));
                else
                    $this->error($ModelObj->getLastSql());
            } else {
                $this->error($ModelObj->getError());
            }
        } else {
            $model = $ModelObj->find($id);

            $par_array = json_decode($model['par_array']);
            $model['par_id'] = $par_array->par_id;
            $model['par_key'] = $par_array->par_key;

            $this->model = $model;
            $this->display();
        }
    }

    /*
     * 删除配送方式
     */

    public function del($id) {
        $ModelObj = new ConfigOauthModel();
        $flag = $ModelObj->del($id);
        if ($flag > 0)
            $this->success('删除成功');
        else
            $this->error($ModelObj->getError());
    }

}
