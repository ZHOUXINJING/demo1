<?php

namespace Fwadmin\Controller;

use Common\Controller\FwadminController;
use Fwadmin\Model\GoodsSpecModel;
//商品品牌

class GoodsSpecController extends FwadminController {

    /**
     * 规格列表
     */
    public function index() {
        $ModelObj = new GoodsSpecModel();
        if (IS_AJAX) {
            $t = I('get.t');
            $v = I('get.v');
            $id = I('get.i');
            echo $ModelObj->where('spec_id=' . $id)->setField($t, $v);
            exit();
        }
        //获得数据
        $result = $ModelObj->search(array());
        $list = $result['list'];
        foreach($list as $k => $v) {
            $arr_spec_value = unserialize($v['spec_value']);
            $str = '';
            foreach($arr_spec_value as $v) {
                 $str .= $v['name'].',';
            }
            $str = trim($str,',');
            $list[$k]['spec_value'] = $str;
        }
        $this->list = $list;
        $this->page = $result['page'];
        $this->display();
    }

    /**
     * 添加规格
     */
    public function add() {
        $ModelObj = new GoodsSpecModel();
        if (IS_POST) {
            if ($ModelObj->create()) {
                $arr_sel = I('sel_name');//获取选项值
                $arr_sel_pic = I('sel_pic');//获取图片值
                $arr = array();
                foreach($arr_sel as $k => $v) {
                    if($v != '') {
                        $arr[$k]['name'] = $v;
                        $arr[$k]['pic'] = $arr_sel_pic[$k];
                    }
                }
                $ModelObj->spec_value = serialize($arr);//序列化选项值保存
                $spec_id = $ModelObj->add();
                if ($spec_id > 0) {
                    $this->success('添加成功', U('index'));
                } else {
                    $this->error($ModelObj->getError());
                }
            } else {
                $this->error($ModelObj->getError());
            }
        } else {
            $this->display();
        }
    }

     /**
     * 编辑规格
     */
    public function edit($spec_id) {
        $ModelObj = new GoodsSpecModel();
        if (IS_POST) {
            if ($ModelObj->create()) {
                $arr_sel = I('sel_name');//获取选项值
                $arr_sel_pic = I('sel_pic');//获取图片值
                $arr = array();
                foreach($arr_sel as $k => $v) {
                    if($v != '') {
                        $arr[$k]['name'] = $v;
                        $arr[$k]['pic'] = $arr_sel_pic[$k];
                    }
                }
                $ModelObj->spec_value = serialize($arr);//序列化选项值保存
                $flag = $ModelObj->save();
                if ($flag !== false)
                    $this->success('修改成功', U('index'));
                else
                    $this->error($ModelObj->getError());
            } else {
                $this->error($ModelObj->getError());
            }
        }else {
            $model = $ModelObj->find($spec_id);
            //序列化选项
            $this->spec_sel = unserialize($model['spec_value']);
            $this->model = $model;
            $this->display();
        }
    }
    
     /**
     * 删除规格
     */
    public function del($spec_id) {
        $ModelObj = new GoodsSpecModel();
        $flag = $ModelObj->del($spec_id);
        if ($flag > 0)
            $this->success('删除成功');
        else
            $this->error('删除失败');
    }
}