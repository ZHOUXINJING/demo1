<?php
namespace Fwadmin\Controller;
use Think\Controller;
use Common\Controller\FwadminController;
//调取全国省份城市地区邮政编码
class AddressController extends FwadminController {

    /**
     * 获取城市选择
     * @param string $province 所选城市名称
     * @param string $city 已选择城市名称
     */
    public function city_sel($province,$city) {
        $ProvinceObj = M('AddressProvince');
        $CityObj = M('AddressCity');
        $provinceid = $ProvinceObj->getFieldByProvince($province,'provinceid');//获取省份ID
        $str = '';
        if($province == "") exit();
        if(!empty($provinceid)) {
            $city_sel = $CityObj->field('city')->where('provinceid='.$provinceid)->select();
            $str = '<select name="city" id="city" onchange="change_city()" class="sel1">';
            $str .= '<option value="">选择城市</option>';
            foreach($city_sel as $c) {
                if($c['city'] == $city) {
                    $str .= '<option selected="selected" value="'.$c['city'].'">'.$c['city'].'</option>';
                } else {
                    $str .= '<option value="'.$c['city'].'">'.$c['city'].'</option>';
                }
            }
            $str .= '</select>';
        }
        echo $str;
        exit();
    }
    
    /**
     * 获取城市选择
     * @param string $province 所选城市名称
     * @param string $city 已选择城市名称
     */
    public function city_sel2($province,$city) {
        $ProvinceObj = M('AddressProvince');
        $CityObj = M('AddressCity');
        $provinceid = $ProvinceObj->getFieldByProvince($province,'provinceid');//获取省份ID
        $str = '';
        if($province == "") exit();
        if(!empty($provinceid)) {
            $city_sel = $CityObj->field('city')->where('provinceid='.$provinceid)->select();
            $str = '<select name="city" id="city"  class="sel1">';
            $str .= '<option value="所有城市">所有城市</option>';
            foreach($city_sel as $c) {
                if($c['city'] == $city) {
                    $str .= '<option selected="selected" value="'.$c['city'].'">'.$c['city'].'</option>';
                } else {
                    $str .= '<option value="'.$c['city'].'">'.$c['city'].'</option>';
                }
            }
            $str .= '</select>';
        }
        echo $str;
        exit();
    }
    
    
    /**
     * AJAX获取地区选择
     * @param string $city 城市
     * @param string $area 已选择地区
     */
    public function area_sel($city,$area) {
        $CityObj = M('AddressCity');
        $AreaObj = M('AddressArea');
        if($city == "") exit();
        $cityid = $CityObj->getFieldByCity($city,'cityid'); //获取城市ID
        $str = '';
        if(!empty($cityid)) {
            $area_sel = $AreaObj->field('area')->where('cityid='.$cityid)->select();
            $str = '<select name="area" id="area" onchange="change_area()" class="sel1">';
            $str .= '<option value="">选择地区</option>';
            foreach($area_sel as $a) {
                if($a['area'] == $area) {
                    $str .= '<option selected="selected" value="'.$a['area'].'">'.$a['area'].'</option>';
                } else {
                    $str .= '<option value="'.$a['area'].'">'.$a['area'].'</option>';
                }
            }
            $str .= '</select>';
        }
        echo $str;
        exit();
    }
    
    /*
     * AJAX获取邮政编码
     * @param string $area 地区名称
     */
    public function get_zip($area) {
        $AreaObj = M('AddressArea');
        $zipcode = $AreaObj->getFieldByArea($area,'zipcode');
        echo $zipcode;
        exit();
    }


}
