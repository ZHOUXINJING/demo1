<?php

namespace Fwadmin\Controller;

use Common\Controller\FwadminController;
use Fwadmin\Model\MemberPointModel;

class MemberPointController extends FwadminController {

    /**
     * 会员积分记录
     */
    public function index() {
        //获得数据       
        $MemberPointObj = new MemberPointModel();
        $conditions = array();
        $param = array('time_begin' => 'time_begin', 'time_end' => 'time_end', 'username' => 'username');
        $this->paramValue($conditions, $param);
        if (I('get.submit','') == '导出报表') {
            $this->export($conditions);
        }
        $results = $MemberPointObj->search($conditions);
        $lists = $results['list'];
        $this->assign('list', $lists); // 赋值数据集
        $this->assign('page', $results['page']); // 赋值分页输出

        $this->display();
    }
    
    /**
     *  会员积分记录-导出报表
     */
    public function export() {
        $MemberPointObj = new MemberPointModel();
        $results = $MemberPointObj->search($conditions);
        $list = $results['list'];
        $filename = 'member_point_record_' . $conditions['time_begin'] .'_'. $conditions['time_begin'];
        $sheet1 = '会员积分记录变更表';
        $data_title = array();
        $data_title['A1'] = '编号';
        $data_title['B1'] = '会员';
        $data_title['C1'] = '积分变化';
        $data_title['D1'] = '操作后积分';
        $data_title['E1'] = '发生时间';
        $data_title['F1'] = '备注';
        $data_title['G1'] = '操作人';

        $data_content = array();
        foreach ($list as $k => $v) {
            $num = $k + 2;
            $data_content['A' . $num] = $k + 1;
            $data_content['B' . $num] = show_member_name($v['member_id']);
            $data_content['C' . $num] = $v['point_change'];
            $data_content['D' . $num] = $v['point'];
            $data_content['E' . $num] = date('Y-m-d h:m:s',$v['create_time']);
            $data_content['F' . $num] = $v['remark'];
            $data_content['G' . $num] = show_admin_name($v['admin_id']);
        }
        excelExport($filename, $data_title, $data_content, $sheet1);
    }
    
    /*
     * 会员积分修改
     */
    public function change() {
        if(IS_POST) {
            $username = I('post.username','');
            $remark = I('post.remark','');
            $point_type = I('post.point_type','');
            $point = I('post.point',0);
            //验证用户名
            if($username == '') {
                $this->error('请输入用户名');
            }
            $MemberObj = new \Fwadmin\Model\MemberModel();
            $member_id = $MemberObj->get_member_id($username);
            if(empty($member_id)) {
                $this->error('不存在此用户！');
            }
            //验证积分类型及值
            if($point == 0 || !check_int($point)) {
                $this->error('请输入整形数据！');
            }
            if($point_type == '-') {
                $point = - $point;
            }
            if($point == 0) {
                $this->error('积分值不能为0！');
            }
            $admin_id = I('session.admin_id', 0);//管理员ID
            //添加操作
            $ModelObj = new MemberPointModel();
            $flag = $ModelObj->add_record($member_id, $point, $remark, $admin_id);
            if($flag > 0) {
                $this->success('提交成功！');
            } else {
                $this->error('提交失败！');
            }
        } else {
            $this->display();
        }
    }
    
    /*
     * 获得会员积分信息
     */
    public function get_member_message($username) {
        if($username == '') {
            echo '<font color=red>请输入会员名！</font>';
            exit();
        }
        $MemberObj = new \Fwadmin\Model\MemberModel();
        $member_id = $MemberObj->get_member_id($username);
        if(empty($member_id)) {
            echo '<font color=red>不存在此会员！</font>';
        } else {
            $point = $MemberObj->get_point($member_id);
            echo '<font color=green>会员：'.$username.' 当前积分：'.$point.'</font>';
        }
        exit();
    }


}
