<?php

namespace Fwadmin\Model;

use Think\Model;

class MemberPointModel extends Model {
    
    /**
     * 注册赠送积分
     * @param int $member_id 会员ID
     * @param int $point_register 赠送的积分
     * @return int $flag 1操作成功
     */
    public function add_by_register($member_id,$point_register) {
        if($point_register > 0) {
            return $this->add_record($member_id, $point_register, '注册赠送');
        } else {
            return -1;
        }
    }
    
    /**
     * 每天登陆赠送积分
     * @param int $member_id 会员ID
     * @param int $point_login_day 赠送的积分
     * @return int $flag 1操作成功
     */
    public function add_by_login($member_id,$point_login_day) {
        if($point_login_day > 0) {
            //判断是否已领取
            $yestoday = strtotime(date('Y-m-d 00:00:00', time()));
            $count = $this->where(array('member_id' => $member_id, 'create_time' => array('egt', $yestoday), 'remark' => '每日登陆赠送'))->count();
            if($count <= 0) {
                return $this->add_record($member_id, $point_login_day, '每日登陆赠送');
            }
        } 
        return -1;
    }
    
    /**
     * 订单购买赠送
     * @param int $order_id 订单ID
     * @param int $point_order_percent 订单购买赠送积分比例
     * @return int $flag 1操作成功
     */
    public function add_by_buy($order_id,$point_order_percent) {
        if($point_order_percent > 0) {
            $OrderObj = new \Fwadmin\Model\OrderModel();
            $order = $OrderObj->field('member_id,total,order_no')->where('order_id='.$order_id)->find();
            $member_id = $order['member_id'];
            $order_total = $order['total'];
            $order_no = $order['order_no'];
            $point_order = round($order_total * $point_order_percent);
            if($point_order > 0) {
                return $this->add_record($member_id, $point_order, '订单'.$order_no.'购买赠送');
            }
        }
        return -1;
    }
	
	
	    /**
     * 订单购买赠送2
     * @param int $order_id 订单ID
     * @param int $point_order_percent 订单购买赠送积分比例
     * @return int $flag 1操作成功
     */
    public function add_by_buy2($order_id) {
     
            $OrderObj = new \Fwadmin\Model\OrderModel();
            $order = $OrderObj->field('member_id,total,order_no')->where('order_id='.$order_id)->find();
			
            $member_id = $order['member_id'];
            $order_total = $order['total'];
            $order_no = $order['order_no'];
			$order_list = M('order_goods')->where(Array('order_id'=>$order_id))->select();
			$point_order = 0;//初始为0积分
			
			foreach($order_list as $k =>$v){
			 $point = M('goods')->getFieldByGoodsId($v['goods_id'],'point');
			 $sutoal_point = $point*$v['goods_num'];
			 $point_order +=$sutoal_point;
			}
			
             $point_order = round($point_order);
            if($point_order > 0) {
				
                return $this->add_record($member_id, $point_order, '订单'.$order_no.'购买赠送');
            }
     
        return -1;
    }
	
    
    /**
     * 评论产品赠送积分
     * @param int $comment_id 评论ID
     * @param int $member_id 会员ID
     * @param int $point_comment 赠送的积分
     * @return int $flag 1操作成功
     */
    public function add_by_comment($comment_id,$member_id,$point_comment) {
        if($point_comment > 0) {
            return $this->add_record($member_id, $point_comment, '评论赠送（ID:'.$comment_id.')');
        } 
        return -1;
    }

    /**
     * 增加记录
     * @param int $member_id 会员ID
     * @param int $point_change 变更的点数
     * @param string $remark 备注
     * @param int $admin_id 管理员ID
     */
    public function add_record($member_id, $point_change, $remark, $admin_id = 0) {
        $MemberObj = new \Fwadmin\Model\MemberModel();
        $point_end = $MemberObj->set_point($member_id, $point_change);
		
        $data['member_id'] = $member_id;
        $data['point_change'] = $point_change;
        $data['point'] = $point_end;
        $data['create_time'] = time();
        $data['remark'] = $remark;
        $data['admin_id'] = $admin_id;
        return $this->data($data)->add();
    }

    /**
     * 后台获取经验值记录
     * @param array $conditions 条件数组
     * @param string $order 排序字段
     * @param int $pagesize 分页数
     */
    public function search($conditions, $order = 'id desc', $pagesize = 0) {
        $map = $conditions;
        if (isset($map['username'])) {
            $MemberObj = new \Fwadmin\Model\MemberModel();
            $member_id = $MemberObj->get_member_id($map['username']);
            $map['member_id'] = $member_id;
            unset($map['username']);
        }
        if (isset($map['time_begin']) && isset($map['time_end'])) {
            $time_begin = strtotime($map['time_begin'].' 00:00:00');
            $time_end = strtotime($map['time_end'].' 23:59:59');
            $map['create_time'] = array('between',array($time_begin,$time_end));
            unset($map['time_begin']);
            unset($map['time_end']);
        } elseif (isset($map['time_begin'])) {
            $time_begin = strtotime($map['time_begin'].' 00:00:00'); 
            $map['create_time'] = array('egt',$time_begin);
            unset($map['time_begin']);
        } elseif (isset($map['time_end'])) {
            $time_end = strtotime($map['time_end'].' 23:59:59'); 
            $map['create_time'] = array('elt',$time_end);
            unset($map['time_end']);
        }
        
        $count = $this->where($map)->count(); // 查询满足要求的总记录数
        if ($pagesize == 0) {
            $pagesize = C('PAGE_NUM');
        }
        $Page = new \Think\Page($count, $pagesize); // 实例化分页类 传入总记录数和每页显示的记录数
        //分页跳转的时候保证查询条件
        foreach ($conditions as $key => $val) {
            $Page->parameter[$key] = urlencode($val);
        }
        $show = $Page->show(); // 分页显示输出
        // 进行分页数据查诟 注意limit 方法癿参数要使用Page 类的属性
        $list = $this->where($map)->order($order)->limit($Page->firstRow . ',' . $Page->listRows)->select();
        return array(
            'list' => $list,
            'page' => $show
        );
    }
    
    /**
     * 会员中心获取经验值记录
     * @param array $conditions 条件数组
     * @param string $order 排序字段
     * @param int $pagesize 分页数
     */
    public function search_member($member_id,$conditions, $order = 'id desc', $pagesize = 20, $limit = 0) {
        $map = $conditions;
        $map['member_id'] = $member_id;
        if (isset($map['time_begin']) && isset($map['time_end'])) {
            $time_begin = strtotime($map['time_begin'].' 00:00:00');
            $time_end = strtotime($map['time_end'].' 23:59:59');
            $map['create_time'] = array('between',array($time_begin,$time_end));
            unset($map['time_begin']);
            unset($map['time_end']);
        } elseif (isset($map['time_begin'])) {
            $time_begin = strtotime($map['time_begin'].' 00:00:00'); 
            $map['create_time'] = array('egt',$time_begin);
            unset($map['time_begin']);
        } elseif (isset($map['time_end'])) {
            $time_end = strtotime($map['time_end'].' 23:59:59'); 
            $map['create_time'] = array('elt',$time_end);
            unset($map['time_end']);
        }
        
        $count = $this->where($map)->count(); // 查询满足要求的总记录数
        $Page = new \Think\Page($count, $pagesize); // 实例化分页类 传入总记录数和每页显示的记录数
        //分页跳转的时候保证查询条件
        foreach ($conditions as $key => $val) {
            $Page->parameter[$key] = urlencode($val);
        }
        $show = $Page->show(); // 分页显示输出
        // 进行分页数据查诟 注意limit 方法癿参数要使用Page 类的属性
        if (empty($limit)) {
            $list = $this->where($map)->order($order)->limit($Page->firstRow . ',' . $Page->listRows)->select();
        } else {
            $start = ($limit - 1) * $pagesize;
            $end = $pagesize * $limit;
            $list = $this->where($map)->order($order)->limit($start . ',' . $end)->select();
        }

        return array(
            'list' => $list,
            'page' => $show
        );
    }
}
