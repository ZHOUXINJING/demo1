<?php

namespace Fwadmin\Model;

use Think\Model;

class MemberMoneyModel extends Model {

    /**
     * 增加记录
     * @param int $member_id 会员ID
     * @param int $money_change 变更的点数
     * @param string $remark 备注
     * @param int $admin_id 管理员ID
     */
    public function add_record($member_id, $money_change, $remark, $admin_id = 0) {
        $MemberObj = new \Fwadmin\Model\MemberModel();
        $money_end = $MemberObj->set_money($member_id, $money_change);
        $data['member_id'] = $member_id;
        $data['money_change'] = $money_change;
        $data['money'] = $money_end;
        $data['create_time'] = time();
        $data['remark'] = $remark;
        $data['admin_id'] = $admin_id;
        return $this->data($data)->add();
    }

    /**
     * 后台获取经验值记录
     * @param array $conditions 条件数组
     * @param string $order 排序字段
     * @param int $pagesize 分页数
     */
    public function search($conditions, $order = 'id desc', $pagesize = 0) {
        $map = $conditions;
        if (isset($map['username'])) {
            $MemberObj = new \Fwadmin\Model\MemberModel();
            $member_id = $MemberObj->get_member_id($map['username']);
            $map['member_id'] = $member_id;
            unset($map['username']);
        }
        if (isset($map['time_begin']) && isset($map['time_end'])) {
            $time_begin = strtotime($map['time_begin'].' 00:00:00');
            $time_end = strtotime($map['time_end'].' 23:59:59');
            $map['create_time'] = array('between',array($time_begin,$time_end));
            unset($map['time_begin']);
            unset($map['time_end']);
        } elseif (isset($map['time_begin'])) {
            $time_begin = strtotime($map['time_begin'].' 00:00:00'); 
            $map['create_time'] = array('egt',$time_begin);
            unset($map['time_begin']);
        } elseif (isset($map['time_end'])) {
            $time_end = strtotime($map['time_end'].' 23:59:59'); 
            $map['create_time'] = array('elt',$time_end);
            unset($map['time_end']);
        }
        
        $count = $this->where($map)->count(); // 查询满足要求的总记录数
        if ($pagesize == 0) {
            $pagesize = C('PAGE_NUM');
        }
        $Page = new \Think\Page($count, $pagesize); // 实例化分页类 传入总记录数和每页显示的记录数
        //分页跳转的时候保证查询条件
        foreach ($conditions as $key => $val) {
            $Page->parameter[$key] = urlencode($val);
        }
        $show = $Page->show(); // 分页显示输出
        // 进行分页数据查诟 注意limit 方法癿参数要使用Page 类的属性
        $list = $this->where($map)->order($order)->limit($Page->firstRow . ',' . $Page->listRows)->select();
        return array(
            'list' => $list,
            'page' => $show
        );
    }
    
    /**
     * 会员中心获取经验值记录
     * @param array $conditions 条件数组
     * @param string $order 排序字段
     * @param int $pagesize 分页数
     */
    public function search_member($member_id,$conditions, $order = 'id desc', $pagesize = 20) {
        $map = $conditions;
        $map['member_id'] = $member_id;
        if (isset($map['time_begin']) && isset($map['time_end'])) {
            $time_begin = strtotime($map['time_begin'].' 00:00:00');
            $time_end = strtotime($map['time_end'].' 23:59:59');
            $map['create_time'] = array('between',array($time_begin,$time_end));
            unset($map['time_begin']);
            unset($map['time_end']);
        } elseif (isset($map['time_begin'])) {
            $time_begin = strtotime($map['time_begin'].' 00:00:00'); 
            $map['create_time'] = array('egt',$time_begin);
            unset($map['time_begin']);
        } elseif (isset($map['time_end'])) {
            $time_end = strtotime($map['time_end'].' 23:59:59'); 
            $map['create_time'] = array('elt',$time_end);
            unset($map['time_end']);
        }
        
        $count = $this->where($map)->count(); // 查询满足要求的总记录数
        $Page = new \Think\Page($count, $pagesize); // 实例化分页类 传入总记录数和每页显示的记录数
        //分页跳转的时候保证查询条件
        foreach ($conditions as $key => $val) {
            $Page->parameter[$key] = urlencode($val);
        }
        $show = $Page->show(); // 分页显示输出
        // 进行分页数据查诟 注意limit 方法癿参数要使用Page 类的属性
        $list = $this->where($map)->order($order)->limit($Page->firstRow . ',' . $Page->listRows)->select();
        return array(
            'list' => $list,
            'page' => $show
        );
    }
}
