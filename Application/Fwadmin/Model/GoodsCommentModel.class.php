<?php

namespace Fwadmin\Model;

use Think\Model;
/**
 * 商品品牌
 */
class GoodsCommentModel extends Model {

     /**
     * 搜索
     * @param array $conditions 条件数组
     * @param string $order 排序字段
     * @param int $page 分页数
     */
    public function search($conditions, $order = 'id desc', $pagesize = 20) {
        $map = $conditions;
        if(isset($map['title'])) {
            $map['detail'] = array('like','%'.$map['title'].'%');
            unset($map['title']);
        }
        $count = $this->where($map)->count(); // 查询满足要求的总记录数

        $Page = new \Think\Page($count, $pagesize); // 实例化分页类 传入总记录数和每页显示的记录数
        //分页跳转的时候保证查询条件
        foreach ($conditions as $key => $val) {
            $Page->parameter[$key] = $val;
        }
        $show = $Page->show(); // 分页显示输出
        //获取分页数据
        $list = $this->where($map)->order($order)->limit($Page->firstRow . ',' . $Page->listRows)->select();
        
        $OrderGoodsObj = M('order_goods');
        foreach($list as $k => $v) {
            $list[$k]['goods_name'] = $OrderGoodsObj->where(array('id'=>$v['cart_id']))->getField('goods_name');
        }
        
        return array(
            'list' => $list,
            'page' => $show,
            'count' => $count
        );
       
    }
    
     /**
     * 删除品牌
     * @param int $id 评论ID
     * @return 大于1表示删除成功
     */
    public function del($id) {
        return $this->delete($id);
    }
    

}