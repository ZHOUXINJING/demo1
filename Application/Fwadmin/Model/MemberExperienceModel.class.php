<?php

namespace Fwadmin\Model;

use Think\Model;

class MemberExperienceModel extends Model {

    /**
     * 增加记录
     * @param type $member_id 会员ID
     * @param type $jia 增加
     * @param type $jian 减少
     * @param type $remark 备注
     * @param type $admin_id 管理员ID
     */
    public function add_record($member_id, $experience_change, $remark, $admin_id = 0) {
        $MemberObj = new \Fwadmin\Model\MemberModel();
        $experience_end = $MemberObj->set_experience($member_id, $experience_change);
        $data['member_id'] = $member_id;
        $data['experience_change'] = $experience_change;
        $data['experience'] = $experience_end;
        $data['create_time'] = time();
        $data['remark'] = $remark;
        $data['admin_id'] = $admin_id;
        return $this->data($data)->add();
    }
    
     /**
     * 会员中心获取经验值记录
     * @param int $member_id 会员ID
     * @param string $order 排序字段
     * @param int $pagesize 分页数
     */
    public function search_member($member_id, $order = 'id desc', $pagesize = 10) {
        $map['member_id'] = $member_id;
        $count = $this->where($map)->count(); // 查询满足要求的总记录数
        $Page = new \Think\Page($count, $pagesize); // 实例化分页类 传入总记录数和每页显示的记录数
        $show = $Page->show(); // 分页显示输出
        // 进行分页数据查诟 注意limit 方法癿参数要使用Page 类的属性
        $list = $this->where($map)->order($order)->limit($Page->firstRow . ',' . $Page->listRows)->select();
        return array(
            'list' => $list,
            'page' => $show
        );
    }
   
}
