<?php

namespace Fwadmin\Model;

use Think\Model;

class SystemMenuModel extends Model {

    protected $_validate = array(
        array('parent_id', 'require', '请选择所属父级！'),
        array('menu_name', 'require', '权限名称必须填写！'),
        array('order_id', 'require', '顺序编号必须填写！'),
        array('is_index', array(0, 1), '请设置是否启用！', 1, 'in'),
        array('is_enable', array(0, 1), '请设置是否启用！', 1, 'in')
    );

    /**
     * 获取所有列表
     * @param int $parent_id 父ID
     * @param int $menu_type 2表示所有0表示类别1表示实际操作
     */
    public function getList($parent_id = 0, $menu_type = 2, $current_id = 0, $str = '|--', $arr = Array()) {
        $arr1 = $this->getSub($parent_id,$menu_type);
        if (is_array($arr1)) {
            foreach ($arr1 as $k => $v) {
                $v['menu_name'] = $str . $v['menu_name'];
                if ($v['menu_id'] != $current_id) {
                    array_push($arr, $v);
                    $arr2 = $this->getSub($v['menu_id']);
                    if (is_array($arr2)) {
                        $arr = $this->getList($v['menu_id'], $menu_type, $current_id, $str . '----', $arr);
                    }
                }
            }
        }
        return $arr;
    }

    /**
     * 获取子类别列表
     * @param int $parent_id 父类别ID
     */
    public function getSub($parent_id, $menu_type = 2) {
        $map['parent_id'] = $parent_id;
        if($menu_type < 2) {
            $map['menu_type'] = $menu_type;
        }
        return $this->where($map)->order('order_id asc')->select();
    }
    
    /**
     * 获取系统主导航
     */
    public function getMenuTop() {
        //获取不存在的权限
        $PurviewObj = new \Fwadmin\Model\AdminPurviewModel();
        $purviews = $PurviewObj->getNotPurviewStr(session('admin_group_id'));
        $list = $this->where(array('is_enable' => 1, 'parent_id' => 0))->order('order_id asc')->select();
        foreach ($list as $k => $v) {
            $sub = $this->where(array('top_menu_id' => $v['menu_id'], 'is_enable' => 1, 'menu_type' => 1, 'menu_link' => array('neq', '')))->order('order_id asc')->field('menu_link,purview')->select();
            $first_menu_link = '';
            foreach ($sub as $k2 => $v2) {
                $menu_link = $v2['menu_link'];
                $purview = $v2['purview'];
                if (!empty($purview)) {
                    if ($PurviewObj->checkByUrl($purview, $purviews)) {
                        unset($sub[$k2]);
                    } else {
                        $first_menu_link = $menu_link;
                        break;
                    }
                } else {
                    if ($PurviewObj->checkByUrl($menu_link, $purviews)) {
                        unset($sub[$k2]);
                    } else {
                        $first_menu_link = $menu_link;
                        break;
                    }
                }
            }
            if ($first_menu_link == '') {
                unset($list[$k]);
            } else {
                $list[$k]['menu_link'] = $first_menu_link;
            }
        }
        return $list;
    }

    /**
     * 获取系统主导航
     */
    public function getMenuLeft() {
        $list = $this->where(array('is_enable' => 1,'menu_type' => 0,'parent_id' => array('gt',0)))->order('order_id asc')->select();
        foreach($list as $k => $v) {
            $sub = $this->getSubMenu($v['menu_id']);
			if(empty($sub)) {
                unset($list[$k]);
            } else {
                $list[$k]['sub'] = $sub;
            }
        }
        return $list;
    }
    
    /**
     * 获取系统首页菜单
     */
    public function getMenuIndex() {
        $list = $this->where(array('is_enable' => 1,'parent_id' => 0))->order('order_id asc')->select();

		//获取不存在的权限
		$PurviewObj = new \Fwadmin\Model\AdminPurviewModel();
        $purviews = $PurviewObj->getNotPurviewStr(session('admin_group_id'));
		
        foreach($list as $k => $v) {
			$map = array('is_enable' => 1,'menu_type' => 1,'is_index' => 1,'top_menu_id' => $v['menu_id']);
            $sub = $this->where($map)->order('order_id asc')->select();
			foreach($sub as $k2 => $v2) {
			    $menu_link = $v2['menu_link'];
				$purview = $v2['purview'];
				if(!empty($purview)) {
				    if($PurviewObj->checkByUrl($purview,$purviews))
					{
						unset($sub[$k2]);
					}
				} else {
					if($PurviewObj->checkByUrl($menu_link,$purviews))
					{
						unset($sub[$k2]);
					}
				}
			}
            if(empty($sub)) {
                unset($list[$k]);
            } else {
                $list[$k]['sub'] = $sub;
            }
        }
        return $list;
    }
    
    /**
     * 获取最底层操作列表
     * @param int $parent_id 父类别ID
     */
    public function getSubMenu($parent_id) {
		//获取不存在的权限
		$PurviewObj = new \Fwadmin\Model\AdminPurviewModel();
        $purviews = $PurviewObj->getNotPurviewStr(session('admin_group_id'));
		
        $map['parent_id'] = $parent_id;
        $map['is_enable'] = 1;
        $map['menu_type'] = 1;
        $list = $this->where($map)->order('order_id asc')->select();
		foreach($list as $k2 => $v2) {
			    $menu_link = $v2['menu_link'];
				$purview = $v2['purview'];
				if(!empty($purview)) {
				    if($PurviewObj->checkByUrl($purview,$purviews))
					{
						unset($list[$k2]);
					}
				} else {
					if($PurviewObj->checkByUrl($menu_link,$purviews))
					{
						unset($list[$k2]);
					}
				}
		}
        foreach($list as $k => $v) {
            if($v['target'] !== '_blank') {
                $list[$k]['target'] = 'main';
            }
            if(strpos($v['menu_link'],'http://') == false) {
                $list[$k]['menu_link'] = '../'.$v['menu_link'];
            }
        }
        return $list;
    }
    
    /**
     * 获取最高top_menu_id
     */
    public function getTopMenuId($menu_id) {
        $parent_id = $this->getFieldByMenuId($menu_id,'parent_id');
        if($parent_id == 0) {
            return $menu_id;
        }
        if($this->getFieldByMenuId($parent_id,'parent_id')==0) {
            return $parent_id;
        } else {
            return $this->getTopMenuId($parent_id);
        }      
    }

}

?>