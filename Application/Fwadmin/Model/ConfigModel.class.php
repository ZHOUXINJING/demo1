<?php

namespace Fwadmin\Model;

use Think\Model;

class ConfigModel extends Model {

    /**
     * 设置键值
     * @param $k string 键
     * @param $v string 值
     */
    public function set($k, $v) {
        $exist = $this->where(Array('k' => $k))->count();
        if ($exist > 0) {
            $flag = $this->where(Array('k' => $k))->setField('v', $v);
        } else {
            $data['k'] = $k;
            $data['v'] = $v;
            $flag = $this->data($data)->add();
        }
        return $flag;
    }

    /**
     * 获取键值
     * @param $k string 键
     */
    public function get($k) {
        return $this->getFieldByK($k, 'v');
    }

    /**
     * 获取所有配置参数名值对
     * @return <type>
     */
    public function getAll() {
        $result = $this->select();
        $returnArray = array();
        foreach ($result as $re) {
            $returnArray[$re['k']] = $re['v'];
        }
        return $returnArray;
    }

}