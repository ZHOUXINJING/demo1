<?php

namespace Fwadmin\Model;

use Think\Model;
/**
 * 商品品牌
 */
class GoodsSpecModel extends Model {

     /**
     * 搜索商品品牌
     * @param array $conditions 条件数组
     * @param string $order 排序字段
     * @param bool $export 是否导出excel
     * @param int $page 分页数
     */
    public function search($conditions, $order = 'order_id asc', $export = false, $page = 0) {
        $map = $conditions;
        if (!$export) {
            $count = $this->where($map)->count(); // 查询满足要求的总记录数
            if ($page == 0) {
                $page = C('PAGE_NUM');
            }
            $Page = new \Think\Page($count, $page); // 实例化分页类 传入总记录数和每页显示的记录数
            //分页跳转的时候保证查询条件
            foreach ($conditions as $key => $val) {
                $Page->parameter[$key] = urlencode($val);
            }
            $show = $Page->show(); // 分页显示输出
            // 进行分页数据查诟 注意limit 方法癿参数要使用Page 类的属性
            $list = $this->where($map)->order($order)->limit($Page->firstRow . ',' . $Page->listRows)->select();
            return array(
                'list' => $list,
                'page' => $show
            );
        } else {
            return $this->where($map)->order($order)->select();
        }
    }
    
    /**
     * 获取规格选项列表
     * @param tinyint $is_enable 是否启用
     */
    public function get_list($is_enable = '') {
       if($is_enable == '') {
           return $this->order('order_id asc')->select();
       } else {
           return $this->where('is_enable='.$is_enable)->order('order_id asc')->select();
       }
    }
    
    /**
     * 删除规格
     * @param int $spec_id 规格ID
     * @return 大于1表示删除成功
     */
    public function del($spec_id) {
        return $this->delete($spec_id);
    }
    

}