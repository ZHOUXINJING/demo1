<?php
namespace Fwadmin\Model;
use Think\Model;
class TuihuanModel extends Model {

    protected $_auto = array(

	 
   );
   /**
     * 搜索符合条件的记录
     * @param <type> $conditions 条件数组
     * @param string $order 排序字段
     * @param bool $mohu 是否模糊检索
     * @param bool $export 是否导出excel
     */
    public function search($conditions, $order = 'id desc', $mohu = true, $export = false,$page=10) {
       
	    $condition = array();	
        if(isset($conditions['typeid'])) {
            $condition['typeid'] = $conditions['typeid'];
        }
        if(isset($conditions['th_status'])) {
            $condition['th_status'] = $conditions['th_status'];		
        }
        if(isset($conditions['member_id'])) {
            $condition['member_id'] = $conditions['member_id'];
        }
        if (!$export) {
            $count = $this->where($condition)->count(); // 查询满足要求的总记录数
			if ($page == 0) {
                $page = C('PAGE_NUM');
            }
            $Page = new \Think\Page($count, $page); // 实例化分页类 传入总记录数和每页显示的记录数
            //分页跳转的时候保证查询条件
            foreach($conditions as $key=>$val) {
                $Page->parameter[$key]   =   urlencode($val);
            }
            $show = $Page->show(); // 分页显示输出
            // 进行分页数据查诟 注意limit 方法癿参数要使用Page 类的属性
            $list = $this->where($condition)->order($order)->limit($Page->firstRow . ',' . $Page->listRows)->select();
            return array(
                'list' => $list,
                'pageShow' => $show
            );
        } else {
            return $this->where($condition)->order($order)->select();
        }
    }

    public function cancel($id) {
        $flag = $this->where(Array('id'=>$id))->setField('th_status', 0);
        return $flag;
    }
	/**
	* 添加退换货
	*/
	function tuihuan_add($data){
	  $flag = $this->add($data);
      return $flag;
	}
	function tuihuan_save($data){
	  $flag = $this->save($data);
      return $flag;
	}
	
}

?>
