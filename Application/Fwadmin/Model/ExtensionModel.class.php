<?php

namespace Fwadmin\Model;

use Think\Model;

class ExtensionModel extends Model
{

    /**
     * 设置键值
     * @param $k string 键
     * @param $v string 值
     */
    public function set($k, $v)
    {
        $exist = $this->where(Array('k' => $k))->count();
        if ($exist > 0) {
            $flag = $this->where(Array('k' => $k))->setField('v', $v);
        } else {
            $data['k'] = $k;
            $data['v'] = $v;
            $flag = $this->data($data)->add();
        }
        return $flag;
    }

    /**
     * 获取值
     * @param $k
     * @return mixed
     */
    public function get($k)
    {
        return $this->where(array('k' => $k))->getField('v');
    }
}