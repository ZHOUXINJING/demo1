<?php

namespace Fwadmin\Model;

use Think\Model;
/*
 * 商品属性
 */
class GoodsModModel extends Model {

    /**
     * 获取列表
     * @param tinyint $is_enable 是否启用：-1所有 0禁用1启用
     * @return 模型数组
     */
    public function get_list($is_enable = -1) {
        if($is_enable == -1) {
            return $this->order('order_id asc,model_id asc')->select();
        } else {
            return $this->where(array('is_enable'=>$is_enable))->order('order_id asc,model_id asc')->select();
        }
    }
    
    /**
     * 删除模型
     * @param int $model_id 模型ID
     * @return 1删除成功 0删除失败
     */
    public function del($model_id) {
        $flag = $this->delete($model_id);
        if($flag > 0) {
            //删除模型表同时删除 属性表
            $MGoodsModAttr = M('GoodsModAttr');
            $MGoodsModAttr->where('model_id='.$model_id)->delete();
            //属性选项表
            $MGoodsModSel = M('GoodsModSel');
            $MGoodsModSel->where('model_id='.$model_id)->delete();
            //商品属性值表
            $MGoodsAttrValue = M('GoodsAttrValue');
            $MGoodsAttrValue->where('model_id='.$model_id)->delete();
        }
        return $flag;
    }
    
    /**
     * 获取模型属性值
     * @param int $model_id 模型ID
     * @param tinyint $is_enable 是否启用
     * @return 属性数组
     */
    public function get_attr_list($model_id, $is_enable = '') {
        $MGoodsModAttr = M('GoodsModAttr');
        if($is_enable == '') {
            return $MGoodsModAttr->where('model_id='.$model_id)->select();
        } else {
            return $MGoodsModAttr->where('model_id='.$model_id.' and is_enable='.$is_enable)->select();
        }
    }
    
    /**
     * 删除模型属性值
     * @param int $attr_id 属性ID
     * @return 大于1 表示删除成功
     */
    public function del_attr($attr_id) {
        $MGoodsModAttr = M('GoodsModAttr');
        $flag = $MGoodsModAttr->delete($attr_id);
        if($flag > 0) {
            //属性选项表
            $MGoodsModSel = M('GoodsModSel');
            $MGoodsModSel->where('attr_id='.$attr_id)->delete();
            //商品属性值表
            $MGoodsAttrValue = M('GoodsAttrValue');
            $MGoodsAttrValue->where('attr_id='.$attr_id)->delete();
        }
        return $flag;
    }
    
    /**
     * 添加模型属性选项
     * @param string $attr_sel_name 选项名
     * @param int $attr_id 属性ID
     * @param int $model_id 模型ID
     * @param int $order_id 顺序编号
     * @param tinyint $is_enable 是否启用
     * @return int 大于1操作成功
     */
    public function add_attr_sel($attr_sel_name,$attr_id,$model_id,$order_id=999,$is_enable=1) {
        $MGoodsModSel = M('GoodsModSel');
        $data['attr_sel_name'] = $attr_sel_name;
        $data['attr_id'] = $attr_id;
        $data['model_id'] = $model_id;
        $data['order_id'] = $order_id;
        $data['is_enable'] = $is_enable;
        return $MGoodsModSel->data($data)->add();
    }
    
    /**
     * 获取模型属性选项值
     * @param int $attr_id 属性ID
     * @return 选项数组
     */
    public function get_attr_sel($attr_id) {
        $MGoodsModSel = M('GoodsModSel');
        return $MGoodsModSel->where('attr_id='.$attr_id)->select();
    }
    
    /**
     * 根据商品选项ID获取商品参数选项值
     * @param int $attr_sel_id 选项ID
     * @return string 选项值
     */
    public function get_attr_sel_value($attr_sel_id) {
        $MGoodsModSel = M('GoodsModSel');
        return $MGoodsModSel->where('attr_sel_id='.$attr_sel_id)->getField('attr_sel_name');
    }
    
    /**
     * 删除属性选项值
     * @param int $attr_sel_id 属性选项ID
     * @return 大于1表示删除成功
     */
    public function del_attr_sel($attr_sel_id) {
        $MGoodsModSel = M('GoodsModSel');
        $flag = $MGoodsModSel->delete($attr_sel_id);
        return $flag;
    }
    
    /**
     * 获取商品属性值
     * @param int $attr_id 属性ID
     * @param int $goods_id 商品ID
     * @return 属性值
     */
    public function get_attr_value($attr_id, $goods_id) {
        $MGoodsAttrValue = M('GoodsAttrValue');
        return $MGoodsAttrValue->where('attr_id='.$attr_id.' and goods_id='.$goods_id)->getField('attr_value');
    }
    
    /**
     * 添加修改商品属性值
     * @param int $attr_id
     * @param int $goods_id
     * @param string $attr_value
     * @param int $model_id
     * @return 大于1表示成功
     */
    public function set_attr_value($attr_id,$goods_id,$attr_value,$model_id) {
        $MGoodsAttrValue = M('GoodsAttrValue');
        $count = $MGoodsAttrValue->where('attr_id='.$attr_id.' and goods_id='.$goods_id)->count();
        //判断是新增还是修改
        if($count > 0) {
            return $MGoodsAttrValue->where('attr_id='.$attr_id.' and goods_id='.$goods_id)->setField('attr_value',$attr_value);
        } else {
            $data['attr_id'] = $attr_id;
            $data['goods_id'] = $goods_id;
            $data['attr_value'] = $attr_value;
            $data['model_id'] = $model_id;
            return $MGoodsAttrValue->data($data)->add();
        }
    }
    
     /**
     * 获取属性输入类型值
     * @param tinyint $attr_type 输入ID
     * @return string 名称
     */
    public function get_attr_type($attr_type) {
        $ret = '';
        switch($attr_type) {
            case '1':$ret = '输入框';
                break;
            case '2':$ret = '选择框';
                break;
            case '3':$ret = '单选框';
                break;
            case '4':$ret = '多选框';
                break;
        }
        return $ret;
    }
    
    /**
     * 获取属性搜索值
     * @param tinyint $is_search 输入ID
     * @return string 名称
     */
    public function get_attr_search($is_search) {
        $ret = '';
        switch($is_search) {
            case '1':$ret = '';
                break;
            case '2':$ret = '单选搜索';
                break;
            case '3':$ret = '多选搜索';
                break;
        }
        return $ret;
    }


}