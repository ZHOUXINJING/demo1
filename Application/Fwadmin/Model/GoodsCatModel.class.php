<?php

namespace Fwadmin\Model;

use Think\Model;

class GoodsCatModel extends Model {

    /**
     * 递归计算类别路径
     * @param int $cat_id 类别ID
     */
    public function getPathId($cat_id, $first = 1) {
        if ($first == 1) {
            $path_id = ',' . $cat_id . ',';
        }
        if ($cat_id != 0) {
            $parent_id = $this->getParentId($cat_id);
            $path_id .= $parent_id . ',' . $this->getPathId($parent_id, 0);
        }
        return $path_id;
    }
    
    /**
     * 递归计算多类别路径
     * @param int $cat_id 类别ID
     */
    public function getPathIds($cat_ids) {
        $str_path = '';
        if($cat_ids != '') {
            $arr = explode(',', $cat_ids);
            foreach($arr as $r) {
                $str_path .= $this->getPathId($r);
            }
        }
        $str_path = str_replace(",,",",",$str_path);
        return $str_path;
    }

    /**
     * 获取父ID
     * @param int $cat_id 类别ID
     */
    public function getParentId($cat_id) {
        $parent_id = $this->getFieldByCatId($cat_id, 'parent_id');
        return $parent_id;
    }

    /**
     * 递归计算类别层级
     * @param int $cat_id 类别ID
     */
    public function getLevel($cat_id) {
        if ($cat_id == 0) {
            return 0;
        }
        $parent_id = $this->getParentId($cat_id);
        if ($parent_id != 0) {
            return 1 + $this->getLevel($parent_id);
        } else {
            return 1;
        }
    }
    
    /**
     * 获取类别路径名称
     * @param string $path_id 类别路径
     * @return string 路径名称
     */
    public function getAllPathName($path_id) {
        $path_id = trim($path_id,',');
        $arr = explode(',',$path_id);
        $str = '';
        foreach ($arr as $v) {
           if($v > 0) {
               $str = $this->getFieldByCatId($v,'cat_name').'/'.$str;
           } 
        }
        $str = trim($str,'/');
        return $str;
    }

    /**
     * 获取子类别列表
     * @param int $cat_id 类别ID
     */
    public function getSubList($cat_id, $level = 5, $str = '|--', $arr = Array(),$i=0) {
        $arr1 = $this->getSub2($cat_id, $level);
		
		switch($i){
		case 0;$color="#000";break;
		case 1;$color="#4A8BF6";break;
		case 2;$color="green";break;
		case 3;$color="#F00";break;
		default:$color="#999";break;
		}
		$i++;
		
        if (is_array($arr1)) {
            foreach ($arr1 as $v) {
                $v['cat_name'] = $str . $v['cat_name'];
                array_push($arr, $v);
                $arr2 = $this->getSub2($v['cat_id'], $level);
                if (is_array($arr2)) {
                    $arr = $this->getSubList($v['cat_id'], $level, $str . '----', $arr);
                }
            }
        }
        return $arr;
    }

    /**
     * 获取子类别列表
     * @param int $cat_id 类别ID
     */
    public function getSub2($cat_id, $level = 5, $title = '') {
        $arr = $this->field('cat_id,cat_name,parent_id,order_id,is_enable,is_menu,cat_level')->where(Array('parent_id' => $cat_id, 'cat_level' => Array('elt', $level)))->order('order_id asc')->select();
        return $arr;
    }

    /**
     * 获取子类别select列表
     * @param int $cat_id 类别ID
     */
    public function getSubSelect($cat_id, $level = 5, $current_id = 0, $str = '|--', $arr = Array()) {
        $arr1 = $this->getSub($cat_id, $level);
        if (is_array($arr1)) {
            foreach ($arr1 as $v) {
                $v['cat_name'] = $str . $v['cat_name'];
                if ($v['cat_id'] != $current_id) {
                    array_push($arr, $v);
                    $arr2 = $this->getSub($v['cat_id'], $level);
                    if (is_array($arr2)) {
                        $arr = $this->getSubSelect($v['cat_id'], $level, $current_id, $str . '----', $arr);
                    }
                }
            }
        }
        return $arr;
    }

    /**
     * 获取子类别列表
     * @param int $cat_id 类别ID
     */
    public function getSub($cat_id, $level = 5) {
        $arr = $this->field('cat_id,cat_name')->where(Array('parent_id' => $cat_id, 'cat_level' => Array('elt', $level)))->order('order_id asc')->select();
        return $arr;
    }

    /**
     * 删除类别,同时删除其所属子类别
     */
    public function del($cat_id) {
        $sub = $this->field('cat_id')->where('parent_id=' . $cat_id)->select();
        $flag = $this->delete($cat_id);
        if ($flag > 0 && !empty($sub)) {
            foreach ($sub as $v) {
                $this->del($v['cat_id']);
            }
        }
        return $flag;
    }
    
    /**
     * 获取顶级类别
     */
    public function getTopSel() {
        return $this->field('cat_id,cat_name')->where(Array('parent_id' => 0, 'is_enable' => 1))->order('order_id asc')->select();
    }
    
    /**
     * 获取顶级类别数组
     */
    public function getTopArray() {
        $arr = $this->field('cat_id,cat_name')->where(Array('parent_id' => 0))->order('order_id asc')->select();
        $ret_arr = array();
        foreach($arr as $k => $v) {
            $ret_arr[$v['cat_id']] = $v['cat_name'];
        }
        return $ret_arr;
    }
	
		/***获取该商品的所有类别***/
	public function get_all_cate($path){
		
	 $path = stringToarray($path);
	
	 $path = array_reverse($path);
	 $c='';
	 foreach($path as $k =>$v){ 
		
	 if($v>0){
		$a=$k+1;
		if($path[$a]!=''){
			$heng='-';
		 }else{
		 $heng='';
		 }
		 $cat_name = $this->getFieldByCatId($v, 'cat_name');
	     $c.=$cat_name?$cat_name:'<span style="color:#F00">不存在</span>';
		 $c.=$heng;
	  }
	 }
	
	 return $c;
	 	
	
	}
	

}
