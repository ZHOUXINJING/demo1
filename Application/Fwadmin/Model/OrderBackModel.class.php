<?php

namespace Fwadmin\Model;

use Think\Model;

class OrderBackModel extends Model {
    
    /**
     * 增加记录
     * @param int $id 订单商品表自动ID
     * @param array $data 数据
     * @return int 是否成功
     */
    public function add_record($id,$data) {
        $OrderGoodsObj = M('order_goods');
        $order_goods = $OrderGoodsObj->find($id);
        $data['back_no'] = $this->get_back_no($data['type'],$data['member_id']);
        $data['order_id'] = $order_goods['order_id'];
        $data['order_no'] = $order_goods['order_no'];
        $data['cart_id'] = $id;
        $data['goods_id'] = $order_goods['goods_id'];
        $data['goods_item_id'] = $order_goods['goods_item_id'];
        $data['status'] = 1;
        $data['status_pay'] = 3;
        if($data['type'] == '退货') {
            $data['status_pay'] = 1;
        }
        $data['create_time'] = time();
        $data['create_ip'] = get_client_ip();
        return $this->data($data)->add();
    }
    
    /**
     * 获取可以申请的订单列表
     */
    public function get_can_back_list($member_id, $conditions, $order='time_complete desc',$pagesize = 10) {
        $OrderObj = M('order');
        $map = $conditions;
        $map['member_id'] = $member_id;
        $map['status'] = 3;
        $max_day = $this->get_can_back_day(); //获得退换货最多天数
        $time_before_7day = strtotime('-'.$max_day.' days');
        $map['time_complete'] = array('egt',$time_before_7day);
        if(isset($map['k'])) {
            $map['order_no'] = $map['k']; 
            unset($map['k']);
        }
        $count = $OrderObj->where($map)->count(); // 查询满足要求的总记录数
        $Page = new \Think\Page($count, $pagesize); // 实例化分页类 传入总记录数和每页显示的记录数
        //分页跳转的时候保证查询条件
        foreach ($conditions as $key => $val) {
            $Page->parameter[$key] = urlencode($val);
        }
        $show = $Page->show(); // 分页显示输出
        // 进行分页数据查诟 注意limit 方法癿参数要使用Page 类的属性
        $list = $OrderObj->field('order_id,order_no,order_id,time_create')->where($map)->order($order)->limit($Page->firstRow . ',' . $Page->listRows)->select();
        return array(
            'list' => $list,
            'page' => $show
        );
    }
    
    /*
     * 获得退换货最多天数
     */
    public function get_can_back_day() {
        $ConfigObj = new \Fwadmin\Model\ConfigModel();
        $day_tuihuo = $ConfigObj->get('orderback_day_tuihuo');
        $day_huanhuo = $ConfigObj->get('orderback_day_huanhuo');
        $day_weixiu = $ConfigObj->get('orderback_day_weixiu');
        $day_array = array($day_tuihuo,$day_huanhuo,$day_weixiu);
        return max($day_array);
    }
    
    /**
     * 搜索
     * @param array $conditions 条件数组
     * @param string $order 排序字段
     * @param int $page 分页数
     */
    public function search_member($member_id,$conditions, $order = 'id desc', $pagesize = 20) {
        $map = $conditions;
        $map['member_id'] = $member_id;
        if(isset($map['title'])) {
            $map['detail'] = array('like','%'.$map['title'].'%');
            unset($map['title']);
        }
        $count = $this->where($map)->count(); // 查询满足要求的总记录数

        $Page = new \Think\Page($count, $pagesize); // 实例化分页类 传入总记录数和每页显示的记录数
        //分页跳转的时候保证查询条件
        foreach ($conditions as $key => $val) {
            $Page->parameter[$key] = $val;
        }
        $show = $Page->show(); // 分页显示输出
        //获取分页数据
        $list = $this->where($map)->order($order)->limit($Page->firstRow . ',' . $Page->listRows)->select();
        
        $OrderGoodsObj = M('order_goods');
        foreach($list as $k => $v) {
            $list[$k]['goods_name'] = $OrderGoodsObj->where(array('id'=>$v['cart_id']))->getField('goods_name');
        }
        
        return array(
            'list' => $list,
            'page' => $show,
            'count' => $count
        );  
    }
    
    /**
     * 搜索
     * @param array $conditions 条件数组
     * @param string $order 排序字段
     * @param int $page 分页数
     */
    public function search($conditions, $order = 'id desc', $pagesize = 20) {
        $map = $conditions;
        if(!empty($map['member_name'])) {
            $MemberObj = new \Fwadmin\Model\MemberModel();
            $map['member_id'] = $MemberObj->get_member_id($map['member_name']);
            unset($map['member_name']);
        }
        $count = $this->where($map)->count(); // 查询满足要求的总记录数

        $Page = new \Think\Page($count, $pagesize); // 实例化分页类 传入总记录数和每页显示的记录数
        //分页跳转的时候保证查询条件
        foreach ($conditions as $key => $val) {
            $Page->parameter[$key] = $val;
        }
        $show = $Page->show(); // 分页显示输出
        //获取分页数据
        $list = $this->where($map)->order($order)->limit($Page->firstRow . ',' . $Page->listRows)->select();
        
        $OrderGoodsObj = M('order_goods');
        foreach($list as $k => $v) {
            $list[$k]['goods_name'] = $OrderGoodsObj->where(array('id'=>$v['cart_id']))->getField('goods_name');
        }
        
        return array(
            'list' => $list,
            'page' => $show,
            'count' => $count
        );
       
    }
    
    /**
     * 生成售后编号
     */
    public function get_back_no($type,$member_id = '00000') {
        $str_type = 0;
        if($type == '换货') {
            $str_type = '2';
        } elseif ($type == '维修') {
            $str_type = '3';
        } elseif ($type == '退货') {
            $str_type = '1';
        } 
        return $str_type.date('ymdHis',time()).$member_id;
    }
    
    
    /**
     * 获取状态名称（后台显示）
     * @param tiny $status 状态ID
     * @return string 状态名称
     */
    public function get_status($status) {
        $str_status = '';
        switch($status) {
            case 1:
                $str_status = '未审核';
                break;
            case 2:
                $str_status = '已审核';
                break;
            case 3:
                $str_status = '商品回寄中';
                break;
            case 4:
                $str_status = '商品已回寄';
                break;
            case 5:
                $str_status = '已完成';
                break;
            case 6:
                $str_status = '审核不通过';
                break;
        }
        return $str_status;
    }
    
     /**
     * 获取状态名称(前台显示）
     * @param tiny $status 状态ID
     * @return string 状态名称
     */
    public function get_status_user($status) {
        $str_status = '';
        switch($status) {
            case 1:
                $str_status = '等待审核';
                break;
            case 2:
                $str_status = '等待回寄商品';
                break;
            case 3:
                $str_status = '商品回寄中';
                break;
            case 4:
                $str_status = '处理中';
                break;
            case 5:
                $str_status = '已完成';
                break;
            case 6:
                $str_status = '审核不通过';
                break;
        }
        return $str_status;
    }
    
    /**
     * 获取退款状态名称
     * @param tiny $status_pay 状态ID
     * @return string 状态名称
     */
    public function get_status_pay($status_pay) {
        $str_status_pay = '';
        switch($status_pay) {
            case 1:
                $str_status_pay = '未退款';
                break;
            case 2:
                $str_status_pay = '已退款';
                break;
            case 3:
                $str_status_pay = '不需要退款';
                break;
        }
        return $str_status_pay;
    }
    
    /**
     * 审核通过
     * @param int $id 编号
     */
    public function confirm_2($id) {
        $status = $this->where(array('id'=>$id))->getField('status');
        if($status == 1) {
            $data['id'] = $id;
            $data['status'] = 2;
            $flag = $this->data($data)->save();//设置状态改变
            return $flag;
        }
        return -1;
    }
    
    /**
     * 审核不通过
     * @param int $id 编号
     */
    public function confirm_6($id) {
        $status = $this->where(array('id'=>$id))->getField('status');
        if($status == 1) {
            $data['id'] = $id;
            $data['status'] = 6;
            $flag = $this->data($data)->save();//设置状态改变
            return $flag;
        }
        return -1;
    }
    
    /**
     * 商品回寄中,用户操作
     * @param int $id 编号
     */
    public function confirm_3($id,$member_id) {
        $status = $this->where(array('id'=>$id,'member_id'=>$member_id))->getField('status');
        if($status == 2) {
            $data['id'] = $id;
            $data['status'] = 3;
            $flag = $this->data($data)->save();//设置状态改变
            return $flag;
        }
        return -1;
    }
    
    /**
     * 商品已回寄
     * @param int $id 编号
     */
    public function confirm_4($id) {
        $status = $this->where(array('id'=>$id))->getField('status');
        if($status == 3) {
            $data['id'] = $id;
            $data['status'] = 4;
            $flag = $this->data($data)->save();//设置状态改变
            return $flag;
        }
        return -1;
    }
    
    /**
     * 已完成
     * @param int $id 编号
     */
    public function confirm_5($id) {
        $status = $this->where(array('id'=>$id))->getField('status');
        if($status == 4) {
            $data['id'] = $id;
            $data['status'] = 5;
            $flag = $this->data($data)->save();//设置状态改变
            return $flag;
        }
        return -1;
    }
    
    /**
     * 退款
     * @param int $id 编号
     */
    public function pay_2($id) {
        $status_pay = $this->where(array('id'=>$id))->getField('status_pay');
        if($status_pay == 1) {
            $data['id'] = $id;
            $data['status_pay'] = 2;
            $flag = $this->data($data)->save();//设置状态改变
            return $flag;
        }
        return -1;
    }

}