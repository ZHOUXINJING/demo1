<?php

namespace Fwadmin\Model;

use Think\Model;

class ConNewsModel extends Model {

    protected $_validate = array(
        array('order_id', 'require', '顺序编号必须填写！'),
        array('is_show', array(0, 1), '请设置是否显示！', 3, 'in')
    );

    /**
     * 搜索符合条件的记录
     * @param <type> $conditions 条件数组
     * @param string $order 排序字段
     * @param bool $export 是否导出excel
     * @param int $pagesize 分页数
     */
    public function search($conditions, $order = 'order_id asc', $pagesize = 0) {
        $map = $conditions;
        if (isset($map['time_begin']) && isset($map['time_end'])) {
            $time_begin = strtotime($map['time_begin'].' 00:00:00');
            $time_end = strtotime($map['time_end'].' 23:59:59');
            $map['create_time'] = array('between',array($time_begin,$time_end));
            unset($map['time_begin']);
            unset($map['time_end']);
        } elseif (isset($map['time_begin'])) {
            $time_begin = strtotime($map['time_begin'].' 00:00:00'); 
            $map['create_time'] = array('egt',$time_begin);
            unset($map['time_begin']);
        } elseif (isset($map['time_end'])) {
            $time_end = strtotime($map['time_end'].' 23:59:59'); 
            $map['create_time'] = array('elt',$time_end);
            unset($map['time_end']);
        }
        $count = $this->where($map)->count(); // 查询满足要求的总记录数
        if ($pagesize == 0) {
            $pagesize = C('PAGE_NUM');
        }
        $Page = new \Think\Page($count, $pagesize); // 实例化分页类 传入总记录数和每页显示的记录数
        //分页跳转的时候保证查询条件
        foreach ($conditions as $key => $val) {
            $Page->parameter[$key] = urlencode($val);
        }
        $show = $Page->show(); // 分页显示输出
        // 进行分页数据查诟 注意limit 方法癿参数要使用Page 类的属性
        $list = $this->where($map)->order($order)->limit($Page->firstRow . ',' . $Page->listRows)->select();
        return array(
            'list' => $list,
            'page' => $show
        );
    }

    public function del($id) {
        return $this->delete($id);
    }

}
