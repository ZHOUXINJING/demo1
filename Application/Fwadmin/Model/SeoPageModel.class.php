<?php

namespace Fwadmin\Model;

use Think\Model;

class SeoPageModel extends Model {

    /**
     * 搜索符合条件的记录
     * @param <type> $conditions 条件数组
     * @param string $order 排序字段
     * @param bool $mohu 是否模糊检索
     * @param bool $export 是否导出excel
     */
    public function search($conditions = array(), $order = 'id asc', $export = false) {
        if (!$export) {
            $count = $this->where($conditions)->count(); // 查询满足要求的总记录数
            $Page = new \Think\Page($count, C('PAGE_NUM')); // 实例化分页类 传入总记录数和每页显示的记录数
            //分页跳转的时候保证查询条件
            foreach ($conditions as $key => $val) {
                $Page->parameter[$key] = urlencode($val);
            }
            $show = $Page->show(); // 分页显示输出
            // 进行分页数据查诟 注意limit 方法癿参数要使用Page 类的属性
            $list = $this->where($conditions)->order($order)->limit($Page->firstRow . ',' . $Page->listRows)->select();
            return array(
                'list' => $list,
                'pageShow' => $show
            );
        } else {
            return $this->where($conditions)->order($order)->select();
        }
    }
    
    /**
     * 设置值
     */
    public function set($page, $title, $keywords, $description) {
        $exist = $this->where(Array('page' => $page))->count();
        if ($exist > 0) {
            $row = $this->where(Array('page' => $page))->find();
            $row['title'] = $title;
            $row['keywords'] = $keywords;
            $row['description'] = $description;
            $flag = $this->save($row);
        } else {
            $data['page'] = $page;
            $data['title'] = $title;
            $data['keywords'] = $keywords;
            $data['description'] = $description;
            $flag = $this->data($data)->add();
        }
        return $flag;
    }

    /**
     * 根据页面参数获取行
     * @param $page string 页面参数
     */
    public function get($page) {
        return  $this->where(Array('page' => $page))->find();
    }

    /**
     * 获取所有
     * @return <type>
     */
    public function getAll() {
        $result = $this->select();
        $returnArray = array();
        foreach ($result as $re) {
            $returnArray[$re['url']] = $re;
        }
        return $returnArray;
    }

}