<?php

namespace Fwadmin\Model;

use Think\Model;

class IndexStyleModel extends Model {

    /**
     * 获取风格列表
     */
     public function get_list() {    
         $list = $this->order('order_id asc,style_id asc')->select();
         return $list;
     }    
     
     /**
      * 删除风格
      * @param int $style_id 模块ID
      * @return int 大于1表示成功
      */
     public function del($style_id) {
         $flag = $this->delete($style_id);
         if($flag > 0) {
             $StyleItemObj = M('index_style_item');
             $StyleItemObj->where(array('style_id'=>$style_id))->delete();
         }
         return $flag;
     }
     
    

}