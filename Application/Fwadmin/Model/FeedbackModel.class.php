<?php
namespace Fwadmin\Model;
use Think\Model;
class FeedbackModel extends Model {
    /**
     * 搜索符合条件的记录
     * @param $conditions 条件数组
     * @param string $order 排序
     * @param int $number 每页条数
     * @param bool $all 是否查找所有
     * @return mixed
     */
	     public function search($conditions, $order = 'create_time DESC,id desc', $pagesize = 0) {

        $count = $this->where($conditions)->count(); // 查询满足要求的总记录数
        if ($pagesize == 0) {
            $pagesize = C('PAGE_NUM');
        }
        $Page = new \Think\Page($count, $pagesize); // 实例化分页类 传入总记录数和每页显示的记录数
        //分页跳转的时候保证查询条件
        foreach ($conditions as $key => $val) {
            $Page->parameter[$key] = urlencode($val);
        }
        $show = $Page->show(); // 分页显示输出
        // 进行分页数据查诟 注意limit 方法癿参数要使用Page 类的属性
        $list = $this->where($conditions)->order($order)->limit($Page->firstRow . ',' . $Page->listRows)->select();
        return array(
            'list' => $list,
            'page' => $show
        );
    }

    public function del($id) {
        return $this->delete($id);
    }

   
}

?>
