<?php

namespace Fwadmin\Model;

use Think\Model;

class AdminModel extends Model {

    protected $_validate = array(
        //新建管理员时必须验证
        array('username', 'require', '用户名必须填写！', 1, 'regex', 1),
        array('username', '', '用户名已经存在！', 1, 'unique', 1),
        array('password', 'require', '密码必须填写！', 1, 'regex', 1),
        array('password2', 'password', '确认密码不正确', 1, 'confirm', 1),
        array('group_id', 'require', '请选择用户组名！', 1, 'regex', 1),
        //修改管理员时必须验证
        array('username', 'require', '用户名必须填写！', 1, 'regex', 2),
        array('username', '', '用户名已经存在！', 1, 'unique', 2),
        //修改密码时必须验证
        array('oldpassword', 'require', '旧密码必须填写！', 1, 'regex', 4),
        array('password', 'require', '新密码必须填写！', 1, 'regex', 4),
        array('password2', 'password', '两次输入的新密码必须一致', 1, 'confirm', 4),
        //登录时必须验证
        array('username', 'require', '请输入用户名！', 1, 'regex', 5),
        array('password', 'require', '请输入密码！', 1, 'regex', 5),
        array('code', 'require', '请输入验证码！', 1, 'regex', 5),
    );
    protected $_auto = array(
        array('login_times', '0'),
        array('create_time', 'time', 1, 'function'),
        array('create_ip', 'get_client_ip', 1, 'function')
    );

    /**
     * 搜索符合条件的记录
     * @param <type> $conditions 条件数组
     * @param string $order 排序字段
     * @param bool $export 是否导出excel
     * @param int $pagesize 分页数
     */
    public function search($conditions, $order = 'admin_id desc', $export = false, $pagesize = 0) {
        $conditions['username'] = array('not in',array('river','fangwei'));//Array('neq', 'river');
        if (!$export) {
            import("ORG.Util.Page.class"); // 导入分页类
            $count = $this->where($conditions)->count(); // 查询满足要求的总记录数
            if ($pagesize == 0) {
                $pagesize = C('PAGE_NUM');
            }
            $Page = new \Think\Page($count, $pagesize); // 实例化分页类 传入总记录数和每页显示的记录数
            //分页跳转的时候保证查询条件
            foreach ($conditions as $key => $val) {
                $Page->parameter[$key] = urlencode($val);
            }
            $show = $Page->show(); // 分页显示输出
            // 进行分页数据查诟 注意limit 方法癿参数要使用Page 类的属性
            $list = $this->where($conditions)->order($order)->limit($Page->firstRow . ',' . $Page->listRows)->select();
            $AGObj = new \Fwadmin\Model\AdminGroupModel();
            foreach ($list as $key => $row) {
                $list[$key]['group_name'] = $AGObj->getFieldByGroupId($row['group_id'], 'group_name');
            }
            return array(
                'list' => $list,
                'pageShow' => $show
            );
        } else {
            return $this->where($conditions)->order($order)->select();
        }
    }

    /**
     * 获取管理员名称
     * @param int $admin_id 管理员ID
     * @return string 管理员名称
     */
    public function show_admin_name($admin_id){
        if($admin_id == 0) return '系统';
        $admin_name = $this->where(array('admin_id'=>$admin_id))->getField('username');
        return $admin_name;
    }
}