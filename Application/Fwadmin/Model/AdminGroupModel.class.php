<?php

namespace Fwadmin\Model;

use Think\Model;

class AdminGroupModel extends Model {

    /**
     * 搜索符合条件的记录
     * @param <type> $conditions 条件数组
     * @param string $order 排序字段
     * @param bool $export 是否导出excel
     * @param int $pagesize 分页数
     */
    public function search($conditions = array(), $order = 'group_id asc', $export = false, $pagesize = 0) {
        if (!$export) {
            $count = $this->where($conditions)->count(); // 查询满足要求的总记录数
            if ($pagesize == 0) {
                $pagesize = C('PAGE_NUM');
            }
            $Page = new \Think\Page($count, $pagesize); // 实例化分页类 传入总记录数和每页显示的记录数
            //分页跳转的时候保证查询条件
            foreach ($conditions as $key => $val) {
                $Page->parameter[$key] = urlencode($val);
            }
            $show = $Page->show(); // 分页显示输出
            // 进行分页数据查诟 注意limit 方法癿参数要使用Page 类的属性
            $list = $this->where($conditions)->order($order)->limit($Page->firstRow . ',' . $Page->listRows)->select();
            return array(
                'list' => $list,
                'page' => $show
            );
        } else {
            return $this->where($conditions)->order($order)->select();
        }
    }

    /**
     * 删除管理员组
     * 如果有管理员组里有成员  则不能删除返回-1
     */
    public function del($group_id) {
        $AObj = new AdminModel();
        $count_admins = $AObj->where(array('group_id' => $group_id))->count();
        if ($count_admins > 0) {
            return -1;
        }
        return $this->delete($group_id);
    }

}