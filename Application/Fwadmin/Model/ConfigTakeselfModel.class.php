<?php

namespace Fwadmin\Model;

use Think\Model;

class ConfigTakeselfModel extends Model {

    /**
     * 后台列表
     */
    public function search($conditions = array(),$pagesize = 30) {
        $order = 'id desc';
        $count = $this->where($conditions)->count(); // 查询满足要求的总记录数
        $Page = new \Think\Page($count, $pagesize); // 实例化分页类 传入总记录数和每页显示的记录数
        //分页跳转的时候保证查询条件
        foreach ($conditions as $key => $val) {
            $Page->parameter[$key] = urlencode($val);
        }
        $show = $Page->show(); // 分页显示输出
        // 进行分页数据查诟 注意limit 方法癿参数要使用Page 类的属性
        $list = $this->where($conditions)->order($order)->limit($Page->firstRow . ',' . $Page->listRows)->select();
        return array(
            'list' => $list,
            'page' => $show
        );
    }
    

    /**
     * 删除
     */
    public function del($id) {
        $flag = $this->delete($id); 
        return $flag;
    }

}
