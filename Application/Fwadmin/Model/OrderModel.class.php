<?php

namespace Fwadmin\Model;

use Think\Model;
//所有前后台的订单都在这里操作
class OrderModel extends Model {
    
    /**
     * 显示完整订单状态
     * @param int $status 订单状态
     * @param int $status_pay 支付状态
     * @param int $status_delivery 配送状态
     */
    public function show_status($status,$status_pay,$status_delivery) {
        //订单状态： 1未审核 2已审核 3已完成 4已取消 5已作废
        //支付状态： 1未支付 2已付定金 3已支付
        //配送状态：1未发货 2已发货 3已部分发货 4已收货
        $str_status = $this->get_status_str($status);
        $str_status_pay = $this->get_status_pay_str($status_pay);
        $str_status_delivery = $this->get_status_delivery_str($status_delivery);
        return $str_status.'/'.$str_status_pay.'/'.$str_status_delivery;
    }
    
    /**
     * 显示友好订单状态
     * @param int $status 订单状态
     * @param int $status_pay 支付状态
     * @param int $status_delivery 配送状态
     */
    public function show_status_member($status,$status_pay,$status_delivery,$pay_type) {
        //订单状态： 1未审核 2已审核 3已完成 4已取消 5已作废
        //支付状态： 1未支付 2已付定金 3已支付
        //配送状态：1未发货 2已发货 3已部分发货 4已收货
        //支付方式：1在线支付 2货到付款
        if($status == 4) {
            return '已取消';
        } elseif ($status == 5) {
            return '已作废';
        } elseif ($status == 3) {
            return '已完成';
        }
        if($pay_type == 1) {
            if($status_pay == 1 || $status_pay == 2) {
                return '待支付';
            } else {
                if($status == 1) {
                    return '待审核';
                } elseif ($status == 2) {   
                    if($status_delivery == 1) {
                        return '待发货';
                    } elseif ($status_delivery == 2) {
                        return '待收货';
                    }
                }
            }
        } elseif ($pay_type == 2) {
            if($status == 1) {
                return '待审核';
            } elseif ($status == 2) {   
                if ($status_delivery == 1) {
                    return '待发货';
                } elseif ($status_delivery == 2) {
                    return '待收货';
                } 
            }
        }
            
    }
    
    /*
     * 显示订单状态
     */
    public function get_status_str($status) {
        $str_status = '';
        switch($status) {
            case 1:
                $str_status = '未审核';
                break;
            case 2:
                $str_status = '已审核';
                break;
            case 3:
                $str_status = '已完成';
                break;
            case 4:
                $str_status = '已取消';
                break;
            case 5:
                $str_status = '已作废';
                break;
        }
        return $str_status;
    }
    
    /*
     * 显示支付状态
     */
    public function get_status_pay_str($status_pay) {
        $str_status_pay = '';
        switch($status_pay) {
            case 1:
                $str_status_pay = '未支付';
                break;
            case 2:
                $str_status_pay = '部分支付';
                break;
            case 3:
                $str_status_pay = '已支付';
                break;
            case 4:
                $str_status_pay = '已退款';
                break;
        }
        return $str_status_pay;
    }
    
    /*
     * 显示发货状态
     */
    public function get_status_delivery_str($status_delivery) {
        $str_status_delivery = '';
        switch($status_delivery) {
            case 1:
                $str_status_delivery = '未发货';
                break;
            case 2:
                $str_status_delivery = '已发货';
                break;
            case 3:
                $str_status_delivery = '已部分发货';
                break;
            case 4:
                $str_status_delivery = '已收货';
                break;
        }
        return $str_status_delivery;
    }
    
    //根据ID获取付款方式
    public function get_pay_type($pay_type) {
        $str_pay_type = '';
        switch ($pay_type) {
            case '1':$str_pay_type = '在线支付';
                break;
            case '2':$str_pay_type = '货到付款';
                break;
        }
        return $str_pay_type;
    }

    
    /**
     * 搜索符合条件的记录
     * @param <type> $conditions 条件数组
     * @param string $order 排序字段
     * @param bool $export 是否导出excel
     * @param int $pagesize 分页数
     */
    public function search($conditions = array(), $order = 'order_id desc', $export = false, $pagesize = 20) {
        $map = $conditions;
        if(!empty($map['member_name'])) {
            $MemberObj = new \Fwadmin\Model\MemberModel();
            $map['member_id'] = $MemberObj->get_member_id($map['member_name']);
            unset($map['member_name']);
        }
		if(!empty($map['order_no'])){
		$map['order_no'] = array('like','%'.$map['order_no'].'%');
		}
		if(!empty($map['addr_realname'])){
		$map['addr_realname'] = array('like','%'.$map['addr_realname'].'%');
		}
		if(!empty($map['addr_phone'])){
		$map['addr_phone'] = array('like','%'.$map['addr_phone'].'%');
		}
        if(!empty($map['addr_province'])){
            $map['addr_province'] = array('like','%'.$map['addr_province'].'%');
        }
        if($map['status'] == 1) {
           $map['_string'] =  "(pay_type=1 and status_pay=3 and status=1) OR (pay_type=2 and status=1)";
           unset($map['status']);
        }
        if($map['status_delivery'] == 1) {
           $map['status'] = 2;
        }
		
        if (!$export) {
            $count = $this->where($map)->count(); // 查询满足要求的总记录数
            $Page = new \Think\Page($count, $pagesize); // 实例化分页类 传入总记录数和每页显示的记录数
            //分页跳转的时候保证查询条件
            foreach ($conditions as $key => $val) {
                $Page->parameter[$key] = urlencode($val);
            }
            $show = $Page->show(); // 分页显示输出
            // 进行分页数据查诟 注意limit 方法癿参数要使用Page 类的属性
            $list = $this->where($map)->order($order)->limit($Page->firstRow . ',' . $Page->listRows)->select();
            return array(
                'list' => $list,
                'page' => $show
            );
        } else {
            return $this->where($map)->order($order)->select();
        }
    }
    
    /**
     * 搜索符合条件的记录
     * @param int $member_id 会员ID
     * @param array $conditions 条件数组
     * @param string $order 排序字段
     * @param int $pagesize 分页数
     */
    public function search_member($member_id,$conditions = array(), $order = 'order_id desc', $pagesize = 20) {
        $map = $conditions;
        $map['member_id'] = $member_id;
        if(isset($map['status_mark'])) {//订单状态搜索
            if($map['status_mark'] == 1) {//待支付
                $map['pay_type'] = 1;
                $map['status'] = 1;
                $map['status_pay'] = 1;
            } elseif ($map['status_mark'] == 2) {//待审核
                $map['_string'] = "(pay_type=1 and status_pay=3 and status=1) OR (pay_type=2 and status=1)";
            } elseif ($map['status_mark'] == 3) {//待发货
                $map['status'] = 2;
                $map['status_delivery'] = 1;
            } elseif ($map['status_mark'] == 4) {//待收货
                $map['status'] = 2;
                $map['status_delivery'] = 2;
            } elseif ($map['status_mark'] == 5) {
                $map['status'] = 3;
            } elseif ($map['status_mark'] == 6) {
                $map['status'] = 4;
            } elseif ($map['status_mark'] == 7) {
                $map['status'] = 5;
            }
            unset($map['status_mark']);
        }
        
        if(isset($map['k'])) {
            $map['order_no'] = $map['k']; 
            unset($map['k']);
        }
        $count = $this->where($map)->count(); // 查询满足要求的总记录数
        $Page = new \Think\Page($count, $pagesize); // 实例化分页类 传入总记录数和每页显示的记录数
        //分页跳转的时候保证查询条件
        foreach ($conditions as $key => $val) {
            $Page->parameter[$key] = urlencode($val);
        }
        $show = $Page->show(); // 分页显示输出
        // 进行分页数据查诟 注意limit 方法癿参数要使用Page 类的属性
        $list = $this->where($map)->order($order)->limit($Page->firstRow . ',' . $Page->listRows)->select();
        
        $total_page = $Page->totalPages; //总页数
        if(empty($total_page)) {
            $total_page = 0;
        }
        
        return array(
            'list' => $list,
            'page' => $show,
            'count' => $count,
            'page_count' => $total_page
        );
    }
    
    /**
     * 获取订单图片数组
     * @param int $order_id 订单ID
     * @return array 订单购物车数组
     */
    public function show_goods_pic($order_id) {
        $OrderGoodsObj = M('OrderGoods');
        $goods = $OrderGoodsObj->where(array('order_id'=>$order_id))->select();
		
        return $goods;
    }
    
    /**
     * 获取待支付金额
     * @param array $order 订单数组
     * @return decimal 待支付金额
     */
    public function get_total_need_pay($order) {
        return $order['total'] - $order['total_discount'] - $order['total_pay'] - $order['total_pay_point'] - $order['total_pay_yhq'];
    }
    
    /**
     * 获取订单购物车信息
     * @param int $order_id 订单ID
     * @return array 订单购物车数组
     */
    public function get_goods($order_id) {
        $OrderGoodsObj = M('OrderGoods');
        $goods = $OrderGoodsObj->where(array('order_id'=>$order_id))->select();
        foreach($goods as $k => $v) {
            $goods[$k]['sub_total'] = $v['goods_price'] * $v['goods_num'];
        }
        return $goods;
    }
    
    /**
     * 获取订单跟踪信息
     * @param int $order_id 订单ID
     * @return array 订单跟踪数组
     */
    public function get_record($order_id) {
        $OrderRecordObj = M('OrderRecord');
        return $OrderRecordObj->where(array('order_id'=>$order_id))->order('id asc')->select();
    }
    
    /**
     * 取消审核通过
     * @param int $order_id 订单编号
     */
    public function confirm_1($order_id) {
        $status = $this->where(array('order_id'=>$order_id))->getField('status');
        if($status == 2) {
            $flag = $this->where(array('order_id'=>$order_id))->setField('status',1);
            if($flag > 0) {
                $this->add_order_record($order_id, '你的订单被取消审核');
            }
            return $flag;
        } elseif($status == 5) {
            $flag = $this->where(array('order_id'=>$order_id))->setField('status',1);
            if($flag > 0) {
                $this->add_order_record($order_id, '你的订单被取消作废');
            }
            return $flag;
        }
        return -1;
    }
    

    /**
     * 订单审核通过
     * @param int $order_id 订单编号
     */
    public function confirm_2($order_id) {
        $status = $this->where(array('order_id'=>$order_id))->getField('status');
        if($status == 1) {
            $data['order_id'] = $order_id;
            $data['status'] = 2;
            $data['time_confirm'] = time();
            $flag = $this->data($data)->save();//设置订单支付状态改变
            if($flag > 0) {
                //订单记录
                $this->add_order_record($order_id, '你的订单已通过审核');
                //扣除库存量
                //$this->reduce_store_num($order_id);
            }
            return $flag;
        }
        return -1;
    }
	
	
	 /**
     * 商品库存判断
     * @param type $order_id
     */
    public function check_store_num($order_id) {
        $OrderGoodsObj = M('order_goods');
        $GoodsItemObj = M('goods_item');
        $order_goods = $OrderGoodsObj->field('goods_item_id,goods_id,goods_num')->where(array('order_id'=>$order_id))->select();
		
        foreach($order_goods as $k => $v) {
		$store_num = $GoodsItemObj->where(array('item_id'=>$v['goods_item_id']))->getField('store_num');
		 if($store_num<$v['goods_num']){
			return $v['goods_id'];
			exit();
		 }  
        }
		return 0;
		
    }
	
    /**
     * 减少商品库存
     * @param type $order_id
     */
    public function reduce_store_num($order_id) {
        $OrderGoodsObj = M('order_goods');
        $GoodsItemObj = M('goods_item');
        $order_goods = $OrderGoodsObj->field('goods_item_id,goods_id,goods_num')->where(array('order_id'=>$order_id))->select();
        foreach($order_goods as $k => $v) {
            $GoodsItemObj->where(array('item_id'=>$v['goods_item_id']))->setDec('store_num',$v['goods_num']);
			/****调用库存接口，同时减少库存****/
			
        }
    }
	
    	 /**
     * 增加商品库存
     * @param type $order_id
     */
    public function increase_store_num($order_id) {
        $OrderGoodsObj = M('order_goods');
        $GoodsItemObj = M('goods_item');
        $order_goods = $OrderGoodsObj->field('goods_item_id,goods_id,goods_num')->where(array('order_id'=>$order_id))->select();
        foreach($order_goods as $k => $v) {
            $GoodsItemObj->where(array('item_id'=>$v['goods_item_id']))->setInc('store_num',$v['goods_num']);
			/****调用库存接口，同时增加库存**/
        }
    }
    /**
     * 取消订单（前台操作）
     * @param int $order_id 订单编号
     * @param int $member_id 会员ID 
     */
    public function confirm_4($order_id,$member_id) {
        $map['order_id'] = $order_id;
        $map['member_id'] = $member_id;
        $status = $this->where($map)->getField('status');
        if($status == 1) {
            $flag = $this->where(array('order_id'=>$order_id))->setField('status',4);
            if($flag > 0) {
                //订单记录
                $this->add_order_record($order_id, '你的订单已取消');
                //退还积分
                $this->back_point($order_id);
                
                //$this->back_money($order_id);
            }
            return $flag;
        }
        return -1;
    }
    
    /**
     * 订单作废（后台操作）
     * @param int $order_id 订单编号
     */
    public function confirm_5($order_id) {
        $status = $this->where(array('order_id'=>$order_id))->getField('status');
        if($status == 1) {
            $flag = $this->where(array('order_id'=>$order_id))->setField('status',5);
            if($flag > 0) {
                //订单记录
                $this->add_order_record($order_id, '你的订单已被作废');
                //退还积分
                $this->back_point($order_id);
                
                //$this->back_money($order_id);
            }
            return $flag;
        }
        return -1;
    }
    
    /**
     * 退还会员积分(订单取消或者订单作废)
     * @param int $order_id 订单编号
     */
    public function back_point($order_id) {
        $order = $this->field('member_id,total_point,total_pay_point')->find($order_id);
        if($order['total_point'] > 0) {
            $MemberPointObj = new \Fwadmin\Model\MemberPointModel();
            $flag = $MemberPointObj->add_record($order['member_id'],$order['total_point'], '积分退还', 0);
            if($flag > 0) {
                //修改订单数据
                $data['order_id'] = $order_id;
                $data['total_point'] = 0;
                $data['total_pay_point'] = 0;
                $this->data($data)->save();
                //订单记录
                $this->add_order_record($order_id, '你订购所用的'.$order['total_point'].'积分已退还');
            }
        }
    }
    


/*
    public function back_money($order_id) {
        $order = $this->field('member_id,total_pay')->find($order_id);
        if($order['total_pay'] > 0) {
            $MemberMoneyObj = new \Fwadmin\Model\MemberMoneyModel();
            $flag = $MemberMoneyObj->add_record($order['member_id'],$order['total_pay'], '订单支付金额退还', 0);
            if($flag > 0) {
                //修改订单数据
                $data['order_id'] = $order_id;
                $data['total_pay'] = 0;
                $this->data($data)->save();
                //订单记录
                $this->add_order_record($order_id, '你支付的￥'.$order['total_pay'].'金额已退还到你的余额账户');
            }
        }
    }
  */  
    /**
     * 订单支付
     * 工作1：修改订单表状态  工作2：添加订单记录  工作3：添加支付记录
     * @param int $order_id 订单编号
     * @param string $payment 支付方式
     */
    public function pay_3($order_id,$payment,$money,$remark='') {
        $order = $this->field('member_id,total,total_discount,total_pay,total_pay_point,status_pay')->where(Array('order_id'=>$order_id))->find();
        $total_need_pay = $order['total'] - $order['total_discount'] - $order['total_pay'] - $order['total_pay_point'];//待支付金额
        $member_id = $order['member_id'];//会员ID
        $status_pay = $order['status_pay'];//获得订单支付状态
        $total_pay = $order['total_pay'];//已支付金额
		
        if($status_pay != 3 && $money > 0) {
            $data['order_id'] = $order_id;
            if($total_need_pay - $money <= 0) {
                $data['status_pay'] = 3;
            } else {
				$data['status_pay'] = 3;
                //$data['status_pay'] = 2;
            }
            $data['time_pay'] = time();
            $data['payment'] = $payment;
            $data['total_pay'] = $total_pay + $money; //总支付金额
            $flag = $this->data($data)->save();//设置订单支付状态改变
			//echo $this->_sql();
            if($flag > 0) {
                if($data['status_pay'] == 3) {
                    $this->add_order_record($order_id, '你的订单已支付');//添加订单记录
                } else {
                    $this->add_order_record($order_id, '你的订单支付了￥'.$money);//添加订单记录
                }
                //添加支付记录
                $PayRecordObj = new \Fwadmin\Model\PayRecordModel();
                $PayRecordObj->record_add(1, $member_id, $order_id, $money, $payment, $remark);
				
				 //扣除库存量
                $this->reduce_store_num($order_id);
                //更新订单完成状态
                $this->order_complete($order_id);
            }
            return $flag;
        }
        
        return -1;
    }
    
    /**
     * 更新订单完成状态
     * @param int $order_id 订单ID
     */
    public function order_complete($order_id) {
        $order = $this->field('status,status_pay,status_delivery')->find($order_id);
		//如果已支付，已审核，已收货，则订单完成
        if($order['status_pay'] == 3 && $order['status_delivery'] == 4 && $order['status'] == 2) {
            $this->where(array('order_id'=>$order_id))->setField('status',3);//设置状态
            $this->add_order_record($order_id, '你的订单已完成');//添加订单记录
            $this->update_count_sale_order($order_id);//更新订单销量数据
            //赠送积分
            $MemberPointObj = new \Fwadmin\Model\MemberPointModel();
            $config = S('config');
            //$MemberPointObj->add_by_buy($order_id, $config['point_order_percent']);
			 $MemberPointObj->add_by_buy2($order_id);
        }
    }
    
    /**
     * 如果待支付金额为0 更新订单支付状态
     * @param int $order_id 订单ID
     */
    public function update_pay_status($order_id) {
        $order = $this->field('member_id,total,total_discount,total_pay,total_pay_point,total_pay_yhq,status_pay')->find($order_id);
        $total_need_pay = $this->get_total_need_pay($order);//待支付金额
        $status_pay = $order['status_pay'];//获得订单支付状态
        if($total_need_pay <= 0 && $status_pay != 3) {
            $data['order_id'] = $order_id;
            $data['time_pay'] = time();
            $data['status_pay'] = 3;
            $flag = $this->data($data)->save();//设置订单支付状态改变
            if($flag > 0) {
                $this->add_order_record($order_id, '你的订单已支付');//添加订单记录
                //更新订单完成状态
                $this->order_complete($order_id);
            }
            return $flag;
        }    
    }
    
    /**
     * 订单发货
     * @param int $order_id 订单ID
     * @param string $delivery_company 配送公司
     * @param string $delivery_no 配送单号
     */
    public function delivery_2($order_id,$delivery_company,$delivery_no,$delivery_code='') {
        $status_delivery = $this->where(array('order_id'=>$order_id))->getField('status_delivery');//获得订单发货状态
        if($status_delivery == 1) {
            $data['order_id'] = $order_id;
            $data['status_delivery'] = 2;
            $data['time_delivery'] = time();
            $data['delivery_company'] = $delivery_company;
			$data['delivery_code'] = $delivery_code;
            $data['delivery_no'] = $delivery_no;
            $flag = $this->data($data)->save();//设置订单状态
            if($flag > 0) {
                //添加订单记录
                $this->add_order_record($order_id, '你的订单已发货');
            }
            return $flag;
        }
        return -1;
    }
    
    /**
     * 确认收货
     * @param int $order_id 订单ID
     * @param int $member_id 会员ID 当前台操作时需要
     */
    public function delivery_4($order_id,$member_id = 0) {
        $map['order_id'] = $order_id;
        if($member_id > 0) {
            $map['member_id'] = $member_id;
        }
        $status_delivery = $this->where($map)->getField('status_delivery');//获得订单发货状态
        if($status_delivery == 2) {
            $data['order_id'] = $order_id;
            $data['status_delivery'] = 4;
            $data['time_complete'] = time();
            $flag = $this->data($data)->save();//设置订单状态
            if($flag > 0) {
                 //添加订单记录
                $this->add_order_record($order_id, '你的订单已确认收货');
                //更新订单完成状态
                $this->order_complete($order_id);
            }
            return $flag;
        }
        return -1;
    }
    
    /**
     * 更新订单商品销量数据
     * @param int $order_id 订单ID
     * @return int 更新的商品数量
     */
    public function update_count_sale_order($order_id) {
        $OrderGoodsObj = M('OrderGoods');
        $goods = $OrderGoodsObj->field('goods_id')->where(array('order_id'=>$order_id))->select();
        $GoodsObj = new \Home\Model\GoodsModel();
        $count = 0;
        foreach($goods as $k => $v) {
            $count += $GoodsObj->update_count_sale($v['goods_id']);
        }
        return $count;
    }
    
    /**
     * 生成订单编号
     */
    public function get_order_no($order_type = 0,$member_id = '00000') {
        return $order_type.date('ymdHis',time()).$member_id;
    }
    
    /**
     * 获取订单来源
     * @param tiny $source 订单来源ID
     */
    public function get_source($source) {
        $str = '';
        switch($source) {
            case 1:$str = 'PC';
                break;
            case 2:$str = 'Web APP';
                break;
            case 3:$str = 'IOS APP';
                break;
            case 4:$str = 'Android APP';
                break;  
        }
        return $str;
    }
    
    /**
     * 生成订单记录
     * @param int $order_id 订单ID
     * @param int $type 记录类型
     */
    public function add_order_record($order_id,$spec,$action_name = '系统') {
        $data['order_id'] = $order_id;
        $data['create_time'] = date("Y-m-d H:i:s",time());
        $data['spec'] = $spec;
        $data['action_name'] = $action_name;
        $OrderRecordObj = M('OrderRecord');
        return $OrderRecordObj->data($data)->add();
    }
    
    /**
     * 获取待付款订单数量
     * @param int $member_id 会员ID
     */
    public function count_need_pay($member_id = 0) {
        $map['pay_type'] = 1;
        $map['status_pay'] = 1;
        if($member_id > 0) {
           $map['member_id'] = $member_id;
        }
        return $this->where($map)->count();
    }
    
    /**
     * 获取待发货订单数量
     * @param int $member_id 会员ID
     */
    public function count_need_delivery($member_id = 0) {
        $map['status'] = 2;
        $map['status_delivery'] = 1;
        if($member_id > 0) {
           $map['member_id'] = $member_id;
        }
        return $this->where($map)->count();
    }
    
    /**
     * 获取待收货订单数量
     * @param int $member_id 会员ID
     */
    public function count_need_complete($member_id = 0) {
        $map['status'] = 2;
        $map['status_delivery'] = 2;
        if($member_id > 0) {
           $map['member_id'] = $member_id;
        }
        return $this->where($map)->count();
    }
    
    /**
     * 销售额统计
     * @param int $pagesize 分页数
     */
    public function tongji_total($conditions = array(), $pagesize = 100) {
        $where = ' where status_pay = 3 and status <> 4 and  status <> 5 '; //只统计已支付有效订单
        if(!empty($conditions['btime'])) {
           $btime = strtotime($conditions['btime'].' 00:00:00');
           $where .= ' and time_create>='.$btime;
        }
        if(!empty($conditions['etime'])) {
           $etime = strtotime($conditions['etime'].' 23:59:59');
           $where .= ' and time_create<'.$etime;
        }
        $data_format = "DATE_FORMAT(from_unixtime(time_create),'%Y-%m-%d')";
        if(!empty($conditions['type'])) {
            if($conditions['type'] == '月') {
                $data_format = "DATE_FORMAT(from_unixtime(time_create),'%Y-%m')";
            } elseif ($conditions['type'] == '年') {
                $data_format = "DATE_FORMAT(from_unixtime(time_create),'%Y')";
            }
        }
        $sql = "select ".$data_format." as day,sum(total) as total from ".C('DB_PREFIX')."order ".$where." group by day order by day desc limit ".$pagesize;
        $list = $this->query($sql);
        $sql_total = "select sum(total) as total from ".C('DB_PREFIX')."order ".$where." ";
        $query_total = $this->query($sql_total);
        $total = $query_total[0]['total'];
        $data['list'] = $list;
        $data['total'] = $total;
        return $data;
    }
    
    /**
     * 销售量注册
     * @param int $pagesize 分页数
     */
    public function tongji_total_num($conditions = array(), $pagesize = 100) {
        $where = ' where status_pay = 3 and status <> 4 and status <> 5 '; //只统计已支付有效订单
        if(!empty($conditions['btime'])) {
           $btime = strtotime($conditions['btime'].' 00:00:00');
           $where .= ' and time_create>='.$btime;
        }
        if(!empty($conditions['etime'])) {
           $etime = strtotime($conditions['etime'].' 23:59:59');
           $where .= ' and time_create<'.$etime;
        }
        $data_format = "DATE_FORMAT(from_unixtime(time_create),'%Y-%m-%d')";
        if(!empty($conditions['type'])) {
            if($conditions['type'] == '月') {
                $data_format = "DATE_FORMAT(from_unixtime(time_create),'%Y-%m')";
            } elseif ($conditions['type'] == '年') {
                $data_format = "DATE_FORMAT(from_unixtime(time_create),'%Y')";
            }
        }
        $sql = "select ".$data_format." as day,count(1) as total from ".C('DB_PREFIX')."order ".$where." group by day order by day desc limit ".$pagesize;
        $list = $this->query($sql);
        $sql_total = "select count(1) as total from ".C('DB_PREFIX')."order ".$where." ";
        $query_total = $this->query($sql_total);
        $total = $query_total[0]['total'];
        $data['list'] = $list;
        $data['total'] = $total;
        return $data;
    }
    
     /**
     * 统计商品销量排行榜
     * @param int $pagesize 分页数
     */
    public function tongji_goods($conditions = array(), $pagesize = 50) {
        $where = ' where o.order_id=g.order_id and o.status_pay = 3 and o.status <> 4 and o.status <> 5';
        if(!empty($conditions['btime'])) {
           $btime = strtotime($conditions['btime'].' 00:00:00');
           $where .= ' and o.time_create>='.$btime;
        }
        if(!empty($conditions['etime'])) {
           $etime = strtotime($conditions['etime'].' 23:59:59');
           $where .= ' and o.time_create<'.$etime;
        }
        $sql = 'select g.goods_id,sum(g.goods_num) as num from '.C('DB_PREFIX').'order_goods g,'.C('DB_PREFIX').'order o '.$where.' GROUP BY g.goods_id order by num desc limit '.$pagesize;
        $list = $this->query($sql);
        $GoodsObj = new \Fwadmin\Model\GoodsModel();
        foreach($list as $k => $v) {
            $list[$k]['goods_name'] = $GoodsObj->where('goods_id='.$v['goods_id'])->getField('goods_name');
        }
        return $list;
    }
    
    

}
