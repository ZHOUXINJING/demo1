<?php

namespace Fwadmin\Model;

use Think\Model;

class ConAdvModel extends Model {

    protected $_validate = array(
        array('is_show', array(0, 1), '请设置是否显示！', 3, 'in')
    );

    /**
     * 搜索符合条件的记录
     * @param array $conditions 条件数组
     * @param string $order 排序字段
     * @param int $pagesize 分页数
     */
    public function search($conditions, $order = 'id asc', $pagesize = 20) {

        $count = $this->where($conditions)->count(); // 查询满足要求的总记录数
        $Page = new \Think\Page($count, $pagesize); // 实例化分页类 传入总记录数和每页显示的记录数
        //分页跳转的时候保证查询条件
        foreach ($conditions as $key => $val) {
            $Page->parameter[$key] = $val;
        }
        $show = $Page->show(); // 分页显示输出
        // 进行分页数据查诟 注意limit 方法癿参数要使用Page 类的属性
        $list = $this->where($conditions)->order($order)->limit($Page->firstRow . ',' . $Page->listRows)->select();
        return array(
            'list' => $list,
            'page' => $show
        );
    }

    public function del($id) {
        $picture = $this->where('id=' . $id)->getField('picture');
        delfile($picture);
        return $this->delete($id);
    }

}
