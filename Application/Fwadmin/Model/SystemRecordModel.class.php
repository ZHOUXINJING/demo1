<?php

namespace Fwadmin\Model;

use Think\Model;

class SystemRecordModel extends Model {
    /**
     * 增加记录
     * @param string $spec 记录内容
     */
    public function addrecord($spec) {
        $this->create_time = time();
        $this->create_ip = get_client_ip();
        $this->admin_id = session('admin_id');
        $this->spec = $spec;
        return $this->add();
    }

    /**
     * 搜索符合条件的记录
     * @param <type> $conditions 条件数组
     * @param string $order 排序字段
     * @param bool $export 是否导出excel
     * @param int $page 分页数
     */
    public function search($conditions = array(), $order = 'record_id desc', $export = false, $page = 0) {
        if (!$export) {
            $count = $this->where($conditions)->count(); // 查询满足要求的总记录数
            if ($page == 0)
                $page = C('PAGE_NUM');
            $Page = new \Think\Page($count, $page); // 实例化分页类 传入总记录数和每页显示的记录数
            //分页跳转的时候保证查询条件
            foreach ($conditions as $key => $val) {
                $Page->parameter[$key] = urlencode($val);
            }
            $show = $Page->show(); // 分页显示输出
            // 进行分页数据查诟 注意limit 方法癿参数要使用Page 类的属性
            $list = $this->where($conditions)->order($order)->limit($Page->firstRow . ',' . $Page->listRows)->select();
            return array(
                'list' => $list,
                'pageShow' => $show
            );
        } else {
            return $this->where($conditions)->order($order)->select();
        }
    }

}

?>
