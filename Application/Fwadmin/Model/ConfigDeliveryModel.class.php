<?php

namespace Fwadmin\Model;

use Think\Model;

class ConfigDeliveryModel extends Model {

    /**
     * 后台列表
     */
    public function get_list() {
        return $this->order('order_id asc,delivery_id asc')->select();
    }
    

    /**
     * 删除
     */
    public function del($id) {
        $flag = $this->delete($id); 
        return $flag;
    }

}
