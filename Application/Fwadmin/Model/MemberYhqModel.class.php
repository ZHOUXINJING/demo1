<?php

namespace Fwadmin\Model;

use Think\Model;

class MemberYhqModel extends Model {

    /**
     * 发放优惠券
     * @param int $member_id 会员ID
     * @param decimal $money 优惠券金额
     * @param datetime $begin_time 可使用开始时间
     * @param datetime $end_time 可使用结束时间
     * @param string $remark 备注
     * @return int 大于0表示添加成功
     */
    public function add_record($member_id, $money, $begin_time, $end_time, $remark = '',$cart_total=0) {
        $data['member_id'] = $member_id;
        $code1 = date('ymd',strtotime($begin_time));
        $code2 = date('ymd',strtotime($end_time));
        $data['youhui_code'] = $member_id.$code1.$code2.randomname(6);
        $data['money'] = $money;
		$data['cart_total'] = $cart_total;
        $data['begin_time'] = $begin_time;
        $data['end_time'] = $end_time;
        $data['create_time'] = date('Y-m-d H:i:s',time());
        $data['remark'] = $remark;
        $data['is_use'] = 0;
        $data['use_money'] = 0;
        return $this->data($data)->add();
    }
    
     /**
     * 获取已发放优惠券记录
     * @param array $map 搜索条件数组
     * @param string $order 排序规则
     * @param int $pagesize 分页数
     */
    public function search($conditions,$order =  '',$pagesize = 30) {
        if(empty($order)) {
            $order = 'is_use asc,id desc';
        }
        $map = $conditions;
        if(isset($map['is_use'])) {
           if($map['is_use'] == 2)$map['is_use'] = 0;
        }
        if (isset($map['username'])) {
            $MemberObj = new \Fwadmin\Model\MemberModel();
            $member_id = $MemberObj->get_member_id($map['username']);
            $map['member_id'] = $member_id;
            unset($map['username']);
        }
        if (isset($map['time_begin']) && isset($map['time_end'])) {
            $time_begin = $map['time_begin'].' 00:00:00';
            $time_end = $map['time_end'].' 23:59:59';
            $map['create_time'] = array('between',array($time_begin,$time_end));
            unset($map['time_begin']);
            unset($map['time_end']);
        } elseif (isset($map['time_begin'])) {
            $time_begin = $map['time_begin'].' 00:00:00'; 
            $map['create_time'] = array('egt',$time_begin);
            unset($map['time_begin']);
        } elseif (isset($map['time_end'])) {
            $time_end = $map['time_end'].' 23:59:59'; 
            $map['create_time'] = array('elt',$time_end);
            unset($map['time_end']);
        }
        
        
        $count = $this->where($map)->count(); // 查询满足要求的总记录数
        $Page = new \Think\Page($count, $pagesize); // 实例化分页类 传入总记录数和每页显示的记录数
        //分页跳转的时候保证查询条件
        foreach ($conditions as $key => $val) {
            $Page->parameter[$key] = $val;
        }
        $show = $Page->show(); // 分页显示输出
        // 进行分页数据查诟 注意limit 方法癿参数要使用Page 类的属性
        $list = $this->where($map)->order($order)->limit($Page->firstRow . ',' . $Page->listRows)->select();
        return array(
            'list' => $list,
            'page' => $show
        );
    }
   
}
