<?php

namespace Fwadmin\Model;

use Think\Model;

class PayRecordModel extends Model {
    
    /**
     * 增加记录
     * @param int $type 类型：1订单支付  2充值  3退款
     * @param int $member_id 会员ID
     * @param int $order_id 订单编号
     * @param decimal $money 支付金额
     * @param string $payment 支付方式
     * @param string $remark 备注
     * @return int 大于1表示添加成功 否则失败
     */
    public function record_add($type,$member_id,$order_id,$money,$payment,$remark) {
        $this->time_create = time();
        $this->admin_id = session('admin_id');
        $this->type = $type;
        $this->member_id = $member_id;
        $this->order_id = $order_id;
        $this->money = $money;
        $this->payment = $payment;
        $this->remark = $remark;
        return $this->add();
    }
    
    /**
     * 获得订单支付记录
     * @param int $order_id 订单ID
     * @return array 支付记录数组
     */
    public function get_record($order_id) {
        return $this->where(array('order_id'=>$order_id))->select();
    }

    
}

?>
