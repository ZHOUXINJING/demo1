<?php

namespace Fwadmin\Model;

use Think\Model;

class ConfigEmailModel extends Model {

    protected $_validate = array(
        array('is_enable', array(0, 1), '请设置是否显示！', 1, 'in')
    );
    
    /*
     * 随机获取一个发邮箱对象
     */
    public function getSendEmailRand() {
        $row = $this->where('is_enable=1')->order("rand()")->limit(1)->find();
        return $row;
    }
    
    /**
     * 后台搜索
     * @param <type> $conditions 条件数组
     * @param int $pagesize 分页数
     */
    public function search($conditions, $pagesize = 20) {
        $order = 'id desc';
        $count = $this->where($conditions)->count(); // 查询满足要求的总记录数
        if ($pagesize == 0) {
            $pagesize = C('PAGE_NUM');
        }
        $Page = new \Think\Page($count, $pagesize); // 实例化分页类 传入总记录数和每页显示的记录数
        //分页跳转的时候保证查询条件
        foreach ($conditions as $key => $val) {
            $Page->parameter[$key] = urlencode($val);
        }
        $show = $Page->show(); // 分页显示输出
        // 进行分页数据查诟 注意limit 方法癿参数要使用Page 类的属性
        $list = $this->where($conditions)->order($order)->limit($Page->firstRow . ',' . $Page->listRows)->select();
        return array(
            'list' => $list,
            'page' => $show
        );
    }
    

    /**
     * 删除邮箱
     */
    public function del($id) {
        $flag = $this->delete($id); 
        return $flag;
    }

}
