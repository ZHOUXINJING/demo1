<?php

namespace Fwadmin\Model;

use Think\Model;

class ConHelpModel extends Model {
    
    /*
     * 返回一级栏目
     */
    public function get_category() {
        return $this->field('help_id,help_name')->where(array('parent_id'=>0))->select();       
    }
    
    /*
     * 获得栏目列表
     */
    public function get_list($parent_id, $str = '|--', $arr = Array()) {
        $arr1 = $this->get_sub_list($parent_id);
        if (is_array($arr1)) {
            foreach ($arr1 as $v) {
                $v['help_name'] = $str . $v['help_name'];
                array_push($arr, $v);
                $arr2 = $this->get_sub_list($v['help_id']);
                if (is_array($arr2)) {
                    $arr = $this->get_list($v['help_id'], $str . '----', $arr);
                }
            }
        }
        return $arr;
    }
    
    /*
     * 返回子栏目列表
     */
    public function get_sub_list($parent_id) {
        return $this->where('parent_id='.$parent_id)->order('order_id asc')->select();
    }
    
    /*
     * 删除栏目
     */
    public function del($help_id) {
        return $this->delete($help_id);
    }
    
    /*
     * 获取资讯栏目
     */
    public function get_news_list() {
        return $this->field('help_id,help_name')->where(array('type'=>'资讯'))->select();
    }

}
