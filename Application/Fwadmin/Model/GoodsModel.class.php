<?php

namespace Fwadmin\Model;

use Think\Model;
/**
 * 商品
 */
class GoodsModel extends Model {
    
    /**
     * 搜索符合条件的记录
     * @param array $conditions 条件数组
     * @param string $order 排序字段
     * @param bool $export 是否导出excel
     * @param int $page 分页数
     */
    public function search($conditions, $field = '', $order = 'order_id asc,goods_id desc', $export = false, $page = 0) {
        $map = array();	
        if(isset($conditions['title'])) {
            $map['goods_name|goods_sn'] = array('like','%'.$conditions['title'].'%');
        }
        if(isset($conditions['cat_id'])) {
            $map['cat_path_ids'] = array('like','%,'.$conditions['cat_id'].',%');		
        }
        if(isset($conditions['brand_id'])) {
            $map['brand_id'] = $conditions['brand_id'];
        }
        if(isset($conditions['status'])) {
            $map['status'] = $conditions['status'];
        } else {
            $map['status'] =  array('neq',3);
        }
        if (!$export) {
            $count = $this->where($map)->count(); // 查询满足要求的总记录数
            if ($page == 0) {
                $page = C('PAGE_NUM');
            }
            $Page = new \Think\Page($count, $page); // 实例化分页类 传入总记录数和每页显示的记录数
            //分页跳转的时候保证查询条件
            foreach ($conditions as $key => $val) {
                $Page->parameter[$key] = urlencode($val);
            }
            $show = $Page->show(); // 分页显示输出
            // 进行分页数据查诟 注意limit 方法癿参数要使用Page 类的属性
            if($field == '') {
                $list = $this->where($map)->order($order)->limit($Page->firstRow . ',' . $Page->listRows)->select();
            } else {
                $list = $this->field($field)->where($map)->order($order)->limit($Page->firstRow . ',' . $Page->listRows)->select();
            }
            return array(
                'list' => $list,
                'page' => $show
            );
        } else {
            if($field == '') {
                 return $this->where($map)->order($order)->select();
             } else {
                 return $this->field($field)->where($map)->order($order)->select();
             }
        }
    }
    
    /*
     * 获得商品单件
     */
    public function get_item($goods_id) {
        $MGoodsItem = M('GoodsItem');
        return $MGoodsItem->where('goods_id='.$goods_id)->order('order_id asc')->select();
    }
    
    /**
     * 删除商品不在列表的单件
     * @param int $goods_id 商品ID
     * @param array $item_ids 包含的单价列表
     * @return int 删除行数
     */
    public function del_item_notin($goods_id,$item_ids) {
        $MGoodsItem = M('GoodsItem');
        $flag =  $MGoodsItem->where(array('goods_id'=>$goods_id,'item_id'=>array('not in',$item_ids)))->delete();
        return $flag;
    }
    
    /**
     * 移动商品到回收站
     * @param int $goods_id 商品ID
     * @return 是否删除成功
     */
    public function move_recycle($goods_id) {
        return $this->where('goods_id='.$goods_id)->setField('status',3);
    }
    
    /**
     * 商品上架
     * @param int $goods_id 商品ID
     * @return 结果
     */
    public function set_status_1($goods_id) {
        return $this->where('goods_id='.$goods_id)->setField('status',1);
    }
    
    /**
     * 商品下架
     * @param int $goods_id 商品ID
     * @return 结果
     */
    public function set_status_2($goods_id) {
        return $this->where('goods_id='.$goods_id)->setField('status',2);
    }
    
    /**
     * 彻底删除商品
     */
    public function del($goods_id) {
        $flag = $this->delete($goods_id);
        if($flag > 0) {
            $MGoodsItem = M('GoodsItem');
            $MGoodsItem->where('goods_id='.$goods_id)->delete();
        }
        return $flag;
    }
    

}