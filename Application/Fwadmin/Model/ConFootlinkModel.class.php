<?php

namespace Fwadmin\Model;

use Think\Model;

class ConFootlinkModel extends Model {

    protected $_validate = array(
        array('order_id', 'require', '顺序编号必须填写！'),
        array('is_show', array(0, 1), '请设置是否显示！', 1, 'in')
    );

    /**
     * 获取所有类别
     * @param int $category_id 类别ID
     */
    public function getList() {
        $arr = $this->getTop();
        foreach ($arr as $k => $v) {
            $arr[$k]['sub'] = $this->getSub($v['category_id']);
        }
        return $arr;
    }

    /**
     * 获取子级列表
     * @param int $category_id 类别ID
     */
    public function getSub($parent_id) {
        $arr = $this->where(Array('parent_id' => $parent_id))->order('order_id asc')->select();
        return $arr;
    }

    /**
     * 获取顶级类别
     */
    public function getTop() {
        return $this->where(Array('parent_id' => 0))->order('order_id asc')->select();
    }
    
    /**
     * 删除类别
     */
    public function del($category_id) {
        $model = $this->find($category_id);
        if($model['parent_id'] == 0) {
            $flag = $this->delete($category_id);
            if($flag > 0) {
                $this->where('parent_id='.$category_id)->delete();
            }
        } else {
            $flag = $this->delete($category_id);
        }
        return $flag;
    }

}
