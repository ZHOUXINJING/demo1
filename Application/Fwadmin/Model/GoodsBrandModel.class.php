<?php

namespace Fwadmin\Model;

use Think\Model;
/**
 * 商品品牌
 */
class GoodsBrandModel extends Model {

     /**
     * 搜索商品品牌
     * @param array $conditions 条件数组
     * @param string $order 排序字段
     * @param bool $export 是否导出excel
     * @param int $page 分页数
     */
    public function search($conditions, $order = 'order_id asc', $export = false, $page = 0) {
        $map = $conditions;
        if (!$export) {
            $count = $this->where($map)->count(); // 查询满足要求的总记录数
            if ($page == 0) {
                $page = C('PAGE_NUM');
            }
            $Page = new \Think\Page($count, $page); // 实例化分页类 传入总记录数和每页显示的记录数
            //分页跳转的时候保证查询条件
            foreach ($conditions as $key => $val) {
                $Page->parameter[$key] = urlencode($val);
            }
            $show = $Page->show(); // 分页显示输出
            // 进行分页数据查诟 注意limit 方法癿参数要使用Page 类的属性
            $list = $this->where($map)->order($order)->limit($Page->firstRow . ',' . $Page->listRows)->select();
            return array(
                'list' => $list,
                'page' => $show
            );
        } else {
            return $this->where($map)->order($order)->select();
        }
    }
    
     /**
     * 删除品牌
     * @param int $brand_id 品牌ID
     * @return 大于1表示删除成功
     */
    public function del($brand_id) {
        return $this->delete($brand_id);
    }
    
    /**
     * 已启用品牌列表
     * @return 数组
     */
    public function get_sel() {
        return $this->field('brand_id,brand_name')->where('is_enable=1')->order('order_id asc,brand_id asc')->select();
    }
    
    /**
     * 推荐品牌列表
     * @return 数组
     */
    public function get_tuijian() {
        return $this->field('brand_id,brand_name,brand_name,logo')->where('is_enable=1 and is_tuijian=1')->order('order_id asc,brand_id asc')->select();
    }
    

}