<?php

namespace Fwadmin\Model;

use Think\Model;

class IndexBlockModel extends Model {

    /**
     * 获取模型列表
     */
     public function get_list($client_type = 1) {    
         $list = $this->where(array('client_type'=>$client_type))->order('order_id asc,block_id asc')->select();
         $StyleObj = M('index_style');
         foreach($list as $k => $v) {
             $list[$k]['style_name'] = $StyleObj->where('style_id='.$v['style_id'])->getField('style_name');
         }
         return $list;
     }    
     
     /**
      * 获取模型风格列表
      * @return array 列表
      */
     public function get_style($client_type = 1) {
         $StyleObj = M('index_style');
         return $StyleObj->where('is_enable=1 and client_type='.$client_type)->order('order_id asc,style_id asc')->select();
     }
     
     /**
      * 删除模型
      * @param int $block_id 模块ID
      * @return int 大于1表示成功
      */
     public function del($block_id) {
         return $this->delete($block_id);
     }
     
     /**
      * 添加模型商品内容
      * @param int $block_id 模型ID
      * @param int $style_id 样式ID 
      * @param int $style_item_id 样式项目ID
      * @param int $goods_id 商品ID
      * @return int 大于1表示添加成功
      */
     public function add_block_goods($block_id,$style_id,$style_item_id,$goods_id) {
         $data['block_id'] = $block_id;
         $data['style_id'] = $style_id;
         $data['style_item_id'] = $style_item_id;
         $data['goods_id'] = $goods_id;
         $data['order_id'] = 999;
         $data['is_show'] = 1;
         $IndexBlockCon = M('index_block_con');
         $flag = $IndexBlockCon->data($data)->add();
        
         return $flag;
     }
     
     /**
      * 获取模型内容
      * @param int $block_id 模型ID
      * @return array
      */
     public function get_block_con($block_id) {
        $IndexBlockCon = M('index_block_con');
        $IndexStyleItem = M('index_style_item');
        $GoodsObj = new \Fwadmin\Model\GoodsModel();
        $list = $IndexBlockCon->where(array('block_id'=>$block_id))->select();
        $k_adv = 0;
        $k_pro = 0;
        foreach($list as $k => $v) {
            $goods = $GoodsObj->where(array('goods_id'=>$v['goods_id']))->field('goods_name,price,price_market,cuxiao_msg,picture')->find();
            if(empty($v['picture'])) {
                $list[$k]['picture'] = str_replace('products','products/thumb',$goods['picture']);
            }
            if(empty($v['goods_name'])) {
                $list[$k]['goods_name'] = $goods['goods_name'];
            }
            if(empty($v['cuxiao_msg'])) {
                $list[$k]['cuxiao_msg'] = $goods['cuxiao_msg'];
            }
            if(empty($v['price']) || $v['price'] == 0) {
                $list[$k]['price'] = $goods['price'];
            }
             if(empty($v['price_market']) || $v['price_market'] == 0) {
                $list[$k]['price_market'] = $goods['price_market'];
            }
            if(empty($v['url']) && !empty($v['goods_id'])) {
                $list[$k]['url'] = __ROOT__.'/Goods/index/id/'.$v['goods_id'];
            }
            $list[$k]['style_item'] =  $IndexStyleItem->field('pic_width,pic_height,type,style_item_id')->find($v['style_item_id']);
            if($list[$k]['style_item']['type'] == '广告'){
                $k_adv ++;
                $list[$k]['t'] = $k_adv;
            } elseif ($list[$k]['style_item']['type'] == '产品'){
                $k_pro ++;
                $list[$k]['t'] = $k_pro; 
            }
        }
        return $list;
     }

}