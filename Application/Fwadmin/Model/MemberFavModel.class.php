<?php

namespace Fwadmin\Model;

use Think\Model;

class MemberFavModel extends Model {

    /**
     * 统计商品收藏排行榜
     * @param int $pagesize 分页数
     */
    public function tongji_goods($conditions = array(), $pagesize = 50) {
        $where = ' where 1=1';
        if(!empty($conditions['btime'])) {
           $btime = strtotime($conditions['btime'].' 00:00:00');
           $where .= ' and create_time>='.$btime;
        }
        if(!empty($conditions['etime'])) {
           $etime = strtotime($conditions['etime'].' 23:59:59');
           $where .= ' and create_time<'.$etime;
        }
        $sql = 'select goods_id,count(1) as num from '.C('DB_PREFIX').'member_fav '.$where.' GROUP BY goods_id order by num desc limit '.$pagesize;
        $list = $this->query($sql);
        $GoodsObj = new \Fwadmin\Model\GoodsModel();
        foreach($list as $k => $v) {
            $list[$k]['goods_name'] = $GoodsObj->where('goods_id='.$v['goods_id'])->getField('goods_name');
        }
        return $list;
    }
  

}

?>