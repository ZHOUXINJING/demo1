<?php
namespace Api\Controller;
use Common\Controller\ApiController;

class ContentController extends ApiController {
    
    function _initialize() {
        parent::_initialize();
        //验证 APP KEY
        $timestamp = I('request.timestamp');
        $token = I('request.token');
        parent::checkAppToken($token,$timestamp);
    }

    //图片轮播图Banner
    public function banner() { 
        $data['code'] = '200';
        $data['msg'] = '';
        $BannerObj = new \Home\Model\ConBannerModel();
        $banner = $BannerObj->get_list('index');
        foreach($banner as $k => $v) {
            $banner[$k]['picture'] = get_host().__ROOT__.$banner[$k]['picture640'];
        }
        $data['data'] = $banner;
        $this->response($data,C('API_TYPE'));
    }
   
    //获取快速搜索
    public function get_hot_search() {
        $data['code'] = '200';
        $data['msg'] = '';
        $HotsearchObj = new \Fwadmin\Model\HotsearchModel();
        $hotsearch = $HotsearchObj->field('title,url,color')->where('is_show=1')->order('order_id asc')->select();
        $data['data'] = $hotsearch;
        $this->response($data,C('API_TYPE'));
    }

}
