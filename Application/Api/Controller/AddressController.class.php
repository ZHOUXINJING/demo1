<?php
namespace Api\Controller;
use Common\Controller\ApiController;

class AddressController extends ApiController {
    
    function _initialize() {
        parent::_initialize();
        //验证 APP KEY
        $timestamp = I('request.timestamp');
        $token = I('request.token');
        parent::checkAppToken($token,$timestamp);
    }
    
    public function province() {
        $data['code'] = '200';
        $ProvinceObj = new \Home\Model\AddressProvinceModel();
        $province = $ProvinceObj->getSel();
        $data['data'] = $province;
        $this->response($data,C('API_TYPE'));
    }

    //获取城市选择
    public function city($province) {
        $data['code'] = '200';
        $ProvinceObj = M('AddressProvince');
        $CityObj = M('AddressCity');
        $provinceid = $ProvinceObj->getFieldByProvince($province,'provinceid');
        if($province == "" || empty($provinceid)) {
            $data['code'] = '500';
            $data['msg'] = '省份为空或者不存在！';
        } else {
            $city_sel = $CityObj->field('city')->where('provinceid='.$provinceid)->select();
            $data['data'] = $city_sel;
        }
        $this->response($data,C('API_TYPE'));
    }
    
    //获取地区选择
    public function area($city) {
        $data['code'] = '200';
        $CityObj = M('AddressCity');
        $AreaObj = M('AddressArea');
        $cityid = $CityObj->getFieldByCity($city,'cityid');
        if($city == "" || empty($cityid)) {
            $data['code'] = '500';
            $data['msg'] = '城市为空或不存在！';  
        } else {
            $area_sel = $AreaObj->field('area')->where('cityid='.$cityid)->select();
            $data['data'] = $area_sel;
        }
        $this->response($data,C('API_TYPE'));
    }
    
    //获取邮政编码
    public function get_zip($area) {
        $data['code'] = '200';
        $AreaObj = M('AddressArea');
        $zipcode = $AreaObj->getFieldByArea($area,'zipcode');
        $data['data'] = $zipcode;
        $this->response($data,C('API_TYPE'));
    }


}
