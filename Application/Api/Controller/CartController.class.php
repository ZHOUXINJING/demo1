<?php
namespace Api\Controller;
use Common\Controller\ApiController;

class CartController extends ApiController {

    function _initialize() {
        parent::_initialize();
        //验证 APP KEY
        $timestamp = I('request.timestamp');
        $token = I('request.token');
        parent::checkAppToken($token,$timestamp);
    }
    /**
     * 根据item_id列表获取购物车商品数据
     * $id_list 商品ID列表，用-分隔
     */
    public function get_goods_list($id_list) {
        $MGoods = new \Home\Model\GoodsModel();
        $id_list = trim($id_list,"-");
        $result = array();
        if($id_list != "") {
            $arr_id_list = explode("-", $id_list);
            for($i = 0; $i < count($arr_id_list); $i++) {
                $item_id = $arr_id_list[$i];
                if($item_id > 0) {
                    $goods_item = $MGoods->get_item_cart_app($item_id);//商品信息
                    if(!empty($goods_item)) {
                         if(empty($goods_item['count_sale'])){
                             $goods_item['count_sale'] = 0;
                         }
                        $goods_item['picture'] = get_host().__ROOT__.str_replace("goods/", 'goods/thumb/',$goods_item['picture']);
                        $result[] = $goods_item;
                    }
                }
            }
        }
        $data['code'] = '200';
        $data['msg'] = '';
        $data['data'] = $result;
        $this->response($data,C('API_TYPE'));
    }

}
