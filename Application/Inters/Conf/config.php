<?php
/**
 * Created by PhpStorm.
 * User: Aaron
 * Date: 2019/7/18
 * Time: 15:17
 */

return  array(
    'HOST'   => 'http://cdn.7jbbk.com',        // 网站域名
    'EXPIRE' => 60 * 60 * 24 * 15,               // token过期时间
    'DEFAULT_HEADPIC' => '',                     // 默认头像
    'CUSTOMER' => 'https://22527.ahc.ink/chat.html?service=25463&grade=2'   // 客服地址
);