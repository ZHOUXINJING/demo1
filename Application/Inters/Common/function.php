<?php
/**
 * Created by PhpStorm.
 * User: Aaron
 * Date: 2019/7/18
 * Time: 15:16
 */

/**
 * 拼接图片地址
 * @param $url
 * @param $filed
 * @return mixed
 */
function spellUrl($data, $filed) {
    $host = C('HOST');
    foreach ($data as &$v) {
        if (is_array($v[$filed])) {
            foreach ($v[$filed] as $k1=> $v1) {
                $v[$filed][$k1] = $host . $v1;
            }
        } else {
            $v[$filed] = $host . $v[$filed];
        }
    }
    return $data;
}

/**
 * 截取路径ID
 * @param $data
 * @param $filed
 */
function spellHtmlId($data, $filed)
{
    $arr = array();
    foreach ($data as $v) {
        preg_match('/id\/(\d+)\.[html|htm]/i', $v[$filed], $matches);
        $v['goods_id'] =  $matches[1];
        $arr[] = $v;
    }
    return $arr;
}


/**
 * 截取路径ID
 * @param $data
 * @return @id
 */
function spellUrlId($url)
{
    preg_match('/id\/(\d+)\.[html|htm]/i', $url, $matches);

    return $matches[1];
}

/**
 * 手机号有效校验
 * @param $phone
 * @return bool
 */
function phoneVerify($phone)
{
    $match = array();
    preg_match("/^1[0-9]{10}$/", $phone, $match);
    return $phone == $match[0];
}