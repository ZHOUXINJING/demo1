<?php

namespace Inters\Tool;

class  Upload
{
    /**
     *  上传图片
     */
    public static function uploadOneImage()
    {
        $file = $_FILES['headpic'];
        if ($file['error'] !== 0) {
            return array('status' => false, 'message' => '上传失败');
        }
        if (!strstr($file['type'], 'image')) {
            return array('status' => false, 'message' => '上传文件类型错误');
        }
        $path = './Public/upload/headpic/';
        if(!is_dir($path)) {
            mkdir($path, 777);
        }
        $filename = $path.time().$file['name'];
        if (!move_uploaded_file($file['tmp_name'], $filename)) {
            return array('status' => false, 'message' => '上传失败');
        }

        // 裁剪
        $image = new \Think\Image();
        $image->open($filename);
        $image->thumb(150, 150,\Think\Image::IMAGE_THUMB_FIXED)->save($filename);

        return array('status' => true, 'message' => '上传成功', 'filename' => trim($filename, '.'));
    }
}