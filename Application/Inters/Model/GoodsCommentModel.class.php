<?php
namespace Inters\Model;

use Think\Model;

class  GoodsCommentModel extends  Model
{
    private $limit = 10;

    /**
     *  商品评价
     */
    public function getCommentCount($goodId)
    {
        $data = $this->where(array('goods_id' => $goodId, 'status' => 2))->field('star,detail')->select();

        return $this->praise($data);
    }

    /**
     *  好评率   4,5 好评
     */
    public function praise($data)
    {
        $count = count($data);
        $i = 0;
        foreach ($data as $v) {
            if ($v['star'] < 4) {
                $i++;
            }
        }
        $percentage = round(($count - $i) / $count, 2) * 100;
        return array(
            'praise_count' => $count,
            'percentage' => $percentage == 0 ? 0 : $percentage
        );
    }

    /**
     *  商品评价列表
     * @param $param
     * @return array
     */
    public function getCommentList($param)
    {
        $data = $this->where(array('goods_id' => $param['goodId'], 'status' => 2))
                     ->join('shop_member ON shop_member.member_id = shop_goods_comment.member_id')
                     ->field('detail, shop_member.nickname, shop_goods_comment.create_time, star')
                     ->order('shop_goods_comment.create_time desc')
                     ->page($param['page'])
                     ->limit($this->limit)
                     ->select();

        return empty($data) ? array() : $data;
    }
}