<?php

namespace Inters\Model;

use Think\Model;

class ConBannerModel extends Model {

    /**
     *  获取 Banner 列表
     */
    public function getList() {
        $data = $this->field('title, skip_status, picture, url')
                     ->where(array('type'=> 'phone_index', 'is_show'=>1))
                     ->order('order_id asc,id asc')
                     ->select();

        return spellHtmlId(spellUrl($data, 'picture'), 'url');
    }



}
