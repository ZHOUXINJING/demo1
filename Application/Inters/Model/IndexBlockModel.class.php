<?php

namespace Inters\Model;

use Think\Model;

class IndexBlockModel extends Model
{
    private $productBlock = array();

    /**
     * 模块类型
     * @param $block
     * 判断类型: APP, 模块显示, 产品显示, 商品上架
     */
    public function getList()
    {
        $productData = $this->getProductData();
        $resultProductData = $this->handleDataProduct($productData);

        $bannerData = $this->getBannerData();
        $resultBannerData = $this->handleDataBanner($bannerData);

        return $this->result($resultProductData, $resultBannerData);
    }

    /**
     *  资源返回
     */
    public function result($resultProductData, $resultBannerData)
    {
        $i = 0;
        $result = array();
        foreach ($resultProductData as $k => $v) {
            if (strpos($this->productBlock[$k], ',') !== false) {
                $result[$i]['block_char_name'] = explode(',', $this->productBlock[$k]);
            }
            $result[$i]['block_name'] = $this->productBlock[$k];
            $result[$i]['block_data'] = $resultProductData[$k];
            $i++;
        }

        array_push($result, $resultBannerData);
        return $result;
    }

    /**
     * 获取产品数据集
     * @return mixed
     */
    public function getProductData()
    {
        return $this->where(array('client_type' => 3, 'is_enable' => 1, 'is_show' => 1, 'status' => 1))
                    ->join('shop_index_block_con ON shop_index_block_con.block_id = shop_index_block.block_id')
                    ->join('shop_goods ON shop_goods.goods_id = shop_index_block_con.goods_id')
                    ->select();
    }

    /**
     *  采集轮播图数据集
     */
    public function getBannerData()
    {
        return $this->where(array('client_type' => 3, 'is_enable' => 1, 'is_show' => 1, 'goods_id' => array('exp','is null')))
                    ->join('shop_index_block_con ON shop_index_block_con.block_id = shop_index_block.block_id')
                    ->select();
    }

    /**
     *  广告数据处理
     */
    public function handleDataBanner($data)
    {
        $arr = array();
        foreach ($data as $v) {
            $arr['block_name'] = $v['block_name'];

            $arr['block_data']['goods_name'] = $v['goods_name'];
            $arr['block_data']['picture'] = C('host') . $v['picture'];
            $arr['block_data']['summary'] = $v['summary'];
            $arr['block_data']['goods_id'] = spellUrlId($v['url']);
            $arr['block_data']['host'] = C('host');
            $arr['block_data']['url'] = $v['url'];
        }
        return $arr;
    }


    /**
     *  产品数据处理
     */
    public function handleDataProduct($data)
    {
        $arr = array();

        // 板块分组
        foreach ($data as $v) {
            if (!in_array($v['block_name'], $this->block_name)) {
                $this->productBlock[$v['block_id']] = $v['block_name'];
            }
        }

        // 筛选各个板块的数据
       foreach ($this->productBlock as $k => $v) {
           foreach ($data as $k1 => $v1) {
               $result = array();
               if ($v1['block_id']  == $k) {
                   $result['goods_name'] = mb_substr($v1['goods_name'],0,9);
                   $result['price'] = $v1['price'];
                   $result['goods_id'] = $v1['goods_id'];
                   $result['cuxiao_msg'] = $v1['cuxiao_msg'];
                   $result['picture'] = C('host') . $v1['picture'];
                   $result['host'] = C('host');
                   $arr[$k][] = $result;
               }
           }
       }
       return $arr;
    }

}