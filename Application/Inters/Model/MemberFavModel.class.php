<?php
/**
 * Created by PhpStorm.
 * User: Aaron
 * Date: 2019/7/23
 * Time: 14:37
 */
namespace Inters\Model;

use Think\Model;

class MemberFavModel extends Model
{
    /**
     *  收藏总数
     */
    public function getStoreCount($id)
    {
        return $this->where(array('member_id' => $id))->count();
    }

    /**
     * 收藏或取消
     * @param $member_id
     * @param $goodId
     * @param $itemId
     * @return true or false
     */
    public function setGoodStore($member_id, $goodId, $itemId)
    {
        $data = $this->where(array('member_id' => $member_id, 'goods_id' => $goodId, 'item_id' => $itemId))->find();
        if ($data === NULL) {
            // 商品价格
            $price = D('Goods')->where(array('goods_id' => $goodId))->getField('price');

            $this->data(array(
                'member_id' => $member_id,
                'goods_id'  => $goodId,
                'item_id'   => $itemId,
                'price' => $price,
                'create_time' => time()
            ))->add();
            return true;
        }
        $this->where(array('id' => $data['id']))->delete();
        return false;
    }
}