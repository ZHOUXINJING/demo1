<?php
/**
 * Created by PhpStorm.
 * User: Aaron
 * Date: 2019/12/17
 * Time: 15:18
 */

namespace Inters\Model;

use Think\Model;

class MemberAddressModel extends Model
{
    /**
     * 收货地址列表
     * @param $id
     * @return $array()
     */
    public function getAddressList($id)
    {
        return $this->where(array('member_id' => $id))->select();
    }

    /**
     * 获取地址详情
     */
    public function addressInfo($id)
    {
        return $this->where(array('id' => $id))->find();
    }

    /**
     * 收货地址修改
     * @param $data
     * @param $mid
     */
    public function addressEdit($data, $mid)
    {
        if ($data["is_default"] == 1) {
            $this->setDefaultAddress($mid);
        }

         $this->where(array('id' => $data['id']))->save(
            array("realname" => $data['realname'], "province" => $data['province'], "city" => $data['city'],
                "area" => $data['area'], "address" => $data['address'], "zip" => $data['zip'], "phone" => $data['phone'],
                "is_default" => $data['is_default'])
        );
    }

    /**
     * 收货地址删除
     * @param $id
     */
    public function addressDel($id)
    {
        $this->where(array('id' => $id))->delete();
    }

    /**
     * 收货地址增加
     * @param $data
     * @param $id
     */
    public function addressAdd($data, $mid)
    {
        if ($data['is_default'] == 1) {
            $this->setDefaultAddress($mid);
        }

        $this->data(array("member_id" => $mid ,"realname" => $data['realname'], "province" => $data['province'], "city" => $data['city'],
            "area" => $data['area'], "address" => $data['address'], "zip" => $data['zip'], "phone" => $data['phone'],
            "is_default" => $data['is_default']))->add();
    }


    /**
     * 修改默认地址
     * @param $mid
     */
    private function setDefaultAddress($mid)
    {
        $this->where(array('member_id' => $mid))->save(array('is_default' => 0));
    }
}