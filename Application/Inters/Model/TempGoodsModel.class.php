<?php

namespace Inters\Model;

use Think\Model;

class TempGoodsModel extends Model
{
    private $temp_time = 604800;     // 缓存时间 7 天
    private $cart_list = array();    // 购物车列表
    private $member_id;

    public function __construct($member_id)
    {
        parent::__construct();
        $data = S($member_id);

        if ($data !== false) {
            $this->cart_list = $data;
        }

        $this->member_id = $member_id;
    }


    /**
     * 添加购物车
     * @param $goods_id 商品id
     * @param $item_id  规格id
     * @param $num  数量
     */
    public function add_cart($goods_id, $item_id, $num)
    {
        if(empty($item_id)) {
            $GoodsObj = new \Inters\Model\GoodsModel();
            $item_id = $GoodsObj->get_default_item_id($goods_id);
        }

        if ($goods_id <= 0 || $item_id <= 0 || $num <= 0) return -1;    //检查数据规范

        $item = array("goods_id" => $goods_id, "item_id" => $item_id, "num" => $num);   //商品单件数组
        $i = $this->check_item($item);
        if ($i == -1) {
            $this->cart_list[] = $item;
        } else {
            $this->cart_list[$i]['num'] += $num;
        }

        $this->save_data();
    }


    /**
     * 检测单件是否存在
     * @param $item 商品数据
     */
    public function check_item($item)
    {
        $count = count($this->cart_list);

        for ($i = 0; $i <= $count; $i++) {
            if ($this->cart_list[$i]['item_id'] == $item['item_id']) {
                return $i;
            }
        }
        return -1;
    }

    /**
     * 保存数据
     */
    public function save_data()
    {
        S($this->member_id, $this->cart_list, $this->temp_time);
    }


    /**
     * 清空购物车
     */
    public function clear_data($item_list)
    {
        $item_list = explode(',', $item_list);

        foreach ($this->cart_list as $k => $v) {
            if (in_array($v['item_id'], $item_list)) {
                unset($this->cart_list[$k]);
            }
        }

        $this->save_data();
    }


    /**
     *  获取商品数据
     */
    public function get_store_data()
    {
        $data = array();

        foreach ($this->cart_list as $k => $v) {
            $item = D('goods_item')->where(array('item_id' => $v['item_id']))->find();
            $good = D('goods')->where(array('goods_id' => $item['goods_id']))->field('goods_name, picture')->find();
            $data[$k]['picture'] = $good['picture'];


            // 取规格
            $new_spec = array();
            if (!empty($item['spec_array'])) {
                $spec_array = explode(',', $item['spec_array']);

                foreach ($spec_array as $v1) {
                    $temp = explode(':', $v1);

                    // 取图片
                    $data[$k]['picture'] = D('goods_color')->where(array('goods_id' => $item['goods_id'], 'title' => $temp[1]))->getField('picture_list');

                    $new_spec[] =  $temp;
                }
            }


            $data[$k]['goods_name'] = $good['goods_name'];
            $data[$k]['spec_array'] = $new_spec;
            $data[$k]['price'] = $item['price'];

            $data[$k]['goods_id'] = $v['goods_id'];
            $data[$k]['item_id'] = $v['item_id'];
            $data[$k]['num'] = $v['num'];
        }

        return $data;
    }

}