<?php

namespace Inters\Model;

use Think\Model;

class GoodsCatModel extends Model {

    /**
     *  获取分类
     */
    public function getListTree()
    {
        $data = array();
        $treeList = $this->getTopSel();
        foreach ($treeList as $k => $v) {
            $subData = $this->getSub($v['cat_id']);
            $v['sub'] = $subData;
            $v['host'] = C('HOST');
            $data[] = $v;
        }
        return $data;
    }

    /**
     * 获取子类列表
     * @param $cat_id
     * @param int $level
     * @return mixed
     */
    public function getSub($cat_id, $level = 2) {
        $arr = $this->field('cat_id,cat_name,ico')->where(Array('parent_id' => $cat_id, 'cat_level' => Array('elt', $level), 'is_enable' => 1))->order('order_id asc')->select();
        return $arr;
    }

    
    /**
     * 获取顶级类别
     */
    public function getTopSel() {
        return $this->field('cat_id,cat_name,ico')->where(Array('parent_id' => 0, 'is_enable' => 1))->order('order_id asc')->select();
    }


    /**
     * 获取搜索分类名称
     * @param $key 关键词
     * @return array
     */
    public function getCatSearchName($key)
    {
        $list = array();
        if (!empty($key)) {
            $list = $this->field('cat_id, cat_name, cat_level')
                        ->where(array("cat_name" => array('like', $key ."%")))
                        ->where(array('is_menu' => 1))
                        ->where(array('is_enable' => 1))
                        ->select();
        }

        return $list;
    }

    /**
     * 增加搜索次数
     * @param $catid 分类id
     */
    public function setCatSearchNum($catId)
    {
        $this->where(array('cat_id' => $catId))->setInc("search_num",1);

    }

    /**
     * 搜索关键词列表
     * @param $limit 关键词
     * @return array
     */
    public function getListSearch($limit)
    {
        return  $this->where(array('is_menu' => 1))
                     ->where(array('is_enable' => 1))
                     ->order('search_num desc')
                     ->field('cat_id, cat_name, cat_level')
                     ->limit($limit)
                     ->select();
    }
}
