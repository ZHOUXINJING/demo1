<?php

namespace Inters\Model;

use Think\Model;

class GoodsModel extends Model
{
    private $order_limit = 20;

    /**
     * 商品信息
     * @param $goodId
     * @return mixed
     */
    public function getInformation($goodId)
    {
        $data = $this->where(array('goods_id' => $goodId))->field('goods_id,goods_name,cuxiao_msg,count_sale,ex_share,price,picture_list,detail')->find();
        $this->getPictureList($data);
        $this->getDetail($data);

        return $data;
    }

    /**
     * 商品推荐
     * @param $goodId
     */
    public function getGoodRecommend($goodId)
    {
        $type = $this->where(array('goods_id' => $goodId))->getField('cat_ids');

        return  $this->where(array('cat_ids' => $type, 'status' => 1, 'is_index' => 1,))->field('price, goods_name, picture, goods_id')->limit(3)->select();
    }


    /**
     *  切割商品图片
     * @param $data
     */
    public function getPictureList(&$data)
    {
        $data['picture_list'] =explode(',', $data['picture_list']);
    }

    /**
     *  商品详情图
     * @param $data
     */
    public function getDetail(&$data)
    {
        preg_match_all ('/<img src="(.*)" alt="" \/>/U', htmlspecialchars_decode($data['detail']), $pat_array);
        $data['detail'] = $pat_array[1];
    }

    /**
     *  商品类别
     */
    public function getSpecArray($id)
    {
        $spec =  $this->where(array('goods_id' => $id))->getField('spec_array');
        $specArray = json_decode($spec,true);

        $data = array();
        foreach ($specArray as $k => $v) {
            $v['sub'] = explode('|', $v['val']);
            $data[] = $v;
        }
        return $data;
    }

    /**
     * 获取商品默认的item_id
     */
    public function get_default_item_id($goods_id) {
        $MGoodsItem = M('goods_item');
        $item_id =  $MGoodsItem->where('goods_id='.$goods_id)->limit(1)->getField('item_id');
        return $item_id;
    }


    /**
     * 商品搜索排序
     * @param $sort 排序
     * @param $key 搜索关键词
     * @param $page 分页
     * @param $type 来源
     */
    public function get_list_order($sort, $key, $page, $type)
    {
        // 商品查询
        if ($type == 1) {
            $map['cat_ids'] = array("eq", ','.$key.',');

            // 增加搜索次数
            $goodCat = new GoodsCatModel();
            $goodCat->setCatSearchNum($key);
        } else {
            $map['goods_name'] = array("like", '%'.$key.'%');
        }

        $map['status'] = array('eq', 1);

        switch ($sort) {
            case 0:
                $order = "order_id desc";
                break;
            case 1:
                $order = "price asc";
                break;
            case 2:
                $order = "price desc";
                break;
            case 3:
                $order = "count_sale asc";
                break;
            case 4:
                $order = "count_sale desc";
                break;
            case 5:
                $order = "time_create desc";
                break;
        }

       $list = $this->where($map)->field("goods_id,goods_name,cuxiao_msg,price,picture,count_sale")->order($order)->page($page)->limit($this->order_limit)->select();

       return $list;
    }
}