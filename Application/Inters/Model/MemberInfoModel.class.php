<?php
/**
 * Created by PhpStorm.
 * User: Aaron
 * Date: 2019/7/23
 * Time: 13:02
 */
namespace Inters\Model;

use Think\Model;

class MemberInfoModel extends Model
{
    /**
     *  获取信息
     */
    public function getMemberInfo($id)
    {
        return $this->where(array('member_id' => $id))->field('sex, descript')->find();
    }
}