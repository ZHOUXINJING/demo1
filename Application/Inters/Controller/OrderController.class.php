<?php

namespace Inters\Controller;

use \Fwadmin\Model\OrderModel;

class OrderController extends BaseController
{
    // 鉴权
    private $auth = array('MemberOrderList');

    public function __construct()
    {
        parent::__construct();
        if(in_array(ACTION_NAME, $this->auth)) {
            $bool = $this->requestAuth();
            if (!$bool){
                echo $this->_json(9006, array(), '授权错误'); exit;
            }
            $this->member_id = $bool;
        }
    }

    /**
     *  用户订单列表
     *  status_mark 0 全部,  1 待支付  2 待审核  3 待发货  4 待收货
     */
    public function MemberOrderList()
    {
        $map['status_mark'] = I('get.status_mark', 0);

        $OrderObj = new OrderModel();

        $results = $OrderObj->search_member($this->member_id, $map);
        $lists = $results['list'];
        foreach($lists as $k => $v) {
            $lists[$k]['status_str'] = $OrderObj->show_status_member($v['status'], $v['status_pay'], $v['status_delivery'],$v['pay_type']);//订单状态
            $lists[$k]['total_show'] = $v['total'] - $v['total_discount']; //显示价格
            $lists[$k]['pay_type_str'] = $OrderObj->get_pay_type($v['pay_type']); //支付方式
            $goods_array = $OrderObj->show_goods_pic($v['order_id']); //订单图片数组
            $lists[$k]['cart_num'] = count($goods_array);
            $lists[$k]['host'] = C('HOST');
            $lists[$k]['goods'] =  $goods_array ;
        }

        echo $this->_json(200, $lists);
    }
}