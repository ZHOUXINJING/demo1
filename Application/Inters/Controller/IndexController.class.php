<?php
/**
 * Created by PhpStorm.
 * User: Aaron
 * Date: 2019/7/18
 * Time: 15:09
 */
namespace Inters\Controller;

use Inters\Model\ConBannerModel;
use Inters\Model\GoodsCatModel;
use Inters\Model\GoodsModel;
use Inters\Model\IndexBlockModel;

class IndexController extends BaseController
{
    /**
     *  首页
     */
    public function home()
    {
        $cbm = new ConBannerModel();
        $ibm = new IndexBlockModel();

        $result = $ibm->getList();

        $data = array();
        $data['banner'] = $cbm->getList();
        foreach ($result as $k => $v) {
            switch ($v['block_name']) {
                case '新品首发 · 探索新风格':
                    $data['oneBlock'] = $v;
                    break;
                case '人气推荐':
                    $data['twoBlock'] = $v;
                    break;
                case '专题精选':
                    $data['threeBlock'] = $v;
                    break;
                case '为你推荐':
                    $data['four'] = $v;

            }
        }

        echo $this->_json(200, $data, "成功");
    }


    /**
     *  默认搜索   option 0:综合  1:低价格  2:高价格  3:低销量  4:高销量  5:新品
     *            type 1:分类名    0: 商品
     */
    public function searchDefault()
    {
        $key = I("get.key");
        $sort = I("get.sort", 0);
        $page = I("get.page", 1);
        $type = I("get.type", 0);


        $goods = new GoodsModel();
        $list = $goods->get_list_order($sort, $key, $page, $type);

        echo $this->_json(200, $list == null ? array() : $list, C('HOST'));
    }


    /**
     * 搜索列表
     */
    public function searchList()
    {
        $goodCat = new GoodsCatModel();
        $list = $goodCat->getListSearch(8);

        echo $this->_json(200, $list, "成功");
    }
}