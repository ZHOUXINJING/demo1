<?php

namespace Inters\Controller;

use Inters\Model\GoodsModel;
use Inters\Model\GoodsCommentModel;
use Inters\Model\MemberFavModel;
use Inters\Model\TempGoodsModel;

class GoodController extends BaseController
{
    private $auth = array('userStoreGood', 'addGoodsStore', 'clearGoodStore', 'getGoodStore');

    public function __construct()
    {
        parent::__construct();
        if(in_array(ACTION_NAME, $this->auth)) {
            $bool = $this->requestAuth();
            if (!$bool){
                echo $this->_json(9006, array(), '授权错误'); exit;
            }
            $this->member_id = $bool;
        }
    }

    /**
     *  商品详情
     */
    public function description()
    {
        $goodId = I('get.goodId');

        $gm = new GoodsModel();
        $goodData = $gm->getInformation($goodId);
        $goodRecommend = $gm->getGoodRecommend($goodId);

        $gcm  = new GoodsCommentModel();
        $goodCommentCount = $gcm->getCommentCount($goodId);

        // 是否收藏
        $this->tokenShiftMemberId();
        $isStore = D('MemberFav')->where(array('goods_id' => $goodId, 'member_id' => $this->member_id))->find() === NULL ? false : true;
        $current_url = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

        $data = array_merge($goodData, $goodCommentCount, array('recommend' => $goodRecommend), array('host' => C('host'),'current_url' => $current_url ,'customerUrl' => C('CUSTOMER'),'isStore' => $isStore, 'login' =>$this->member_id));

        echo $this->_json(200, $data); exit;
    }

    /**
     *  商品评价
     */
    public function getGoodsComment()
    {
        $param = I("get.");

        $gcm  = new GoodsCommentModel();
        $data = $gcm->getCommentList($param);

        echo $this->_json(200, $data); exit;
    }



    /**
     *  用户收藏, 取消收藏
     */
    public function userStoreGood()
    {
        $goodId = I('get.goodsId');      // 商品id
        $itemId = I('get.itemId');      // 款式id

        $mfm = new MemberFavModel();
        $bool = $mfm->setGoodStore($this->member_id, $goodId, $itemId);
        if ($bool) {
            D('goods')->where(array('goods_id' => $goodId))->setInc('count_fav');
            echo $this->_json(200, array('bool' => 1),'收藏成功'); exit;
        }else {
            D('goods')->where(array('goods_id' => $goodId))->setDec('count_fav');
            echo $this->_json(200, array('bool' => 0),'取消收藏'); exit;
        }
    }


    /**
     *  显示购物车规格
     *  颜色 和 款式会有图片展示
     */
    public function showSpec()
    {
        $id = I('get.goodId');
        $gm = new GoodsModel();
        $specArray = $gm->getSpecArray($id);

        // 查找图片
        $data= array();
        foreach ($specArray as $k => $v) {
            if ($v['name'] == '颜色' || $v['name'] == '款式') {
                $imageArray = D('goods_color')->where(array('goods_id' => $id))->select();
                foreach ($v['sub'] as $k1 => $v1) {
                    foreach ($imageArray as $k2 => $v2) {
                        if ($v2['title'] == $v1) {
                            $v['picture_list'][] = $v2['picture_list'];
                        }
                    }
                }
            }
            $data['spec'][] = $v;
        }

        // 是否代理, 赚多少钱
        $data['money'] = '0.2';
        $data['is_agent'] = true;


        echo $this->_json(200, $data);
    }


    /**
     *  规格库存 和 价格
     *  克重:30g,款式:金鱼款
     */
    public function showSpecPrice()
    {
        $specString = I('get.specString');
        $id  = I('get.goodId');

        $data = D('Goods_item')->where(array('spec_array' => $specString, 'goods_id' => $id))->field('item_id, price, store_num')->find();

        echo $this->_json(200, $data);
    }


    /**
     *  加入购物车
     * @param $goods_id 商品id
     * @param $item_id 规格id   不传 或者 传0 取默认
     * @param $num 数量
     */
    public function addGoodsStore()
    {
        $tgm = new TempGoodsModel($this->member_id);
        $bool = $tgm->add_cart(I('get.goods_id'), I('get.item_id'), I('get.num'));

        if ($bool == -1) {
            echo $this->_json(300, array(), '参数有误');
        } else {
            echo $this->_json(200, array(), '成功');
        }
    }


    /**
     *  获取购物车商品数据
     */
    public function getGoodStore()
    {
        $tgm = new TempGoodsModel($this->member_id);
        $data['temp'] = $tgm->get_store_data();
        $data['host'] = C('HOST');

        echo $this->_json(200, $data, '成功');
    }



    /**
     *  删除购物车商品
     */
    public function clearGoodStore()
    {
        $tgm = new TempGoodsModel($this->member_id);
        $tgm->clear_data(I('get.item_list'));

    }
}