<?php

namespace Inters\Controller;

use Inters\Model\GoodsCatModel;
use Inters\Model\ConfigModel;

class TypeController extends BaseController
{
    /**
     *  分类
     */
    public function treeList()
    {
        $gcm = new GoodsCatModel();
        $treeList = $gcm->getListTree();

        echo $this->_json(200, $treeList, md5(json_encode($treeList)));
    }


}