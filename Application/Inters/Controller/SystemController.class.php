<?php

namespace Inters\Controller;

use Inters\Model\ConfigModel;

class SystemController extends BaseController
{
    /**
     *  网站协议
     */
    public function getAgreement()
    {
        $cm = new ConfigModel();
        $data = $cm->where(array('k' => 'editorValue'))->getField('v');
        echo $this->_json(200, htmlspecialchars_decode($data));
    }


    /**
     * 二维码下载
     */
    public function codeDownload()
    {
        echo "下载";
    }


    /**
     * 二维码跳转注册
     */
    public function codeSkip()
    {
        if (IS_POST) {
            $post   = I('post.');
            $member = new \Phone\Model\MemberModel();

            // 验证码
            if (session('phonecode') != $post['code'] OR session('register_phone') != $post['phone']) {
                echo 1; exit;
            }

            // 是否注册
            if($member->where(array('phone' => $post['phone']))->find()) {
                echo 2; exit;
            }

            // 注册
            $member_id = $member->register($post['phone'], rand_username() ,$post['password']);
            if ($member_id) {
                echo 0; exit;
            }
        }

        $this->display("/Public/login");
    }


    /**
     * 发送验证码
     */
    public function sendMsg()
    {
        $code= rand(100000,999999);
        $phone = I('phone');

        $reData['status'] = 0;
        $reData['info'] = '发送成功!';

        $MemberObj = new \Phone\Model\MemberModel();
        if($phone != '' && $MemberObj->exist_phone($phone, 0) > 0) {
            $result['title'] = 'false' ;
            $this->error('你已是我们网站的会员了！');
        }

        $register_phone = S('register_phone'.$phone);
        if($register_phone){
            $result['title'] = 'false';
            $this->error('请不要重复发送验证码');

        }else{

            S('register_phone'.$phone, $phone, 60);
            D('Qcloudsms', 'Service')->sendMsg($phone, $code);
        }

        if($reData['status'] == 0 ) {
            session('phonecode', $code);
            session('register_phone', $phone);

            $this->success($reData['info']);
        }else{

            $this->error($reData['info']);
        }
    }

}

