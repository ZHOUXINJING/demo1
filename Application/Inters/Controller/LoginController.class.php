<?php

namespace Inters\Controller;

use Inters\Model\MemberModel;
use Inters\Tool\VerificationPhone;
use Phone\Service\QcloudsmsService;

class LoginController extends BaseController
{
    private $auth = array();

    public function __construct()
    {
        parent::__construct();
        if(in_array(ACTION_NAME, $this->auth)) {
            $bool = $this->requestAuth();
            if (!$bool){
                echo $this->_json(9006, array(), '授权错误'); exit;
            }
            $this->member_id = $bool;
        }
    }

    /**
     *  微信授权登录
     */
    public function weChatLogin()
    {

        $mm = new MemberModel();
        $openId = I('post.openId');

        $data = $mm->where(array('login_weixin' => $openId))->find();

        if (empty($data)) {
            echo $this->_json(1001, array(),'绑定手机号'); exit;
        }

        //当前用户存在
        echo $this->_json(200, array('token' => $this->create_token($data['member_id'])));

    }

    /**
     *  绑定手机号发生短信
     */
    public function bindPhoneMsg()
    {
        $phone = I('post.phone');

        if (S('phone')) {
            echo $this->_json(300, array(),'验证码已发送');exit;
        }

        $phoneData = VerificationPhone::createCheck($phone, 60 * 1);

        $cloud  = new QcloudsmsService();
        $result = $cloud->sendMsg($phone, $phoneData['code']);
        if ($result['result'] != 0) {
            echo $this->_json(300, array(), '服务器繁忙！请稍后再试'); exit;
        } else {
            S('phone', $phone, 60 * 1);
        }

        echo $this->_json(200, array('secret' => $phoneData['secret']), '成功'); exit;
    }

    /**
     * 绑定手机号
     */
    public function bindPhone()
    {
        $code   = I('post.code');
        $secret = I('post.secret');
        $openId = I('post.openId');
        $avatarUrl = I('post.avatarUrl');
        $nickName = I('post.nickName');

        $phone = VerificationPhone::getCheck($code, $secret);
        if ($phone == false) {
            echo $this->_json(300, array(), '验证码有误'); exit;
        }

        $mm = new MemberModel();

        $data = $mm->where(array('phone' => $phone))->find();
        if (!empty($data)) {
            $mm->where(array('phone' => $phone))->save(array('login_weixin' => $openId));
            echo $this->_json(200, array('token' => $this->create_token($data['member_id'])));exit;
        }

        $id = $mm->createUser($phone, 123456,$openId, $nickName, $avatarUrl);
        echo $this->_json(200, array('token' => $this->create_token($id)));
    }



    /**
     *  账号密码登录
     */
    public function passLogin()
    {
        $mm = new MemberModel();
        $phone = I('post.phone');
        $password = I('post.password');

        $data = $mm->where(array('phone' => $phone))->find();
        if (empty($data)) {
            echo $this->_json(300, array(), '账户未注册'); exit;
        }

        if ($data['password'] != strongmd5($password)) {
            echo $this->_json(300, array(), '密码错误'); exit;
        }

        if ($data['status'] == 2 || $data['status'] == 3) {
            echo $this->_json(300, array(), '账号已禁用'); exit;
        }

        echo $this->_json(200, array('token' => $this->create_token($data['member_id'])));
    }


    /**
     *  手机快捷登录
     */
    public function phoneLogin()
    {
        $mm = new MemberModel();
        $phone = I('post.phone');

        $data = $mm->where(array('phone' => $phone))->find();
        if (empty($data)) {
            // 不存在则创建
            $mm->createUser($phone, 123456);
        }
        if ($data['status'] == 2 || $data['status'] == 3) {
            echo $this->_json(300, array(), '账号已禁用'); exit;
        }
        if (S('phone')) {
            echo $this->_json(301, array(),'验证码已发送');exit;
        }

        $phoneData = VerificationPhone::createCheck($phone, 60 * 1);

        $cloud  = new QcloudsmsService();
        $result = $cloud->sendMsg($phone, $phoneData['code']);
        if ($result['result'] != 0) {
            echo $this->_json(301, array(), '服务器繁忙！请稍后再试'); exit;
        } else {
            S('phone', $phone, 60 * 1);
        }

        echo $this->_json(200, array('secret' => $phoneData['secret']), '成功'); exit;
    }


    /**
     *  校验手机号
     */
    public function verificationCode()
    {
        $code   = I('post.code');
        $secret = I('post.secret');

        $phone = VerificationPhone::getCheck($code, $secret);
        if ($phone == false) {
            echo $this->_json(301, array(), '验证码有误'); exit;
        }

        $mm = new MemberModel();
        $id = $mm->where(array('phone' => $phone))->getField('member_id');

        echo $this->_json(200, array('token' => $this->create_token($id)));
    }

    /**
     * 验证token
     */
    public function verificationToken()
    {
        if (!$this->requestAuth()) {
            echo $this->_json(300, array(), 'Token有误'); exit;
        }
        echo $this->_json(200, array(), '正确'); exit;
    }

}