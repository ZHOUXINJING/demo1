<?php
/**
 * Created by PhpStorm.
 * User: Aaron
 * Date: 2019/7/23
 * Time: 13:03
 */
namespace Inters\Controller;

use Inters\Model\MemberAddressModel;
use Inters\Model\MemberModel;
use Inters\Model\MemberYhqModel;
use Inters\Model\MemberFavModel;
use Inters\Model\MemberInfoModel;
use Inters\Tool\Upload;
use Inters\Tool\VerificationPhone;
use Phone\Service\QcloudsmsService;

class MemberController extends BaseController
{
    // 鉴权
    private $auth = array('info', 'uploadOneHeadPic', 'editUsername', 'modifyPassword', '
                           sendPassword', 'addressList', 'addressEdit', 'addressDel', 'addressAdd',
                             'MyStore', 'MyIntegral', 'MyCoupon');

    public function __construct()
    {
        parent::__construct();
        if(in_array(ACTION_NAME, $this->auth)) {
            $bool = $this->requestAuth();
            if (!$bool){
                echo $this->_json(9006, array(), '授权错误'); exit;
            }
            $this->member_id = $bool;
        }
    }

    /**
     *  个人资料
     */
    public function info()
    {
        $member = new MemberModel();
        $memberYhq = new MemberYhqModel();
        $memberfav = new MemberFavModel();

        $info = $member->getInfo($this->member_id);
        $yhq = $memberYhq->getCouponCount($this->member_id);
        $store = $memberfav->getStoreCount($this->member_id);

        $mi = new MemberInfoModel();
        $miData = $mi->getMemberInfo($this->member_id);
		
		$info['headpic'] = 'http://www.7jbbk.com'. $info['headpic'];

        $data = array_merge($info, array('coupon' => $yhq), array('store' => $store), array('host' => C('host')), $miData == null ? array() : $miData);

        $data['sign'] = md5(json_encode($data));

        echo $this->_json(200, $data,'成功');
    }

    /**
     *  上传图片
     */
    public function uploadOneHeadPic()
    {
        $resource = Upload::uploadOneImage();
        if (!$resource['status']) {
            echo $this->_json('300', array(), $resource['message']);exit;
        }

        $mm = new MemberModel();
        $mm->where(array('member_id' => $this->member_id))->save(array('headpic' => $resource['filename']));

        echo $this->_json(200, array('host' => C('HOST'), 'filename' => 'http://www.7jbbk.com'.$resource['filename']),'成功');
    }

    /**
     *  修改个人资料
     */
    public function editUsername()
    {
        if (IS_POST) {
            $post = I('post.');
            if(strlen($post['descript']) > 300){
                echo $this->_json(300, array(), '个人简介已超出'); exit;
            }

            $mm = new MemberModel();
            $mm->where(array('member_id' => $this->member_id))->save(array('nickname' => $post['nickname']));

            $mim = new MemberInfoModel();
            $data = array('descript' => $post['descript'], 'sex' => $post['sex']);
            $bool = $mim->where(array('member_id' => $this->member_id))->save($data);
            if (!$bool) {
                $data['member_id'] = $this->member_id;
                $mim->data($data)->add();
            }
            echo $this->_json(200, array(), '成功'); exit;
        }

        $mm = new MemberModel();
        $mmData = $mm->getInfo($this->member_id);

        $mi = new MemberInfoModel();
        $miData = $mi->getMemberInfo($this->member_id);

        echo $this->_json(200, array_merge($mmData, $miData), '成功');
    }

    /**
     * 修改密码时短信发送
     */
    public function sendPassword()
    {
        $phone = I('get.phone');
        if (empty($phone) || !phoneVerify($phone)) {
            echo $this->_json(300, array(), '手机号格式有误'); exit;
        }

        $mm = new MemberModel();
        if (!$mm->verifyPhone($this->member_id, $phone)) {
            echo $this->_json(300, array(), '请填写注册时手机号'); exit;
        }

        $code = VerificationPhone::__createCode(6);
        $cloud  = new QcloudsmsService();
        $result = $cloud->sendMsg($phone, $code);
        if ($result['result'] != 0) {
            echo $this->_json(300, array(), '短信发送失败'); exit;
        } else {
            S($phone, $code, 60 * 1);
        }

        echo $this->_json(200, array(), '验证码已发送'); exit;
    }


    /**
     * 账号密码修改
     */
    public function modifyPassword()
    {
        $password = I("post.password");
        $code = I("post.code");

        if (empty($password)) {
            echo $this->_json(300, array(), '请输入密码'); exit;
        }

        $member = new MemberModel();
        $phone = $member->getInfo($this->member_id)['phone'];
        $old_code = S($phone);
        if (!$old_code || $code != $old_code) {
            echo $this->_json(300, array(), '验证码错误'); exit;
        }

        $member->where(array('member_id' => $this->member_id))->save(array('password' => strongmd5($password)));
        S($phone, NULL);

        echo $this->_json(200, array(), '密码修改成功'); exit;
    }

    /**
     * 收货地址列表
     */
    public function addressList()
    {
        $memberAdd = new MemberAddressModel();
        $list = $memberAdd->getAddressList($this->member_id);
        echo $this->_json(200, $list, '成功'); exit;
    }

    /**
     * 收货地址修改
     */
    public function addressEdit()
    {
        $memberAdd = new MemberAddressModel();
        if (IS_GET) {
            $list = $memberAdd->addressInfo(I('get.id'));
            echo $this->_json(200, $list, '成功'); exit;
        }

        $data = I('post.');
        $memberAdd = new MemberAddressModel();
        $memberAdd->addressEdit($data, $this->member_id);

        echo $this->_json(200, array(), '成功'); exit;
    }

    /**
     * 收货地址删除
     */
    public function addressDel()
    {
        $id = I('get.id');

        $memberAdd = new MemberAddressModel();
        $memberAdd->addressDel($id);

        echo $this->_json(200, array(), '成功'); exit;
    }

    /**
     * 收货地址增加
     */
    public function addressAdd()
    {
        $post = I('post.');

        $memberAdd = new MemberAddressModel();
        $memberAdd->addressAdd($post, $this->member_id);

        echo $this->_json(200, array(), '成功'); exit;
    }

    /**
     * 获取临时token
     */
    public function tempToken()
    {
        $id = I('get.id');
        echo $this->create_token($id);
    }

    /**
     *  我的收藏
     */
    public function MyStore()
    {
        $FavObj = new \Phone\Model\MemberFavModel();
        $result = $FavObj->search_member($this->member_id,15, I('get.page', 0));
        $list = $result['list'];

        foreach($list as $k => $v) {
            $list[$k]['picture'] = 'http://www.7jbbk.com'.$list[$k]['picture'];
        }

        echo $this->_json(200, $list == null ? array() : $list, '成功'); exit;
    }

    /**
     *  优惠劵
     */
    public function MyCoupon()
    {
        $MemberYhqObj = new \Phone\Model\MemberYhqModel();
        $conditions = array('end_time' => array('gt',date('Y-m-d H:i:s', time())));
        $results = $MemberYhqObj->search($this->member_id, $conditions,'',15, I('get.page', 0));

        $lists = $results['list'];
        foreach($lists as $k =>$v){
            $lists[$k]['nowtime'] = date('Y-m-d 00:00:00');
        }

        echo $this->_json(200, $lists == null ? array() : $lists, '成功'); exit;
    }

    /**
     *  我的积分
     */
    public function MyIntegral()
    {
        $ModelObj = new \Fwadmin\Model\MemberPointModel();
        $MemberObj = new \Phone\Model\MemberModel();
        $point = $MemberObj->get_point($this->member_id);

        $conditions = array();
        $results = $ModelObj->search_member($this->member_id, $conditions, 'id desc',15, I('get.page', 0));
        $lists = $results['list'];

        foreach ($lists as $key=>$item){

            if($item['point_change']>0){
                $item['point_change'] = '+'.$item['point_change'];
                $item['create_time'] =  date('Y-m-d H:i:s', $item['create_time']);
                $lists[$key] =  $item;
            }

        }

        echo $this->_json(200, array('point' => $point, 'list' => $lists == null ? array() : $lists), '成功'); exit;
    }

}