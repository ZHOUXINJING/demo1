<?php

namespace Home\Controller;

use Think\Controller;
use Common\Controller\HomeController;

class PayAlipayController extends HomeController {

    /**
     * 初始化，调用支付宝核心类库
     */
    public function _initialize() {
        vendor('Alipay.corefunction');
        vendor('Alipay.md5function');
        vendor('Alipay.notify');
        vendor('Alipay.submit');
    }
    
    /**
     * 支付
     */
    public function pay($order_id) {
        $OrderObj = new \Fwadmin\Model\OrderModel();
        $order = $OrderObj->field('order_no,total,total_discount,total_pay,total_pay_point')->find($order_id);
        if(empty($order)) {
            echo '无此订单';
            exit();
        }
        if($order['status_pay'] == 3) {
            echo '订单已支付';
            exit();
        }
        $total_need_pay = $order['total'] - $order['total_discount'] - $order['total_pay'] - $order['total_pay_money'] - $order['total_pay_point'];
		$total_need_pay = abs(round($total_need_pay,2));
		
       
        //配置文件
        $alipay_config = C('alipay_config');
        /*************************请求参数************************* */
        $payment_type = "1"; //支付类型 //必填，不能修改
        $notify_url = $alipay_config['notify_url']; //服务器异步通知页面路径
        $return_url = $alipay_config['return_url']; //页面跳转同步通知页面路径
        $seller_email = $alipay_config['seller_email']; //卖家支付宝帐户必填
        $out_trade_no = $order['order_no']; //商户订单号 通过支付页面的表单进行传递，注意要唯一！
        $subject = '订单'.$order['order_no'];  //订单名称 //必填 通过支付页面的表单进行传递
        $total_fee = $total_need_pay;   //付款金额  //必填 通过支付页面的表单进行传递
        $body = '';  //订单描述 通过支付页面的表单进行传递
        $show_url = '';  //商品展示地址 通过支付页面的表单进行传递
        $anti_phishing_key = ""; //防钓鱼时间戳 //若要使用请调用类文件submit中的query_timestamp函数
        $exter_invoke_ip = get_client_ip(); //客户端的IP地址 
        /********************************************************** */


        //构造要请求的参数数组，无需改动
        $parameter = array(
            "service" => "create_direct_pay_by_user",
            "partner" => trim($alipay_config['partner']),
            "payment_type" => $payment_type,
            "notify_url" => $notify_url,
            "return_url" => $return_url,
            "seller_email" => $seller_email,
            "out_trade_no" => $out_trade_no,
            "subject" => $subject,
            "total_fee" => $total_fee,
            "body" => $body,
            "show_url" => $show_url,
            "anti_phishing_key" => $anti_phishing_key,
            "exter_invoke_ip" => $exter_invoke_ip,
            "_input_charset" => trim(strtolower($alipay_config['input_charset']))
        );
        //建立请求
        $alipaySubmit = new \Vendor\Alipay\AlipaySubmit($alipay_config);
        $html_text = $alipaySubmit->buildRequestForm($parameter, "post", "确认");
        echo $html_text;
    }

    /*     * ****************************
      服务器异步通知页面方法
      其实这里就是将notify_url.php文件中的代码复制过来进行处理

     * ***************************** */
    function notifyurl() {
        $alipay_config = C('alipay_config');
        //计算得出通知验证结果
        $alipayNotify = new \Vendor\Alipay\AlipayNotify($alipay_config);
        $verify_result = $alipayNotify->verifyNotify();
        if ($verify_result) {
            //验证成功
            //获取支付宝的通知返回参数，可参考技术文档中服务器异步通知参数列表
            $out_trade_no = $_POST['out_trade_no'];      //商户订单号
            $trade_no = $_POST['trade_no'];          //支付宝交易号
            $trade_status = $_POST['trade_status'];      //交易状态
            $total_fee = $_POST['total_fee'];         //交易金额
            $notify_id = $_POST['notify_id'];         //通知校验ID。
            $notify_time = $_POST['notify_time'];       //通知的发送时间。格式为yyyy-MM-dd HH:mm:ss。
            $buyer_email = $_POST['buyer_email'];       //买家支付宝帐号；
            $parameter = array(
                "out_trade_no" => $out_trade_no, //商户订单编号；
                "trade_no" => $trade_no, //支付宝交易号；
                "total_fee" => $total_fee, //交易金额；
                "trade_status" => $trade_status, //交易状态
                "notify_id" => $notify_id, //通知校验ID。
                "notify_time" => $notify_time, //通知的发送时间。
                "buyer_email" => $buyer_email, //买家支付宝帐号；
            );
            if ($_POST['trade_status'] == 'TRADE_FINISHED') {
                //
            } else if ($_POST['trade_status'] == 'TRADE_SUCCESS') {
                //进行订单处理，并传送从支付宝返回的参数；
            
			$OrderObj = new \Fwadmin\Model\OrderModel();
			$out_trade_no = $parameter['out_trade_no'];
			$model = $OrderObj->where(Array('order_no'=>$out_trade_no))->find();
			if($model['status_pay']==3){
		      exit();//如果已经更新订单状态，就不用再继续运行下面的了
		   }
		        $order_id = $model['order_id'];
                $payment = '支付宝即时到账';
                $money = $parameter['total_fee'];
               
                $OrderObj->pay_3($order_id, $payment, $money);
            }
            echo "success";        //请不要修改或删除
        } else {
            //验证失败
            echo "fail";
        }
    }

    /*
      页面跳转处理方法；
      这里其实就是将return_url.php这个文件中的代码复制过来，进行处理；
     */

   function returnurl() {
		 $alipay_config = C('alipay_config');
		
        //计算得出通知验证结果
        $alipayNotify = new \Vendor\Alipay\AlipayNotify($alipay_config);
       // print_r($_GET);
		$verify_result = $alipayNotify->verifyReturn();
		
		//print_r($verify_result);
		//exit();
        if ($verify_result) {
            //验证成功
            //获取支付宝的通知返回参数，可参考技术文档中页面跳转同步通知参数列表
            $out_trade_no = $_GET['out_trade_no'];      //商户订单号
            $trade_no = $_GET['trade_no'];          //支付宝交易号
            $trade_status = $_GET['trade_status'];      //交易状态
            $total_fee = $_GET['total_fee'];         //交易金额
            $notify_id = $_GET['notify_id'];         //通知校验ID。
            $notify_time = $_GET['notify_time'];       //通知的发送时间。
            $buyer_email = $_GET['buyer_email'];       //买家支付宝帐号；

            $parameter = array(
                "out_trade_no" => $out_trade_no, //商户订单编号；
                "trade_no" => $trade_no, //支付宝交易号；
                "total_fee" => $total_fee, //交易金额；
                "trade_status" => $trade_status, //交易状态
                "notify_id" => $notify_id, //通知校验ID。
                "notify_time" => $notify_time, //通知的发送时间。
                "buyer_email" => $buyer_email, //买家支付宝帐号
            );

            if ($_GET['trade_status'] == 'TRADE_FINISHED' || $_GET['trade_status'] == 'TRADE_SUCCESS') {
				
                redirect($alipay_config['successpage']); //跳转到配置项中配置的支付成功页面；
				exit();
            } else {
                //echo "trade_status=" . $_GET['trade_status'];
                redirect($alipay_config['errorpage']); //跳转到配置项中配置的支付失败页面；
				exit();
            }
        } else {
            //验证失败
            //如要调试，请看alipay_notify.php页面的verifyReturn函数
            echo "支付失败！";
        }
    }

}
