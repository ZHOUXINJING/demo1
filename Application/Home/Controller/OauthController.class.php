<?php

namespace Home\Controller;

use Think\Controller;
use Home\Model\MemberModel;
use Common\Controller\HomeController;

class OauthController extends HomeController {

    
     
    public function login($type = null) {
        empty($type) && $this->error('参数错误');
        $sns = \Org\ThinkSDK\ThinkOauth::getInstance($type);
        redirect($sns->getRequestCodeURL());
    }

    public function callback($type = null, $code = null) {
        header("Content-type: text/html; charset=utf-8");
        (empty($type) || empty($code)) && $this->error('参数错误');
       
        $sns = \Org\ThinkSDK\ThinkOauth::getInstance($type);

        //腾讯微博需传递的额外参数
        $extend = null;
        if ($type == 'tencent') {
            $extend = array('openid' => $this->_get('openid'), 'openkey' => $this->_get('openkey'));
        }
        $tokenArr = $sns->getAccessToken($code, $extend);
        $openid = $tokenArr['openid'];
        $token = $tokenArr['access_token'];
        session("openid", $openid);
        session("access_token", $token);
//        $con = "openid:".$openid."\n"."token".$token;
//	file_put_contents("1.txt", $con);
        //获取当前登录用户信息
        if ($openid) {
            $MemberObj = new MemberModel();
            $field = strtolower($type);
            session("field", $field);
            if($field == 'qq') {
               $data_field = 'login_qq';
            }elseif($field == 'sina') {
                $data_field = 'login_weibo';
            }elseif($field == 'tencent') {
                $data_field = 'login_tencent';
            }
            $arr = $MemberObj->field('member_id,status')->where(array($data_field=> $openid))->find();

            if ($arr) { //若是有该账号就登录
                if ($arr['status'] != 1) {
                    $this->error('你的账号已被禁用！');
                }
                session('member_id', $arr['member_id']);
                $key_login_code = $arr['member_id'] . randomname(30);
                $arr['code_keep_login'] = $key_login_code;
                $MemberObj->save($arr);
                if ($remember == 1) {
                    $expire = 604800;
                    cookie('member_id', $arr['member_id'], $expire);
                    cookie('member_keep_login', $key_login_code, $expire);
                }
                //登录信息
                $LoginObj = new \Home\Model\MemberLoginModel();
                $LoginObj->add_login_record($arr['member_id']);
                $this->redirect("/");
            } else { //没有的话绑定
                $member_id = session('member_id');
                if ($member_id > 0) { //用户已登录，自动绑定
                    //绑定账号
                    $MemberObj->where("member_id = " . $member_id . "")->setField($data_field,$openid);
                    session("openid",null);
                    session("access_token", null);
                    session("field",null);
                    $this->redirect("Member/bangding");
                } else { //用户未登录，跳转到绑定页面
                    if ($filed == 'qq') { //针对新版qq互联在绑定页，要显示昵称，否则不通过***
                        $data = $sns->call('user/get_user_info');
                        $nickname = $data['nickname'];
                    } else {
                        $userinfo = A('Type', 'Event')->$type($tokenArr);
                        $nickname = $userinfo['name'];
                    }
                    session('nickname', $nickname);
                    
                    $member_id = $MemberObj->register_auto($data_field,$nickname,$openid);
                    session('member_id', $arr['member_id']);
                    $key_login_code = $arr['member_id'] . randomname(30);
                    $arr['code_keep_login'] = $key_login_code;
                    $MemberObj->save($arr);
                    if ($remember == 1) {
                        $expire = 604800;
                        cookie('member_id', $member_id, $expire);
                        cookie('member_keep_login', $key_login_code, $expire);
                    }
                    //登录信息
                    $LoginObj = new \Home\Model\MemberLoginModel();
                    $LoginObj->add_login_record($member_id);
                    
                    session("openid",null);
                    session("access_token", null);
                    session("field",null);
                    $this->redirect("/");
                    exit;
                }
            }
        } else {
            $this->error('系统出错;请稍后再试！');
        }
    }
     


}
