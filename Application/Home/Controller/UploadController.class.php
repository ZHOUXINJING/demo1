<?php
namespace Home\Controller;

use Think\Controller;
use Common\Controller\HomeController;
/**
 * 上传文件操作
 */
class UploadController extends HomeController {
    /**
     * 上传文件
     */
    public function uploadfile($path, $filetype) {
        $Model = D('Upload', 'Service');
        $Model->setconfig('savePath', '/Uploads/member/' . $path . '/');
        $info = $Model->upload($filetype);
        if (!$info) {
            $data['Message'] = $Model->getError();
            $data['Success'] = false;
        } else {
            foreach ($info as $file) {
                $filepath = $file['savepath'] . $file['savename'];
                $data['Success'] = true;
                $data['SaveName'] = $filepath;
                $thumbw = I('get.thumbw', '');
                $thumbh = I('get.thumbh', '');
                if ($thumbw != '' || $thumbh != '') {
                    $saveName = $Model->thumb($file, $thumbw, $thumbh);
                    $data['SaveName'] = $saveName;
                }
            }
        }
        $this->ajaxReturn($data);
    }

    /**
     * 删除文件
     */
    public function delfile($filename) {
        $Model = D('Upload', 'Service');
		$member_id = session('member_id');
		 $fileurl='/Uploads/member/tuihuan/'.$member_id.'/'.$filename;
		
        $result = $Model->del($fileurl);
        echo 1;
    }

   

}
