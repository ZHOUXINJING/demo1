<?php

namespace Home\Controller;

use Think\Controller;
use Common\Controller\HomeController;

class IndexController extends HomeController {

    //首页
    public function index() {
        
        //推荐品牌
        $GoodsBrandObj = new \Fwadmin\Model\GoodsBrandModel();
        $brand_tuijian = $GoodsBrandObj->get_tuijian();
        $this->brand_tuijian = $brand_tuijian;
        //echo md5('4008009385');
        //图片切换Banner
        $BannerObj = new \Home\Model\ConBannerModel();
        $this->banner = $BannerObj->get_list('index');
        
        //首页模块
        $IndexBlockObj = new \Home\Model\IndexBlockModel();
        $index_block = $IndexBlockObj->show_html();
		
        $this->index_block = $index_block;
        
        
        
        
        //友情链接
		/*
        $linkfriend = S('linkfriend');
        if (empty($linkfriend)) {
            $LinkObj = new \Fwadmin\Model\ConLinkModel();
            $linkfriend = $LinkObj->field('title,url')->where('is_show=1')->order('order_id asc')->select();
            S('linkfriend', $linkfriend);
        }
        $this->assign('linkfriend', $linkfriend);
        */
        $this->display();
    }
    
     //404页面
    function _empty() {
        header("HTTP/1.0 404 Not Found");
        $this->display('Index/404');
    }
    

}
