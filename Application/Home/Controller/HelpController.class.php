<?php

namespace Home\Controller;

use Think\Controller;
use Common\Controller\HomeController;

class HelpController extends HomeController {

    //帮助中心首页
    public function index($id = 0) {
        //获取左侧菜单
        $ConHelpObj = new \Home\Model\ConHelpModel();
        $left_menu = $ConHelpObj->get_left_menu();
        $this->left_menu = $left_menu;

        if ($id == 0 && !empty($left_menu)) {
           $id = 3;//$left_menu[0]['help_id'];         
        }
       
        $model = $ConHelpObj->find($id);
       

        if ($model['type'] == '资讯') {
            //内容
            $conditions['help_id'] = $id;
            $result = $ConHelpObj->search_news($conditions);
            $this->list = $result['list'];
            $this->page = $result['page'];
        } elseif ($model['type'] == '类别') {
            $id = $ConHelpObj->get_sub_id($id);
            $model = $ConHelpObj->find($id);
        }
        
        $this->model = $model;
        $this->id = $id;
        $this->seo=array('seo_title'=> $model['seo_title'],'seo_keywords'=>$model['seo_keywords'],'seo_description'=>$model['seo_description']);
        $this->display();
    }

    //新闻内容
    public function info($id) {
        //获取左侧菜单
        $ConHelpObj = new \Home\Model\ConHelpModel();
        $left_menu = $ConHelpObj->get_left_menu();
        $this->left_menu = $left_menu;

        //内容
        $NewsObj = M('con_news');
        $model = $NewsObj->find($id);
        $model['help_name'] = $ConHelpObj->get_help_name($model['help_id']);
        $this->model = $model;

        $this->display();
    }

    

}
