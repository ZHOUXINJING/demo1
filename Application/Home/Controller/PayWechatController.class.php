<?php

/**
 * @author Dream-Seeker
 * @copyright 2015 shiyichuangxiang.com
 */
namespace Home\Controller;
use Think\Controller;
use Common\Controller\HomeController;
class PayWechatController extends HomeController {

    /**
     * 初始化，调用支付宝核心类库
     */
    public function _initialize() {
        vendor('PayWechat.PayNotifyCallBack');
        vendor('PayWechat.Weixin');
    }
    


    //微信支付
    public function pay() {
		$order_id = I('get.order_id',0);
        $OrderObj = new \Fwadmin\Model\OrderModel();
	    $weixin_config = C('weixin_config');
        $order = $OrderObj->field('order_no,total,total_discount,total_pay,status_pay,total_pay_point')->find($order_id);
		
        if (empty($order)) {
            echo '无此订单';
            exit();
        }
        if ($order['status_pay'] == 3) {
            
			$this->redirect('Member/orderinfo',Array('id'=>$order_id));
            exit();
        }
         $total_need_pay = $order['total'] - $order['total_discount'] - $order['total_pay'] - $order['total_pay_money'] - $order['total_pay_point']- $order['total_pay_yhq'];
        $title = '订单编号：' . $order['order_no'];
        $this->total = $total_need_pay;
		$order_no = $order['order_no'].'_'.time();
        $this->order_no = $order['order_no'];
        $this->order_id = $order_id;
		$this->cart_list = M('order_goods')->field('goods_name,goods_spec')->where(Array('order_id'=>$order_id))->select();
        $this->success_page = $weixin_config['successpage'];//get_host().__ROOT__.'/Member/myorder?pay=1';
		
        $notify_url = $weixin_config['notify_url'];//get_host().__ROOT__.'/PayWechat/wxnotify';
        $params = array('body' => $title, 'money' => $total_need_pay, 'mch' => '1', 'out_trade_no' => $order_no,'notify_url'=>$notify_url);
        $code_url = \Vendor\PayWechat\Weixin::getCodeUrl($params);
        $this->assign('code_url', $code_url);
		 vendor("phpqrcode.phpqrcode");
		$data = $code_url;
	    $level = 'L';// 点的大小：1到10,用于手机端4就可以了
        $size = 8;// 下面注释了把二维码图片保存到本地的代码,如果要保存图片,用$fileName替换第二个参数false
        $fileName ='Uploads/qcode/'.time().'.png'; // 生成的文件名
		$QRcode = new \QRcode();
		if (empty($data)) {
		    log("支付提交参数有误");
        }
        $QRcode->png($data, $fileName, $level, $size);//生成二维码
        $this->qrcode =  __ROOT__.'/'.$fileName;//二维码路径
        $this->qcode_imgage = '<img src="'.__ROOT__.'/'.$fileName.'"  style="width:200px;height:200px;">';
		
        $this->display();
    }



    function wxnotify() {
        $log_name = "Public/paywechat/notify_url.log"; //log文件路径
        $this->log_result($log_name, "【接收到的notify开始】:\n签名开始\n");
        $notify = new \Vendor\PayWechat\PayNotifyCallBack();
        $xml = file_get_contents("php://input");
        $array = $notify->xmlToArray($xml);
        if ($array['result_code'] == 'SUCCESS' && $array['return_code'] == 'SUCCESS') {
            $OrderObj = new \Fwadmin\Model\OrderModel();
			$out_trade_no = $array['out_trade_no'];
			
			$out_trade_no2 = explode('_',$out_trade_no);//防止微信提示重复提交订单支付
		    $out_trade_no = $out_trade_no2[0];//订单号
			$model = $OrderObj->where(Array('order_no'=>$out_trade_no))->find();
			 if($model['status_pay']==3){
		      exit();//如果已经更新订单状态，就不用再继续运行下面的了
		   }
		     $order_id = $model['order_id'];
		     $payment='微信支付';
			 $money = $model['total'] - $model['total_discount'] - $model['total_pay'] - $model['total_pay_money'] - $model['total_pay_point']-$model['total_pay_yhq'];
			
             $OrderObj->pay_3($order_id, $payment, $money);
   
           // $data2['pay_time'] = date('Y-m-d H:i:s');
            //$data2['pay_status'] = 1;
            //$flag = $OrderObj->where("orderno='" . $out_trade_no . "'")->save($data2);
			$log_name = "Public/paywechat/".$out_trade_no."_".date('YmdHis').".log";
            $this->log_result($log_name, "[支付成功]");
			
			
        } else {
            $this->log_result($log_name, "【接收到的notify签名】:\n签名失败\n".$array['result_code']."|".$array['out_trade_no']);
        }

        //验证签名，并回应微信。
        //对后台通知交互时，如果微信收到商户的应答不是成功或超时，微信认为通知失败，
        //微信会通过一定的策略（如30分钟共8次）定期重新发起通知，
        //尽可能提高通知的成功率，但微信不保证通知最终能成功。

        /*
          $this->log_result($log_name,"【接收到的notify开始】:\n获得".$xml."\n");
          if($notify->CheckSign() == FALSE){
          $notify->setReturnParameter("return_code","FAIL");//返回状态码
          $this->log_result($log_name,"【接收到的notify签名】:\n签名失败\n");
          $notify->setReturnParameter("return_msg","签名失败");//返回信息
          }else{
          $notify->setReturnParameter("return_code","SUCCESS");//设置返回码
          }
          $returnXml = $notify->returnXml();
          echo $returnXml;
          //==商户根据实际情况设置相应的处理流程，此处仅作举例=======

          //以log文件形式记录回调信息
          //     	$log_ = new Log_();


          $this->log_result($log_name,"【接收到的notify通知】:\n".$xml."\n");

          if($notify->CheckSign() == TRUE)
          {
          if ($notify->data["return_code"] == "FAIL") {
          //此处应该更新一下订单状态，商户自行增删操作
          $this->log_result($log_name,"【通信出错】:\n".$xml."\n");
          }
          elseif($notify->data["result_code"] == "FAIL"){
          //此处应该更新一下订单状态，商户自行增删操作
          $this->log_result($log_name,"【业务出错】:\n".$xml."\n");
          }
          else{
          //此处应该更新一下订单状态，商户自行增删操作
          $OrderObj=new \Fwadmin\Model\OrderModel();
          $data2['pay_time'] = date('Y-m-d H:i:s');
          $data2['order_status']=6;
          $out_trade_no = $notify->data["out_trade_no"];
          $flag = $OrderObj->where("orderno='".$out_trade_no."'")->save($data2);
          $this->log_result($log_name,"【支付成功】:\n".$xml."\n");
          }
          }
         */
        //商户自行增加处理流程,
        //例如：更新订单状态
        //例如：数据库操作
        //例如：推送支付完成信息
        //$notify->Handle(false);
    }
	
	
	
	function hwu(){
      $order_id = I('get.order_id',0); 
	 $OrderObj = new \Fwadmin\Model\OrderModel();
     $model = $OrderObj->where(Array('order_id'=>$order_id))->find();   
     if(empty($model)){
	    $this->redirect('Member/index');
	  }
	  
      if ($model['status_pay'] == 3) {
           // echo '订单已支付';
		$this->redirect('Member/myorder');
        exit();
       }
		
	 $order_no = $model['order_no'].'_'.time();

    $total_need_pay = $order['total'] - $order['total_discount'] - $order['total_pay'] - $order['total_pay_money'] - $order['total_pay_point'];
    $title = '订单编号：' . $model['order_no'];
    $this->title = '订单编号：' . $model['order_no'];
    $this->order_no= $model['order_no']; 
    $this->order_id = $order_id;
    $this->success_page = C('WEB_URL').'/Member/myorder?pay=1';
    
    $headers[] = 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8';   
    $headers[] = 'Connection: Keep-Alive';   
    $headers[] = 'Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3';  
    $headers[] = 'Accept-Encoding: gzip, deflate';  
    $headers[] = 'User-Agent: Mozilla/5.0 (Windows NT 6.1; rv:22.0) Gecko/20100101 Firefox/22.0'; 

  $userip =$this->get_client_ip2(); //获得用户设备IP
  $appid = "wx818bc95d248b6817";//微信
  $mch_id = "1515599701";//微信官方的
  $key = "5AEB62F378C433C0B06D4241B18BF191";//自己设置的微信商家key

  $nonce_str=MD5($order_no);//随机字符串
  //dump($orderArr['orderno']);
  $total_fee = $total_need_pay*100; //金额
  $spbill_create_ip = $userip; //IP
  $notify_url = C('WEB_URL').'/PayWechat/wxnotify_h5'; //回调地址
  $trade_type = 'MWEB';//交易类型 具体看API 里面有详细介绍
  $body="百宝库商城订单支付";
  $scene_info ='{"h5_info":{"type":"Wap","wap_url":"'.C('WEB_URL').'","wap_name":"支付"}}';//场景信息 必要参数
  $signA ="appid=$appid&body=$body&mch_id=$mch_id&nonce_str=$nonce_str&notify_url=$notify_url&out_trade_no=$order_no&scene_info=$scene_info&spbill_create_ip=$spbill_create_ip&total_fee=$total_fee&trade_type=$trade_type";
  $strSignTmp = $signA."&key=$key"; //拼接字符串  注意顺序微信有个测试网址 顺序按照他的来 直接点下面的校正测试 包括下面XML  是否正确
  $sign = strtoupper(MD5($strSignTmp)); // MD5 后转换成大写
  $post_data="<xml><appid>$appid</appid><body>$body</body><mch_id>$mch_id</mch_id><nonce_str>$nonce_str</nonce_str><notify_url>$notify_url</notify_url><out_trade_no>$order_no</out_trade_no><scene_info>$scene_info</scene_info><spbill_create_ip>$spbill_create_ip</spbill_create_ip><total_fee>$total_fee</total_fee><trade_type>$trade_type</trade_type><sign>$sign</sign>
  </xml>";//拼接成XML 格式
 

  $url = "https://api.mch.weixin.qq.com/pay/unifiedorder";//微信传参地址
  $dataxml = $this->http_post($url,$post_data,$headers); 
  header("content-type:text/html;charset=utf-8");
  $objectxml = (array)simplexml_load_string($dataxml,'SimpleXMLElement',LIBXML_NOCDATA); //将微信返回的XML 转换成数组
  //print_r($objectxml);
  if($objectxml['return_code'] == 'SUCCESS'){
    $mweb_url= $objectxml['mweb_url'];
  // header("Location:$mweb_url");
    $this->mweb_url=$mweb_url;
  }else{
    $this->mweb_url=$this->success_page; 
  }
    
   $this->display(); 
}

 function wxnotify_h5(){
      $log_name = "Public/paywechat/notify_url.log"; //log文件路径
        $this->log_result($log_name, "【接收到的notify开始】:\n签名开始\n");
        $notify = new \Vendor\PayWechat\PayNotifyCallBack();
        $xml = file_get_contents("php://input");
        $array = $notify->xmlToArray($xml);
        if ($array['result_code'] == 'SUCCESS' && $array['return_code'] == 'SUCCESS') {
            $OrderObj = new \Fwadmin\Model\OrderModel();
			$out_trade_no = $array['out_trade_no'];
			
			$out_trade_no2 = explode('_',$out_trade_no);//防止微信提示重复提交订单支付
		    $out_trade_no = $out_trade_no2[0];//订单号
			$model = $OrderObj->where(Array('order_no'=>$out_trade_no))->find();
			 if($model['status_pay']==3){
		      exit();//如果已经更新订单状态，就不用再继续运行下面的了
		   }
		     $order_id = $model['order_id'];
		     $payment='H5微信';
			 $money = $model['total'] - $model['total_discount'] - $model['total_pay'] - $model['total_pay_money'] - $model['total_pay_point'];
			
             $OrderObj->pay_3($order_id, $payment, $money);
   
           // $data2['pay_time'] = date('Y-m-d H:i:s');
            //$data2['pay_status'] = 1;
            //$flag = $OrderObj->where("orderno='" . $out_trade_no . "'")->save($data2);
			$log_name = "Public/paywechat/".$out_trade_no."_".date('YmdHis').".log";
            $this->log_result($log_name, "[支付成功]");
			
			
        } else {
            $this->log_result($log_name, "【接收到的notify签名】:\n签名失败\n");
        }
	 
    }
	



  function http_post($url='',$post_data=array(),$header=array(),$timeout=30) {

  $ch = curl_init();  
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // 跳过证书检查  
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);  // 从证书中检查SSL加密算法是否存在  
  curl_setopt($ch, CURLOPT_URL, $url);  
  curl_setopt($ch, CURLOPT_HTTPHEADER, $header);  
  curl_setopt($ch, CURLOPT_POST, true);  
  curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);  
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);   
  curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);  

  $response = curl_exec($ch);  

  curl_close($ch);

  return $response;
  }   


  function get_client_ip2(){
    if(getenv('HTTP_CLIENT_IP') && strcasecmp(getenv('HTTP_CLIENT_IP'),'unknown')) {

        $ip = getenv('HTTP_CLIENT_IP');

    } elseif(getenv('HTTP_X_FORWARDED_FOR') && strcasecmp(getenv('HTTP_X_FORWARDED_FOR'),'unknown')) {

        $ip = getenv('HTTP_X_FORWARDED_FOR');

    } elseif(getenv('REMOTE_ADDR') && strcasecmp(getenv('REMOTE_ADDR'),'unknown')) {

        $ip = getenv('REMOTE_ADDR');

    } elseif(isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] && strcasecmp($_SERVER['REMOTE_ADDR'], 'unknown')) {

        $ip = $_SERVER['REMOTE_ADDR'];

    }

    return preg_match ( '/[\d\.]{7,15}/', $ip, $matches ) ? $matches [0] : '';
}
	
	
	

    public function xmltoarray() {
        $notify = new \Vendor\PayWechat\PayNotifyCallBack();
        $row = $notify->xmlToArray($xml);
    }

    public function wechat_pay($order_id) {
        $OrderObj = new \Fwadmin\Model\OrderModel();
        $status_pay = $OrderObj->where(array('order_id'=>$order_id))->getField('status_pay');
        if($status_pay == 3) {
            echo '1';
        }
    }

    function log_result($file, $word) {
        $fp4 = fopen($file, "r"); //只读打开模板,底部;
        $str = fread($fp4, filesize($file)); //读取模板内容
        $str = $str . "\n执行日期：" . date("Y-m-d H:i:s") . "\n" . $word . "\n\n";
        $handle = fopen($file, "w"); //不存在就创建;,创建页面
        fwrite($handle, $str);
    }
	

/******微信登录功能,手机扫码登录*****/
function login(){
$APPID= 'wx93ac7736b1e564e1';//AppID
$rand_code = 'M'.time().rand('100001','999999');//随机字符串
//登录后返回的地址
$REDIRECT_URI = C('WEB_URL').'/PayWechat/login_return?rand_code='.$rand_code;
//微信登录地址，也是二维码地址
$scope='snsapi_userinfo';//需要授权
$url='https://open.weixin.qq.com/connect/oauth2/authorize?appid='.$APPID.'&redirect_uri='.urlencode($REDIRECT_URI).'&response_type=code&scope='.$scope.'&state=1#wechat_redirect';//登录验证

       vendor("phpqrcode.phpqrcode");//调用二维码类
	    $level = 'L';// 点的大小：1到10,用于手机端4就可以了
        $size = 7;// 下面注释了把二维码图片保存到本地的代码,如果要保存图片,用$fileName替换第二个参数false
        $fileName ='Uploads/qcode/'.time().'.png'; // 生成的文件名
		$QRcode = new \QRcode();
        $QRcode->png($url, $fileName, $level, $size);//生成二维码
        $this->qrcode =  __ROOT__.'/'.$fileName;//二维码路径
		$this->rand_code = $rand_code;
		$this->CURRENT_URL = $CURRENT_URL; 
		$this->seo=array('seo_title'=>'微信登录','seo_keywords'=>'微信登录','seo_description'=>'微信登录');
		$this->display();
	  }
	
	
	
function login_status(){
	/****PC端的Session***/
if(IS_AJAX){
	
    $rand_code = I('get.rand_code');
	
	$rows = M('member')->where(array('random_code'=>$rand_code))->find();
	if(empty($rows)){//正式表没有就调用临时表
	 $rows = M('member_temp')->where(array('random_code'=>$rand_code))->find();//找不到就去临时表那里找
	}
	
	if(!empty($rows)){
	    //session('member_id', $rows['member_id']);//session要放在这里
	   session('login_bind_name', $rows['login_weixin']);
	   session('login_bind_type', '1');
	   session('login_nickname', $rows['nickname']);
	   if($rows['phone']!=''||$rows['email']){
	    session('member_id', $rows['member_id']);
	    echo '2';
	   }else{
	    echo '1';
	   }
	}else{
	 //echo '未登录';
	}
	
	}
	
}
	  
function login_return(){
$rand_code = I('get.rand_code','');

$appid = 'wx93ac7736b1e564e1';
$secret = 'aaf3835923b64a1f0f32ff057220241d';
 $code = $_GET["code"]; 
$get_token_url = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid='.$appid.'&secret='.$secret.'&code='.$code.'&grant_type=authorization_code';
$data = fopen($get_token_url, "rb"); 
$data = stream_get_contents($data);
@fclose($data); 
$json_obj = json_decode($data,true); 
//根据openid和access_token查询用户信息 
 $access_token = $json_obj['access_token'];
 $openid = $json_obj['openid']; 
$get_user_info_url = 'https://api.weixin.qq.com/sns/userinfo?access_token='.$access_token.'&openid='.$openid.'&lang=zh_CN'; 
 $data2 = fopen($get_user_info_url, "rb"); 
$data2 = stream_get_contents($data2);
@fclose($data2); 
//解析json 
$info = json_decode($data2,true); 

 $login_weixin = $info['openid'];
 if($login_weixin==''){
 echo 'Error';
 }
 /*
 if($login_weixin==''){
 header("content-type:text/html;charset=utf-8");  
 echo $appid.'<br>'.$secret;
 print_r($info);
 die('<div style="margin: 20% auto; text-align:center;font-size:2rem;">请重新扫码登录了！</div>');
 }
 */

  $find_count =  M("member")->where(Array('login_weixin'=>$login_weixin))->count();
  if($find_count>0){
  M("member")->where(Array('login_weixin'=>$login_weixin))->setField('random_code',$rand_code);
  }
  $rows=M("member_temp")->where("login_weixin='$login_weixin'")->find();
	$count=count($rows);
	$nowTime=time();
    if($count>0){
	   // session('access_token', $access_token);
       // $rows['headpic']=$info['headimgurl'];
		$rows['nickname'] = $info['nickname'];
		 $rows['random_code']=$rand_code;
		
		$rows['last_login_time']=$nowTime;
		//$rows['access_token']=$access_token;
		$model =M("member")->where("login_weixin='$login_weixin'")->find();//正式表里有没有
		
		if($model){
		$data['nickname'] = $info['nickname'];
		$data['random_code'] = $rand_code;
		$data['last_login_time'] = $nowTime;
        M("member")->where(Array('member_id'=>$model['member_id']))->save($data);

		}else{
		M("member_temp")->save($rows);
	
		}
	
	}else{
	   $data3['login_weixin']=$login_weixin;
	   //$data3['access_token']=$access_token;
	   $data3['nickname']=$info['nickname'];
	  // $data3['headpic']=$info['headimgurl'];
	   $data3['create_time']=time();
	  // $data3['sex']=$info['sex']=='1'?'先生':'女士';
	   $data3['last_login_time']=$nowTime;
	   $data3['random_code']=$rand_code;
	   M("member_temp")->data($data3)->add();
	}
      $this->display();
    //$this->redirect($return_url);

}
	  

}
