<?php

namespace Home\Controller;

use Think\Controller;
use Home\Model\MemberModel;
use Common\Controller\HomeController;

use Home\Form;

class RegisterController extends HomeController {
    
	
    //注册用户
    public function index() {
        $this->login_url = I('get.ret',U('Member/index'));
        $RegisterForm= new Form\RegisterForm();
		$this->RegisterForm=$RegisterForm;
        //第三方登录
        $ConfigOauthObj = new \Home\Model\ConfigOauthModel();
        $this->oauth_list = $ConfigOauthObj->get_list();
        	$this->seo=array('seo_title'=>'会员注册','seo_keywords'=>'会员注册','seo_description'=>'会员注册');
        $this->display();
    }
    
    //登陆用户
    public function login() {
        $this->member_id = session('member_id');
		
        if (!empty($this->member_id)) {
            redirect(U('Member/index'));
        }
        
        $this->login_url = I('get.ret',U('Member/index'));
        $this->remember_name = I('cookie.remember_name');
        //第三方登录
        $ConfigOauthObj = new \Home\Model\ConfigOauthModel();
        $this->oauth_list = $ConfigOauthObj->get_list();
		$this->seo=array('seo_title'=>'会员登录','seo_keywords'=>'会员登录','seo_description'=>'会员登录');
        $this->display();
    }
	
	
	public function bind_tips(){
	  $this->display();
	}
	
	
 	//第三方登录，已有手机号
	 public function bind0() {

        $this->login_url = I('get.ret', __ROOT__);
		$login_bind_name = session('login_bind_name');
		$login_bind_type = session('login_bind_type');
		 if (trim($login_bind_name) == '' || trim($login_bind_type) == '') {
            //$this->redirect('/');//找不到绑定信息
        }
        $MemberObj = new MemberModel();
		$flag = $MemberObj->login_api($login_bind_name, $login_bind_type);
		
		
		if($flag==1){
		$this->redirect('/');//已经绑定过了
		}elseif($flag<1){
		 die('error');
		}
		
		//print_r($_SESSION);
		$this->seo=array('seo_title'=>'百宝库-手机账号绑定','seo_keywords'=>'百宝库-账号绑定','seo_description'=>'百宝库-账号绑定');
		$this->login_bind_name = $login_bind_name;
		$this->login_bind_type = $login_bind_type;
		$this->nickname = session('nickname');
		$RegisterBind0PhoneForm= new Form\RegisterBind0PhoneForm();
		$this->RegisterBind0PhoneForm=$RegisterBind0PhoneForm;
		
        $this->display();

    }

	//第三方登录，手机注册
	 public function bind() {

        $this->login_url = I('get.ret', __ROOT__);
		$login_bind_name = session('login_bind_name');
		$login_bind_type = session('login_bind_type');
		 if (trim($login_bind_name) == '' || trim($login_bind_type) == '') {
            //$this->redirect('/');//找不到绑定信息
        }
        $MemberObj = new MemberModel();
		$flag = $MemberObj->login_api($login_bind_name, $login_bind_type);
		
		
		if($flag==1){
		$this->redirect('/');//已经绑定过了
		}elseif($flag<1){
		 die('error');
		}
		
		//print_r($_SESSION);
		$this->seo=array('seo_title'=>'百宝库-手机账号注册','seo_keywords'=>'百宝库-手机账号注册','seo_description'=>'百宝库-手机账号注册');
		$this->login_bind_name = $login_bind_name;
		$this->login_bind_type = $login_bind_type;
		$this->nickname = session('nickname');
		$RegisterBindPhoneForm= new Form\RegisterBindPhoneForm();
		$this->RegisterBindPhoneForm=$RegisterBindPhoneForm;
		
        $this->display();

    }

	
	
    
    //顶部登陆状态
    function top_state() {
        $MemberObj = new MemberModel();
        $MemberObj->update_state();
        $member_id = I('session.member_id', 0);
        if($member_id > 0) {
            $data['member_id'] = $member_id;
            $data['member_name'] = $MemberObj->show_member_name($member_id);
        } else {
            $data['member_id'] = 0;
            $data['member_name'] = '';
        }
        $this->ajaxReturn($data);
    }
    
    //登陆提交
    function login_submit() {
        $MemberObj = new MemberModel();
        $username = trim(I('post.username',''));
        $password = trim(I('post.password',''));
        $remember_name = I('post.remember_name','1');
        $remember = I('post.remember','0');
        if($remember_name != '') {
            cookie('remember_name',$username,604800);
        }
        echo $MemberObj->login($username,$password,$remember);
    }
    
    //注册提交
    function register_submit() {
        $MemberObj = new MemberModel();
        $verify = new \Think\Verify();
        $email = trim(I('post.email',''));
        $phone = trim(I('post.phone',''));
        $password = trim(I('post.password',''));
        $validate_code = trim(I('post.validate_code',''));
        $result['title']='success';
        if (empty($email) && empty($phone)) {
            $result['title']='false';
            $result['msg']='请输入邮箱或手机！';
        } elseif (!empty($email) &&  !is_email($email)) {
            $result['title']='false';
            $result['msg']='邮箱格式错误！';
        } elseif (!empty($phone) &&  !is_phone($phone)) {
            $result['title']='false';            
            $result['msg']='手机格式有误！';
        } elseif (!empty($email) && $MemberObj->exist_email($email, 0)>0){
            $result['title']='false';
            $result['msg']='此邮箱已被使用！';
        } elseif(!empty($phone) && $MemberObj->exist_phone($phone, 0)>0) {
            $result['title']='false';
            $result['msg']='此手机已被使用！';
        } elseif ($password == '') {
            $result['title']='false';
            $result['msg']='请填写密码！';
        } elseif ($validate_code == '' || !$verify->check($validate_code)) {
            $result['title']='false';
            $result['msg']='验证码不对！';
        } else {
            $member_id = $MemberObj->register($email, $phone,'', $password);
            if($member_id > 0) {
                if(empty($email)) $email = $phone;
                $MemberObj->login($email, $password, 0); 
            } else {
                $result['title']='false';
                $result['msg']='系统繁忙，注册失败！';
            }
        }
        $this->ajaxReturn($result);
    }
    
    //退出登陆AJAX
    public function logout_ajax()
    {
        $MemberObj = new \Home\Model\MemberModel();
        $MemberObj->logout();
        echo '0';
    }
    
    //退出登陆
    public function logout()
    {
        $MemberObj = new \Home\Model\MemberModel();
        $MemberObj->logout();
        $this->redirect('/');
    }
    
    //验证用户名是否重名
    public function exist_nickname($e) {
        $MemberObj = new MemberModel();
        echo $MemberObj->exist_nickname($e, 0);
    }
    
    //验证邮箱是否重名
    public function exist_email($e) {
        $MemberObj = new MemberModel();
        echo $MemberObj->exist_email($e, 0);
    }
    
    //验证手机是否重名
    public function exist_phone($e) {
        $MemberObj = new MemberModel();
        echo $MemberObj->exist_phone($e, 0);
    }
    
    //验证验证码是否正确
    public function check_valid_code($e) {
        $verify = new \Think\Verify();
        if($verify->check($e)) {
            echo '1';
        } else {
            echo '0';
        }
    }
    
    //显示验证码
    public function verify() {
        layout(false);
        $Verify = new \Think\Verify();
        $Verify->fontSize = 14;
        $Verify->useImgBg = false;
        $Verify->length = 4;
        $Verify->useNoise = false;
        $Verify->useCurve = false;
        $Verify->codeSet = '0123456789';
        $Verify->entry();
    }
    
    // 发送手机验证码
    public function get_phone_code($phone) {
        $MemberObj = new MemberModel();
        if (!is_phone($phone)) {
            $result['code'] = '400';
            $result['message'] = '手机号码格式不对';
        } else {
            $phone_code = rand(100000, 999999);
            session('phone_code',$phone_code);
            session('phone_no',$phone);
            $content = "您的验证码为".$phone_code."，在20分钟内有效。";
            if(sendSMS($phone, $content) == 'success') {
                $result['code'] = '200';
                $result['message'] = '手机验证短信发送成功';
            } else {
                $result['code'] = '400';
                $result['message'] = '手机验证短信发送失败！';
            }
        }
        return $this->ajaxReturn($result);
    }
    
    //找回密码
    public function forget() {
		$FindpwdForm= new Form\FindpwdForm();
		$this->FindpwdForm=$FindpwdForm;
		$this->seo=array('seo_title'=>'找回密码','seo_keywords'=>'找回密码','seo_description'=>'找回密码');
        $this->display();
    }
	
	 public function resetpassword() {
	 if(session('valid_code_sus')!= session('phonecode')||session('valid_code_sus')==null){
		$this->redirect('Register/forget');
	  }
	   $ResetpwdForm= new Form\ResetpwdForm();
		$this->ResetpwdForm=$ResetpwdForm;
		$this->seo=array('seo_title'=>'找回密码','seo_keywords'=>'找回密码','seo_description'=>'找回密码');
       $this->display();   

    }
    
    //找回密码发送邮件
    public function forget_email() {
        $email = trim(I('post.email',''));
        $validate_code = trim(I('post.validate_code',''));
        $MemberObj = new \Home\Model\MemberModel();
        if(empty($email)) {
            echo '请输入邮箱地址！';
            exit();
        }
        $verify = new \Think\Verify();
        if ($validate_code == '' || !$verify->check($validate_code)) {
            echo '请输入验证码或者验证码不对！';
            exit();
        }
        if($MemberObj->exist_email($email,0) <= 0) {
            echo '用户不存在！';
            exit();
        }
        $config = S('config');
        $member_id = $MemberObj->getFieldByEmail($email,'member_id');
        $member_name = $MemberObj->show_member_name($member_id);
        $status = $MemberObj->getFieldByMemberId($member_id,'status');
        if($status !== '1') {
            echo '用户已被禁用或删除！';
            exit();
        }
        $code_find_password = randomname(18).$member_id;
        $MemberInfoObj = new \Home\Model\MemberInfoModel();
        $MemberInfoObj->where('member_id='.$member_id)->setField('code_find_password',$code_find_password);
        $host = get_host();
        $url = $host.U('Register/forget_reset',array('code'=>$code_find_password));
        $url2 = '<a href="'.$url.'" target="_blank">'.$url.'</a>';
        $title = $config['forget_email_title'];
        $content = htmlspecialchars_decode($config['forget_email_detail']);
        $content = str_replace("{nickname}", $member_name, $content);
        $content = str_replace("{url}",$url2,$content);
        $content = '<html><body>'.$content.'</body></html>';
        $flag = sendMail($email, $title, $content);
        if($flag > 0) {
            echo '找回密码邮件发送成功，请到你的邮箱'.$email.'查看';
            exit();
        } else {
            echo '系统邮件发送失败，请联系网站客服！';
            exit();   
        }  
    }
    
    //手机找回密码提交
    public function forget_phone_ajax() {
        $phone_validate_code = trim(I('post.phone_validate_code',''));
        $phone = trim(I('post.phone',''));
        $phone_code = session('phone_code');
        $phone_no =  session('phone_no');
        if($phone_no == $phone && $phone_code == $phone_validate_code) {    
            $new_password = trim(I('post.password_new',''));
            $new_password = strongmd5($new_password);
            $MemberObj = new \Home\Model\MemberModel();
            $flag = $MemberObj->where(array('phone'=> $phone))->setField('password', $new_password);
            if($flag !== false) {
                session('phone_code',nul);
                session('phone_no',nul);
                echo 'success';
            } else {
                echo '修改失败！';
            }
        } else {
            echo '手机验证码错误！';
        }
    }
    
    //邮箱重置密码界面
    public function forget_reset() {
		
		
        $c = I('get.code','');
        if(empty($c)) {
            redirect(U('login'));
        } else {
            $MemberInfoObj = new \Home\Model\MemberInfoModel();
            $MemberObj = new \Home\Model\MemberModel();
            $member_id = $MemberInfoObj->where(array('code_find_password'=>$c))->getField('member_id');
            if(empty($member_id)) {
                $this->error('校验码己失效',U('forget'));
            }
            $this->member_name = $MemberObj->show_member_name($member_id);
            $this->code = $c;
            $this->display();
        }
    }
    
    //邮箱重置密码提交 AJAX
    public function forget_reset_ajax() {
        $c = trim(I('post.code',''));
        if(empty($c)) {
            echo '非法操作';
            exit();
        }
        $MemberInfoObj = new \Home\Model\MemberInfoModel();
        $MemberObj = new \Home\Model\MemberModel();
        $member_id = $MemberInfoObj->where(array('code_find_password'=>$c))->getField('member_id');
        if($member_id <= 0) {
            echo '非法操作';
            exit();
        }
        $new_password = trim(I('post.password',''));
        $new_password = strongmd5($new_password);
        $old_password = $MemberObj->where(array('member_id'=> $member_id))->getField('password');
        if($new_password == $old_password) {
            echo '新密码与原密码一致';
            exit();
        }
        $flag = $MemberObj->where(array('member_id'=> $member_id))->setField('password', $new_password);
        if($flag > 0) {
            $MemberInfoObj->where(array('member_id'=> $member_id))->setField('code_find_password', '');
            echo 'success';
        } else {
            echo '修改失败！';
        }
    }
}