<?php
namespace Home\Service;
/**
 * phpemail邮件发送服务类，下载的phpmail把src时的文件放入指定文件夹，同时修改去除PHPMailer.php和SMTP.php时的命名空间即可
 * 库下载地址：https://github.com/PHPMailer/PHPMailer
 * Class SendemailService
 * @package Fwadmin\Service
 */
class SendemailService {
    function __construct() {
       // $fwconfig = D('Config')->getAll();
        $config = array(
            'MAIL_CHARSET' => 'UTF-8',
            'MAIL_SMTPAUTH' => true, //是否使用身份验证
            'MAIL_ISHTML' => true, //是否使用html
            'MAIL_USERNAME' => 'customerservice@leseulstore.com', //发件地址
            'MAIL_SMTPHOST' => 'smtp.exmail.qq.com', //smtp地址
            'MAIL_PASSWORD' => 'Leseul2018*', //发件地址密码
            'MAIL_SMTPSECURE' => 'ssl', //使用ssl协议方式，ssl协议方式端口号是465/994
            'MAIL_PORT' => 465, //端口
            'MAIL_GET_ADDRESS' => '', //默认单一收件箱
        );
        $this->mailconfig = $config;
    }

    /**
     * 发送邮件
     * @param $get_email
     * @param $data array('title'=>'','detail'=>'')
     * @return bool
     */
    public function sendEmail($data, $get_email = '') {
		
        vendor('PHPMailer.PHPMailer');
        vendor('PHPMailer.SMTP');
        $mail = new \PHPMailer();
        $mail = $this->setBaseConfig($mail, $data, $get_email);
        if (!$mail->send()) {
            return "Mailer Error: " . $mail->ErrorInfo; //返回错误信息
        } else {
            return 0; //为0表示发送成功
        }
    }

    /**
     * 邮件基本设置
     * @param $mail
     * @param $data
     * @param string $get_email
     * @return mixed
     */
    public function setBaseConfig($mail, $data, $get_email = '') {
        $config = $this->mailconfig;
        $get_email = $get_email ? $get_email : $config['MAIL_GET_ADDRESS'];
        $mail->isSMTP(); // 使用SMTP服务
        $mail->CharSet = "utf8"; // 编码格式为utf8，不设置编码的话，中文会出现乱码
        $mail->Host = $config['MAIL_SMTPHOST']; // 发送方的SMTP服务器地址
        $mail->SMTPAuth = $config['MAIL_SMTPAUTH']; // 是否使用身份验证
        $mail->Username = $config['MAIL_USERNAME']; // 发送方的163邮箱用户名
        $mail->Password = $config['MAIL_PASSWORD']; // 发送方的邮箱密码，注意用163邮箱这里填写的是“客户端授权密码”而不是邮箱的登录密码！
        $mail->SMTPSecure = $config['MAIL_SMTPSECURE']; // 使用ssl协议方式
        $mail->Port = $config['MAIL_PORT']; // 163邮箱的ssl协议方式端口号是465/994
        $mail->IsHTML($config['MAIL_ISHTML']); //支持html格式内容
        $mail->setFrom($config['MAIL_USERNAME'], ''); // 设置发件人信息，如邮件格式说明中的发件人，这里会显示为Mailer(xxxx@163.com），Mailer是当做名字显示
        $mail->addAddress($get_email, ''); // 设置收件人信息，如邮件格式说明中的收件人，这里会显示为Liang(yyyy@163.com)
        //$mail->addReplyTo("","王");// 设置回复人信息，指的是收件人收到邮件后，如果要回复，回复邮件将发送到的邮箱地址
        //$mail->addCC("1132083961@qq.com"); // 设置邮件抄送人，可以只写地址，上述的设置也可以只写地址
        //$mail->addBCC("");// 设置秘密抄送人
        //$mail->addAttachment("bug0.jpg");// 添加附件
        $mail->Subject = $data['title']; // 邮件标题
        $mail->Body = $data['detail']; // 邮件正文
        //$mail->AltBody = "This is the plain text纯文本";// 这个是设置纯文本方式显示的正文内容，如果不支持Html方式，就会用到这个，基本无用
        return $mail;
    }
}