<?php
namespace Home\Form;

use Common\Form\HomeActiveForm;

/**
 * 表单验证器
 * Class FeedbackForm
 * @package Home\Form
 * ------------------------------------------------
 * 同时还需要如下操作
 * 1、在表单模板输出的控制器中
 * $FeedbackForm= new Form\FeedbackForm();
 * $this->FeedbackForm=$FeedbackForm;
 * 别记了引入空间use Home\Form;
 * 2、在表单所在的模板页面中
 * <?php $FeedbackForm->run();?>
 */
class LoginForm extends HomeActiveForm {
    /*表单id，如果不定义，自动套用类名Feedback*/
    public $formId = '';
    /*表单验证规则*/
    public $rules = array(
        'mobile' => array('required' => true, 'isMobile' => true),
		'password2' => array('required' => true)
        
       
    );
    /*表单验证信息*/
    public $messages = array(
       'mobile' => array('required' => "请输入您的手机号码", 'isMobile' => "手机号码格式错误"),
        'password2' => array('required' => "请输入密码")
        
       
    );
	
	
	
	
    public $ajaxAction = 'Ajax/login'; //ajax提交地址
    public $successJs = 'document.location = "/"'; //提交成功执行的js代码
    public $errorJs = ""; //提交失败执行的js代码

    /**
     * 执行输出
     */
    public function run() {
        $vars = get_class_vars(get_class($this));
        $vars['formId'] = $vars['formId'] ? $vars['formId'] : $this->formId;
        $vars['messages'] = parent::langExchange($vars['messages']); //多语言转换
		$vars['successJs'] = 'document.location = "'.$_SERVER["HTTP_REFERER"].'"';//"window.location.reload();";
        $this->assign('vars', $vars);
        $this->display('Public/validate2');
    }
}