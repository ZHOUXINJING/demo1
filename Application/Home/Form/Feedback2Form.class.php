<?php
namespace Home\Form;

use Common\Form\HomeActiveForm;

/**
 * 表单验证器
 * Class FeedbackForm
 * @package Home\Form
 * ------------------------------------------------
 * 同时还需要如下操作
 * 1、在表单模板输出的控制器中
 * $FeedbackForm= new Form\FeedbackForm();
 * $this->FeedbackForm=$FeedbackForm;
 * 别记了引入空间use Home\Form;
 * 2、在表单所在的模板页面中
 * <?php $FeedbackForm->run();?>
 */
class Feedback2Form extends HomeActiveForm {
    /*表单id，如果不定义，自动套用类名Feedback*/
    public $formId = '';
    /*表单验证规则*/
    public $rules = array(
          'chufadi' => array('required' => true),
		 'mudidi' => array('required' => true),
		 'start_time' => array('required' => true),
		 'end_time' => array('required' => true),
		 'renshu1' => array('required' => true),
		 'renshu2' => array('required' => true),
		 'total' => array('required' => true),
		'realname' => array('required' => true),
        'phone' => array('required' => true),
        'email' => array('required' => true, 'email' => true),
        'detail' => array('required' => true, 'maxlength' => 300),
    );
    /*表单验证信息*/
    public $messages = array(
         'chufadi' => array('required' => "请选择出发地"),
		 'mudidi' => array('required' => "请输入目的地"),
		 'start_time' => array('required' => "请选择出发日期"),
		 'end_time' => array('required' => "请选择返回日期"),
		 'renshu1' => array('required' => "请输入成人数量"),
		 'renshu2' => array('required' => "请输入儿童数量"),
		 'total' => array('required' => "请输入人均预算"),
		 'realname' => array('required' => "请输入联系人"),
		 'phone' => array('required' => "请输入联系电话"),
        'email' => array('required' => "请输入电子邮箱", 'email' => "邮箱格式错误"),
        'detail' => array('required' => "请输入其他需求", 'maxlength' => "其他需求内容最长为300个字符"),
    );
    public $ajaxAction = 'Ajax/feedback2'; //ajax提交地址
    public $successJs = "window.location.reload();"; //提交成功执行的js代码
    public $errorJs = ""; //提交失败执行的js代码

    /**
     * 执行输出
     */
    public function run() {
        $vars = get_class_vars(get_class($this));
        $vars['formId'] = $vars['formId'] ? $vars['formId'] : $this->formId;
        $vars['messages'] = parent::langExchange($vars['messages']); //多语言转换
		$vars['successJs'] = "window.location.href='".U('/')."';";
        $this->assign('vars', $vars);
        $this->display('Public/dingzhi');
    }
}