<?php

namespace Home\Model;

use Think\Model;

class AddressProvinceModel extends Model {

    /**
     * 获取省份选择
     * @param string $type 切换类别
     */
    public function getSel() {
        $sel = $this->field('province')->select();
        return $sel;
    }
}
