<?php

namespace Home\Model;

use Think\Model;

class IndexBlockModel extends Model {
    
    /**
     * 获取首页HTML
     * @param tinyint $client_type 1表示PC 2表示手机
     */
    public function show_html($client_type = 1) {
        $html = '';
        $block_list = $this->get_list($client_type);
		$key = 0;
        foreach($block_list as $block) {
			
            $style_templet = $this->get_style_templet($block['style_id']);
			$style_templet = $this->get_key_templet($style_templet,$key);
            $style_templet = $this->replace_block($style_templet, $block);//替换模块相关
            $style_templet = $this->replace_con($style_templet, $block['con']);//替换产品和广告图片
			$spec_arr = $block['spec'];
			$style_templet = $this->replace_spec_arr($style_templet, $spec_arr);
            $html .= $style_templet;
			$key++;
        }
        return $html;
    }
    
    /**
     * 替换板块信息
     * @param string $templet 模板
     * @param array $block 模块数组
     */
    public function replace_block($templet,$block) {
        $templet = str_replace('__ROOT__', __ROOT__, $templet);
        $templet = str_replace('【block_name】', $block['block_name'], $templet);
		 $templet = str_replace('【block_name_en】', $block['block_name_en'], $templet);
        $templet = str_replace('【block_name_color】', $block['block_name_color'], $templet);
        $templet = str_replace('【floor_name】', $block['floor_name'], $templet);
        $templet = str_replace('【ico】', $block['ico'], $templet);
        $templet = str_replace('【more_url】', file_url_http($block['more_url']), $templet);
        return $templet;
    }
	
	/*****替换key值,不一定会用到**/
	
	public function get_key_templet($templet,$key) {
        $templet = str_replace('【key】', $key, $templet);
        return $templet;
    }
    
    public function replace_con($templet,$con) {
        $k_adv = 0;
        $k_pro = 0;
        foreach($con as $item) {
            if($item['style_item']['type'] == '广告') {
                $k_adv++;
                $templet = str_replace('【adv_'.$k_adv.'_picture】', $item['picture'], $templet);
                $templet = str_replace('【adv_'.$k_adv.'_title】', $item['goods_name'], $templet);
                $templet = str_replace('【adv_'.$k_adv.'_url】', $item['url'], $templet);                
            } elseif($item['style_item']['type'] == '产品') {
                $k_pro++;
                $templet = str_replace('【pro_'.$k_pro.'_goods_id】', $item['goods_id'], $templet);
                $templet = str_replace('【pro_'.$k_pro.'_goods_name】', $item['goods_name'], $templet);
                $templet = str_replace('【pro_'.$k_pro.'_picture】', $item['picture'], $templet);
                $templet = str_replace('【pro_'.$k_pro.'_price】', $item['price'], $templet);       
                $templet = str_replace('【pro_'.$k_pro.'_price_market】', $item['price_market'], $templet);  
                $templet = str_replace('【pro_'.$k_pro.'_cuxiao_msg】', $item['cuxiao_msg'], $templet); 
                $templet = str_replace('【pro_'.$k_pro.'_url】', $item['url'], $templet); 
            }	
        }
		
	for($i=0;$i<10;$i++){
		 $templet = str_replace('【adv_'.$i.'_url】', '" style="display:none;', $templet);
		 $templet = str_replace('【pro_'.$i.'_url】', '" style="display:none;', $templet); 
		 $templet = str_replace('【pro_'.$i.'_goods_id】', '', $templet);
         $templet = str_replace('【pro_'.$i.'_goods_name】', '', $templet);
         $templet = str_replace('【pro_'.$i.'_picture】', '', $templet);
         $templet = str_replace('【pro_'.$i.'_price】', '', $templet);       
         $templet = str_replace('【pro_'.$i.'_price_market】', '', $templet);  
         $templet = str_replace('【pro_'.$i.'_cuxiao_msg】', '', $templet); 
         $templet = str_replace('【pro_'.$i.'_url】', '', $templet); 
		 $templet = str_replace('【adv_'.$i.'_picture】', '', $templet);
         $templet = str_replace('【adv_'.$i.'_title】', '', $templet);
         $templet = str_replace('【adv_'.$i.'_url】', '', $templet);  
		
		}
        return $templet;
    }
    
    /**
     * 获取模型风格模板
     * @param int $style_id 风格ID
     */
    public function get_style_templet($style_id) {
        $StyleObj = M('index_style');
        $style_templet = $StyleObj->where(array('style_id'=>$style_id))->getField('html');
        $style_templet = htmlspecialchars_decode($style_templet);
        return $style_templet;
    }

    /**
     * 获取模型列表
     */
     public function get_list($client_type = 1) {
         $list = $this->where(array('is_enable'=>1,'client_type'=>$client_type))->order('order_id asc,block_id asc')->select();
         $IndexBlockCon = M('index_block_con');
         foreach($list as $k => $v) {
             $list[$k]['con'] = $this->get_block_con($v['block_id']);
             $list[$k]['spec'] = unserialize($list[$k]['spec']);
         }
         return $list;
     }
	 
	 
	 //替换小类列表
	 public function replace_spec_arr($templet, $spec_arr) {
		 $small_class='';
		 foreach($spec_arr as $k=>$v){
		  $small_class.='<a href="'.file_url_http($v['v']).'" target="_blank">'.$v['k'].'</a>';
		 }
	    $templet = str_replace('【small_class】', $small_class, $templet);
        return $templet;
	 }
     
     /**
      * 获取模型风格列表
      * @return array 列表
      */
     public function get_style() {
         $StyleObj = M('index_style');
         return $StyleObj->where('is_enable=1')->order('order_id asc,style_id asc')->select();
     }
     
     /**
      * 获取模型内容
      * @param int $block_id 模型ID
      * @return array
      */
     public function get_block_con($block_id) {
        $IndexBlockCon = M('index_block_con');
        $IndexStyleItem = M('index_style_item');
        $GoodsObj = new \Fwadmin\Model\GoodsModel();
        $list = $IndexBlockCon->where(array('block_id'=>$block_id))->order('order_id asc')->select();
        foreach($list as $k => $v) {
            $goods = $GoodsObj->where(array('goods_id'=>$v['goods_id']))->field('goods_name,price,price_market,cuxiao_msg,picture')->find();
            
			if(empty($v['picture'])) {
                $list[$k]['picture'] = $goods['picture'];//str_replace('goods','goods/thumb',$goods['picture']);
            }
			
            if(empty($v['goods_name'])) {
                $list[$k]['goods_name'] = $goods['goods_name'];
            }
            if(empty($v['cuxiao_msg'])) {
                $list[$k]['cuxiao_msg'] = $goods['cuxiao_msg'];
            }
            if(empty($v['price']) || $v['price'] == 0) {
                $list[$k]['price'] = $goods['price'];
            }
             if(empty($v['price_market']) || $v['price_market'] == 0) {
                $list[$k]['price_market'] = $goods['price_market'];
            }
            if(empty($v['url'])) {
                $list[$k]['url'] = __ROOT__.'/Goods/index/id/'.$v['goods_id'];
            }else{
			 $list[$k]['url'] = file_url_http($list[$k]['url']);
			}
            $list[$k]['style_item'] =  $IndexStyleItem->field('pic_width,pic_height,type,style_item_id')->find($v['style_item_id']);
        }
        return $list;
     }

}