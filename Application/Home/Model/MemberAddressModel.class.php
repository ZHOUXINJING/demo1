<?php

namespace Home\Model;

use Think\Model;

class MemberAddressModel extends Model {


    /**
     * 设置为默认收货地址
     * @param int $member_id 会员ID
     * @param int $id 地址ID
     * @return int 大于0表示设置成功  否则是吧
     */
    public function set_default($member_id,$id) {
        $this->where(array('member_id'=>$member_id))->setField('is_default',0);
        $flag = $this->where(array('member_id'=>$member_id,'id'=>$id))->setField('is_default',1);
        return $flag;
    }
    
    /**
     * 根据会员ID和地址ID获取收货地址信息
     * @param int $member_id 会员ID
     * @param int $id 地址ID
     * @return array 地址信息
     */
    public function get_address($member_id,$id) {
        return $this->where(array('member_id'=>$member_id,'id'=>$id))->order('is_default desc,id desc')->find();
    }
    
    /**
     * 获取会员默认地址
     * @param int $member_id 会员ID
     * @return array 默认地址数组
     */
    public function get_default_address($member_id) {
        return $this->where(array('member_id'=>$member_id))->order('is_default desc,id desc')->limit(1)->find();
    }
    
    /**
     * 根据会员ID和地址ID删除收货地址信息
     * @param int $member_id 会员ID
     * @param int $id 地址ID
     * @return int 大于1表示删除成功 否刚失败
     */
    public function del_address($member_id,$id) {
        return $this->where(array('member_id'=>$member_id,'id'=>$id))->delete();
    }
    
    /**
     * 我的收货地址
     * @param array $conditions 条件数组
     * @param int $page 分页数
     */
    public function search($member_id, $pagesize = 10) {
        $map['member_id'] = $member_id;
        //排序
        $order = 'is_default desc,id desc';
        $count = $this->where($map)->count(); // 查询满足要求的总记录数
        $Page = new \Think\Page($count, $pagesize); // 实例化分页类 传入总记录数和每页显示的记录数
        //分页跳转的时候保证查询条件
        foreach ($conditions as $key => $val) {
            $Page->parameter[$key] = urlencode($val);
        }
        $show = $Page->show(); // 分页显示输出
        //获取分页数据
        $list = $this->where($map)->order($order)->limit($Page->firstRow . ',' . $Page->listRows)->select();
        
        return array(
            'list' => $list,
            'page' => $show,
            'count' => $count
        );
    }
    

}
