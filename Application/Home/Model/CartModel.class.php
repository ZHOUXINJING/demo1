<?php

namespace Home\Model;

use Think\Model;

class CartModel extends Model {

    //购物车中商品列表 
    public $cart_list = array();
    private $drive = 'cookie'; //使用cookie保存还是session
    private $cookie_time = 36000000;//cookie保存时间  单位秒
    private $save_id = 'my_cart';//保存的cookie或者session ID

    /**
     * 初始化
     */
    public function __construct() {
        parent::__construct();
        if($this->drive == 'cookie') {
             $cookie = cookie($this->save_id);
             if (isset($cookie)) {
                 $this->cart_list = unserialize(stripslashes(cookie($this->save_id)));
             } else {
                 $this->cart_list = array();
             }
        } elseif ($this->drive == 'session') {
            $session = session($this->save_id);
            if (isset($session)) {
                 $this->cart_list = unserialize(stripslashes(session($this->save_id)));
             } else {
                 $this->cart_list = array();
             }
        }
    }
    
    /**
     * 获得购物车商品数组
     * @param string $id 已选择购物车数组
     * @return array 商品数组
     */
    public function get_cart($id = '') {
        $row = array(); //购物车数组
        if($id != '') {
            $arr_id = explode('-',$id);
        }
        $MGoods = new \Home\Model\GoodsModel();
        for($i = 0; $i < count($this->cart_list); $i++) {
            if(empty($arr_id) || (!empty($arr_id) && in_array($i, $arr_id))) {
                $goods_id = $this->cart_list[$i]['goods_id'];
                $item_id = $this->cart_list[$i]['item_id'];
                $num = $this->cart_list[$i]['num'];
                $goods_item = $MGoods->get_item_cart($item_id, $goods_id);//商品信息
                if(!empty($goods_item)) {
                    $row[$i] = $goods_item;
                    $row[$i]['num'] = $num;
                    $row[$i]['cart_id'] = $i;
                }
            }
        }
        return $row;	 
    }
    
    /**
     * 获得购物车商品数组APP
     * @param string $id 已选择购物车数组
     * @return array 商品数组
     */
    public function get_cart_app($ids = '') {
        $row = array(); //购物车数组
        $MGoods = new \Home\Model\GoodsModel();
        $MGoodsItem = M('goods_item');
        if($ids != '') {
            $ids = trim($ids,'|');
            $arr_id = explode('|',$ids);
            for($i = 0; $i < count($arr_id); $i++) {
                if($arr_id[$i] != '') {
                    $arr_id2 = explode('-',$arr_id[$i]);
                    $item_id = $arr_id2[0];
                    $num = $arr_id2[1];
                    $goods_id = $MGoodsItem->getFieldByItemId($item_id,'goods_id');
                    $goods_item = $MGoods->get_item_cart($item_id, $goods_id);//商品信息
                    if(!empty($goods_item)) {
                        $row[$i] = $goods_item;
                        $row[$i]['num'] = $num;
                        $row[$i]['cart_id'] = $i;
                    }
                }
            }
        }
        return $row;	 
    }
    
    /**
     * 添加商品到购物车
     * @param int $goods_id 商品ID
     * @param int $item_id 单件ID
     * @param int $num 购物车数量
     * @return int 返回新商品的购物车索引
     */
    public function add_cart($goods_id,$item_id,$num)  
    {
        if($item_id == 0) {
            $GoodsObj = new \Home\Model\GoodsModel();
            $item_id = $GoodsObj->get_default_item_id($goods_id);
        }
        if($item_id <= 0 || $goods_id <= 0 || $num <= 0) return 0;//检查数据规范
        $item = array("goods_id" => $goods_id, "item_id" => $item_id, "num" => $num);//商品单件数组
        $i = $this->check_item($item_id); //检查是否已存在该商品
        if($i == -1) {
            $this->cart_list[] = $item; //不存在新增数组
            $i = count($this->cart_list)-1;
        } else {
            $this->cart_list[$i]['num'] += $num; //已存在则新增数量
        }
        $this->save_data();
        return $i;
    }
    
   /**
    * 修改购物车数量
    * @param int $i 购物车ID
    * @param int $num 修改后的数量
    * @return int 返回1表示成功
    */
    public function edit_num($i,$num) {
	 /*
	if($i > $this->count_row()) {
	    return 0;
	}
	*/
	$this->cart_list[$i]['num'] = $num;
	$this->save_data();
	return 1;
    }

    /**
     * 保存数据
     */
    private function save_data() {
        if($this->drive == 'cookie') {
            cookie($this->save_id, serialize($this->cart_list), $this->cookie_time);
        } elseif ($this->drive == 'session') {
            session($this->save_id, serialize($this->cart_list));
        }
    }
    
    /**
     * 检查购物车中是否存在此商品单件
     */
    public function check_item($item_id) { 
        for($i = 0; $i < count($this->cart_list); $i++) {   
            if($this->cart_list[$i]['item_id']==$item_id) {
                return $i;
            }
        }    
        return -1; 
    }
    
    /**
     * 检查购物车库存
     * @return boolean 返回true表示OK
     */
    public function check_store_num() {
        $MGoods = new \Home\Model\GoodsModel();
	for($i = 0; $i < count($this->cart_list); $i++) {
	    $item_id = $this->cart_list[$i]['item_id'];
            $num = $this->cart_list[$i]['num'];
            $store_num = $MGoods->get_item_store_num($item_id);//获取单件价格
            if($num > $store_num) {
                return false;
            }
	}
        return true;
    }

    /**
     * 计算购物车中商品总件数
     */
    public function count_row() {
        $row = $this->get_cart();
	return count($row);
    }
    
    /**
     * 计算总价格
     * @param string $id 已选择购物车数组
     */
    public function get_total($id = '') {
	$total = 0;
        if($id != '') {
            $arr_id = explode('-',$id);
        }
        $MGoods = new \Home\Model\GoodsModel();
	for($i = 0; $i < count($this->cart_list); $i++) {
            if(empty($arr_id) || (!empty($arr_id) && in_array($i, $arr_id))) {
                $item_id = $this->cart_list[$i]['item_id'];
                $num = $this->cart_list[$i]['num'];
                $price = $MGoods->get_item_price($item_id);//获取单件价格
                $total += $num * $price;
            }
	}
	return $total;
    }
    
    /**
     * 清空购物车
     */
    public function clear() {
        if($this->drive == 'cookie') {
            cookie($this->save_id, null);
        } elseif ($this->drive == 'session') {
            session($this->save_id, null);
        }
        return 1;
    }
    
    /**
     * 删除购物车商品
     * @param int $i 购物车ID
     * @return int 返回1表示成功
     */
    public function del($i) {
	   /*
        if($i > $this->count_row()) {
            return 0;
        }
		*/
        if ($i != -1) {
            array_splice($this->cart_list, $i, 1);
            $this->save_data();
            return 1;
        } else {
            return -1;
        }
    } 

    /**
     * 批量删除购物车商品
     * @param array $arr 购物车ID数组
     * @return int 返回1表示成功
     */
    public function del_sel($arr) {
        $arr=array_reverse($arr);
        foreach($arr as $i) {
           $cart_num=$this->count_row();
           if($i!=-1&&$i<$cart_num) {
              array_splice($this->cart_list,$i,1);
           }
        }
        $this->save_data();
        return 1; 
    }

}
