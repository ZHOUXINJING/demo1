<?php

namespace Home\Model;

use Think\Model;

class ConBannerModel extends Model {

    /**
     * 获取banner
     * @param string $type 切换类别
     */
    public function get_list($type) {
        $banner = $this->field('title,picture,picture1200,picture640,url')->where(array('type'=>$type,'is_show'=>1))->order('order_id asc,id asc')->select();
        return $banner;
    }
}
