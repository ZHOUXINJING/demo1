<?php

namespace Home\Model;

use Think\Model;

class ConHelpModel extends Model {
    
    /**
     * 获取左侧列表
     */
    public function get_list($parent_id = 0) {
        return $this->field('help_id,help_name,type,link')->where(Array('parent_id' => $parent_id, 'is_show' => 1))->order('order_id asc,help_id asc')->select();
    }
    
     /**
     * 获取第一个子类别ID
     */
    public function get_sub_id($id) {
        return $this->where(array('parent_id'=>$id,'is_show'=>1,'type'=>array('in','单页,资讯')))->order('order_id asc,help_id asc')->getField('help_id');
    }
    
    /**
     * 获取左侧帮助名称
     */
    public function get_help_name($help_id) {
        return $this->getFieldByHelpId($help_id,'help_name');
    }
    
    /**
     * 获取左侧菜单
     */
    public function get_left_menu() {
        $left_menu = $this->get_list();
        foreach ($left_menu as $k => $v) {
            $left_menu[$k]['sub'] = $this->get_list($v['help_id']);
        }
        return $left_menu;
    }
    
    /**
     * 搜索符合条件的记录
     * @param array $condition 会员ID
     * @param int $pagesize 分页数
     */
    public function search_news($conditions, $pagesize = 15, $field = 'news_id,title,detail,create_time') {
        $NewsObj = M('con_news');
        $order = 'order_id asc,create_time desc';
        $map = $conditions;
        $map['is_show'] = 1;
        $count = $NewsObj->where($map)->count(); // 查询满足要求的总记录数
        $Page = new \Think\Page($count, $pagesize); // 实例化分页类 传入总记录数和每页显示的记录数
        //分页跳转的时候保证查询条件
        foreach ($conditions as $key => $val) {
            $Page->parameter[$key] = $val;
        }
        $show = $Page->show(); // 分页显示输出
        // 进行分页数据查诟 注意limit 方法癿参数要使用Page 类的属性
        $list = $NewsObj->field($field)->where($map)->order($order)->limit($Page->firstRow . ',' . $Page->listRows)->select();
        return array(
            'list' => $list,
            'page' => $show
        );
    }

   

}
