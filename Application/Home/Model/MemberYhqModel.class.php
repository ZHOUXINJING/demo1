<?php

namespace Home\Model;

use Think\Model;

class MemberYhqModel extends Model {

    /**
     * 获取会员可用优惠券数梁
     * @param int $member_id 会员ID
     */
    public function count_can_use($member_id) {
        $map['member_id'] = $member_id;
        $map['is_use'] = 0;
        $map['end_time'] = array('egt',date('Y-m-d H:i:s',time()));
        return $this->where($map)->count();
    }
    
    /**
     * 获取会员当前可用优惠券
     * @param int $member_id 会员ID
     */
    public function can_use_now($member_id,$cart_total=0) {
        $map['member_id'] = $member_id;
        $map['is_use'] = 0;
		$map['cart_total'] = array('lt',$cart_total);
        $map['begin_time'] = array('elt',date('Y-m-d H:i:s',time()));
        $map['end_time'] = array('egt',date('Y-m-d H:i:s',time()));
        return $this->where($map)->select();
    }
    
	    /**
     * 获取会员当前可用优惠券
     * @param int $member_id 会员ID
     */
    public function can_use_now_one($member_id,$cart_total=0) {
        $map['member_id'] = $member_id;
        $map['is_use'] = 0;
		$map['cart_total'] = array('lt',$cart_total);
        $map['begin_time'] = array('elt',date('Y-m-d 23:59:59',time()));
        $map['end_time'] = array('egt',date('Y-m-d 00:00:00',time()));
        return $this->where($map)->order('money desc,end_time asc')->find();
    }
    
	
	
    /**
     * 计算当前已选优惠券总金额
     * @param string $yhq_ids 优惠券IDs
     * @param int $member_id 会员编号
     * @return decimal 优惠券金额
     */
    public function total_money($yhq_ids,$member_id) {
        $map['member_id'] = $member_id;
        $map['is_use'] = 0;
        $map['begin_time'] = array('elt',date('Y-m-d 23:59:59',time()));
        $map['end_time'] = array('egt',date('Y-m-d 00:00:00',time()));
        $map['id'] = array('in',$yhq_ids);
        $money_list = $this->field('money')->where($map)->select();
        $total = 0;
        foreach($money_list as $k => $v) {
            $total += $v['money'];
        }
        return $total;
    }
    
    /**
     * 使用优惠券
     * @param type $yhq_ids
     * @param type $member_id
     * @param type $order_id
     * @return type
     */
    public function use_ids($yhq_ids,$member_id,$order_id){
        $map['member_id'] = $member_id;
        $map['id'] = array('in',$yhq_ids);
        $map['is_use'] = 0;
        $map['begin_time'] = array('elt',date('Y-m-d 23:59:59',time()));
        $map['end_time'] = array('egt',date('Y-m-d 00:00:00',time()));
        $list = $this->field('id')->where($map)->select();
        
        $data['order_no'] = $order_id;
        $data['is_use'] = 1;
        $data['use_time'] = date('Y-m-d H:i:s',time());
        foreach($list as $k => $v) {
            $this->where(array('id'=>$v['id']))->setField($data);
        }
        return $flag;
        
    }
   
     /**
     * 获取已发放优惠券记录
     * @param array $map 搜索条件数组
     * @param string $order 排序规则
     * @param int $pagesize 分页数
     */
    public function search($member_id,$conditions,$order =  '',$pagesize = 9) {
        if(empty($order)) {
            $order = 'is_use asc,id desc';
        }
        $map = $conditions;
        $map['member_id'] = $member_id;
//		$map['begin_time'] = array('lt',date('Y-m-d 00:00:00'));

        $map['is_use'] = 0;
        if(isset($map['is_use'])) {
            if($map['is_use'] == 2)$map['is_use'] = 0;
        }

        if (isset($map['username'])) {
            $MemberObj = new \Fwadmin\Model\MemberModel();
            $member_id = $MemberObj->get_member_id($map['username']);
            $map['member_id'] = $member_id;
            unset($map['username']);
        }
        if (isset($map['time_begin']) && isset($map['time_end'])) {
            $time_begin = $map['time_begin'].' 00:00:00';
            $time_end = $map['time_end'].' 23:59:59';
            $map['create_time'] = array('between',array($time_begin,$time_end));
            unset($map['time_begin']);
            unset($map['time_end']);
        } elseif (isset($map['time_begin'])) {
            $time_begin = $map['time_begin'].' 00:00:00';
            $map['create_time'] = array('egt',$time_begin);
            unset($map['time_begin']);
        } elseif (isset($map['time_end'])) {
            $time_end = $map['time_end'].' 23:59:59';
            $map['create_time'] = array('elt',$time_end);
            unset($map['time_end']);
        }


        $count = $this->where($map)->count(); // 查询满足要求的总记录数
        $Page = new \Think\Page($count, $pagesize); // 实例化分页类 传入总记录数和每页显示的记录数
        //分页跳转的时候保证查询条件
        foreach ($conditions as $key => $val) {
            $Page->parameter[$key] = $val;
        }
        $show = $Page->show(); // 分页显示输出
        // 进行分页数据查诟 注意limit 方法癿参数要使用Page 类的属性
        $list = $this->where($map)->order($order)->limit($Page->firstRow . ',' . $Page->listRows)->select();

        return array(
            'list' => $list,
            'page' => $show
        );
    }
   
}
