<?php

namespace Home\Model;

use Think\Model;

class MemberLevelModel extends Model {


    //获取级别数组
    public function get_array() {
        $level = array();
        $arr = $this->field('level_id,level_name,point_up,ico')->where('is_show=1')->select();
        foreach($arr as $k => $v) {
            //如果出现跳级 后面的都不算
            if($k+1 == $v['level_id']) {
                $level[$v['level_id']] = $v;
            }
        }
        //获取升级到下一等级需要的经验值
        $max_level = count($level);
        foreach($level as $k => $v) {
            if($k < $max_level) {
               $level[$k]['next_point_up'] = $level[$k+1]['point_up'];
            } else {
               $level[$k]['next_point_up'] = $level[$k]['point_up'];
            }
        }
        return $level;
    }
    
    /**
     * 根据会员经验值获得对应等级
     * @param int $experience 经验值
     */
    public function get_level($experience) {
        $arr = $this->where(array('is_show'=>1,'point_up'=>array('elt',$experience)))->order('point_up desc')->limit(1)->getField('level_id');
        if(!empty($arr)) {
            return $arr;
        } else {
            return $this->where('is_show=1')->order('point_up desc')->limit(1)->getField('level_id');
        }
    }
    
    /**
     * 根据会员级别ID获得级别名称
     * @param int $level_id 会员ID
     * @return string 会员级别名称
     */
    public function get_level_name($level_id) {
        return $this->where(array('level_id'=>$level_id))->getField('level_name');
    }

    /**
     * 根据会员级别ID获得级别折扣
     * @param int $level_id 会员ID
     * @return string 会员级别折扣
     */
    public function get_level_discount($level_id) {
        return $this->where(array('level_id'=>$level_id))->getField('distinct');
    }

    
    

}
