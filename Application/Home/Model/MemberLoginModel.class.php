<?php

namespace Home\Model;

use Think\Model;

class MemberLoginModel extends Model {
 
    /**
     * 增加登录记录
     */
    public function add_login_record($member_id) {
        $data['member_id'] = $member_id;
        $data['login_ip'] = get_client_ip();
        $data['login_time'] = time();
        $this->add($data);
    }

    /**
     * 获取最后一次登际时间
     * @param type $member_id 会员ID
     */
    public function get_last_login_time($member_id) {
        return $this->where(array('member_id'=>$member_id))->order('login_time desc')->limit(1)->getField('login_time');   
    }

}

?>