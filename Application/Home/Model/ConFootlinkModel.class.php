<?php

namespace Home\Model;

use Think\Model;

class ConFootlinkModel extends Model {

    /**
     * 获取所有类别
     * @param int $category_id 类别ID
     */
    public function getList() {
        $arr = $this->getTop();
        foreach ($arr as $k => $v) {
            $arr[$k]['sub'] = $this->getSub($v['category_id']);
        }
        return $arr;
    }

    /**
     * 获取子级列表
     * @param int $category_id 类别ID
     */
    public function getSub($parent_id) {
        $arr = $this->field('category_name,url')->where(Array('parent_id' => $parent_id, 'is_show' => 1))->order('order_id asc')->select();
        foreach($arr as $k => $v) {
			/*
            if(strpos($v['url'],"http") === false) {
                $arr[$k]['url'] = __ROOT__. $v['url'];
            }
			*/
        }
        return $arr;
    }

    /**
     * 获取顶级类别
     */
    public function getTop() {
        $arr = $this->field('category_id,category_name,url')->where(Array('parent_id' => 0, 'is_show' => 1))->order('order_id asc')->select();
       
        foreach($arr as $k => $v) {
            if(strpos($v['url'],'http') == false) {
                $arr[$k]['url'] = __ROOT__. $arr[$k]['url'];
            }
        }
        return $arr;
    }

}
