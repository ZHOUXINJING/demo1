<?php
namespace Home\Widget;

use Think\Controller;
use Common\Controller\HomeController;

class HomeWidget extends HomeController {
    /**
     * 面包屑
     */
    public function top_login_state() {
		$MemberObj = new \Home\Model\MemberModel();
		$MemberObj->update_state();
        $member_id = I('session.member_id', 0);
        if($member_id > 0) {
            $data['member_id'] = $member_id;
            $data['member_name'] = $MemberObj->show_member_name($member_id);
			
			$data['member_level_id'] = $MemberObj->show_member_level($member_id);
			
        } else {
            $data['member_id'] = 0;
            $data['member_name'] = '';
        }
		$this->data = $data;
		$this->ret = urlencode(__SELF__);
        $this->display('Widget:top_login_state');
    }
	
	public function search_history(){
	$ModelGoods = new \Home\Model\GoodsModel();
	$search_list = $ModelGoods->search_history_show(5);
	$this->search_list = $search_list;
	 $this->display('Widget:top_search');
	}
	
	
	public function top_cart(){
	 $CartObj = new \Home\Model\CartModel();
        $cart_list = $CartObj->get_cart(); 
        foreach($cart_list as $k => $v) {
            $cart_list[$k]['total'] = tofixed($v['price'] * $v['num'],2);
        }
        $this->cart_list = $cart_list;
		$this->cart_count = count($cart_list);
        $this->cart_total = $CartObj->get_total();
	 $this->display('Widget:top_cart');
	}
	
	public function message(){
   $MemberObj = new \Home\Model\MemberModel();
	$member_id = I('session.member_id', 0);
	$this->Member = $MemberObj->where(Array('member_id'=>$member_id))->find();
	$this->display('Widget:top_message');
	}

}
