<?php

namespace Phone\Model;

use Think\Model;

class GoodsCatModel extends Model {
    
    /**
     * 导航菜单
     * @return array 菜单列表
     */
    public function menu_list() {
        $list = $this->field('cat_id,cat_name')->where(Array('is_enable' => 1, 'is_menu' => 1,'cat_level'=>1))->order('order_id asc')->select();
        return $list;
    }

    /**
     * 前台类别列表，最多显示3级
     */
    public function show_list() {
        $arr1 = $this->get_sub(0);
        foreach ($arr1 as $k1 => $v1) {
			
			$arr1[$k1]['adv_picture'] = stringToarray($v1['adv_picture']); 
			$arr1[$k1]['adv_links'] = stringToarray(nl2br($v1['adv_links']),'<br />'); 
			
            $arr2 = $this->get_sub($v1['cat_id']);
            foreach ($arr2 as $k2 => $v2) {
                $arr2[$k2]['sub'] = $this->get_sub($v2['cat_id']);
            }
            $arr1[$k1]['sub'] = $arr2;
        }
        return $arr1;
    }

    /**
     * 获取子类别列表
     * @param int $cat_id 类别ID
     * @return array 类别列表
     */
    public function get_sub($cat_id) {
        $arr = $this->field('cat_id,cat_name,adv_picture,adv_links,is_menu,parent_id,path_id,cat_level,ico,picture')->where(Array('is_enable' => 1, 'parent_id' => $cat_id))->order('order_id asc')->select();
        return $arr;
    }
    
     /**
     * 获取搜索子类别列表
     * @param int $cat_id 类别ID
     * @return array 类别列表
     */
    public function get_search_sub($cat_id,$keywords,$brand_id) {
        if(!empty($keywords) && empty($cat_id)) {
            $arr = $this->list_by_keywords($keywords);
        } elseif($brand_id > 0 && empty($cat_id)) {
            $arr = $this->list_by_brand($brand_id);
        } else {
            $arr = $this->field('cat_id,cat_name,adv_picture,adv_links,is_menu,parent_id,path_id,cat_level')->where(Array('is_enable' => 1, 'parent_id' => $cat_id))->order('order_id asc')->select();
        }
	
        return $arr;
    }
    
    /**
     * 根据品牌获取类别列表
     * @param int $brand_id 品牌ID
     * @return array 类别列表
     */
    public function list_by_brand($brand_id) {
        $sql = "select cat_ids from ".C('DB_PREFIX')."goods g where g.status=1 and g.brand_id='$brand_id'";
        $cat_list = $this->query($sql);
        $str_cat_list = '';
        foreach($cat_list as $k => $v) {
            $str_cat_list .= $v['cat_ids'];
        }
        if(!empty($str_cat_list)) {
            $str_cat_list = trim(str_replace(',,', ',', $str_cat_list),',');
            $arr = $this->field('cat_id,cat_name')->where(Array('is_enable' => 1, 'cat_id'=>array('in',$str_cat_list)))->order('order_id asc')->select();
        }
        return $arr;
    }
    
    /**
     * 根据关键词获取类别列表
     * @param int $keywords 关键词
     * @return array 类别列表
     */
    public function list_by_keywords($keywords) {
        $sql = "select cat_ids from ".C('DB_PREFIX')."goods g where g.status=1 and (g.goods_name like '%$keywords%' or g.cuxiao_msg like '%$keywords%' or g.goods_sn like '%$keywords%' )";
        $cat_list = $this->query($sql);
        $str_cat_list = '';
        foreach($cat_list as $k => $v) {
            $str_cat_list .= $v['cat_ids'];
        }
        if(!empty($str_cat_list)) {
            $str_cat_list = trim(str_replace(',,', ',', $str_cat_list),',');
            $arr = $this->field('cat_id,cat_name')->where(Array('is_enable' => 1, 'cat_id'=>array('in',$str_cat_list)))->order('order_id asc')->select();
        }
        return $arr;
    }
    
    /**
     * 获取类别路径名称
     * @param string $path_id 类别路径
     * @return string 路径名称
     */
    public function getAllPathName($path_id) {
        $path_id = trim($path_id,',');
        $arr = explode(',',$path_id);
        $str = '';
        foreach ($arr as $v) {
           if($v > 0) {
               $str = '<a href="'.U('GList/index',array('cid'=>$v)).'" target="_blank">'.$this->getFieldByCatId($v,'cat_name').'</a>/'.$str;
           } 
        }
        $str = trim($str,'/');
        return $str;
    }
    
    /**
     * 获取类别名称
     * @param int $cat_id 类别ID
     * @return string 类别名称
     */
    public function get_cat_name($cat_id) {
        return $this->getFieldByCatId($cat_id,'cat_name');
    }
    
    /**
     * 获取父类别ID
     * @param int $cat_id 类别ID
     * @return int  父类别ID
     */
    public function get_parent_id($cat_id) {
        return $this->getFieldByCatId($cat_id,'parent_id');
    }
    
    /**
     * 获取最顶级类别ID
     * @param int $cat_id 类别ID
     * @return int  父类别ID
     */
    public function get_top_parent_id($cat_id) {
        $top1 = $this->getFieldByCatId($cat_id,'parent_id');
        if($top1 == 0) {
            return $cat_id;
        } elseif($top1 != "") {
            return $this->get_top_parent_id($top1);
        } else {
            return $cat_id;
        }
    }
	
	
	/***获取该商品的所有类别***/
	public function get_all_cate($path){
		
	 $path = stringToarray($path);
	
	 $path = array_reverse($path);
	 $c='';
	 foreach($path as $k =>$v){ 
		
	 if($v>0){
		$a=$k+1;
		if($path[$a]!=''){
			$heng=' > ';
		 }else{
		 $heng='';
		 }
		 $cat_name = '<a href="'.U('GList/index',array('cid'=>$v)).'" target="_blank">'.$this->getFieldByCatId($v, 'cat_name').'</a>';
	     $c.=$cat_name!=''?$cat_name:'<span style="color:#F00">不存在</span>';
		 $c.=$heng;
	  }
	 }
	
	 return $c;
	 	
	
	}
	

}
