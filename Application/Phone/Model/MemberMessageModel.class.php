<?php

namespace Phone\Model;

use Think\Model;

class MemberMessageModel extends Model {

    /**
     * 获取会员可用优惠券数梁
     * @param int $member_id 会员ID
     */
    public function count_can_look($member_id) {
        $map['member_id'] = $member_id;
        $map['is_look'] = 0;
        return $this->where($map)->count();
    }
	
	    public function count_have_look($member_id) {
        $map['member_id'] = $member_id;
        //$map['is_look'] = 1;
        return $this->where($map)->setField('is_look',1);
    }
    
     /*
     * 删除消息
     */
    public function del_message($id,$member_id) {
        $map['id'] = $id;
        $map['member_id'] = $member_id;
        return $this->where($map)->delete();
    }

   
     /**
     * 获取已发放优惠券记录
     * @param array $map 搜索条件数组
     * @param string $order 排序规则
     * @param int $pagesize 分页数
     */
    public function search($member_id,$conditions,$order =  '',$pagesize = 10) {
        if(empty($order)) {
            $order = 'create_time desc,id desc';
        }
        $map = $conditions;
        $map['member_id'] = $member_id;

        
        $count = $this->where($map)->count(); // 查询满足要求的总记录数
        $Page = new \Think\Page($count, $pagesize); // 实例化分页类 传入总记录数和每页显示的记录数
        //分页跳转的时候保证查询条件
        foreach ($conditions as $key => $val) {
            $Page->parameter[$key] = $val;
        }
        $show = $Page->show(); // 分页显示输出
        // 进行分页数据查诟 注意limit 方法癿参数要使用Page 类的属性
        $list = $this->where($map)->order($order)->limit($Page->firstRow . ',' . $Page->listRows)->select();
        
        return array(
            'list' => $list,
            'page' => $show
        );
    }
   
}
