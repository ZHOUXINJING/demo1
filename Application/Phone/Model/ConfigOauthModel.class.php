<?php

namespace Phone\Model;

use Think\Model;

class ConfigOauthModel extends Model {

    /**
     * 前台列表
     */
    public function get_list() {
        return $this->where('is_enable=1')->order('order_id asc,id asc')->select();
    }
    
    
    /**
     * 获取配置参数
     */
    public function get_config($class_name) {
        $par_array  = $this->where(array('class_name'=>$class_name))->getField('par_array');
        if(empty($par_array)) {
            echo '未配置参数';
            exit();
        }
        $par_array = json_decode($par_array,true);
        return $par_array;
    }

}
