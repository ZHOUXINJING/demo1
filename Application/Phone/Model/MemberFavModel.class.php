<?php

namespace Phone\Model;

use Think\Model;

class MemberFavModel extends Model {

    
    /**
     * 添加收藏
     */
    public function add_fav_app($member_id,$goods_id,$item_id,$price) {
        $count = $this->where(array('member_id'=>$member_id,'goods_id'=>$goods_id,'item_id'=>$item_id))->count();
        if($count > 0) {
            $map['item_id'] = $item_id;
            $map['goods_id'] = $goods_id;
            $map['member_id'] = $member_id;
            $this->where($map)->delete();
            return -2;//已经收藏此商品
        }
        $data['member_id'] = $member_id;
        $data['goods_id'] = $goods_id;
        $data['item_id'] = $item_id;
        $data['price'] = $price;
        $data['member_id'] = $member_id;
        $data['create_time'] = time();
        $flag = $this->add($data);
        if($flag > 0) {
            //更新商品收藏数量
            $GoodsObj = new \Phone\Model\GoodsModel();
            $GoodsObj->update_count_fav($goods_id);
        }
        return $flag;
    }
    
    /**
     * 添加收藏
     */
    public function add_fav($member_id,$goods_id,$item_id,$price) {
        $count = $this->where(array('member_id'=>$member_id,'goods_id'=>$goods_id,'item_id'=>$item_id))->count();
        if($count > 0) {
            return -2;//已经收藏此商品
        }
        $data['member_id'] = $member_id;
        $data['goods_id'] = $goods_id;
        $data['item_id'] = $item_id;
        $data['price'] = $price;
        $data['member_id'] = $member_id;
        $data['create_time'] = time();
        $flag = $this->add($data);
        if($flag > 0) {
            //更新商品收藏数量
            $GoodsObj = new \Phone\Model\GoodsModel();
            $GoodsObj->update_count_fav($goods_id);
        }
        return $flag;
    }
    
    /*
     * 删除收藏
     */
    public function del_fav($id,$member_id) {
        $map['id'] = $id;
        $map['member_id'] = $member_id;
        return $this->where($map)->delete();
    }
	
	
	    /*
     * 删除收藏
     */
    public function del_goods_fav($goods_id,$item_id,$member_id) {
        $map['goods_id'] = $goods_id;
		$map['item_id'] = $item_id;
        $map['member_id'] = $member_id;
        return $this->where($map)->delete();
    }

    /**
     * 搜索符合条件的记录
     * @param int $member_id 会员ID
     * @param int $pagesize 分页数
     */
    public function search_member($member_id, $pagesize = 20, $limit = 0) {
        $sql_from = ' from '.C('DB_PREFIX').'goods g,'.C('DB_PREFIX').'member_fav f';
        $sql_where = ' where f.goods_id=g.goods_id and f.member_id='.$member_id.' and g.status=1';
        $sql_order = ' order by f.id desc';
        $sql = 'select f.id,f.goods_id,f.item_id,f.create_time,f.price as price_before,g.picture,g.goods_name,g.cuxiao_msg '.$sql_from.$sql_where.$sql_order;
        $sql_count = "select count(1)".$sql_from.$sql_where;
        
        $count = $this->query($sql_count); // 查询满足要求的总记录数
        $Page = new \Think\Page($count[0][0], $pagesize); // 实例化分页类 传入总记录数和每页显示的记录数
        $show = $Page->show(); // 分页显示输出
        // 进行分页数据查诟 注意limit 方法癿参数要使用Page 类的属性
        if (empty($limit)) {
            $sql.=' limit '.$Page->firstRow.','.$Page->listRows;
        } else {
            $start = ($limit - 1) * $pagesize;
            $end = $pagesize * $limit;
            $sql.=' limit '.$start.','.$end;
        }

        $list = $this->query($sql);
        return array(
            'list' => $list,
            'page' => $show
        );
    }
    
    /**
     * 获取商品收藏数量
     * @param int $goods_id 商品ID
     * @return int 收藏总数量
     */
    public function count_fav($goods_id) {
        return $this->where(array('goods_id'=>$goods_id))->count();
    }
	
	
	    /**
     * 判断该会员是否收藏该商品
     * @param int $goods_id 商品ID
	 * @param int $item_id 规格ID
     * @return int member_id 会员id
     */
    public function member_is_fav($goods_id,$item_id,$member_id=0) {
        $count =  $this->where(array('goods_id'=>$goods_id,'item_id'=>$item_id,'member_id'=>$member_id))->count();
		return $count;
    }


}

?>