<?php

namespace Phone\Model;

use Think\Model;

class GoodsCommentModel extends Model {

    /**
     * 增加评价记录
     * @param int $member_id 会员ID
     * @param int $id 购物车ID
     * @param int $star 评价星级
     * @param string $detail 评价内容
     */
    public function add_comment($member_id,$id,$star,$detail) {
        $OrderGoods = M('order_goods');
        $model = $OrderGoods->where(array('member_id'=>$member_id))->find($id);
        if(empty($model)) {
            return -2;
        }
        if($model['is_comment'] == 1) {
            return -3;
        }
        $data['cart_id'] = $id;
        $data['member_id'] = $member_id;
        $data['order_id'] = $model['order_id'];
        $data['goods_id'] = $model['goods_id'];
        $data['item_id'] = $model['goods_item_id'];
        $data['star'] = $star;
        $data['detail'] = $detail;
        $data['create_ip'] = get_client_ip();
        $data['create_time'] = time();
        $data['count_up'] = 0;
        $data['count_down'] = 0;
        $data['status'] = 2;//默认直接显示 1未审核  2已审核
        $flag = $this->add($data);
        if($flag > 0) {
            $OrderGoods->where(array('member_id'=>$member_id,'id'=>$id))->setField('is_comment',1);//设置订单商品为以评论
            //赠送积分
            $MemberPointObj = new \Fwadmin\Model\MemberPointModel();
            $config = S('config');
            $MemberPointObj->add_by_comment($flag,$member_id, $config['point_comment']);
        }
        return $flag;
    }

    /**
     * 获取待评价商品列表
     */
    public function get_need_comment_list($member_id,$pagesize = 10) {
        $OrderGoodsObj = M('order_goods');
		$OrderObj = M('order');
		$order_id_array = $OrderObj->where(Array('member_id'=>$member_id,'status_pay'=>3))->getField('order_id',true);//获取已支付的订单id
		if(!empty($order_id_array)){
		$map['order_id'] = array('in',$order_id_array);
		}
        $map['member_id'] = $member_id;
        $map['is_comment'] = 0;
        //排序
		
        $order = 'id desc';
        
        $count = $OrderGoodsObj->where($map)->count(); // 查询满足要求的总记录数

        $Page = new \Think\Page($count, $pagesize); // 实例化分页类 传入总记录数和每页显示的记录数
        //分页跳转的时候保证查询条件
        foreach ($conditions as $key => $val) {
            $Page->parameter[$key] = $val;
        }
        $show = $Page->show(); // 分页显示输出
        //获取分页数据
        $list = $OrderGoodsObj->where($map)->order($order)->limit($Page->firstRow . ',' . $Page->listRows)->select();
        
        $OrderObj = M('order');
        foreach($list as $k => $v) {
            $list[$k]['time_create'] = $OrderObj->where('order_id='.$v['order_id'])->getField('time_create');
        }
        
        return array(
            'list' => $list,
            'page' => $show,
            'count' => $count
        );
    }
    
    /**
     * 获取待审核商品数量
     */
    public function count_need_comment($member_id) {
        $OrderGoodsObj = M('order_goods');
        $map['member_id'] = $member_id;
        $map['is_comment'] = 0;
        return $OrderGoodsObj->where($map)->count();
    }
    
    /**
     * 搜索符合条件的记录
     * @param int $member_id 会员ID
     * @param int $pagesize 分页数
     */
    public function search_member($member_id,$pagesize = 5) {
        $map['member_id'] = $member_id;
        //排序
        $order = 'id desc';
        
        $count = $this->where($map)->count(); // 查询满足要求的总记录数

        $Page = new \Think\Page($count, $pagesize); // 实例化分页类 传入总记录数和每页显示的记录数
        //分页跳转的时候保证查询条件
        foreach ($conditions as $key => $val) {
            $Page->parameter[$key] = urlencode($val);
        }
        $show = $Page->show(); // 分页显示输出
        //获取分页数据
        $list = $this->where($map)->order($order)->limit($Page->firstRow . ',' . $Page->listRows)->select();
        
        $OrderGoodsObj = M('order_goods');
        foreach($list as $k => $v) {
			$model =  $OrderGoodsObj->where(array('id'=>$v['cart_id']))->find();
			$list[$k]['goods_picture'] = $model['goods_picture'];
			$list[$k]['goods_name'] =  $model['goods_name'];
			$list[$k]['goods_spec'] =  $model['goods_spec'];
			$list[$k]['goods_num'] =  $model['goods_num'];
			$list[$k]['goods_price'] =  $model['goods_price'];
			$list[$k]['order_no'] =  $model['order_no'];
           
        }
        
        return array(
            'list' => $list,
            'page' => $show,
            'count' => $count
        );
    }
    
    /**
     * 搜索符合条件的记录
     * @param int $goods_id 商品ID
     * @param int $pagesize 分页数
     */
    public function search($goods_id,$pagesize = 10) {
        //排序
        $order = 'id desc';
        $map['goods_id'] = $goods_id;
		$map['status'] = 2;
        $count = $this->where($map)->count(); // 查询满足要求的总记录数

        $Page = new \Think\Page($count, $pagesize); // 实例化分页类 传入总记录数和每页显示的记录数
        //分页跳转的时候保证查询条件
        foreach ($conditions as $key => $val) {
            $Page->parameter[$key] = urlencode($val);
        }
        $show = $Page->show(); // 分页显示输出
        //获取分页数据
        $list = $this->where($map)->order($order)->limit($Page->firstRow . ',' . $Page->listRows)->select();
        
         $total_page = $Page->totalPages; //总页数
        if(empty($total_page)) {
            $total_page = 0;
        }
        
        return array(
            'list' => $list,
            'page' => $show,
            'count' => $count,
            'total_page' =>$total_page
        );
    }
    
    /**
     * 商品评价数
     * @param int $goods_id 商品ID
     * @return int 数量
     */
    public function count_goods($goods_id) {
        return $this->where(array('goods_id'=>$goods_id,'status'=>2))->count();
    }
    
    /**
     * 计算商品差评数
     * @param int $goods_id 商品ID
     * @return int 数量
     */
     public function count_star_12($goods_id) {
        return $this->where(array('goods_id'=>$goods_id,'star'=>array('in','1,2'),'status'=>2))->count();
     }
     
     /**
     * 计算商品中评数
     * @param int $goods_id 商品ID
     * @return int 数量
     */
     public function count_star_3($goods_id) {
        return $this->where(array('goods_id'=>$goods_id,'star'=>3,'status'=>2))->count();
     }
     
     /**
     * 计算商品好评数
     * @param int $goods_id 商品ID
     * @return int 数量
     */
     public function count_star_45($goods_id) {
        return $this->where(array('goods_id'=>$goods_id,'star'=>array('in','4,5'),'status'=>2))->count();
     }
    
    /**
     * 计算商品平均星级
     * @param int $goods_id 商品ID
     * @return int 星级
     */
    public function comment_star($goods_id) {
        $star = $this->where(array('goods_id'=>$goods_id,'status'=>2))->avg('star');
        return floor($star);
    }
    
   

}

?>