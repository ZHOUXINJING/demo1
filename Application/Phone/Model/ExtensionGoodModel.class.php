<?php
namespace Phone\Model;

use Think\Model;

class ExtensionGoodModel extends  Model
{
    /**
     * 查询
     *
     * @param $id
     * @param int $status
     * @return mixed
     */
    public function getListGood($id, $status = 1)
    {
        $today = $this->today();
        if ($status == 1 || empty($status)) {
            $map['shop_extension_good.ex_create_time']  =  array(array('gt', $today[0]), array('lt', $today[1]), 'and');
        } else {
            $map['shop_extension_good.ex_create_time']  =  array('lt', $today[0]);
        }

        return  $this->where($map)
                     ->where(array('shop_extension_good.ex_p_id' => $id))
                     ->join('shop_goods ON shop_goods.goods_id = shop_extension_good.ex_good_id')
                     ->join('shop_member ON shop_member.member_id = shop_extension_good.ex_s_id')
                     ->order('shop_extension_good.ex_create_time desc')
                     ->field('shop_goods.goods_name,shop_extension_good.ex_create_time,shop_extension_good.ex_money,
                     shop_member.username,shop_member.phone
                     ')
                     ->select();
    }


    /**
     * 开始时间 | 结束时间
     *
     * @return array
     */
    public function today()
    {
        $start_time = strtotime(date("Y-m-d",time()));
        $end_time   =  $start_time + 60 * 60 * 24;
        return array(date("Y-m-d H:i:s", $start_time), date("Y-m-d H:i:s", $end_time));
    }
}