<?php
namespace Phone\Controller;
use Think\Controller;
use Common\Controller\PhoneController;
use Phone\Model\MemberModel;
class AjaxController extends PhoneController {
	
	
	
  	//手机注册
	public function register(){
	 $phone = I('phone');
	 $code = I('code');
	 $password = I('password');
	 $repassword = I('repassword');
	 $phonecode = session('phonecode');
	 $phone2 = session('register_phone');//注册手机
	 if($phone!=$phone2){
		$this->error('请使用发送的验证码的手机号注册'); 
	 }elseif($phonecode==''&&$code!=''){
		 $this->error('验证码已过期');
		 }elseif($phonecode!=$code){
	   $this->error('验证码不正确');
	 }else{
		 $MemberObj = new MemberModel();
		if($phone!='' && $MemberObj->exist_phone($phone, 0)>0) {
            $result['title']='false';
            $this->error('此手机号已被使用');
		   
        } elseif ($password == '') {
           $this->error('请填写密码');
            
        }elseif($repassword==''){
			$this->error('请填写确认密码');
			
		}elseif($password!=$repassword){
		$this->error('两次输入的密码不一致');
		}
	    // $flag = $MemberObj->register($phone,$password);
	     $member_id = $MemberObj->register($phone,'', $password);
            if($member_id > 0) {
             $MemberObj->login($phone, $password, 0); 
			$this->success('注册成功！'); 
            } else {
				$this->error('系统繁忙，注册失');
            }
	   
		
	 } 
	}
	
	  	//邮箱注册
	public function register_email(){
	 $email = I('email');
	 $code = I('code3');
	 $password = I('password3');
	 $repassword = I('repassword3');
	 $emailcode = session('emailcode');
	
	 if($emailcode!=$code){
	   $this->error('验证码不正确');
	 }else{
		 $MemberObj = new \Fwadmin\Model\MemberModel();
		if($email!='' && $MemberObj->existEmail($email, 0)>0) {
            $result['title']='false';
            $this->error('此邮箱已被使用');
		   
        } elseif ($password == '') {
           $this->error('请填写密码');
            
        }elseif($repassword==''){
			$this->error('请填写确认密码');
			
		}elseif($password!=$repassword){
		$this->error('两次输入的密码不一致');
		}
	   $flag = $MemberObj->register_email($email,$password);
	    if($flag>0){
		session('member_id',$flag);
		$this->success('注册成功！');  	
		}else{
		$this->error('注册失败！');  
		}
		
	 } 
	}


  //绑定手机注册
	public function register_bind_phone(){
	 $phone = I('phone');
	 $code = I('code');
	 $password = I('password');
	 $repassword = I('repassword');
	 $phonecode = session('phonecode');
	 
	 if($phonecode!=$code){
	   $this->error('验证码不正确');
	 }else{
		 $MemberObj = new \Fwadmin\Model\MemberModel();
		if ($password == '') {
           $this->error('请填写密码');
            
        }elseif($repassword==''){
			$this->error('请填写确认密码');
			
		}elseif($password!=$repassword){
		$this->error('两次输入的密码不一致');
		}
	   $flag = $MemberObj->register_bind_phone($phone,$password);
	    if($flag>0){
		session('member_id',$flag);
		$this->success('注册成功！');  	
		}else{
		$this->error('注册失败！');  
		}
		
	 } 
	}
	



	
	//登录
	public function login(){
	   $phone = I('mobile');
	   $password = I('password2');
	   $remember = I('remember');
       $ModelObj = new \Fwadmin\Model\MemberModel();
	   $flag = $ModelObj->login($phone,$password,$remember);
	    if($flag>0){
		$this->success('登录成功！');  	
		}else{
		$this->error('手机号或密码不正确！');  
		}

	}
	

   function findpwd(){
   
      if (IS_AJAX) {
            //这里有个问题就是不断刷新生会生成很多次验证码，设计的版面很有问题，建议参考http://www.moneydai.com/User/forget.html
            $MemberObj = new MemberModel();
            $phone = I('post.phone');
			 $code = I('code');
			$result['title'] = 'success'; 
	
	  $phone2 = session('find_phone');//注册手机
	 if($phone!=$phone2){
		$this->error('请使用发送的验证码的手机号找回'); 
	 }elseif ($MemberObj->exist_phone($phone, 0) > 0) {
	       $phonecode = session('phonecode');
			  if($code==$phonecode){
				 session('valid_phone',$phone);
				 session('valid_code_sus',$code);
				$this->success('验证码正确！'); 
			    }elseif($phonecode==''){
					$this->error('短信验证码已过期'); 
					}else{
					$this->error('短信验证码错误'); 
			  
			     }
           } else {
			    $result['title'] = 'false';
				$this->error('手机号码不存在'); 
              
                }		

          

        }
   }
   
   public function resetpwd(){
   
   if (IS_POST) {

        $MemberObj = new MemberModel();
        $phone =  session('valid_phone');
        $newpassword = I('post.newpassword', '');
        $repassword = I('post.repassword', '');
        $result['title'] = 'success'; 
             if ($MemberObj->exist_phone($phone, 0) <= 0) {
                $this->error('手机号码不存在'); 

            }

        //如果都没问题，继续判断，有问题就不用判断下面的

            if ($newpassword == '') {
				$this->error('请填写新密码'); 
            }elseif ($newpassword != $repassword) {
                 $this->error('两次输入的密码不一致'); 
            } else {
               $flag = $MemberObj->reset_password($phone, $newpassword);

                if ($flag > -1) {
                  $this->success('修改成功'); 
                } else {
              $this->error('修改失败'); 
                }

       

        }

            $this->ajaxReturn($result); 

	}
   }


	
	
	//发送短信,新用户注册
   public function sendmsg(){
	$code= rand(100000,999999);
	$phone = I('phone');
	$reData['status']=0;
	$reData['info']='发送成功!';//$code
	 //$this->sendSmstest($phone,$code);
    $MemberObj = new MemberModel();
	if($phone!='' && $MemberObj->exist_phone($phone, 0)>0) {
            $result['title']='false';
            $this->error('你已是我们网站的会员了！');
     } 
	  $register_phone = S('register_phone'.$phone);
	  if($register_phone){
	  $result['title']='false';
      $this->error('请不要重复发送验证码');      
	  }else{
	  S('register_phone'.$phone, $phone,60);//一分钟内只能发送一次
	   D('Qcloudsms', 'Service')->sendMsg($phone, $code);

	  }
   
	
	if($reData['status']==0){
		
   	 session('phonecode',$code);
	 session('register_phone',$phone);
	 $this->success($reData['info']);
	}else{
	 $this->error($reData['info']);
	 }
	}
	
	//发送找回密码验证码
   public function sendmsg2(){
	   if(IS_AJAX){
	$code= rand(100000,999999);
	$phone = I('phone');
	$reData['status']=0;
	$reData['info']='发送成功!';//.$code
	$MemberObj = new MemberModel();
	if($MemberObj->exist_phone($phone, 0)<=0) {
            $result['title']='false';
            $this->error('该手机号不存在');  
     } 
	 
	   $find_phone = S('find_phone'.$phone);
	  if($find_phone){
	  $result['title']='false';
      $this->error('请不要重复发送验证码');      
	  }else{
	  S('find_phone'.$phone, $phone,60);//一分钟内只能发送一次
	  D('Qcloudsms2', 'Service')->sendMsg($phone, $code);
	  }
	
    //D('Qcloudsms', 'Service')->sendMsg($phone, $code);
	if($reData['status']==0){
	
	 session('phonecode',$code);
	 session('find_phone',$phone);	
	 $this->success($reData['info']);
	}else{
	 $this->error($reData['info']);
	 }
	   }
	}
	
	
	//发送短信,绑定手机号
	public function sendmsg3(){
    if(IS_AJAX){
	$code= rand(100000,999999);
	$phone = I('phone');
	$reData['status']=0;
	$reData['info']='发送成功!';
	 //$this->sendSmstest($phone,$code);
	 
    D('Qcloudsms2', 'Service')->sendMsg($phone, $code);
	
	if($reData['status']==0){
   	 session('phonecode',$code);
	 $this->success($reData['info']);
	}else{
	 $this->error($reData['info']);
	 }
	   }
	}
	
	
	
	


	public function verify() {
	    ob_clean();
		layout(false);
		$Verify = new \Think\Verify();
		$Verify->fontSize = 16;
		$Verify->useImgBg = false;
		$Verify->length = 4;
		$Verify->useNoise = false;
		$verify->useCurve = true;
		$Verify->codeSet = '0123456789ABCDEFGHIJKLNMOPQRSTUXYZ';
		$Verify->entry();
	}
	
	
	/******获取消息数****/
	
    public function message_count() {
       $MemberMessageObj = new \Phone\Model\MemberMessageModel();
	    $member_id = session('member_id');
        if (empty($member_id)) {
		echo '0';
		}else{
        echo $MemberMessageObj->count_can_look($member_id);
		}
    }
	
	
	
	/******用户留言*****/
	public function feedback(){
    if (IS_POST) {
	 
	 $result['title']='success';	
	  $member_id = session('member_id'); 
	 $data['nickname'] = I('post.realname_s','');
	 $data['member_id'] = $member_id;
	 $data['phone'] = I('post.phone_s','');
	 $data['detail'] = I('post.detail_s','');
	 $data['picture'] = I('post.picture','');
     $data['create_ip'] = getIP(); 
	 $data['create_time'] = time();
	
	 if (empty($member_id)) {
	     $result['title']='false';
        $result['msg']='请先登录！';
	 }else{
	 $flag = D('feedback')->data($data)->add();
	  if($flag>0){
	   $result['msg']='感谢您的留言，我们会尽快联系您！';
	  }else{
	    $result['title']='false';
        $result['msg']='留言失败！';
	  }
	 }
	  $this->ajaxReturn($result);	
     }
		
	
			
	
	}
	
	
	
}
