<?php

namespace Phone\Controller;

use Think\Controller;
use Common\Controller\PhoneController;

class AddressController extends PhoneController {

   public function province_list(){
   $ProvinceObj = M('AddressProvince');
    $str = '';
	$Prolist= $ProvinceObj->order('id asc')->select();
     foreach($Prolist as $k =>$v){
	 $str.= ' <a onclick="get_city(\''.$v['province'].'\');">'.$v['province'].'</a>';
	 }
	 echo $str;
     exit();
   }

    //获取城市选择
    public function city_sel($province,$city,$cityid='') {
        $ProvinceObj = M('AddressProvince');
        $CityObj = M('AddressCity');
        $provinceid = $ProvinceObj->getFieldByProvince($province,'provinceid');
        $str = '';
        if($province == "") exit();
        if(!empty($provinceid)) {
            $city_sel = $CityObj->field('city')->where('provinceid='.$provinceid)->select();
            $str = '<select name="city'.$cityid.'" id="city'.$cityid.'" class="share_sel_1" onchange="change_city'.$cityid.'()">';
            $str .= '<option value="">选择城市</option>';
            foreach($city_sel as $c) {
                if($c['city'] == $city) {
                    $str .= '<option selected="selected" value="'.$c['city'].'">'.$c['city'].'</option>';
                } else {
                    $str .= '<option value="'.$c['city'].'">'.$c['city'].'</option>';
                }
            }
            $str .= '</select>';
        }
        echo $str;
        exit();
    }
	
	
	
	    //获取城市选择
   public function city_sel2($province) {
        $ProvinceObj = M('AddressProvince');
        $CityObj = M('AddressCity');
        $provinceid = $ProvinceObj->getFieldByProvince($province,'provinceid');
        $str = '';
        if($province == "") exit();
        if(!empty($provinceid)) {
            $city_sel = $CityObj->field('city')->where('provinceid='.$provinceid)->select();
	        foreach($city_sel as $c) {
			$str.='<a onclick="get_area(\''.$c['city'].'\');">'.$c['city'].'</a>';
			}
			
        }
        echo $str;
        exit();
    }
	
	
	  public function area_sel2($city) {
        $CityObj = M('AddressCity');
        $AreaObj = M('AddressArea');
        if($city == "") exit();
        $cityid = $CityObj->getFieldByCity($city,'cityid');
        $str = '';
        if(!empty($cityid)) {
            $area_sel = $AreaObj->field('area')->where('cityid='.$cityid)->select();
			
            foreach($area_sel as $a) {
                      $str.='<a onclick="get_zip(\''.$a['area'].'\');">'.$a['area'].'</a>';
            
            }
          
        }
        echo $str;
        exit();
    }
    
	
    
    //获取地区选择
    public function area_sel($city,$area,$areaid='') {
        $CityObj = M('AddressCity');
        $AreaObj = M('AddressArea');
        if($city == "") exit();
        $cityid = $CityObj->getFieldByCity($city,'cityid');
        $str = '';
        if(!empty($cityid)) {
            $area_sel = $AreaObj->field('area')->where('cityid='.$cityid)->select();
            $str = '<select name="area'.$areaid.'" id="area'.$areaid.'" class="share_sel_1" onchange="change_area'.$areaid.'()">';
            $str .= '<option value="">选择地区</option>';
            foreach($area_sel as $a) {
                if($a['area'] == $area) {
                    $str .= '<option selected="selected" value="'.$a['area'].'">'.$a['area'].'</option>';
                } else {
                    $str .= '<option value="'.$a['area'].'">'.$a['area'].'</option>';
                }
            }
            $str .= '</select>';
        }
        echo $str;
        exit();
    }
    
    //获取邮政编码
    public function get_zip($area) {
        $AreaObj = M('AddressArea');
        $zipcode = $AreaObj->getFieldByArea($area,'zipcode');
        echo $zipcode;
        exit();
    }


}
