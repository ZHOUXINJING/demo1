<?php

namespace Phone\Controller;

use Think\Controller;
use Common\Controller\PhoneController;

class CartController extends PhoneController {

    /**
     * 购物车首页
     */
    public function index() {
        $CartObj = new \Phone\Model\CartModel();
        $cart_list = $CartObj->get_cart(); 
        foreach($cart_list as $k => $v) {
            $cart_list[$k]['total'] = tofixed($v['price'] * $v['num'],2);
        }
        $this->cart_list = $cart_list;
		$this->cart_count = count($cart_list);
        $this->cart_total = $CartObj->get_total();
		$this->seo=array('seo_title'=>'购物车','seo_keywords'=>'购物车','seo_description'=>'购物车');
		 
		 //最近浏览记录
		$ModelGoods = new \Phone\Model\GoodsModel();
		$this->history_list = $ModelGoods->history_show(5);
        $this->display();
    }
    
    
    /**
     * 加入购物车
     * @param int $goods_id 商品ID
     * @param int $item_id 单件ID
     * @param int $num 购物车数量
     * @return int 等于1表示加入成功  否则失败
     */
    public function add_cart($goods_id,$item_id,$num) {
        $CartObj = new \Phone\Model\CartModel();
        echo $CartObj->add_cart($goods_id,$item_id,$num);
    }
    
    /**
     * 修改购物车价格
     * @param int $cart_id 购物车ID
     * @param int $num 数量
     */
    public function edit_num($cart_id,$num) {
        $CartObj = new \Phone\Model\CartModel();
        echo $CartObj->edit_num($cart_id, $num);
    }
    
    /**
     * 删除购物车
     * @param int $cart_id 购物车ID
     */
    public function del($cart_id) {
		
        $CartObj = new \Phone\Model\CartModel();
        echo $CartObj->del($cart_id);
    }
    
    /**
     * 
     */
    public function delsel() {
        $CartObj = new \Phone\Model\CartModel();
        $sel = I('post.sel');
        $CartObj->del_sel($sel);
        return redirect("index");
    }
    
    /**
     * 清空购物车
     */
    public function clear() {
        $CartObj = new \Phone\Model\CartModel();
        echo $CartObj->clear();
    }
    
    /**
     * 获得购物车总价
     */
    public function total() {
         $id = I('id','');
        $CartObj = new \Phone\Model\CartModel();
        echo $CartObj->get_total($id);
    }
    
	
	    public function total2() {
        $CartObj = new \Phone\Model\CartModel();
        echo $CartObj->get_total();
    }
    
	
    /**
     * 检查购物车价格
     */
    public function check_store_num() {
        $CartObj = new \Phone\Model\CartModel();
        if($CartObj->check_store_num()) {
            echo '1';
        } else {
            echo '0';
        }
    }
    
    /**
     * 获得购物车件数
     */
    public function num_count() {
        $CartObj = new \Phone\Model\CartModel();
        echo $CartObj->count_row();
    }

}
