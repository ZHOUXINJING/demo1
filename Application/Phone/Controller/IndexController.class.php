<?php

namespace Phone\Controller;

use Think\Controller;
use Common\Controller\PhoneController;

class IndexController extends PhoneController {

    //首页
    public function index() {
        
        //推荐品牌
        $GoodsBrandObj = new \Fwadmin\Model\GoodsBrandModel();
        $brand_tuijian = $GoodsBrandObj->get_tuijian();
        $this->brand_tuijian = $brand_tuijian;
      
        //图片切换Banner
        $BannerObj = new \Phone\Model\ConBannerModel();
        $this->banner = $BannerObj->get_list('phone_index');
		
		
      
        //首页模块
        $IndexBlockObj = new \Phone\Model\IndexBlockModel();
        $index_block = $IndexBlockObj->show_html(2);
		
        $this->index_block = $index_block;
		
		$this->index_product = M('goods')->field('goods_id,goods_name,cuxiao_msg,picture,price')->where(Array('is_index'=>1,'status'=>1))->order('order_id asc,goods_id desc')->limit(8)->select();
		
        
         $guide_pages = session('guide_pages');
		 if($guide_pages!=1){
			  $this->redirect('Main/index');
		 }
       $this->display();
        
    }
    


}
