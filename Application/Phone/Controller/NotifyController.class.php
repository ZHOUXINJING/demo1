<?php

namespace Phone\Controller;

use Think\Controller;
use Phone\Model\MemberModel;
use Phone\Model\ExtensionModel;
use Phone\Model\ExtensionOpenModel;

class NotifyController extends  Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->em  = new ExtensionModel();
        $this->mm  = new MemberModel();
        $this->eom = new ExtensionOpenModel();
    }


    /**
     *  微信回调
     */
    public function wechatNotify()
    {
        vendor('PayWechat.PayNotifyCallBack');
        vendor('PayWechat.Weixin');
      
        $notify = new \Vendor\PayWechat\PayNotifyCallBack();
        $xml    = file_get_contents("php://input");
        $array  = $notify->xmlToArray($xml);
        if ($array['result_code'] == 'SUCCESS' && $array['return_code'] == 'SUCCESS') {
			$out_trade_no = explode('-1111-', $array['out_trade_no'])[0];
            $member       = $this->mm->where(array('ex_invitation' => $out_trade_no))->find();
          
          	if ($member['ex_level']) {
                exit();
            }
          
            $register     = $this->em->get('ex_register');
            $divide       = $this->em->get('ex_divide');

            echo $this->writeData($member, $register, $divide);
        }
    }

    // 支付回调
    public function aliNotify()
    {
        vendor('Alipay.corefunction');
        vendor('Alipay.md5function');
        vendor('Alipay.notify');
        vendor('Alipay.submit');

        $alipay_config = C('alipay_config');
        $alipayNotify  = new \Vendor\Alipay\AlipayNotify($alipay_config);
        $verify_result = $alipayNotify->verifyNotify();
        if ($verify_result) {
            if ($_POST['trade_status'] == 'TRADE_SUCCESS') {
                $out_trade_no = explode('-1111-', $_POST['out_trade_no'])[0];
                $member       = $this->mm->where(array('ex_invitation' => $out_trade_no))->find();

                if ($member['ex_level']) {
                    exit();
                }

                $register     = $this->em->get('ex_register');
                $divide       = $this->em->get('ex_divide');

                echo $this->writeData($member, $register, $divide);
            }
        } else {
            echo "fail";
        }
    }



    /**
     *  写入数据
     */
    public function writeData($member, $register, $divide)
    {
        $this->mm->where(array('member_id' => $member['member_id']))->save(array('ex_level' => 1));

        if (!empty($member['ex_uid'])) {
//            $this->mm->where(array('member_id' => $member['ex_uid']))->setInc('ex_money', $divide);

//            $this->eom->data(array(
//                'ex_u_id'       =>  $member['member_id'],
//                'ex_p_id'       =>  $member['ex_uid'],
//                'ex_p_money'    =>  $divide,
//                'ex_u_money'    =>  $register,
//                'ex_create_time'=>  date('Y-m-d H:i:s', time())
//            ))->add();

            // 升级代理
            $data   = $this->mm->where(array('ex_uid' => $member['ex_uid']))->select();
            $level2 = $this->em->get('ex_condition_2');
            $level3 = $this->em->get('ex_condition_3');
            if (count($data) >= $level3) {
                $level = 3;
            }elseif (count($data) >= $level2) {
                $level = 2;
            } else {
                $level = 1;
            }
            $this->mm->where(array('member_id' => $member['ex_uid']))->save(array('ex_level' => $level));
        }

        // 充值记录表
        $er = D('ExtensionRecord');
        $er->data(array(
            'u_id'        =>  $member['member_id'],
            'u_money'     =>  $register,
            'create_time' =>  time()
        ))->add();

       return "success";
    }
}