<?php

namespace Phone\Controller;

use Phone\Model\ExtensionModel;
use Think\Controller;
use Common\Controller\PhoneController;
use Phone\Model\MemberModel;
use Phone\Model\MemberInfoModel;
use Fwadmin\Model\TuihuanModel;
use Phone\Model\ExtensionOpenModel;
use Phone\Model\ExtensionGoodModel;

class MemberController extends PhoneController {
    
    public function __construct() {
        parent::__construct();
        $this->member_id = session('member_id');
		  $MemberObj = new MemberModel();
	    
		$member_arr = $MemberObj->where(Array('member_id'=>$this->member_id))->find();
        if (empty($this->member_id)||empty($member_arr)) {
            redirect(U('Register/login') . '?ret=' . urlencode(__SELF__));
        }
		$member_arr['member_name'] = $MemberObj->show_member_name($this->member_id);
		$this->member_arr = $member_arr;
		
		$this->headpic = $member_arr['headpic'];//会员头像
		
		$this->seo=array('seo_title'=>'会员中心','seo_keywords'=>'会员中心','seo_description'=>'会员中心');
   
      
        }
    
    //会员中心首页
    public function index() {
		/*
        $MemberObj = new MemberModel();
        $member = $MemberObj->find($this->member_id);
        $this->headpic = $MemberObj->get_headpic($this->member_id);//会员头像
        
        $MemberLevelObj = new \Phone\Model\MemberLevelModel();
        $this->member_level = $MemberLevelObj->get_level_name($member['level_id']);//会员等级
        $this->member = $member;
        
        //最近订单
        $ModelObj = new \Fwadmin\Model\OrderModel();
        $results = $ModelObj->search_member($this->member_id,$conditions,'order_id desc',10);
        $lists = $results['list'];
        foreach($lists as $k => $v) {
            $lists[$k]['status_str'] = $ModelObj->show_status_member($v['status'], $v['status_pay'], $v['status_delivery'],$v['pay_type']);//订单状态
            $lists[$k]['total_show'] = $v['total'] - $v['total_discount'];//显示价格
            $lists[$k]['pay_type_str'] = $ModelObj->get_pay_type($v['pay_type']);//支付方式
            $lists[$k]['goods'] = $ModelObj->show_goods_pic($v['order_id']);//订单图片数组
        }
        $this->assign('list', $lists);
        
        $this->count_need_pay = $ModelObj->count_need_pay($this->member_id); //待付款订单
        $this->count_need_complete = $ModelObj->count_need_complete($this->member_id); //待完成订单
        $GoodsCommentObj = new \Phone\Model\GoodsCommentModel();
        $this->count_need_comment = $GoodsCommentObj->count_need_comment($this->member_id); //待评论订单
        $MemberYhqObj = new \Phone\Model\MemberYhqModel();
        $this->count_youhuiquan = $MemberYhqObj->count_can_use($this->member_id);
        $this->left_menu = 1;
        $this->display();
		*/
        $conditions = array();//搜索条件

        $MemberObj = new MemberModel();
        $point = $MemberObj->get_point($this->member_id);

        $MemberYhqObj = new \Phone\Model\MemberYhqModel();
        $yhj = $MemberYhqObj->search($this->member_id,$conditions);

        $FavObj  = new \Phone\Model\MemberFavModel();
        $storage = $FavObj->search_member($this->member_id);

        $this->assign('storage', count($storage['list']));
        $this->assign('yhj', count($yhj['list']));
        $this->assign('point', $point);

        $this->display();
    }

    // 游览记录
    public function search_list()
    {
        $ModelGoods = new \Phone\Model\GoodsModel();
        $history_list = $ModelGoods->history_show();


        $this->assign('history_list', $history_list);
        $this->display();
    }

    // 意见反馈
    public function feedback()
    {
        if (IS_POST) {
            $post = I('post.');

            $data['nickname'] = $this->member_arr['nickname'];
            $data['member_id'] = $this->member_id;
            $data['phone'] = $this->member_arr['phone'];
            $data['detail'] = $post['content'];
            $data['picture'] = $post['img_data'];
            $data['create_ip'] = getIP();
            $data['create_time'] = time();

            D('feedback')->data($data)->add();

            return redirect('index');
        }
        $this->display();
    }

    // 意见反馈上传
    public function feedback_upload() {
        $base64=file_get_contents("php://input"); //获取输入流
        $base64=json_decode($base64,1);
        $data = $base64['base64'];
        preg_match("/data:image\/(.*);base64,/",$data,$res);
        $ext = $res[1];
//        if(!in_array($ext,array("jpg","jpeg","png","gif","JPG","JPEG","PNG","GIF"))){
//            echo json_encode(array("error"=>2,"msg"=>$ext));die;
//        }
        //if(length($data)<=10){echo json_encode(array("error"=>2,"msg"=>$data));die;}
        if($ext == null){$ext = 'jpeg';}
        $savepath = 'Uploads/detail/headpic/'.time().'/';
        $savepath2 = '/Uploads/detail/headpic/'.time().'/';
        if(!is_file($savepath)) {mkdir($savepath,0777);}//判断存诸文件夹里否存在，不存在则创建文件夹
        //$savename = uniqid().'.'.$ext;
        $savename = 'headpic.jpg';//.$ext;

        $data2 = preg_replace("/data:image\/(.*);base64,/","",$data);
        $file_length = strlen($data);
        $data2 = base64_decode($data2);
        $file_length2 = strlen($data2);
        if($file_length2 <= 0) {
            $data2 = preg_replace("/data:image\/jpeg;base64,/","",$data);
            $data2 = base64_decode($data2);
        }
        if (file_put_contents($savepath.$savename,$data2)===false) {
            echo json_encode(array("error"=>1,"msg"=>$ext.$file_length));
        }else{
            /*
             $thumbpath = 'thumb/'.$savepath;
             if(!is_file($thumbpath)) {mkdir($thumbpath,0777);}
             $image = new \Think\Image();
             $image->open('./' .  $savepath.$savename);
             $image->thumb(240, 240, \Think\Image::IMAGE_THUMB_CENTER)->save($thumbpath.$savename);*/
            echo json_encode(array("error"=>0,"src"=>"/".$savepath.$savename,"src2"=>$savepath2.$savename,"msg"=>$ext.$file_length.'-'.$file_length2));

        }
    }


    
    //资料修改
    public function edit() {
        $MemberObj = new MemberModel();
        $MemberInfoObj = new MemberInfoModel();
        if (IS_POST) {
			/*
            $username = I('post.username','');
            if (!empty($username) && $MemberObj->exist_username($username, $this->member_id) > 0) {
                echo '用户名已被使用！';
                exit();
            } else {
                $MemberObj->where('member_id=' . $this->member_id)->setField('username', $username);
            }
			*/
            $nickname = I('post.nickname','');
            if (!empty($nickname) && $MemberObj->exist_nickname($nickname, $this->member_id) > 0) {
                echo '昵称已被使用！';
                exit();
            } else {
                $MemberObj->where('member_id=' . $this->member_id)->setField('nickname', $nickname);
            }
            $data['sex'] = I('post.sex');
            $year = I('post.year');
            $month = I('post.month');
            $day = I('post.day');
            if ($year != '' && $month != '' && $day != '') {
                $data['birthday'] = date('Y-m-d', strtotime($year . '-' . $month . '-' . $day));
            }
            $data['realname'] = trim(I('post.realname'));
            $data['province'] = I('post.province');
            $data['city'] = I('post.city');
            $data['area'] = I('post.area');
            $data['address'] = trim(I('post.address'));
            $data['zip'] = trim(I('post.zip'));
            $data['tel'] = trim(I('post.tel'));
            $data['qq'] = trim(I('post.qq'));
            $data['member_id'] = $this->member_id;
            $flag = $MemberInfoObj->save($data);
            if ($flag !== false) {
                echo 'success';
            } else {
                echo '保存失败';
            }
            exit();
        }
        $member = $MemberObj->find($this->member_id);
        $memberinfo = $MemberInfoObj->find($this->member_id);
        $this->member = $member;
        $memberinfo['year'] = date('Y', strtotime($memberinfo['birthday']));
        $memberinfo['month'] = date('m', strtotime($memberinfo['birthday']));
        $memberinfo['day'] = date('d', strtotime($memberinfo['birthday']));
        $this->memberinfo = $memberinfo;
        //调取省份

		
        $this->display();
    }
	
	
	public function headpic(){
	
	 $this->display();
	}
	
    
	 public function upload() {
		 $member_id = session('member_id');
        $base64=file_get_contents("php://input"); //获取输入流
        $base64=json_decode($base64,1);
        $data = $base64['base64'];
        preg_match("/data:image\/(.*);base64,/",$data,$res);
        $ext = $res[1];
//        if(!in_array($ext,array("jpg","jpeg","png","gif","JPG","JPEG","PNG","GIF"))){
//            echo json_encode(array("error"=>2,"msg"=>$ext));die;
//        }
        //if(length($data)<=10){echo json_encode(array("error"=>2,"msg"=>$data));die;}
        if($ext == null){$ext = 'jpeg';}
        $savepath = 'Uploads/member/headpic/'.$member_id.'/'; 
		 $savepath2 = '/Uploads/member/headpic/'.$member_id.'/'; 
        if(!is_file($savepath)) {mkdir($savepath,0777);}//判断存诸文件夹里否存在，不存在则创建文件夹
        //$savename = uniqid().'.'.$ext;
		$savename = 'headpic.jpg';//.$ext;
		
		$headpic = $savepath2.$savename.'?h='.time();
		$MemberObj = new MemberModel();
		$MemberObj->where('member_id=' . $this->member_id)->setField('headpic', $headpic);
        $data2 = preg_replace("/data:image\/(.*);base64,/","",$data);
        $file_length = strlen($data);
        $data2 = base64_decode($data2);
        $file_length2 = strlen($data2);
        if($file_length2 <= 0) {
            $data2 = preg_replace("/data:image\/jpeg;base64,/","",$data);
            $data2 = base64_decode($data2);
        }
        if (file_put_contents($savepath.$savename,$data2)===false) {
                echo json_encode(array("error"=>1,"msg"=>$ext.$file_length));
        }else{
           /*
		    $thumbpath = 'thumb/'.$savepath;
            if(!is_file($thumbpath)) {mkdir($thumbpath,0777);}
            $image = new \Think\Image();
            $image->open('./' .  $savepath.$savename);
            $image->thumb(240, 240, \Think\Image::IMAGE_THUMB_CENTER)->save($thumbpath.$savename);*/
            echo json_encode(array("error"=>0,"src"=>"/".$savepath.$savename,"src2"=>$savepath2.$savename,"msg"=>$ext.$file_length.'-'.$file_length2));
			
        }
    }
	
	


   //退换货列表
  public function tuihuan(){
   $typeid = I('get.typeid',1);
   $Thobj = new TuihuanModel();
   $member_id = $this->member_id ;
     $conditions['member_id'] = $member_id ;
	 $conditions['typeid'] = $typeid ;
	 $conditions['th_status'] = Array('neq',4) ;
        $results = $Thobj->search($conditions,'id desc',true,$export=false,$page=8);
        $th_list = $results['list'];
		$this->assign('empty','<div style=" text-align:center;margin-top:100px">暂无记录</div>');
		
		foreach($th_list as $k =>$v){
		 $th_list[$k]['Cart']=  M('order_goods')->where(Array('id'=>$v['cart_id']))->find();
		}
		 $this->assign('th_list', $th_list); // 赋值数据集
		 $this->typeid = $typeid;
        $this->assign('page', $results['pageShow']); // 赋值分页输出
       $this->m=1;
	   $this->display();
  }
  

	
	
	  //退货发货
  public function shenqing_tuihuan(){

  $member_id = $this->member_id ;
  $id = I('get.id',0);
  $model = M('order_goods')->where(Array('id'=>$id,'member_id'=>$member_id))->find();
  
  if(empty($model)){
  $this->redirect('Member/index');
  }

   $Tmodel = M('tuihuan')->where(Array('member_id'=>$member_id,'cart_id'=>$id))->find();

  if(!empty($Tmodel)){
	  $this->redirect('Member/tuihuan_info',Array('id'=>$Tmodel['id']));
 // $this->error('您已经申请过了！');
  }
  //$model = $Thobj->where(Array('cart_id'=>$id))->find();
  $this->model = $model;
  $this->model2 = M('order')->where(Array('order_no'=>$model['order_no']))->find();
  $this->m=1;
  $this->display();
  }
  
  
  /*****提交申请****/
  public function shenqing_tuihuan_submit(){
  
   if (IS_POST) {

	 $result['title']='success';
	 $is_shouhuo = I('is_shouhuo',0);
	 if($is_shouhuo==1){
	 $shouhuo='已收到货';
	 }else{
	 $shouhuo='未收到货'; 
	 }
	 
	 $data['shouhuo'] = $shouhuo;
	 $data['member_id'] = session('member_id');
	 $data['cart_id'] = I('cart_id',0);
	 $data['orderno']= '75'.date('ymd',time()).rand(100000,999999);
	 $data['reason'] = I('post.reason','');
	 $data['price'] = I('post.price','');
	 $data['picture'] = I('post.picture','');
	 $data['detail'] = I('post.detail','');
	 $data['th_status'] = 1;
	 $data['create_time'] = time();
	 $flag = D('tuihuan')->data($data)->add();
	if($flag>0){
	   $result['msg']='申请成功，我们会尽快联系您！';
	}else{
	   $result['title']='false';
       $result['msg']='申请失败！';
	 }

	$this->ajaxReturn($result);	
			
		}
	
  }
  
   public function kuaidi_submit(){
  
   if (IS_POST) {

	 $result['title']='success';
	 $id = I('id',0);
	 $member_id = session('member_id');
	 $data['kuaidi'] = I('kuaidi','');
	 $data['kuaidi_no'] = I('kuaidi_no','');
	 $flag = D('tuihuan')->where(Array('member_id'=>$member_id,'id'=>$id))->save($data);
	if($flag>-1){
	   $result['msg']='我们会在收到退货的第一时间完成退款，请耐心等待';
	}else{
	   $result['title']='false';
       $result['msg']='提交物流单号失败！';
	 }

	$this->ajaxReturn($result);	
			
		}
	
  }
  
  
  public function tuihuan_info(){
   $TuiObj = new TuihuanModel();
   $id = I('id');
   $member_id = $this->member_id;
   $model = $TuiObj->where(Array('member_id'=>$member_id,'id'=>$id))->find();
   //print_r($model);
   if(empty($model)){
     $this->redirect('Member/index');
   }
   $this->model = $model;
   $this->picture_list = stringToarray($model['picture']);
   $model2 = M('order_goods')->where(Array('id'=>$model['cart_id']))->find();

   $this->model2 = $model2;
   $this->model3 = M('order')->where(Array('order_no'=>$model2['order_no']))->find(); 
   
   
   $this->m=1;
   $this->display();
  }


    //密码修改
    public function change_password() {
        if(IS_POST) {
            $MemberObj = new MemberModel();
            $old_password = strongmd5(trim(I('post.old_password')));
            $new_password = strongmd5(trim(I('post.new_password')));
            //检测旧密码是否正确
            $is_right = $MemberObj->check_old_password($this->member_id, $old_password);
            if(!$is_right) {
                echo '旧密码错误！';
                exit();
            }
            //执行修改密码操作
            $flag = $MemberObj->change_password($this->member_id,$new_password);
            if ($flag > 0) {
                echo 'success';
            } else {
                echo '修改密码失败！';
            }
            exit();
        } else {
            $this->display();
        }
    }
    

    
    //收货地址列表
    public function address() {
        $MemberAddressObj = new \Phone\Model\MemberAddressModel();
        $result = $MemberAddressObj->search($this->member_id);
        $this->page = $result['page'];
        $this->list = $result['list'];
		 $ProvinceObj = new \Home\Model\AddressProvinceModel();
        $this->province = $ProvinceObj->getSel();
        $this->display();
    }
    
    //新增收货地址
    //$type = ajax时为订单确认过程时使用
    public function address_add($type = '') {
        //提交操作
        if(IS_POST) {
            $data['realname'] = trim(I('post.realname',''));
            $data['province'] = trim(I('post.province',''));
            $data['city'] = trim(I('post.city',''));
            $data['area'] = trim(I('post.area',''));
            $data['address'] = trim(I('post.address',''));
            $data['zip'] = trim(I('post.zip',''));
            $data['phone'] = trim(I('post.phone',''));
            $data['tel'] = trim(I('post.tel',''));
            $data['email'] = trim(I('post.email',''));
            $data['is_default'] = I('post.is_default',0);
            $data['member_id'] = $this->member_id;
            if($data['realname'] == '') {
                echo '请填写收件人';
                exit();
            }
            if($data['province'] == '') {
                echo '请选择省份';
                exit();
            }
            if($data['city'] == '') {
                echo '请选择城市';
                exit();
            }
            if($data['phone'] == '' && $data['tel'] == '') {
                echo '请填写手机号码';
                exit();
            }
            $MemberAddressObj = new \Phone\Model\MemberAddressModel();
            $id = $MemberAddressObj->data($data)->add();
            if($id > 0) {
                if($data['is_default'] == 1) {
                    $MemberAddressObj->set_default($this->member_id, $id);
                }
                echo 'success';
            } else {
                echo '新增收货地址出错！';
            }
            exit();
        }

        //调取省份
    
        
        if($type == 'ajax') { //订单确认过程修改地址
            $this->display('address_add_ajax'); 
        } else {
            $this->display(); 
        }
    }
    
    //修改收货地址
    //$type = ajax时为订单确认过程时使用
    public function address_edit($id,$type = '') {
        $MemberAddressObj = new \Phone\Model\MemberAddressModel();
        $model = $MemberAddressObj->get_address($this->member_id, $id);
        //提交操作
        if(IS_POST) {
            //防止修改其他人的收货地址
            if(empty($model)) {
                echo '非法操作！';
                exit();
            }
            $data['realname'] = trim(I('post.realname',''));
            $data['province'] = trim(I('post.province',''));
            $data['city'] = trim(I('post.city',''));
            $data['area'] = trim(I('post.area',''));
            $data['address'] = trim(I('post.address',''));
            $data['zip'] = trim(I('post.zip',''));
            $data['phone'] = trim(I('post.phone',''));
            $data['tel'] = trim(I('post.tel',''));
            $data['email'] = trim(I('post.email',''));
            $data['is_default'] = I('post.is_default',0);
            $data['member_id'] = $this->member_id;
            $data['id'] = $id;
            if($data['realname'] == '') {
                echo '请填写收件人';
                exit();
            }
            if($data['province'] == '') {
                echo '请选择省份';
                exit();
            }
            if($data['city'] == '') {
                echo '请选择城市';
                exit();
            }
            if($data['phone'] == '' && $data['tel'] == '') {
                echo '请填写手机号码';
                exit();
            }
           
            $flag = $MemberAddressObj->save($data);
            if($flag !== false) {
                if($data['is_default'] == 1) {
                    $MemberAddressObj->set_default($this->member_id, $id);
                }
                echo 'success';
            } else {
                echo '修改收货地址出错！';
            }
            exit();
        }

        //调取省份
        $ProvinceObj = new \Phone\Model\AddressProvinceModel();
        $this->province = $ProvinceObj->getSel();
        //防止查看其他人的收货地址
        if(empty($model)) {
            $this->error('非法操作！');
        }
        $this->model = $model;
        
        if($type == 'ajax') {
		
            $this->display('address_edit_ajax');
        } else {
            $this->display(); 
        }
    }
    
    //删除收货地址AJAX操作
    public function address_del($id) {
        $MemberAddressObj = new \Phone\Model\MemberAddressModel();
        $flag = $MemberAddressObj->del_address($this->member_id, $id);
        if($flag > 0) {
            echo 'success';
        } else {
            echo '删除出错';
        }
    }
    
    //我的订单列表
    public function myorder() {     
        $ModelObj = new \Fwadmin\Model\OrderModel();
        $conditions = array();//搜索条件
        $status_mark = I('get.status_mark',0);

        if(!empty($status_mark)) {
            $conditions['status_mark'] = $status_mark;
            $this->status_mark = $status_mark;
        }
	    $this->pay = I('get.pay',0);
        $k = I('get.k','');
        if(!empty($k)) {
            $conditions['k'] = $k;
            $this->k = $k;
        }
        $results = $ModelObj->search_member($this->member_id,$conditions, 'order_id desc',5);
        $lists = $results['list'];
        foreach($lists as $k => $v) {
			
            $lists[$k]['status_str'] = $ModelObj->show_status_member($v['status'], $v['status_pay'], $v['status_delivery'],$v['pay_type']);//订单状态
            $lists[$k]['total_show'] = $v['total'] - $v['total_discount'];//显示价格
            $lists[$k]['pay_type_str'] = $ModelObj->get_pay_type($v['pay_type']);//支付方式
            $goods_array = $ModelObj->show_goods_pic($v['order_id']);//订单图片数组
			$lists[$k]['cart_num'] = count($goods_array);  
             $lists[$k]['goods'] =  $goods_array ;
		}
        
        $this->assign('list', $lists); // 赋值数据集
		$this->assign('empty','<div style="text-align:center; margin-top:50px;">暂无订单</div>'); 
        $this->assign('page', $results['page']); // 赋值分页输出
		
        $this->display();
    }
    
    //订单详细页面
    public function orderinfo($id) {
        $OrderObj = new \Fwadmin\Model\OrderModel();
        $order = $OrderObj->find($id);
        
        //防止查看别人的订单
        if($order['member_id'] !== $this->member_id) {
            echo '非法操作';
            exit();
        }
        $order['pay_type_str'] = $OrderObj->get_pay_type($order['pay_type']);//支付方式
        $order['status_str'] = $OrderObj->show_status_member($order['status'], $order['status_pay'], $order['status_delivery'],$order['pay_type']);//订单状态
        $order['total_show'] = $order['total'] - $order['total_discount'];//显示价格
        $order['total_need_pay'] = $OrderObj->get_total_need_pay($order);//待支付金额
        $order['total_need_pay'] = priceTofixed(round($order['total_need_pay'],2));
      
        $this->order = $order;
		  if($order['delivery_code']!=''&&$order['delivery_no']!=''){
		 $data = D('Kuaidi', 'Service')->getOrderTracesByJson($order['order_no'],$order['delivery_code'],$order['delivery_no']);
		 //$this->kuaiid_list = json_decode($data,true); 
		  $kuaidi_list = json_decode($data,true); 
		  if(!empty($kuaidi_list['Traces'])){
		   $kuaidi_list['Traces'] = array_reverse($kuaidi_list['Traces']);
		  }
		  
		 $this->kuaidi_list = $kuaidi_list;
		
		 }
        
        //订单跟踪信息
        $this->record = $OrderObj->get_record($id);
        //订单购物车信息
        $this->goods = $OrderObj->get_goods($id);
        
        $this->display();
    }
    
    //订单确认收货AJAX操作
    public function order_delivery_4($order_id) {
        $OrderObj = new \Fwadmin\Model\OrderModel();
        $flag = $OrderObj->delivery_4($order_id,$this->member_id);
        if($flag > 0) {
            $data['status'] = 'success';
            $data['msg'] = '';

            // 发放代理商品费用
            $this->pushMemberMoney($order_id);

            $this->ajaxReturn($data);
        } elseif ($flag == 0) {
            $data['status'] = 'error';
            $data['msg'] = '操作失败';
            $this->ajaxReturn($data);
        } else {
            $data['status'] = 'error';
            $data['msg'] = '非法操作';
            $this->ajaxReturn($data);
        }
    }

    // 发放代理商品费用
    public function pushMemberMoney($order_id)
    {
        $og = D('OrderGoods');
        $me = D('Member');
        $g  = D('Goods');
        $eg = D('ExtensionGood');
        $ex = new ExtensionModel();
        $mm = new MemberModel();
        $eom = new ExtensionOpenModel();
        $egm = new ExtensionGoodModel();


        $time = Date('Y-m-d H:i:s', time());

        $share  = array();
        $ogData = $og->where(array('order_id' => $order_id))->select();

        foreach ($ogData as $v) {
           $share['good'][]     = $g->where(array('goods_id' => $v['goods_id']))->getField('ex_share');
           $share['id'][]       = $v['goods_id'];
           $share['num'][]      = $v['goods_num'];
           $share['member'][]   = $v['member_id'];

            // 分享收益
            if ($this->member_arr['ex_level'] != 0) {
                if ($v['extension_share_id']) {
                    $mm->where(array('member_id' => $v['extension_share_id']))->setInc('ex_money', $v['extension_share_money']);
                    $eom->data(array(
                        'ex_u_id'       =>  $v['member_id'],
                        'ex_p_id'       =>  $v['extension_share_id'],
                        'ex_p_money'    =>  $v['extension_share_money'],
                        'ex_u_money'    =>  $v['goods_price'],
                        'ex_create_time'=>  date('Y-m-d H:i:s', time())
                    ))->add();
                } else {
                    $mm->where(array('member_id' => $v['member_id']))->setInc('ex_money', $v['extension_share_money']);
                    $egm->data(array(
                        'ex_s_id'       =>  $v['member_id'],
                        'ex_u_id'       =>  $v['member_id'],
                        'ex_p_id'       =>  $v['member_id'],
                        'ex_good_id'    =>  $v['goods_id'],
                        'ex_money'      =>  $v['extension_share_money'],
                        'ex_create_time'=>  date('Y-m-d H:i:s', time()),
                        'ex_level'      =>  1,
                        'ex_good_num'   =>  $v['goods_num']
                    ))->add();
                }
            }
        }

        $level[1] = $ex->get('ex_profit_1');
        $level[2] = $ex->get('ex_profit_2');
        $level[3] = $ex->get('ex_profit_3');
        $sid  = $share['member'][0];

        $this->recursion($sid, $level, $i = 1, $uid = null, $share, $time, $me, $eg);
    }

    // 递归代理
    public function recursion($sid, $level, $i, $uid, $share, $time, $me, $eg)
    {
        $uid = is_null($uid) ? $sid : $uid;

        $pid = $me->where(array('member_id' => $uid))->find()['ex_uid'];
        if ($pid  and $i < 4) {
            foreach ($share['good'] as $k => $v) {
                $money = ($v * $share['num'][$k] * $level[$i]) / 100;
                $me->where(array('member_id' => $pid))->setInc('ex_money', $money);
                $eg->data(
                    array(
                        'ex_s_id'   =>  $sid,
                        'ex_u_id'   =>  $uid,
                        'ex_p_id'   =>  $pid,
                        'ex_good_id'=>  $share['id'][$k],
                        'ex_money'  =>  $money,
                        'ex_create_time'=> $time,
                        'ex_level'      => $i,
                        'ex_good_num'   => $share['num'][$k]
                    )
                )->add();
            }
            ++$i;
            $this->recursion($sid, $level, $i, $pid ,$share, $time, $me, $eg);
        }
        return false;
    }

    //订单取消AJAX操作
    public function order_confirm_4($order_id) {
        $OrderObj = new \Fwadmin\Model\OrderModel();
        $flag = $OrderObj->confirm_4($order_id,$this->member_id);
        if($flag > 0) {
            $data['status'] = 'success';
            $data['msg'] = '';
            $this->ajaxReturn($data);
        } elseif ($flag == 0) {
            $data['status'] = 'error';
            $data['msg'] = '操作失败';
            $this->ajaxReturn($data);
        } else {
            $data['status'] = 'error';
            $data['msg'] = '非法操作';
            $this->ajaxReturn($data);
        }   
    }
    
    //我的收藏
    public function myfav() {
        $FavObj = new \Phone\Model\MemberFavModel();
        $GoodsObj = new \Phone\Model\GoodsModel();
        $GoodsItemObj = M('goods_item');
        $result = $FavObj->search_member($this->member_id);
        $list = $result['list'];
        foreach($list as $k => $v) {
			//echo $v['price_before'];
            $item = $GoodsItemObj->field('goods_sn,price,price_market,spec_array')->where(array('item_id'=>$v['item_id']))->find();
            if(!empty($item)) {
                $list[$k]['price_duibi'] = $v['price_before'] - $item['price'];
                $list[$k]['price'] = $item['price'];
				$list[$k]['price_market'] = $item['price_market'];
                $list[$k]['goods_sn'] = $item['goods_sn'];
                $list[$k]['spec_array'] = $item['spec_array'];
            } else {
                unset($list[$k]);
            }
        }
        $this->list = $list;
        $this->page = $result['page'];
		$this->left_menu = 2;
        $this->display();
    }
    
    //删除收藏
    public function myfav_del($id) {
        $data['status'] = 'error';
        $FavObj = new \Phone\Model\MemberFavModel();
        $flag = $FavObj->del_fav($id,$this->member_id);
        if($flag > 0) {
            $data['status'] = 'success';
            $data['msg'] = '删除成功！';
        } else {
            $data['msg'] = '删除失败！';
        }
        return $this->ajaxReturn($data);
    }
    
    //会员积分记录
    public function point() {
        $ModelObj = new \Fwadmin\Model\MemberPointModel();
        $MemberObj = new \Phone\Model\MemberModel();
        $point = $MemberObj->get_point($this->member_id);
        $this->point = $point;//会员积分
        
        $conditions = array();
        $results = $ModelObj->search_member($this->member_id,$conditions);
        $lists = $results['list'];
        $this->list = $lists; // 赋值数据集
        $this->page = $results['page']; // 赋值分页输出
        $this->left_menu =4; 
        $this->display();
    }
    

    
    //商品评价
    public function comment() {

        $GoodsCommentObj = new \Phone\Model\GoodsCommentModel();
        $result = $GoodsCommentObj->get_need_comment_list($this->member_id);
        $this->list = $result['list'];
        $this->page = $result['page'];
        $this->display();
    }
    
    //商品评价详情
    public function comment_add($id) {
        if( IS_POST ) {
            $star = 5;
            $detail = I('post.detail','');
            if ($detail == '') {
                echo '请填写评价内容';
            } else {
                $GoodsCommentObj = new \Phone\Model\GoodsCommentModel();
                $flag = $GoodsCommentObj->add_comment($this->member_id, $id, $star, $detail);
                if($flag > 0) {
                    echo 'success';
                } elseif($flag == -3) {
                    echo '你已评价过此商品';
                } else {
                    echo '评价失败';
                }
            }
        } else {
            $OrderGoods = M('order_goods');
            $model = $OrderGoods->where(array('member_id'=>$this->member_id,'id'=>$id))->find();
			$status_pay = M('order')->getFieldByOrderId($model['order_id'],'status_pay'); //判断该商品是否已经支付
            
			if($model['is_comment']>0){//已评价的就不要再评价了
			   $this->redirect('Member/comment_record'); 
			 }elseif($status_pay!=3){
			   $this->redirect('Member/comment');
			}
			
			$comment_list = M('goods_comment')->where(Array('goods_id'=>$model['goods_id'],'status'=>2))->order('id desc')->limit(10)->select();
			$this->comment_list = $comment_list;
            $this->model = $model;
            $this->display();
        }
    }
    
    //我的商品评价记录
    public function comment_record() {
        $member_id = $this->member_id;
        $GoodsCommentObj = new \Phone\Model\GoodsCommentModel();
        $result = $GoodsCommentObj->search_member($member_id);
        $this->list = $result['list'];
        $this->page = $result['page'];
        $this->display(); 
    }
    
	/*
    //商品退换货
    public function back() {
        $k = I('get.k','');
        if(!empty($k)) {
            $conditions['k'] = $k;
            $this->k = $k;
        }
        $OrderObj = new \Fwadmin\Model\OrderModel();
        $OrderBackObj = new \Fwadmin\Model\OrderBackModel();
        $result = $OrderBackObj->get_can_back_list($this->member_id,$conditions);
        $lists = $result['list'];
        foreach($lists as $k => $v) {
            $lists[$k]['goods'] = $OrderObj->show_goods_pic($v['order_id']);//订单图片数组
        }
        $this->list = $lists; // 赋值数据集
        $this->page = $result['page'];
        $this->display();
    }
    
    //申请退换货
    public function back_add($id) {
        $OrderGoods = M('order_goods');
        $model = $OrderGoods->where(array('member_id'=>$this->member_id))->find($id);
        if(empty($model)) {
            echo '非法操作';
            exit();
        }
        //退换货限制时间
        $ConfigObj = new \Fwadmin\Model\ConfigModel();
        $day_tuihuo = $ConfigObj->get('orderback_day_tuihuo');
        $day_huanhuo = $ConfigObj->get('orderback_day_huanhuo');
        $day_weixiu = $ConfigObj->get('orderback_day_weixiu');
        if( IS_POST ) {
            $type = I('post.type','');
            $goods_num = I('post.goods_num',0);
            $waiguan = I('post.waiguan','');
            $baozhuang = I('post.waiguan','');
            $pinzheng = I('post.pinzheng','');
            $detail = I('post.detail','');
            $addr_realname = I('post.addr_realname','');
            $addr_tel = I('post.addr_tel','');
            $addr_address = I('post.addr_address','');
            if($type == '') {
                echo '请选择类型';
            } elseif ($detail == '') {
                echo '请填写申请原因';
            } elseif ($goods_num <= 0 || $goods_num > $model['goods_num']) {
                echo '申请数量必须大约1并且小于购买数量';
            } else {
                $OrderBackObj = new \Fwadmin\Model\OrderBackModel();
                $data['type'] = $type;
                $data['waiguan'] = $waiguan;
                $data['baozhuang'] = $baozhuang;
                $data['pinzheng'] = $pinzheng;
                $data['detail'] = $detail;
                $data['goods_num'] = $goods_num;
                $data['addr_realname'] = $addr_realname;
                $data['addr_tel'] = $addr_tel;
                $data['addr_address'] = $addr_address;
                $data['member_id'] = $this->member_id;
                $flag = $OrderBackObj->add_record($id,$data);
                if($flag > 0) {
                    echo 'success';
                } else {
                    echo '申请失败';
                }
            }
        } else {
            $order_id = $model['order_id'];
            $OrderObj = new \Fwadmin\Model\OrderModel();
            $order = $OrderObj->field('addr_realname,addr_tel,addr_phone,addr_province,addr_city,addr_area,addr_address,time_complete')->find($order_id);
            if($order['addr_phone'] == '') $order['addr_phone'] = $order['addr_tel'];
            $this->order = $order;
            $this->model = $model;
            
            //计算订单完成日期和当前日期天数差
            $time_now = time();
            $time_complete = $order['time_complete'];
            $between_days = ($time_now - $time_complete)/86400 ;
            if($between_days < $day_tuihuo) {
                $this->is_tuihuo = 1;
            }
            if($between_days < $day_huanhuo) {
                $this->is_huanhuo = 1;
            }
            if($between_days < $day_weixiu) {
                $this->is_weixiu = 1;
            }
           
            $this->display();
        }
    }
    
    //我的商品退换货记录
    public function back_record() {
        $member_id = $this->member_id;
        $OrderBackObj = new \Fwadmin\Model\OrderBackModel();
        $result = $OrderBackObj->search_member($member_id);
        $list = $result['list'];
        foreach($list as $k => $v) {
            $list[$k]['str_status'] = $OrderBackObj->get_status_user($v['status']);
        }
        $this->list = $list;
        $this->page = $result['page'];
        $this->display(); 
    }
    
    //商品退换货记录详细内容
    public function back_info($id) {
        $OrderBackObj = new \Fwadmin\Model\OrderBackModel();
        $model = $OrderBackObj->where(array('member_id'=>$this->member_id))->find($id);
        
        $OrderGoods = M('order_goods');
        $order_goods = $OrderGoods->where(array('member_id'=>$this->member_id))->find($model['cart_id']);
        
        $model['str_status'] = $OrderBackObj->get_status_user($model['status']);
        
        $this->model = $model;
        $this->order_goods = $order_goods;
        $this->display();
    }
    
    public function back_goods() {
        $OrderBackObj = new \Fwadmin\Model\OrderBackModel();
        $id = I('post.id',0);
        if($id > 0) {
            $flag = $OrderBackObj->confirm_3($id,$this->member_id);
            if($flag > 0) {
                echo 'success';
            } else {
                echo '提交出错，请联系客服！';
            }
        }
    }
	*/
    
	/**优惠劵列表**/
    public function youhui() {
        $MemberYhqObj = new \Phone\Model\MemberYhqModel();
        $conditions = array();
        $results = $MemberYhqObj->search($this->member_id,$conditions);
       
        $lists = $results['list'];
		foreach($lists as $k =>$v){
		 $lists[$k]['nowtime'] = date('Y-m-d 00:00:00');
		}

        $this->assign('list', $lists); // 赋值数据集
        $this->assign('page', $results['page']); // 赋值分页输出
        $this->left_menu = 5;
        $this->display();
    }
	
	/**消息列表**/
	 public function message() {
        $MemberMessageObj = new \Phone\Model\MemberMessageModel();
        
        $conditions = array();
        $results = $MemberMessageObj->search($this->member_id,$conditions);
       
        $lists = $results['list'];
        $this->assign('list', $lists); // 赋值数据集
        $this->assign('page', $results['page']); // 赋值分页输出
        $this->left_menu = 5;
		$MemberMessageObj->count_have_look($this->member_id);
        $this->display();
    }
	
	
	  //删除消息
    public function message_del($id) {
		if(IS_AJAX){
        $data['status'] = 'error';
        $MemberMessageObj = new \Phone\Model\MemberMessageModel();
        $flag = $MemberMessageObj->del_message($id,$this->member_id);
        if($flag > 0) {
            $data['status'] = 'success';
            $data['msg'] = '删除成功！';
        } else {
            $data['msg'] = '删除失败！';
        }
        return $this->ajaxReturn($data);
		}
    }
	
	
	public function info_edit(){
		$MemberObj = new MemberModel();
        $MemberInfoObj = new MemberInfoModel();
		$member_id = $this->member_id;
	 if(IS_AJAX){ 
	   $tname = I('tname');
	   $beizhu = I('beizhu');
	   if($tname=='nickname'&&!empty($beizhu) && $MemberObj->exist_nickname($nickname, $member_id) > 0){
	    $result['title'] ='error';
	    $result['msg'] = '昵称已被使用';
		 $this->ajaxReturn($result);
		 exit();
	    }elseif($tname=='nickname'){
		$flag = $MemberObj->where(Array('member_id'=>$member_id))->setField($tname,$beizhu);
		}else{
	     $flag = $MemberInfoObj->where(Array('member_id'=>$member_id))->setField($tname,$beizhu);
		}
	   $result['title'] ='success';
	   if($flag>-1){
	   $result['msg'] = '修改成功';
	   }else{
	   $result['title'] ='error';
	  $result['msg'] = '修改失败';
	   }
	    $this->ajaxReturn($result);
	 }
	 
	}
	
	
	
	    public function edit_other() {
        $MemberObj = new MemberModel();
        $MemberInfoObj = new MemberInfoModel();
        if (IS_POST) {
            $nickname = I('post.nickname','');
            if (!empty($nickname) && $MemberObj->exist_nickname($nickname, $this->member_id) > 0) {
                echo '昵称已被使用！';
                exit();
            } else {
                $MemberObj->where('member_id=' . $this->member_id)->setField('nickname', $nickname);
            }
            $data['sex'] = I('post.sex');
            $year = I('post.year');
            $month = I('post.month');
            $day = I('post.day');
            if ($year != '' && $month != '' && $day != '') {
                $data['birthday'] = date('Y-m-d', strtotime($year . '-' . $month . '-' . $day));
            }
            $data['realname'] = trim(I('post.realname'));
			$data['idno'] = trim(I('post.idno'));
            $data['province'] = I('post.province');
            $data['city'] = I('post.city');
            $data['area'] = I('post.area');
            $data['address'] = trim(I('post.address'));
            $data['member_id'] = $this->member_id;
            $flag = $MemberInfoObj->save($data);
		
            if ($flag !== false) {
                echo 'success';
            } else {
                echo '保存失败';
            }
            exit();
        }
        $member = $MemberObj->find($this->member_id);
        $memberinfo = $MemberInfoObj->find($this->member_id);
        $this->member = $member;
        $memberinfo['year'] = date('Y', strtotime($memberinfo['birthday']));
        $memberinfo['month'] = date('m', strtotime($memberinfo['birthday']));
        $memberinfo['day'] = date('d', strtotime($memberinfo['birthday']));
		$this->typeid = I('typeid');
        $this->memberinfo = $memberinfo;
        //调取省份
        $ProvinceObj = new \Home\Model\AddressProvinceModel();
        $this->province = $ProvinceObj->getSel();
		
        $this->display();
    }
    
    
}