<?php

namespace Phone\Controller;

use Think\Controller;
use Common\Controller\PhoneController;

class GListController extends PhoneController {
    
	
	public function category(){
	  $ModelGoodsCat =  new \Phone\Model\GoodsCatModel();
	  $all_cat = $this->all_cat;
	  $cid = I('cid',$all_cat[0]['cat_id']);
	  $model = $ModelGoodsCat ->where(Array('cat_id'=>$cid))->find();
	  $this->seo=array('seo_title'=> $model['cat_name']);
	  $this->all_cat = $all_cat;
	  $this->cid = $cid;
	  $this->picture_list = M('sort_banner')->where(Array('typeid'=>$cid,'is_show'))->order('order_id asc,id asc')->select();//大类轮播图
	  $sub_list = $ModelGoodsCat->get_sub($cid);
	  //echo $ModelGoodsCat->_sql();
	  foreach($sub_list as $k =>$v){
	  $sub_list[$k]['sub'] = $ModelGoodsCat->get_sub($v['cat_id']);
	  }
      $this->sub_list = $sub_list;
	  $this->display();
	
	}
	
    //首页
    public function index() {
        //初始化模型
        $ModelGoodsCat =  new \Phone\Model\GoodsCatModel();
        $ModelGoods = new \Phone\Model\GoodsModel();
        $ModelGoodsBrand = new \Phone\Model\GoodsBrandModel();
        $M_attr = D('goods_mod_attr');
        $M_mod_sel = D('goods_mod_sel');
        
        //获取参数
        $cid = I('get.cid',0); //已选类别
        $bid = I('get.bid',0); //已选品牌
        $price = I('get.price',''); //价格
        $this->price = $price;
        $o = I('get.o','');
        $keywords = I('get.keywords','');
        
        //搜索参数URL路径
        $url_arr = array(); //URL路径数组
        $url_arr['cid'] = $cid;
        //获取自定义参数
        foreach($_GET as $k => $v) {
            if(strpos($k,'attr_') === 0) {
                $url_arr[$k] = $v;
            }
        }
        $url_arr['price'] = $price;
        $url_arr['bid'] = $bid;
        $url_arr['o'] = $o;
        $url_arr['keywords'] = $keywords;
        
        //排序
        $this->curr_order = $o;
        $this->order_list = $ModelGoods->get_search_order($url_arr);


        //顶级类别
        $this->top_cat_id = $ModelGoodsCat->get_top_parent_id($cid);
            
        //筛选类别
        $cat_list = $ModelGoodsCat->get_search_sub($cid,$keywords,$bid);//类别筛选
		
        //赋值类别URL
        foreach($cat_list as $k => $v) {
            $cat_list[$k]['url'] = U('GList/index',array('cid'=>$v['cat_id'],'keywords'=>$keywords,'bid'=>$bid));
        }
		
        $this->cat_list = $cat_list;
		
        $this->cid = $cid;
        
        $search_select = '';//已选搜索条件
        $seo_title = '商品搜索';
        //如果已选择关键词
        if(!empty($keywords)) {
            $search_select .= '<a href="'.$ModelGoods->search_url($url_arr, 'keywords', '').'" class="name1">关键词：<em>'.$keywords.'</em><i></i></a><br>';
            $seo_title .= '_'.$keywords;
			$ModelGoods->search_history_add($keywords);
        }
		
        //如果已选择类别
        if(!empty($cid)) {
		
            $cat_name = $ModelGoodsCat->get_cat_name($cid);
			
			$parent_id = $ModelGoodsCat->getFieldByCatId($cid,'parent_id');
			 $sort_name = '类别：';
			if($parent_id>0){
			 $parent_id2 = $ModelGoodsCat->getFieldByCatId($parent_id,'parent_id');
			 if($parent_id2>0){
			 $search_select .= '<span class="txt">'.$ModelGoodsCat->get_cat_name($parent_id2).'</span> > ';
			 }
				
			 $search_select .= '<span class="txt">'.$ModelGoodsCat->get_cat_name($parent_id).'</span> > ';
			
			 $sort_name='';
			}
			
            $search_select .= '<a href="'.$ModelGoods->search_url($url_arr, 'cid', $parent_id).'" class="name1"><b>'.$sort_name.'</b>'.$cat_name.'<i></i></a>';
			
            $seo_title .= '_'.$cat_name;
			
        } 
		
		$this->cat_name = $cat_name?$cat_name:'所有类别';
		
        //如果已选择品牌
        if($bid > 0) {
            $bid_name = $ModelGoodsBrand->get_name($bid);
            $search_select .= '&nbsp; <a href="'.$ModelGoods->search_url($url_arr, 'bid', '').'" class="name1">品牌：'.$bid_name.'<i></i></a>';
            $seo_title .= '_'.$bid_name;
        } 
        //如果已选择价格
        if($price !== '') {
            $search_select .= '&nbsp; <a href="'.$ModelGoods->search_url($url_arr, 'price', '').'"  class="name1">价格：'.$price.'<i></i></a>';
		  $this->price_s = explode('-',$price);
		  
		  
        }
        //如果已选择自定义属性
        foreach($url_arr as $k => $v) {
            if (strpos($k,'attr_') === 0) {
                $attr_id = str_replace("attr_","",$k);
                $attr_name = $M_attr->getFieldByAttrId($attr_id,'attr_name');
                $attr_value = $M_mod_sel->getFieldByAttrSelId($v,'attr_sel_name');
                $search_select .= '<a href="'.$ModelGoods->search_url($url_arr, $k, '').'"><b>'.$attr_name.'：</b>'.$attr_value.'<i></i></a>';
                $seo_title .= '_'.$attr_value;
            }
        }
        $this->search_select = $search_select;
        $this->seo_title = $seo_title;
        $this->seo_keywords = str_replace('_', ',', $seo_title);
        $this->o = $ModelGoods->search_url($url_arr,'o',$o);
        
        //筛选自定义属性
        $attr_list = $ModelGoods->get_search_arr($url_arr);
        $this->attr_list = $attr_list;
        //自定义参数选中JS
        $attr_js = '';
        foreach($attr_list as $v) {
            $attr_v = I('get.attr_'.$v['attr_id'],0);
            $attr_js .= '$("#attr_'.$v['attr_id'].'_'.$attr_v.'").addClass("sel");';
            $this->assign('attr_js', $attr_js);
        }
        
        //如果未选择品牌
        if($bid <= 0) {
             //筛选品牌
            $brand_list = $ModelGoods->get_search_brand($url_arr);
            //赋值品牌URL
            foreach($brand_list as $k => $v) {
                $brand_list[$k]['url'] = $ModelGoods->search_url($url_arr,'bid', $v['brand_id']);
            }
            $this->bid = $bid;
            $this->brand_list = $brand_list;
        }
        
        //如果未选择价格
        if($price == '') {
            $price_list = $ModelGoods->get_search_price($url_arr,6);
			
            $this->price_list= $price_list;
        }
        
        //商品
        $pagesize = 30; //分页数
        $result = $ModelGoods->search($url_arr,$pagesize);//获得查询结果
		$map = $ModelGoods->search_condition($url_arr);
		$this->total_count = $ModelGoods->where($map)->count();
        $glist = $result['list'];//当前页数据
		foreach($glist as $k =>$v){
		$glist[$k]['count_comment'] = M('goods_comment')->where(Array('goods_id'=>$v['goods_id']))->count();
		}
		$this->glist = $glist;
		//print_r($this->glist);
        $this->page = $result['page'];//分页
        $this->page_count = $result['page_count'];//总页数
        $this->count = $result['count'];//总记录数
        $this->p = I('get.p',1);//当前第几页
        $this->pagesize = $pagesize;//分页数
        if($this->p - 1 > 0)$this->page_prev_url = $ModelGoods->search_url($url_arr, 'p', $this->p-1);
        if($this->p + 1 <= $this->page_count)$this->page_next_url = $ModelGoods->search_url($url_arr, 'p', $this->p+1);
        
		$url_arr['price'] ='';
		$this->current_url =  $ModelGoods->search_url($url_arr,'','');//当前url
		$this->seo=array('seo_title'=> $seo_title);
        $this->display();
    }
	
	
	/****获取搜索记录*****/
	public function search_history_content(){
	
	if(IS_AJAX){
	   $ModelGoods = new \Phone\Model\GoodsModel();
	   $search_history_list = $ModelGoods->search_history_show();
	   
	  
	  }
	
	}
	

    
    

}
