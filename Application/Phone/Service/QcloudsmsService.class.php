<?php
namespace Phone\Service;

class QcloudsmsService {
    function __construct() {
        $config = array(
            'appid' => '1400179566',//控制台查看
            'appkey' => '9a3dc5bfeb82c9faf6d468d02edb3365',//控制台查看
            'templId' => '268717',
            //'nationCode' => '852', //国家或地区区号,香港852，大陆86,台湾886
        );
        $this->config = $config;
    }

    /**
     * 发送验证码
     * @param $phone
     * @param $code 验证码
     * @return mixed
     */
    public function sendMsg($phone, $code) {
        vendor('Qcloudsms.SmsSender');
		/*
		$length = strlen($phone);
		if($length==9||$length==10){
		 $nationCode='886';
		}elseif($length==8){
		$nationCode='852';
		}else{
		$nationCode='86';
		}
		*/
		$nationCode='86';
        $config = $this->config;
        $singleSender = new \SmsSingleSender($config['appid'], $config['appkey']);
        // 普通单发
        $result = $singleSender->send(0, $nationCode, $phone, "您的注册验证码是".$code."，请不要把验证码泄漏给其他人，如非本人请勿操作。", "", "");
        //返回的成功示例：{"result":0,"errmsg":"OK","ext":"","sid":"2:670479-0268698729-028972-001510040916","fee":1}
        //result为0表示发送成功
        $rsp = json_decode($result, true);
        return $rsp;
    }

}