<?php

function yc_phone($str){
    $str=$str;
    $resstr=substr_replace($str,'****',3,4);
    return $resstr;
}

    function member_name($member_id) {
        $row = M('member')->field('username,phone,nickname')->where(array('member_id'=>$member_id))->find();
        if(!empty($row)) {
            if (!empty($row['nickname'])) { 
                return $row['nickname'];
            }elseif (!empty($row['phone'])) {
                return substr_replace($row['phone'],'****',3,4);
            } else {
                return $member_id;
            }
        } else {
            return '无此用户';
        }
    }

    function member_headpic($member_id,$n=1) {

		 $headpic =  M('member')->getFieldByMemberId($member_id,'headpic');
        if(empty($headpic)) {
          $headpic = '/Public/system/images/headpic.jpg';
        }
        return $headpic;
    }



 function get_pro_color_pic($goods_id,$color){
	 return M('goods_color')->where(Array('goods_id'=>$goods_id,'title'=>$color))->getField('picture_list');
 }

//获得多选的URL
 function getParam($arr_url,$id_name,$cat_id,$url=''){
 $exist = false;
//print_r($arr_url);
 if($cat_id==''){ //如果cat_id为空，则清除该条件
 unset($arr_url[$id_name]);
 return U($url,$arr_url);
 }
foreach($arr_url as $k =>$v){
	//先判断该参数是否已经选中了
  if($k == $id_name) { //循环参数名，相等时再进行操作
   if(!empty($arr_url[$k])){
     $array = explode(',',$arr_url[$k]);//将字符串改成数组
	 $exist2 = false; //如果选中的类别，再次点击该类别后，将取消该类别的选中，如果再点击其他类别，则出现多个类别的选中
		foreach($array as $y=>$z){
			 // echo $array[$y].$cat_id;
		 if($array[$y]==$cat_id){ //包含已有类别
			  unset($array[$y]);
			  $exist2 = true;
		  }
		}
		if($exist2==true){
		$arr_url[$k] = implode(',',$array);//再转化
		}else{
		 $arr_url[$k] = urldecode($arr_url[$k].','.$cat_id);//把类别的id连接上
		}	
   }
	$exist = true; 
    }
}
     if($exist == false){
		 $arr_url[$id_name] = $cat_id;
	  }
     return U($url,$arr_url);
 }
 
 //判断元素是否在数组中
 function item_exist_aray($item,$string){
	 if($string==''){
		 return '';
	 }else{
	  $array = explode(',',$string);

	  if(in_array($item,$array)){
	   return 'class="on"';
	  }else{
	    return '';
	   }
	 }
 }
 
 
 /**
小数转为整数
*/

function floattoint($price){

return sprintf("%.0f", $price);
}
 
 
/**
折扣计算
*/

function zhekou($price,$market_price){
$num = $price*10/$market_price;
return sprintf("%.1f", $num);
}

/**
  +----------------------------------------------------------
 * 浏览记录按照时间排序
  +----------------------------------------------------------
 */
 function my_sort($a, $b){
    $a = substr($a,1);
    $b = substr($b,1);
    if ($a == $b) return 0;
    return ($a > $b) ? -1 : 1;
  }
  
  /**
 * 获取上一条下一条数据数组，注意变量$Mobdel表模型,category_id有类别区别
 * @param $id 当前id值
 * @param $order 排列顺序
 * @param $Mobdel 当前模型
 * @param int $map 自带查找条件
 * @param bool $isCategory 是否带类别
 * @return mixed
 */
  function get_prev_next($id, $order, $Mobdel, $map = 1, $isCategory = true) {
    if ($isCategory) {
        $thiscontent = $Mobdel->find($id);
        $map['category_id'] = $thiscontent['category_id'];
    }
    $contentlist = $Mobdel->where($map)->order($order)->select();
    for ($i = 0; $i < count($contentlist); $i++) {
        if ($contentlist[$i]['id'] == $id) {
            $mid = $i;
            break;
        }
    }
    $data['prev'] = ($mid - 1) > -1 ? $contentlist[$mid - 1] : "";
    $data['next'] = ($mid + 1) < count($contentlist) ? $contentlist[$mid + 1] : "";
    return $data;
}
  
   /**
  +----------------------------------------------------------
 * 网页浏览记录生成
  +----------------------------------------------------------
 */
 function cookie_history($id,$title,$price,$img){
    $dealinfo['goods_id'] = $id;
    $dealinfo['title'] = $title;
    $dealinfo['price'] = $price;
    $dealinfo['img'] = $img;
    $time = 't'.NOW_TIME;
    $cookie_history = array($time => json_encode($dealinfo));  //设置cookie
    if (!cookie('history')){//cookie空，初始一个
        cookie('history',$cookie_history);
        }else{
        $new_history = array_merge(cookie('history'),$cookie_history);//添加新浏览数据
        uksort($new_history, "my_sort");//按照浏览时间排序
        $history = array_unique($new_history);
        if (count($history) > 4){
            $history = array_slice($history,0,4);
        }
        cookie('history',$history);
    }
 }
 
  /**
  +----------------------------------------------------------
 * 网页浏览记录读取
  +----------------------------------------------------------
 */
 function cookie_history_read(){
        $arr = cookie('history');
        foreach ((array)$arr as $k => $v){
            $list[$k] = json_decode($v,true);
        }
        return $list;
 }
 
 /**
 * 路由生成url链接,目前只支持正则路由
 * @param $url
 * @param $vars
 * @param bool $suffix
 * @return string
 */
function urlrotue($url, $vars, $suffix = true) {
    if (!C('URL_ROUTER_ON')) {
        return U($url, $vars, $suffix);
        exit();
    }
    $rotueRules = C('URL_ROUTE_RULES');
    $rulesArr = array_values($rotueRules); //路由规则键值数组
    $varsArr = array_values($vars); //参数键值数组
    $patternArr = array(); //字符串子匹配数组
    $params = '';
    //生成需要查找的解析字符串
    $i = 1;
    foreach ($vars as $k => $v) {
        $params .= $k . '=:' . $i . '&';
        $i++;
        $patternArr[] = '/\(.*?\)/';
    }
    $params = $params ? substr($params, 0, -1) : ''; //解析参数字符串

    $parater = ''; //匹配到的原路由规则键值
    foreach ($rulesArr as $k => $v) {
        if ($url . ($params ? '?' : '') . $params == $v) {
            $parater = $v;
            break;
        }
    }
    if ($parater) {
        $key = array_search($parater, $rotueRules); //获取匹配的路由规则键名
        $str = substr('/' . substr($key, 2), 0, -2); //把路由规则键名的正则规则变成纯字符串
        $result = preg_replace($patternArr, $varsArr, $str, 1);
        $suffix = $suffix ? '.' . C('URL_HTML_SUFFIX') : '';
        $result = stripslashes($result);
        return __ROOT__ . $result . (substr($result, strlen($result) - 1, 1) == '/' ? '' : $suffix);
    } else {
        //如果没有写相应的路由规则则使用原方式
        return U($url, $vars, $suffix);
    }
}